<?php

$_OBSOLETE_path = __DIR__ . '/../old_view-DISABLED';
$view_base_path = __DIR__ . '/../view';

$notImplementedRoute = [
    'type'    => 'Segment',
    'metadata' => [
        'id' => '-1',
        'description' => "Link Placeholder",
    ],
    'options' => [
        'route'    => '/$not-ready-yet[/[:url]]',
        'defaults' => [ // todo: create a notification page
            'controller'    => 'LabBase\Controller\HomePage',
            'action'        => 'notImplemented',
        ],
    ],
];

return [
    'site_title' => 'Love a Brand',

    'service_manager' => [

    ],

    'controllers' => [
        'invokables' => [
            'LabBase\Controller\HomePage'       => 'LabBase\Controller\HomePageController',
            'LabBase\Controller\BrandFeed'      => 'LabBase\Controller\BrandFeedController',
            'LabBase\Controller\BrandProduct!OBSOLETE!'   => 'LabBase\Controller\BrandProductController',
            'LabBase\Controller\BrandService!OBSOLETE!'   => 'LabBase\Controller\BrandServiceController',
            'LabBase\Controller\BrandAbout!OBSOLETE!'     => 'LabBase\Controller\BrandAboutController',
            'LabBase\Controller\AboutMe!OBSOLETE!'        => 'LabBase\Controller\AboutMeController',
            'LabBase\Controller\PersonalPage!OBSOLETE!'   => 'LabBase\Controller\PersonalPageController',
            'LabBase\Controller\BrandSettings'  => 'LabBase\Controller\BrandSettingsController',
            'LabBase\Controller\Interaction'    => 'LabBase\Controller\InteractionController',
            'LabBase\Controller\AdminPanel'     => 'LabBase\Controller\AdminPanelController',
            'LabBase\Controller\UserSettings'   => 'LabBase\Controller\UserSettingsController',
            'LabBase\Controller\BrandPost'      => 'LabBase\Controller\BrandPostController',
            'LabBase\Controller\BrandSearch!OBSOLETE!'    => 'LabBase\Controller\BrandSearchController',
            'LabBase\Controller\StaticContent'  => 'LabBase\Controller\StaticContentController',
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,

        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'default_template_suffix'  => 'phtml',
        'layout'                   => 'lab-base/layout',

        'template_map' => [

            // Layout templates
            'lab-base/layout'                       => $view_base_path . '/site-layout.phtml',
            'lab-base/feed-layout'                  => $view_base_path . '/feed-layout.phtml',
            'lab-base/bare-layout'                  => $view_base_path . '/bare-layout.phtml',
            'lab-base/user-layout'                  => $_OBSOLETE_path . '/user-layout.phtml',

            // Home page controller (incl. registration/login)
            'lab-base/home-page/index'              => $view_base_path . '/home-page/index.phtml',
            'lab-base/home-page/register'           => $_OBSOLETE_path . '/home-page/register.phtml',
            'lab-base/home-page/register-email'     => $view_base_path . '/home-page/register-email.phtml',
            'lab-base/home-page/register-brand'     => $view_base_path . '/home-page/register-brand.phtml',
            'lab-base/home-page/register-welcome'   => $_OBSOLETE_path . '/home-page/register-welcome.phtml',
            'lab-base/home-page/signin'             => $view_base_path . '/home-page/signin.phtml',
            'lab-base/home-page/logout'             => $view_base_path . '/home-page/logout.phtml',
            'lab-base/home-page/forgot-password'    => $_OBSOLETE_path . '/home-page/forgot-password.phtml',
            'lab-base/home-page/recover-password'   => $_OBSOLETE_path . '/home-page/recover-password.phtml',
            'lab-base/home-page/deactivated'        => $_OBSOLETE_path . '/home-page/deactivated.phtml',
            'lab-base/home-page/not-implemented'    => $view_base_path . '/home-page/not-implemented.phtml',

            // Brand feed related
            'brand-info-partial'                    => $view_base_path . '/brand-feed/brand-info-partial.phtml',
            'brand-post-partial'                    => $view_base_path . '/brand-feed/brand-post-partial.phtml',

            'lab-base/brand-feed/index'             => $view_base_path . '/brand-feed/post-feed.phtml',
            'lab-base/brand-feed/home'              => $view_base_path . '/brand-feed/home.phtml',
            'lab-base/brand-feed/stories'           => $view_base_path . '/brand-feed/stories.phtml',
            'lab-base/brand-feed/gallery'           => $view_base_path . '/brand-feed/gallery.phtml',
            'lab-base/brand-feed/album'             => $view_base_path . '/brand-feed/album.phtml',
            'lab-base/brand-feed/video'             => $view_base_path . '/brand-feed/video.phtml',
            'lab-base/brand-feed/products'          => $view_base_path . '/brand-feed/products.phtml',
            'lab-base/brand-feed/services'          => $view_base_path . '/brand-feed/services.phtml',
            'lab-base/brand-feed/aboutus'           => $view_base_path . '/brand-feed/aboutus.phtml',
            'lab-base/brand-feed/aboutme'           => $view_base_path . '/brand-feed/aboutme.phtml',
            'lab-base/brand-feed/management-team'   => $view_base_path . '/brand-feed/management-team.phtml',
            'lab-base/brand-feed/drafts'            => $view_base_path . '/brand-feed/drafts.phtml',
            'lab-base/brand-feed/get-post-data'     => $view_base_path . '/brand-feed/get-post.phtml',
            'lab-base/brand-feed/discovery'         => $view_base_path . '/brand-feed/discovery.phtml',
            'lab-base/brand-feed/search-brands'     => $view_base_path . '/brand-feed/search.phtml',
            'lab-base/brand-feed/post-modal'        => $view_base_path . '/brand-feed/brand-post-modal.phtml',
            'lab-base/brand-feed/portfolio'         => $view_base_path . '/brand-feed/portfolio.phtml',
            'brand-post-modal'    => $view_base_path . '/brand-feed/brand-post-modal.phtml',
            'lab-base/brand-feed/mybrands'          => $view_base_path . '/brand-feed/mybrands.phtml',

            'lab-base/brand-feed/get-about'          => $view_base_path . '/brand-feed/get-about.phtml',
            'lab-base/brand-feed/show-team-member'          => $view_base_path . '/brand-feed/show-team-member.phtml',

            'lab-base/brand-feed/manage-comments'   => $_OBSOLETE_path . '/brand-feed/manage-comments.phtml',

            'lab-base/brand-post/get-single-album'  => $_OBSOLETE_path . '/brand-feed/get-album.phtml',

            'lab-base/brand-product/index'          => $_OBSOLETE_path . '/brand-product/index.phtml',
            'lab-base/brand-product/get-popup-product' =>$_OBSOLETE_path.'/brand-product/get-product.phtml',

            'lab-base/brand-service/index'          => $_OBSOLETE_path . '/brand-service/index.phtml',
            'lab-base/brand-feed/get-popup-service' => $view_base_path.'/brand-feed/get-service.phtml',

            'lab-base/brand-about/index'            => $_OBSOLETE_path . '/brand-about/index.phtml',
            'lab-base/brand-about/management-team'  => $_OBSOLETE_path . '/brand-about/management-team.phtml',
            'lab-base/brand-about/get-popup-about'  => $_OBSOLETE_path . '/brand-about/get-about.phtml',

            'lab-base/brand-about/show-team-member-detail'=> $_OBSOLETE_path.'/brand-about/show-team-detail.phtml',


            //about me controller
            'lab-base/about-me/index'               => $_OBSOLETE_path . '/about-me/index.phtml',
            //end

            // personel page controller
            'lab-base/personal-page/home'           => $_OBSOLETE_path . '/personal-page/home.phtml',
            'lab-base/personal-page/photos'         => $_OBSOLETE_path . '/personal-page/photos.phtml',
            'lab-base/personal-page/story'          => $_OBSOLETE_path . '/personal-page/story.phtml',
            'lab-base/personal-page/portfolio'      => $_OBSOLETE_path . '/personal-page/portfolio.phtml',
            'lab-base/personal-page/videos'         => $_OBSOLETE_path . '/personal-page/videos.phtml',
            'lab-base/personal-page/albums'         => $_OBSOLETE_path . '/personal-page/albums.phtml',
            'lab-base/personal-page/get-single-album' =>$_OBSOLETE_path. '/personal-page/get-album.phtml',
            'lab-base/personal-page/get-single-post'  =>$_OBSOLETE_path. '/personal-page/get-post.phtml',

            'lab-base/user-settings/index'          => $_OBSOLETE_path . '/user-settings/index.phtml',
            'lab-base/user-settings/profile'        => $view_base_path . '/user-settings/profile.phtml',

            'lab-base/brand-settings/edit-categories'       => $_OBSOLETE_path . '/brand-settings/edit-categories.phtml',
            'lab-base/brand-settings/edit-brand-settings'   => $_OBSOLETE_path . '/brand-settings/edit-brand-settings.phtml',

            'lab-base/admin-panel/index'            => $_OBSOLETE_path . '/admin-panel/index.phtml',
            'lab-base/admin-panel/brands'           => $_OBSOLETE_path . '/admin-panel/brands.phtml',
            'lab-base/admin-panel/users'            => $_OBSOLETE_path . '/admin-panel/users.phtml',
            'lab-base/admin-panel/site-page-list'   => $_OBSOLETE_path . '/admin-panel/site-page-list.phtml',
            'lab-base/admin-panel/site-docs'        => $_OBSOLETE_path . '/admin-panel/site-docs.phtml',
            'lab-base/admin-panel/image-manager'    => $_OBSOLETE_path . '/admin-panel/image-manager.phtml',

            'lab-base/brand-search/index'           => $_OBSOLETE_path . '/brand-search/index.phtml',
            'lab-base/brand-search/search'          => $_OBSOLETE_path . '/brand-search/search.phtml',

            'lab-base/static-content/brand-owner-help' => $_OBSOLETE_path . '/static-content/brand-owner-help.phtml',
            'lab-base/static-content/terms-of-service' => $_OBSOLETE_path . '/static-content/terms-of-service.phtml',

            'error/404'                             => $view_base_path . '/error-404.phtml',
            'error/index'                           => $view_base_path . '/error.phtml',
        ],
        'template_path_stack' => [
            $_OBSOLETE_path
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],

    // Models which extend DBModel
    'db_models' => [
        'Brand',
        'BrandAbout',
        'User',
        'Image',
        'BrandService',
        'BrandServiceMeta',
        'BrandTeamMeta',
        'BrandTeamMember',
        'BrandProductsCategory',
        //'BrandProduct',
        'BrandProductsCollection',
        'BrandPost',
        'Category',
        'Notification',
        'BrandPostLoved',
        'Comment',
        'AboutMe',
        'PortfolioProject',
    ],

    'router' => [
        'routes' => [

            'homepage' => [
                'type'    => 'Literal',
                'metadata' => [
                    'id' => '1',
                    'description' => "The home page",
                ],
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller'    => 'LabBase\Controller\HomePage',
                        'action'        => 'index',
                    ],
                ],
            ],

            // Brand Feed Controller
            'brand' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.1', // this has to be at the beginning of the route array because of
                    'description' => "The brand's main feed page.", // the order in which entries are checked!
                    'longdesc' => 'XML-HTTP requests produce only the inner contents.
                    Available segments: landing, home, stories, gallery, products, services, about',
                    'params' => [
                        'route url Brand URL.',
                        'route segmentOrId PostID or segment name',
                        "route format Either 'html', 'json'",
                    ],
                ],
                'options' => [
                    /** note: url hardcoded in feed.js */
                    'route'    => '/:url[/[:segmentOrId[/[:param1[/[:param2[/[:param3[/[:param4]]]]]]]]]]',
                    /*'route'    => '/:url[/[:segmentOrId[/[:albums[/[:albumId]]]]]]',*/
                    /*'route'    => ':brandUrl[/[:categoryUrl[/[:postId]]]]',*/
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'index',
                        'segmentOrId' => 'landing',
                        'format' => 'html',
                        /*'categoryUrl' => 'home', */ // todo: check

                    ],
                    'constraints' => [
                        'url' => '[^$][^$/?]*',
                    ],
                ],
            ],
            'brand-json' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.2',
                    'description' => "The brand's main feed page.", // the order in which entries are checked!
                    'longdesc' => 'XML-HTTP requests produce only the inner contents.
                    Available segments: landing, home, stories, gallery, products, services, about',
                    'params' => [
                        'route url Brand URL.',
                        'route segmentOrId PostID or segment name',
                        "route format Either 'html', 'json'",
                    ],
                ],
                'options' => [
                    /** note: url hardcoded in feed.js */
                    'route'    => '/$json-feed/:url[/[:segmentOrId[/[:param1[/[:param2[/[:param3[/[:param4]]]]]]]]]]',
                    /*'route'    => '/:url[/[:segmentOrId[/[:albums[/[:albumId]]]]]]',*/
                    /*'route'    => ':brandUrl[/[:categoryUrl[/[:postId]]]]',*/
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'index',
                        'segmentOrId' => 'landing',
                        'format' => 'json',
                    ],
                    'constraints' => [
                        'url' => '[^$][^$/?]*',
                    ],
                ],
            ],

            'brandPostModal' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '1.0', // this has to be at the beginning of the route array because of
                    'description' => "Generate modal window", // the order in which entries are checked!
                    'params' => [
                        'Post ID.',
                    ],
                ],
                'options' => [
                    'route'    => '/$brandpostmodal[/[:id]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'postModal',
                    ]
                ],
            ],

            // Brand Settings Controller
            'brandSettings' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => 'x',
                    'description' => "The brand settings controller with parametric action",
                    'longdesc' => 'Available actions: uploadAvatar, uploadBanner'
                ],
                'options' => [
                    'route'    => '/$brandSettings[/[:url[/[:action[/[:param]]]]]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandSettings',
                        'action' => 'index',
                    ],
                ],
                'constraints' => [
                    'url' => '[^$][^$/?]*',
                ],
            ],

            'manageBrand' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => 'x',
                ],
                'options' => [
                    'route'    => '/$manageBrand[/[:id]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'manageBrand',
                    ],
                ]
            ],
            
            'submit-brand-relation' => [
                /** note: url hardcoded in feed.js */
                'type'    => 'Segment',
                'metadata' => [
                    'id' => 'x',
                    'description' => "The brand's main feed page.", // the order in which entries are checked!
                    'longdesc' => 'XML-HTTP requests produce only the inner contents.
                    Available segments: landing, home, stories, gallery, products, services, about',
                    'params' => [
                        'route url Brand URL.',
                        'route segmentOrId PostID or segment name',
                        "route format Either 'html', 'json'",
                    ],
                ],
                'options' => [
                    /** note: url hardcoded in feed.js */
                    'route'    => '/$brandRelation/:url/:relationType/:value',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\Interaction',
                        'action' => 'submitRelation',

                    ],
                    'constraints' => [
                        'url' => '[^$][^$/?]*',
                    ],
                ],
            ],

            'brand-services' => $notImplementedRoute,

            'add-post' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.0',
                    'description' => "Creates new post",
                ],
                'options' => [
                    'route'    => '/$brandPost/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addPost',
                    ],
                ],
            ],
            
            'add-post-image' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.0.1',
                    'description' => "Allows upload of new image for a post being created",
                ],
                'options' => [
                    'route'    => '/$addPostImage/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addPostImage',
                    ],
                ],
            ],
            
            'delete-post' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.0.2',
                    'description' => "Deletes given post id",
                ],
                'options' => [
                    'route'    => '/$brandPostDelete/:postId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'deletePost',
                    ],
                ],
            ],
            
            'get-post' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.0.3',
                    'description' => "get single post by id",
                ],
                'options' => [
                    'route'    => '/$getPost/:postId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'getPostData',
                    ],
                ],
            ],
            
            'edit-post' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.0.4',
                    'description' => "Used for post edit requests",
                ],
                'options' => [
                    'route'    => '/$brandPostEdit/:postId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'editPost',
                    ],
                ],
            ],
            
            'post-love' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.0.5',
                    'description' => "Allows love and unlove of a post",
                ],
                'options' => [
                    'route'    => '/$brandPostSetLove/:postId/:loveOrNot',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'setPostLoved',
                    ],
                ],
            ],
            
            'post-comment' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.0.6',
                    'description' => "Posts a comment in a post with given ID",
                ],
                'options' => [
                    'route'    => '/$postComment/:postId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'postComment',
                    ],
                ],
            ],
            
            'get-comments' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.0.7',
                    'description' => "Returns comments for a post with given id in JSON",
                ],
                'options' => [
                    'route'    => '/$getComments/:postId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'getComments',
                    ],
                ],
            ],
            
            'manage-comment' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.0.8',
                    'description' => "Executes an action in the BrandPost controller, used for approving and deleting comments",
                ],
                'options' => [
                    'route'    => '/$commentAction/:commentId/:action',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed'
                    ],
                ],
            ],
            
            'edit-album-photo' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.0.9',
                    'description' => "Edit album photo data",
                ],
                'options' => [
                    'route'    => '/$editAlbumPhoto/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'editAlbumPhoto',
                    ],
                ],
            ],

            'add-service' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.1.1', // this has to be at the beginning of the route array because of
                    'description' => "add service", // the order in which entries are checked!
                ],
                'options' => [
                    'route'    => '/$serviceAdd[/[:brandUrl]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addService',
                    ],
                ],
            ],


            'my-brand' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '1.7',
                    'description' => "Redirects the user to their brand or profile (if they haven't got one)",
                ],
                'options' => [
                    'route'    => '/$myBrand',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\HomePage',
                        'action' => 'goToMyBrand',
                    ],
                ],
            ],

            'get-service-popup' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.1.2',
                    'description' => "get single service for popup",
                ],
                'options' => [
                    'route'    => '/$brandServicePopup/:serviceId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'getPopupService',
                    ],
                ],
            ],
            
            'edit-service' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.1.3',
                    'description' => "edit service",
                ],
                'options' => [
                    'route'    => '/$brandServiceEdit/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'editService',
                    ],
                ],
            ],
            
            'delete-service' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.1.4',
                    'description' => "Deletes given service id",
                ],
                'options' => [
                    'route'    => '/$brandServiceDelete/:serviceId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'deleteService',
                    ],
                ],
            ],
            
            
            'manage-service-meta' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.1.5', // this has to be at the beginning of the route array because of
                    'description' => "Change brand's service title and decription", // the order in which entries are checked!
                ],
                'options' => [
                    'route'    => '/$manageServiceMeta[/[:brandUrl]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'manageServiceSettings',
                    ],
                ],
            ],
            
            'service-add-image' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.1.6',
                    'description' => "Allows upload of new image for a service being created",
                ],
                'options' => [
                    'route'    => '/$brandServiceAddImage/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addServiceImage',
                    ],
                ],
            ],
            
            
            
            // About Us
            'brand-about' => $notImplementedRoute,

            'add-aboutus' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.2.1', // this has to be at the beginning of the route array because of
                    'description' => "add about us", // the order in which entries are checked!
                ],
                'options' => [
                    'route'    => '/$aboutAdd[/[:brandUrl]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addAbout',
                    ],
                ],
            ],
            
            'get-about-popup' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.2.2',
                    'description' => "get single about for popup",
                ],
                'options' => [
                    'route'    => '/$brandAboutPopup/:aboutId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'getPopupAbout',
                    ],
                ],
            ],
            
            'edit-about' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.2.3',
                    'description' => "edit about us data",
                ],
                'options' => [
                    'route'    => '/$brandAboutEdit/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'editAbout',
                    ],
                ],
            ],

            'delete-about' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.2.4',
                    'description' => "Deletes given about us id",
                ],
                'options' => [
                    'route'    => '/$brandAboutDelete/:aboutId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'deleteAbout',
                    ],
                ],
            ],
            
            'update-about-index' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.2.5', // this has to be at the beginning of the route array because of
                    'description' => "update index", // the order in which entries are checked!
                ],
                'options' => [
                    'route'    => '/$aboutUpdateOrder[/[:brandUrl]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'updateIndexAbout',
                    ],
                ],
            ],

            // END
            
            /* MANAGEMENT TEAM */
            'management-team' => $notImplementedRoute,

            'add-team-member' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.3.1', // this has to be at the beginning of the route array because of
                    'description' => "Add brand's team member", // the order in which entries are checked!
                ],
                'options' => [
                    'route'    => '/$addTeamMember[/[:brandUrl]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addTeamMember',
                    ],
                ],
            ],
            'show-team-member' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.3.2',
                    'description' => "Show team member details",
                ],
                'options' => [
                    'route'    => '/$teamMemberDetail/:tmId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'showTeamMemberDetail',
                    ],
                ],
            ],
            
            'edit-team-member' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.3.3', // this has to be at the beginning of the route array because of
                    'description' => "Edit brand's team member", // the order in which entries are checked!
                ],
                'options' => [
                    'route'    => '/$editTeamMember/:tmId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'editTeamMember',
                    ],
                ],
            ],
            'delete-team-member' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.3.4',
                    'description' => "Delete team member details",
                ],
                'options' => [
                    'route'    => '/$deleteTeamMember/:tmId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'deleteTeamMember',
                    ],
                ],
            ],
            
            'manage-team-meta' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.3.5', // this has to be at the beginning of the route array because of
                    'description' => "Change brand's team's title and decription", // the order in which entries are checked!
                ],
                'options' => [
                    'route'    => '/$teamMeta[/[:brandUrl]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'manageBradsTeamMeta',
                    ],
                ],
            ],
            
            'add-iteam-mage' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '11.10',
                    'description' => "Allows upload of new image for a team member being created",
                ],
                'options' => [
                    'route'    => '/$brandTeamMemberAddImage/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addTeamMemberImage',
                    ],
                ],
            ],
            /* END */ 
            
            /* PRODUCT */
            'brand-products' => $notImplementedRoute,

            'products-category' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.1', // this has to be at the beginning of the route array because of
                    'description' => "The brand's products manage category", // the order in which entries are checked!
                ],
                'options' => [
                    'route'    => '/$manageProductsCategory[/[:brandUrl]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'manageProductsCategory',
                    ],
                ],
            ],
            
            'products-collections' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.2', // this has to be at the beginning of the route array because of
                    'description' => "The brand's products manage collections", // the order in which entries are checked!
                ],
                'options' => [
                    'route'    => '/$manageProductsCollections[/[:brandUrl]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'manageProductsCollection',
                    ],
                ],
            ],
            
            'product-add-image' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.3',
                    'description' => "Allows upload of new image for a product being created",
                ],
                'options' => [
                    'route'    => '/$brandProductAddImage/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addProductImage',
                    ],
                ],
            ],

            'add-product' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.4',
                    'description' => "Creates new product",
                ],
                'options' => [
                    'route'    => '/$brandProductAdd/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addProduct',
                    ],
                ],
            ],

            'edit-product' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.5',
                    'description' => "edit product",
                ],
                'options' => [
                    'route'    => '/$brandProductEdit/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'editProduct',
                    ],
                ],
            ],
            
            'get-product' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.6',
                    'description' => "get single product for edit data",
                ],
                'options' => [
                    'route'    => '/$brandProductGet/:productId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'getSingleProduct',
                    ],
                ],
            ],
            'delete-product' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.7',
                    'description' => "Deletes given product id",
                ],
                'options' => [
                    'route'    => '/$brandProductDelete/:productId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'deleteProduct',
                    ],
                ],
            ],
            /* END */
            
            /* PORTFOLIO */
            'portfolio-add-image' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.7',
                    'description' => "add portfolio image",
                ],
                'options' => [
                    'route'    => '/$addPortfolioiImage/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addPortfolioImage',
                    ],
                ],
            ],
            'add-portfolio' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.7',
                    'description' => "add portfolio",
                ],
                'options' => [
                    'route'    => '/$addPortfolio/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addPortfolio',
                    ],
                ],
            ],

            'delete-portfolio-project' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.7',
                    'description' => "delete portfolio project",
                ],
                'options' => [
                    'route'    => '/$portfolioProjectDelete/:projectId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'deletePortfolioProject',
                    ],
                ],
            ],
            'manage-portfolio-project' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.7',
                    'description' => "manage portfolio project",
                ],
                'options' => [
                    'route'    => '/$managePortfolioProject/:projectId',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'managePortfolioProject',
                    ],
                ],
            ],
            /* END */

            /*ABOUT ME*/
            'aboutme-add-image' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.4.3',
                    'description' => "Allows upload of new image for a About me being created",
                ],
                'options' => [
                    'route'    => '/$aboutmeAddImage/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addAboutmeImage',
                    ],
                ],
            ],

            'aboutMeAdd' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '12.2', // this has to be at the beginning of the route array because of
                    'description' => "add about me data", // the order in which entries are checked!
                ],
                'options' => [
                    'route'    => '/$aboutMeAdd[/[:brandUrl]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'addAboutMe',
                    ],
                ],
            ],

            'aboutMeEdit' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '12.3', // this has to be at the beginning of the route array because of
                    'description' => "edit about me data", // the order in which entries are checked!
                ],
                'options' => [
                    'route'    => '/$aboutMeEdit[/[:brandUrl]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'editAboutMe',
                    ],
                ],
            ],

            /*END*/

            /* Discover Brands and search brands */
            'discovery' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.5',
                    'description' => "Brand search page",
                ],
                'options' => [
                    'route'    => '/discovery[/[:param]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'discovery',
                    ],
                ],
            ],
            
            'getBrandSearchAutocomplete' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '2.5.1',
                    'description' => "Returns search autocomplete in JSON, expects 'query' via POST",
                ],
                'options' => [
                    'route'    => '/$autocompleteDiscover',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'autocomplete',
                    ],
                ],
            ],
            
            /* END */
            
            
            'not-implemented' => $notImplementedRoute,
            'user' => $notImplementedRoute,

            'sign-in' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '3',
                    'description' => "The signin page",
                    'params' => [
                        'post email Login email.',
                        'post password Login password.',
                    ],
                    'returns' => [
                        'bool invalidEmailOrPassword True if an error has ocurred during login.',
                        'string|false errorMessage Registration errors as HTML with &lt;br&gt;\'s'
                    ]
                ],
                'options' => [
                    'route'    => '/$signin[/]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\HomePage',
                        'action' => 'signin',
                    ],
                ],
            ],
            'sign-up' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => 'x',
                    'description' => "Registration and new brand creation form.",
                    'longdesc' => "Registration and new brand creation form. Brand registration requires " .
                        "being logged in. For brand registration brandName and brandType are required. ".
                        "For user registration username, email and password are required.",
                    'params' => [
                        'post username Registration username.',
                        'post email Registration email.',
                        'post password Registration password.',
                        'post brandName Registered brand name.',
                        'post brandType One of `brand`, `pro`, `custom`; probably going to be changed.',
                        'post brandRegistration If not set register user, if true register brand.',
                    ],
                    'returns' => [
                        'string registrationErrors Registration errors as HTML with &lt;br&gt;\'s',
                        'string username Username entered by the user. May be empty. Not sanitized.',
                        'string email Email provided by the user. May be empty. Not sanitized.',
                        '  todo: fill in the rest',
                    ]
                ],
                'options' => [
                    'route'    => '/$register[/[:page[/]]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\HomePage',
                        'action' => 'register',
                        'page' => 'email',
                    ],
                ],
            ],
            'sign-up-brand-welcome' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '1.2',
                    'description' => "Welcome screen shown after registraion",
                ],
                'options' => [
                    'route'    => '/$register/welcome/:brandUrl',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\HomePage',
                        'action' => 'registerWelcome',
                    ],
                ],
            ],
            'sign-out' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '4',
                    'description' => "The logout page",
                ],
                'options' => [
                    'route'    => '/$sign-out[/[:sessionId]]',
                    'constraints' => [
                        'sessionId'     => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => 'LabBase\Controller\HomePage',
                        'action' => 'logout',
                        'sessionId' => 'invalid',
                    ],
                ],
            ],
            'checkLogined' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '1.0.0',
                    'description' => "To check the user is authorized",
                    'returns' => [
                        'User class'
                    ]
                ],
                'options' => [
                    'route'    => '/$checkLogined[/]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\HomePage',
                        'action' => 'checkUserLogined',
                    ],
                ],
            ],
            'signinAjax' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '1.4.1',
                    'description' => "The signin page with ajax",
                    'params' => [
                        'post email Login email.',
                        'post password Login password.',
                    ],
                    'returns' => [
                        'bool invalidEmailOrPassword True if an error has ocurred during login.',
                        'string|false errorMessage Registration errors as HTML with &lt;br&gt;\'s'
                    ]
                ],
                'options' => [
                    'route'    => '/$signinAjax[/]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\HomePage',
                        'action' => 'signinAjax',
                    ],
                ],
            ], 
            'forgot-password' => $notImplementedRoute,
            'user-settings' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '1.0',
                    'description' => "The user settings",
                ],
                'options' => [
                    'route'    => '/userSettings[/]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\UserSettings',
                        'action' => 'profile',
                    ],
                ],
            ],
            'admin-panel' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '6.1',
                    'description' => "The admin panel",
                ],
                'options' => [
                    'route'    => '/$admin[/[:action[/[:param]]]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\AdminPanel',
                        'action' => 'index',
                    ],
                ],
            ],
            
            
            'inbox' => $notImplementedRoute,
            /*'drafts' => $notImplementedRoute,*/
            'search-brands' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => '7.1',
                    'description' => "Brand search page",
                ],
                'options' => [
                    'route'    => '/search[/[:param]]',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\BrandFeed',
                        'action' => 'searchBrands',
                    ],
                ],
            ],



            'changeUserAvatar' => [
                'type'    => 'Segment',
                'metadata' => [
                    'id' => 'ajax',
                    'description' => "change User Avatar",
                ],
                'options' => [
                    'route'    => '/changeUserAvatar',
                    'defaults' => [
                        'controller' => 'LabBase\Controller\UserSettings',
                        'action' => 'changeAvatar',
                    ],
                ],
            ],


            'old' => [
                'type'    => 'Literal',
                'metadata' => [
                    'id' => '-2',
                    'description' => "The home page",
                ],
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller'    => 'LabBase\Controller\HomePage',
                        'action'        => 'index',
                    ],
                ],
                'child_routes' => [
                    /*'brandProducts' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '2.2', // this has to be at the beginning of the route array because of
                            'description' => "The brand's product page", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$products[/[:brandUrl[/[:categoryId[/[:collectionId]]]]]]',
                            
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'index',
                            ],
                        ],
                    ],*/

                    /*'brandProductsManageCategory' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '2.3', // this has to be at the beginning of the route array because of
                            'description' => "The brand's products manage category", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$manageProductsCategory[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'manageProductsCategory',
                            ],
                        ],
                    ],*/

                    /*'brandProductsManageCollections' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '2.3', // this has to be at the beginning of the route array because of
                            'description' => "The brand's products manage collections", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$manageProductsCollections[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'manageProductsCollection',
                            ],
                        ],
                    ],*/

                    /*'brandProductAddImage' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '2.4',
                            'description' => "Allows upload of new image for a product being created",
                        ],
                        'options' => [
                            'route'    => '$brandProductAddImage/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'addProductImage',
                            ],
                        ],
                    ],

                    'brandAddProduct' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '2.5',
                            'description' => "Creates new product",
                        ],
                        'options' => [
                            'route'    => '$brandProductAdd/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'addProduct',
                            ],
                        ],
                    ],

                    'brandEditProduct' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '2.6',
                            'description' => "edit product",
                        ],
                        'options' => [
                            'route'    => '$brandProductEdit/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'editProduct',
                            ],
                        ],
                    ],*/

                    /*'brandGetSingleProduct' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '2.7',
                            'description' => "get single product for edit data",
                        ],
                        'options' => [
                            'route'    => '$brandProductGet/:productId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'getSingleProduct',
                            ],
                        ],
                    ],*/

                    'brandGetProductPopup' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '2.8',
                            'description' => "get single product for popup",
                        ],
                        'options' => [
                            'route'    => '$brandProductPopup/:productId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'getPopupProduct',
                            ],
                        ],
                    ],

                    'brandProductLove' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '2.8',
                            'description' => "Allows love and unlove of a product",
                        ],
                        'options' => [
                            'route'    => '$brandProductSetLove/:productId/:loveOrNot',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'setProductLoved',
                            ],
                        ],
                    ],

                    /*'brandDeleteProduct' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.7',
                            'description' => "Deletes given product id",
                        ],
                        'options' => [
                            'route'    => '$brandProductDelete/:productId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'deleteProduct',
                            ],
                        ],
                    ],*/

                    'brandPostProductComment' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.8',
                            'description' => "Posts a comment in a product with given ID",
                        ],
                        'options' => [
                            'route'    => '$postProductComment/:productId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'postProdcutsComment',
                            ],
                        ],
                    ],

                    'brandProductComments' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.9',
                            'description' => "Returns comments for a product with given id in JSON",
                        ],
                        'options' => [
                            'route'    => '$getProductComments/:productId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct',
                                'action' => 'getProductComments',
                            ],
                        ],
                    ],

                    'brandProductManageComment' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.10',
                            'description' => "Executes an action in the BrandProduct controller, used for approving and deleting comments",
                        ],
                        'options' => [
                            'route'    => '$productCommentAction/:commentId/:action',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandProduct'
                            ],
                        ],
                    ],

                    'manageComments' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '2.1',
                            'description' => "Comment management page",
                        ],
                        'options' => [
                            'route'    => '$manageComments/:brandId[/[:commentsType[/]]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandFeed',
                                'action' => 'manageComments',
                                'commentsType' => 'latest',
                            ],
                        ],
                    ],

                    // Home Page Controller
                    'register' => $notImplementedRoute,

                    'registrationDone' => $notImplementedRoute,

                    'logout' => $notImplementedRoute,

                    'deactivated' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '1.3',
                            'description' => "Account deactivated page",
                        ],
                        'options' => [
                            'route'    => '$deactivated[/]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\HomePage',
                                'action' => 'deactivated',
                            ],
                        ],
                    ],

                    'signin' => $notImplementedRoute,                  

                    'forgotPassword' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '1.5',
                            'description' => "Page for requesting password change email",
                        ],
                        'options' => [
                            'route'    => '$forgotPassword[/]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\HomePage',
                                'action' => 'forgotPassword',
                            ],
                        ],
                    ],


                    'recoverPassword' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '1.6',
                            'description' => "Page for changing the password with a link from email",
                        ],
                        'options' => [
                            'route'    => '$recoverPassword[/]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\HomePage',
                                'action' => 'recoverPassword',
                            ],
                        ],
                    ],
                    'myBrand' => $notImplementedRoute,

                    'getNotifications' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '1.8',
                            'description' => "Provides JSON response giving user's notifications",
                        ],
                        'options' => [
                            'route'    => '$getNotifications',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\HomePage',
                                'action' => 'getNotifications',
                            ],
                        ],
                    ],


                    // Brand Settings Controller
                    'brandSettings' => $notImplementedRoute,

                    'editBrandSettings' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '3.2',
                            'description' => "Page for editing brand settings",
                        ],
                        'options' => [
                            'route'    => '$editBrandSettings/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandSettings',
                                'action' => 'editBrandSettings',
                            ],
                        ],
                    ],

                    'editBrandCategories' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '3.3',
                            'description' => "Page for editing brand menus",
                        ],
                        'options' => [
                            'route'    => '$brandCategories/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandSettings',
                                'action' => 'editCategories',
                            ],
                        ],
                    ],

                    'getBrandCategories' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '3.4',
                            'description' => "Page for editing brand menus",
                        ],
                        'options' => [
                            'route'    => '$getBrandCategories/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandSettings',
                                'action' => 'getCategories',
                            ],
                        ],
                    ],

                    'saveBrandCategories' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '3.5',
                            'description' => "Ajax action for saving the brand categories",
                        ],
                        'options' => [
                            'route'    => '$saveBrandCategories/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandSettings',
                                'action' => 'saveCategories',
                            ],
                        ],
                    ],

                    // Brand Post Controller
                    /*'brandPostComments' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.1',
                            'description' => "Returns comments for a post with given id in JSON",
                        ],
                        'options' => [
                            'route'    => '$getComments/:postId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'getComments',
                            ],
                        ],
                    ],*/
                    'brandGetAllComments' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.2',
                            'description' => "Returns comments for a post with given id in JSON",
                        ],
                        'options' => [
                            'route'    => '$getBrandComments/:brandId[/[:commentsType[/]]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'getBrandComments',
                                'commentsType' => 'latest'
                            ],
                        ],
                    ],
                    /*'brandPostPostComment' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.3',
                            'description' => "Posts a comment in a post with given ID",
                        ],
                        'options' => [
                            'route'    => '$postComment/:postId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'postComment',
                            ],
                        ],
                    ],*/
                    /*'brandPostManageComment' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.4',
                            'description' => "Executes an action in the BrandPost controller, used for approving and deleting comments",
                        ],
                        'options' => [
                            'route'    => '$commentAction/:commentId/:action',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost'
                            ],
                        ],
                    ],*/
                    /*'brandFeedAddPost' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.5',
                            'description' => "Creates new post",
                        ],
                        'options' => [
                            'route'    => '$brandPost/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'addPost',
                            ],
                        ],
                    ],*/

                    'brandFeedEditPostType' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.6',
                            'description' => "Edit post type data",
                        ],
                        'options' => [
                            'route'    => '$brandEditPostType/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'editPostType',
                            ],
                        ],
                    ],

                    /*'brandFeedEditAlbumPhoto' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.6',
                            'description' => "Edit album photo data",
                        ],
                        'options' => [
                            'route'    => '$brandEditAlbumPhoto/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'editAlbumPhoto',
                            ],
                        ],
                    ],*/

                    /*'brandFeedEditPost' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.6',
                            'description' => "Used for post edit requests",
                        ],
                        'options' => [
                            'route'    => '$brandPostEdit/:postId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'editPost',
                            ],
                        ],
                    ],*/
                    /*'brandFeedDeletePost' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.7',
                            'description' => "Deletes given post id",
                        ],
                        'options' => [
                            'route'    => '$brandPostDelete/:postId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'deletePost',
                            ],
                        ],
                    ],*/
                    /*'brandFeedAddPostImage' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.5',
                            'description' => "Allows upload of new image for a post being created",
                        ],
                        'options' => [
                            'route'    => '$brandPostAddImage/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'addPostImage',
                            ],
                        ],
                    ],*/
                    /*'brandFeedPostLove' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.8',
                            'description' => "Allows love and unlove of a post",
                        ],
                        'options' => [
                            'route'    => '$brandPostSetLove/:postId/:loveOrNot',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'setPostLoved',
                            ],
                        ],
                    ],*/

                    'brandGetSingleAlbum' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.9',
                            'description' => "get single album for edit data",
                        ],
                        'options' => [
                            'route'    => '$brandAlbumGet/:albumId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'getSingleAlbum',
                            ],
                        ],
                    ],

                    'brandGetSliderPost' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '4.10',
                            'description' => "get single slider post for display",
                        ],
                        'options' => [
                            'route'    => '$brandSliderPost/:postId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandPost',
                                'action' => 'getSingleSliderPost',
                            ],
                        ],
                    ],


                    // User Settings Controller
                    'userSettings' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '5.2',
                            'description' => "The user settings controller with parametric action",
                        ],
                        'options' => [
                            'route'    => '$settings[/[:action[/[:param]]]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\UserSettings',
                                'action' => 'index',
                            ],
                        ],
                    ],
                    'changeUserSettings' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => 'ajax',
                            'description' => "The user settings controller with parametric action",
                        ],
                        'options' => [
                            'route'    => '$changeSettings',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\UserSettings',
                                'action' => 'changeSettings',
                            ],
                        ],
                    ],





                    // Admin Panel Controller
                    'adminPanel' => $notImplementedRoute,

                    'siteDocs' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '6.2',
                            'description' => "The site docs",
                        ],
                        'options' => [
                            'route'    => '$docs[/[:param]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\AdminPanel',
                                'action' => 'siteDocs',
                            ],
                        ],
                    ],


                    // Static Content Controller
                    'brandOwnerHelp' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '7.1',
                            'description' => "Brand owner's FAQ/Help page",
                        ],
                        'options' => [
                            'route'    => '$help/faq',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\StaticContent',
                                'action' => 'brandOwnerHelp',
                            ],
                        ],
                    ],
                    'termsOfService' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '7.2',
                            'description' => "Terms of service",
                        ],
                        'options' => [
                            'route'    => '$help/tos',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\StaticContent',
                                'action' => 'termsOfService',
                            ],
                        ],
                    ],

                    /*'discover' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '8.1',
                            'description' => "Brand search page",
                        ],
                        'options' => [
                            'route'    => '$discover[/[:action[/[:param]]]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandSearch',
                                'action' => 'index',
                            ],
                        ],
                    ],*/
                    /*'getBrandSearchAutocomplete' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '8.2',
                            'description' => "Returns search autocomplete in JSON, expects 'query' via POST",
                        ],
                        'options' => [
                            'route'    => '$autocompleteDiscover',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandSearch',
                                'action' => 'autocomplete',
                            ],
                        ],
                    ],*/

                    'interactionController' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '9.1',
                            'description' => "Access to the interaction controller",
                        ],
                        'options' => [
                            'route'    => '$interact/:action[/[:param[/[:param2]]]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\Interaction',
                            ],
                        ],
                    ],

                    // Service Controller
                    /*'brandServices' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '10', // this has to be at the beginning of the route array because of
                            'description' => "The brand's service page", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$services[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandFeed',
                                'action' => 'services',
                            ],
                        ],
                    ],*/

                    /*'brandManageSettings' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '10.1', // this has to be at the beginning of the route array because of
                            'description' => "Change brand's title and decription", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$brands[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandService',
                                'action' => 'manageBradsSettings',
                            ],
                        ],
                    ],*/

                    /*'brandServiceAdd' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '10.2', // this has to be at the beginning of the route array because of
                            'description' => "add service", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$serviceAdd[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandService',
                                'action' => 'addService',
                            ],
                        ],
                    ],*/

                    /*'brandServiceAddImage' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '10.3',
                            'description' => "Allows upload of new image for a service being created",
                        ],
                        'options' => [
                            'route'    => '$brandServiceAddImage/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandService',
                                'action' => 'addServiceImage',
                            ],
                        ],
                    ],*/

                    /*'brandDeleteService' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '10.4',
                            'description' => "Deletes given service id",
                        ],
                        'options' => [
                            'route'    => '$brandServiceDelete/:serviceId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandService',
                                'action' => 'deleteService',
                            ],
                        ],
                    ],*/

                    /*'brandGetServicePopup' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '10.5',
                            'description' => "get single service for popup",
                        ],
                        'options' => [
                            'route'    => '$brandServicePopup/:serviceId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandService',
                                'action' => 'getPopupService',
                            ],
                        ],
                    ],*/

                    /*'brandEditService' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '10.6',
                            'description' => "edit service",
                        ],
                        'options' => [
                            'route'    => '$brandServiceEdit/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandService',
                                'action' => 'editService',
                            ],
                        ],
                    ],*/



                    /*// About Us
                    'brandAbout' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11', // this has to be at the beginning of the route array because of
                            'description' => "The brand's about us", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$aboutUs[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'index',
                            ],
                        ],
                    ],

                    'brandManagementTeam' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.1', // this has to be at the beginning of the route array because of
                            'description' => "The brand's managemnt team", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$managemntTeam[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'managementTeam',
                            ],
                        ],
                    ],

                    'brandAboutAdd' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.2', // this has to be at the beginning of the route array because of
                            'description' => "add about us", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$aboutAdd[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'addAbout',
                            ],
                        ],
                    ],

                    'brandUpdateAboutIndex' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.3', // this has to be at the beginning of the route array because of
                            'description' => "update index", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$aboutUpdateOrder[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'updateIndexAbout',
                            ],
                        ],
                    ],

                    'brandDeleteAbout' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.4',
                            'description' => "Deletes given about us id",
                        ],
                        'options' => [
                            'route'    => '$brandAboutDelete/:aboutId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'deleteAbout',
                            ],
                        ],
                    ],

                    'brandGetAboutPopup' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.5',
                            'description' => "get single about for popup",
                        ],
                        'options' => [
                            'route'    => '$brandAboutPopup/:aboutId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'getPopupAbout',
                            ],
                        ],
                    ],

                    'brandEditAbout' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.6',
                            'description' => "edit about us data",
                        ],
                        'options' => [
                            'route'    => '$brandAboutEdit/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'editAbout',
                            ],
                        ],
                    ],*/

                    // Management Teams

                    'brandUpdateTeamName' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.7',
                            'description' => "Update management team title name",
                        ],
                        'options' => [
                            'route'    => '$brandUpdateTeamName/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'updateTeamName',
                            ],
                        ],
                    ],

                    /*'brandManageTeamMeta' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.8', // this has to be at the beginning of the route array because of
                            'description' => "Change brand's team's title and decription", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$teamMeta[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'manageBradsTeamMeta',
                            ],
                        ],
                    ],*/

                    /*'brandAddTeamMember' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.9', // this has to be at the beginning of the route array because of
                            'description' => "Add brand's team member", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$addTeamMember[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'addTeamMember',
                            ],
                        ],
                    ],*/

                    /*'brandTeamMemberAddImage' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.10',
                            'description' => "Allows upload of new image for a team member being created",
                        ],
                        'options' => [
                            'route'    => '$brandTeamMemberAddImage/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'addTeamMemberImage',
                            ],
                        ],
                    ],*/

                    /*'brandShowTeamMember' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.11',
                            'description' => "Show team member details",
                        ],
                        'options' => [
                            'route'    => '$teamMemberDetail/:tmId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'showTeamMemberDetail',
                            ],
                        ],
                    ],*/

                    /*'brandDeleteTeamMember' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.12',
                            'description' => "Delete team member details",
                        ],
                        'options' => [
                            'route'    => '$deleteTeamMember/:tmId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'deleteTeamMember',
                            ],
                        ],
                    ],*/

                    /*'brandEditTeamMember' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '11.13', // this has to be at the beginning of the route array because of
                            'description' => "Edit brand's team member", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$editTeamMember/:tmId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\BrandAbout',
                                'action' => 'editTeamMember',
                            ],
                        ],
                    ],*/



                    // ABOUT ME CONTROLLER
                    /*'aboutMe' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '12', // this has to be at the beginning of the route array because of
                            'description' => "Get About me data of a user", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$aboutMe[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\AboutMe',
                                'action' => 'index',
                            ],
                        ],
                    ],

                    'aboutMeAddImage' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '12.1',
                            'description' => "Allows upload of new image for a about me",
                        ],
                        'options' => [
                            'route'    => '$aboutMeAddImage/:brandUrl',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\AboutMe',
                                'action' => 'addAboutMeImage',
                            ],
                        ],
                    ],

                    'aboutMeAdd' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '12.2', // this has to be at the beginning of the route array because of
                            'description' => "add about me data", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$aboutMeAdd[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\AboutMe',
                                'action' => 'addAboutMe',
                            ],
                        ],
                    ],

                    'aboutMeEdit' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '12.3', // this has to be at the beginning of the route array because of
                            'description' => "edit about me data", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$aboutMeEdit[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\AboutMe',
                                'action' => 'editAboutMe',
                            ],
                        ],
                    ],*/

                    //END

                    // PersonalPageController
                    'perosonlPageHome' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '13', // this has to be at the beginning of the route array because of
                            'description' => "perosonl Page home page", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$personal-page[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\PersonalPage',
                                'action' => 'home',
                                'categoryUrl' => 'home',
                            ],
                        ],
                    ],

                    'perosonlPagePhoto' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '13.1', // this has to be at the beginning of the route array because of
                            'description' => "perosonl Page photo", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$personal-page[/[photo[/[:brandUrl]]]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\PersonalPage',
                                'action' => 'photos',
                            ],
                        ],
                    ],

                    'perosonlPageStory' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '13.2', // this has to be at the beginning of the route array because of
                            'description' => "perosonl Page stories", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$personal-page[/[story[/[:brandUrl]]]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\PersonalPage',
                                'action' => 'story',
                                'categoryUrl' => 'home',
                            ],
                        ],
                    ],

                    'perosonlPagePortfolio' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '13.3', // this has to be at the beginning of the route array because of
                            'description' => "Personel page portfolio", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$personal-page[/[portfolio[/[:brandUrl]]]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\PersonalPage',
                                'action' => 'portfolio',
                            ],
                        ],
                    ],

                    'perosonlPageVideo' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '13.4', // this has to be at the beginning of the route array because of
                            'description' => "Personel page video section", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$personal-page[/[videos[/[:brandUrl]]]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\PersonalPage',
                                'action' => 'videos',
                            ],
                        ],
                    ],

                    'perosonlPageAlbum' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '13.5', // this has to be at the beginning of the route array because of
                            'description' => "Personel page album section", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$personal-page[/[albums[/[:brandUrl[/[:albumId]]]]]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\PersonalPage',
                                'action' => 'albums',
                            ],
                        ],
                    ],

                    'perosonlPagePhotoAdd' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '13.5', // this has to be at the beginning of the route array because of
                            'description' => "Personel page photo add", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$addPersonalPagePhoto[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\PersonalPage',
                                'action' => 'addPersonalPagePhotoImage',
                            ],
                        ],
                    ],

                    'perosonlPagePhotoPostAdd' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '13.5', // this has to be at the beginning of the route array because of
                            'description' => "Personel page photo add", // the order in which entries are checked!
                        ],
                        'options' => [
                            'route'    => '$photoAdd[/[:brandUrl]]',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\PersonalPage',
                                'action' => 'addPersonalPagePhotoPost',
                            ],
                        ],
                    ],

                    'personalPageGetSingleAlbumData' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '13.6',
                            'description' => "get single album for edit data from personal page",
                        ],
                        'options' => [
                            'route'    => '$personalPageAlbumGet/:albumId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\PersonalPage',
                                'action' => 'getSingleAlbum',
                            ],
                        ],
                    ],

                    'personalPageGetSinglePost' => [
                        'type'    => 'Segment',
                        'metadata' => [
                            'id' => '13.7',
                            'description' => "get single post for edit data from personal page",
                        ],
                        'options' => [
                            'route'    => '$personalPageSinglePost/:postId',
                            'defaults' => [
                                'controller' => 'LabBase\Controller\PersonalPage',
                                'action' => 'getSinglePost',
                            ],
                        ],
                    ],
                ],
            ],





            //END



        ], // routes
    ], // router

];
