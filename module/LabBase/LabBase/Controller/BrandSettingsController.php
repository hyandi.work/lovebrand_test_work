<?php
/**
 * Author: Sebi
 * Date: 2015-06-14
 * Time: 18:53
 */

namespace LabBase\Controller;


use LabBase\Model\Brand;
use LabBase\Model\BrandTable;
use LabBase\Model\Category;
use LabBase\Model\CategoryTable;
use LabBase\Model\ImageSaver;
use LabBase\Model\Slim;
use LabBase\Model\UserTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class BrandSettingsController extends AbstractActionController {

    /** @var UserTable $userTable */
    private $userTable;
    /** @var BrandTable $brandTable */
    private $brandTable;

    /** @var int Maximum avatar size in pixels */
    const BRAND_BANNER_MAX_WIDTH = 1920;
    const BRAND_AVATAR_MAX_WIDTH = 256;

    /** @var Brand $brand */
    private $brand = null;
    private $uploaded = false;

    public function indexAction() {
        return [];
    }

    /**
     * Post parameter 'imageString' with base64 of the image or 'image' file required
     * @param $imagePrefix string Image will be saved with id of $prefix-$brandId
     * @param $maxWidth int
     * @param $maxHeight int
     * @return JsonModel|bool or redirect (depends on whether 'image' file is present or 'imageString' post value is present)
     */
    private function uploadImage($imagePrefix, $maxWidth, $maxHeight) {
        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        $brandUrl = $this->params('url', false);

        if(!$req->isPost()) {
            return new JsonModel(['status' => 'Only POST is supported']);
        }

        $isAsyncRequest = false;

        // Try getting imageString from POST imageString
        $imageString = $req->getPost('imageString', '');
        if(!empty($imageString)) {
            $isAsyncRequest = true;
            $imageString = base64_decode($imageString);
        }

        // Try getting imageString from FILES image
        if(empty($imageString)) {
            $imageString = $req->getFiles('image');
        }

        // Try getting imageString from Slim
        if(empty($imageString)) {
            $images = Slim::getImages();
            if(empty(!$images)) {
                $img = $images[0]['output'];
                $imageString = $img['data'];
            }
        }

        if(empty($imageString) || empty($brandUrl)) {
            return new JsonModel(['status' => 'Invalid image or brand URL.']);
        }

        $sl = $this->getServiceLocator();
        $this->userTable = $sl->get('UserTable');
        $this->brandTable = $sl->get('BrandTable');

        $brand = $this->brandTable->getBrandByUrl($brandUrl);
        if(!$brand) {
            return new JsonModel(['status' => 'An error has occurred. Brand URL points to nonexistent brand.']);
        }
        $this->brand = $brand;

        $user = $this->userTable->getLoggedInUser();
        if(!$user) {
            return new JsonModel(['status' => 'An error has occurred. You are not logged in.']);
        }

        if(!$this->brandTable->isBrandManagedByUser($brand->id, $user->id)) {
            return new JsonModel(['status' => 'An error has occurred. You do not have permission to edit this brand.']);
        }

        /** @var $imageSaver ImageSaver */
        $imageSaver = $sl->get('ImageSaver');

        // todo: either check the return value or wrap in proper try/catch
        $imageSaver->uploadImageSimple($imageString, $imagePrefix, $brand->id, $user->id, $brand->id, null, $maxWidth, $maxHeight, false, true);
        //$imageSaver->uploadImage($imageString, $imagePrefix.'-'.$brand->id, $maxWidth, $maxHeight, $user->id, $brand->id);

        $this->uploaded = true;

        //error_log(var_export($img, true));

        if($isAsyncRequest) {
            return new JsonModel(['status' => 'ok']);
        }else{
            $this->redirect()->toRoute("brand", ['url' => $brandUrl]);
            return FALSE;
        }
    }

    public function uploadBannerAction() {
        $ret = $this->uploadImage('brand-banner', BrandSettingsController::BRAND_BANNER_MAX_WIDTH, BrandSettingsController::BRAND_BANNER_MAX_WIDTH);

        if($this->uploaded && !$this->brand->hasBanner) {
            $this->brand->hasBanner = true;
            $this->brandTable->saveBrand($this->brand);
        }

        return $ret;
    }

    public function uploadAvatarAction() {
        return $this->uploadImage('brand-avatar', BrandSettingsController::BRAND_AVATAR_MAX_WIDTH, BrandSettingsController::BRAND_AVATAR_MAX_WIDTH);
    }


    /**
     * @param $categories Category[]
     * @return array
     */
    public static function categoryArrayToJson($categories) {
        $ret = [];
        foreach($categories as $cat) {
            $ret[] = [
                'id' => $cat->id,
                'label' => $cat->categoryName,
                'url' => $cat->url,
                'weight' => $cat->position,
                'children' => BrandSettingsController::categoryArrayToJson($cat->children)
            ];
        }
        return $ret;
    }

    
    public function editCategoriesAction() {
        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        /** @var CategoryTable $categoryTable */
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\CategoryTable');
        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        $user = $userTable->getLoggedInUser();

        $brand = $brandTable->getBrandByUrl($this->params()->fromRoute('brandUrl'));
        if(!$user || !$brand || !$brandTable->isBrandManagedByUser($brand->id, $user->id)) {
            return $this->notFoundAction();
        }
        $rawCats = $categoryTable->getCategoriesByBrand($brand->id);

        return [
            'categoryList' => BrandSettingsController::categoryArrayToJson($rawCats),
            'thisBrand' => $brand
        ];
    }


    public function getCategoriesAction() {

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        /** @var CategoryTable $categoryTable */
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\CategoryTable');
        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        $user = $userTable->getLoggedInUser();

        $brand = $brandTable->getBrandByUrl($this->params()->fromRoute('brandUrl'));

        if(!$user || !$brand || !$brandTable->isBrandManagedByUser($brand->id, $user->id)) {
            return $this->notFoundAction();
        }
        $rawCats = $categoryTable->getCategoriesByBrand($brand->id);

        return new JsonModel([
            'categoryList' => BrandSettingsController::categoryArrayToJson($rawCats),
            'status' => 'ok'
        ]);
    }

    public function saveCategoriesAction() {
        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        if(!$req->isPost() || !$req->getPost('categories')) {
            return new JsonModel(['status' => 'Expecting POST. Please try again or contact the site administrator']);
        }

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        /** @var CategoryTable $categoryTable */
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\CategoryTable');
        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        $user = $userTable->getLoggedInUser();

        $brand = $brandTable->getBrandByUrl($this->params()->fromRoute('brandUrl'));
        if(!$user || !$brand || !$brandTable->isBrandManagedByUser($brand->id, $user->id)) {
            return new JsonModel(['status' => 'Something went wrong. Please try again or contact the site administrator']);
        }

        $oldCategories = $categoryTable->getCategoriesByBrand($brand->id);
        $postCategories = $req->getPost('categories');

        if(count($postCategories) < 1) {
            return new JsonModel(['status' => 'You cannot delete all categories.']);
        }

        // flatten the arrays
        foreach($oldCategories as $oldCatFlat) {
            foreach($oldCatFlat->children as $child) {
                $oldCategories[] = $child;
            }
            $oldCatFlat->children = [];
        }
        foreach($postCategories as &$postCatFlat) {
            if(isset($postCatFlat['children'])) {
                foreach ($postCatFlat['children'] as $child) {
                    $child['parentId'] = $postCatFlat['id'];
                    $postCategories[] = $child;
                }
            }
            $postCatFlat['children'] = [];
        }

        foreach($postCategories as $postCreateNew) {
            $found = false;
            foreach($oldCategories as $oldCatCreateNew) {
                if($postCreateNew['id'] == $oldCatCreateNew->id) {
                    $catObj = $categoryTable->getCategoryById($postCreateNew['id']);
                    if($catObj) {
                        $found = true;
                    }
                    break;
                }
            }
            if(!$found) {
                $newCat = new Category($postCreateNew['label'],
                    $categoryTable->generateCategoryUrl($postCreateNew['label'], $brand->id), $brand->id, $postCreateNew['weight'],
                    true, isset($postCreateNew['parentId']) ? $postCreateNew['parentId'] : null);

                $id = $categoryTable->saveCategory($newCat);
                foreach ($postCategories as &$postCreateNewLink) {
                    if(isset($postCreateNewLink['parentId']) && $postCreateNewLink['parentId'] == $postCreateNew['id']) {
                        $postCreateNewLink['parentId'] = $id;
                    }
                }
            }
        }
        foreach($postCategories as $postUpdate) {
            foreach($oldCategories as $oldCatUpdate) {
                if($postUpdate['id'] == $oldCatUpdate->id) {
                    $catObj = $categoryTable->getCategoryById($postUpdate['id']);
                    if($catObj) {
                        if($catObj->brandId != $brand->id) {
                            break;
                        }
                        if( $catObj->position != (int) $postUpdate['weight'] ||
                            $catObj->categoryName != htmlspecialchars($postUpdate['label']) ||
                            (isset($postUpdate['parentId']) && (int)$catObj->parentId != (int)$postUpdate['parentId']) ||
                            (!isset($postUpdate['parentId']) && (int)$catObj->parentId != 0))
                        {
                            $catObj->position = (int)$postUpdate['weight'];
                            $catObj->categoryName = htmlspecialchars($postUpdate['label']);
                            if(isset($postUpdate['parentId']) && $postUpdate['parentId'] > 0) {
                                $catObj->parentId = (int)$postUpdate['parentId'];
                            }else{
                                $catObj->parentId = null;
                            }
                            $categoryTable->saveCategory($catObj);
                        }
                    }
                    break;
                }
            }
        }


        foreach($oldCategories as $postCatDelete) {
            $found = false;
            foreach($postCategories as $post) {
                if($post['id'] == $postCatDelete->id) {
                    $found = true;
                    break;
                }
            }
            if(!$found) {
                $categoryTable->deleteCategory($postCatDelete->id);
            }
        }


        return new JsonModel(['status' => 'ok']);
    }

}
