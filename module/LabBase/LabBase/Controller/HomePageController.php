<?php
/**
 * Author: Sebi
 * Date: 2014-10-27
 * Time: 21:42
 */

namespace LabBase\Controller;

use LabBase\Model\Brand;
use LabBase\Model\BrandRelationTable;
use LabBase\Model\BrandTable;
use LabBase\Model\Category;
use LabBase\Model\CategoryTable;
use LabBase\Model\Notification;
use LabBase\Model\NotificationTable;
use LabBase\Model\User;
use LabBase\Model\UserTable;
use Zend\Http\Header\SetCookie;
use Zend\Http\Request;
use Zend\Http\Response;

use Hybrid_Auth;
use ScnSocialAuth\Mapper\Exception as MapperException;
use ScnSocialAuth\Mapper\UserProviderInterface;
use ScnSocialAuth\Options\ModuleOptions;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class HomePageController extends AbstractActionController {

    /**
     * @var UserProviderInterface
     */
    protected $mapper;

    /**
     * @var Hybrid_Auth
     */
    protected $hybridAuth;

    /**
     * @var ModuleOptions
     */
    protected $options;

    /*
     * @todo Make this dynamic / translation-friendly
     * @var string
     */
    protected $failedAddProviderMessage = 'Add provider failed. Please try again.';

    /**
     * @var callable $redirectCallback
     */
    protected $redirectCallback;
    
    public function indexAction() {
        $this->layout("lab-base/bare-layout");
        return [];
    }

    public function registerAction() {
        // todo: also handle registration from the "delegation" e-mail

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Register');

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $usernameError = FALSE;
        $emailError = FALSE;
        $passwordError = FALSE;
        $brandnameError = FALSE;

        $name = "";
        $email = "";
        $brandNameGET = htmlspecialchars($req->getQuery("BrandName", ""));
        $confirmBrand = false;
        $createAnotherBrand = false;

        $routePage = $this->params("page");

        $sl = $this->getServiceLocator();
        /** @var UserTable $userTable */
        $userTable = $sl->get('UserTable');
        $loggedInUser =  $userTable->getLoggedInUser();

        if($loggedInUser) {
            $confirmBrand = true;
            $createAnotherBrand = true;
        }


        $redirect = false;
        if ($this->getServiceLocator()->get('zfcuser_module_options')->getUseRedirectParameterIfPresent() && $this->getRequest()->getQuery()->get('redirect')) {
            $redirect = $this->getRequest()->getQuery()->get('redirect');
        }
        
        do { // one-off loop (for breaks)
            if($req->isPost()) {
                $name  = $req->getPost('username', '');
                $email = $req->getPost('email', '');
                $pass  = $req->getPost('password', '');
                $brand = $req->getPost('brandName', '');
                $brandType = $req->getPost('brandType');
                $brandRegistration = $req->getPost('brandRegistration');

                // Handle brand registration form
                if(!empty($brandRegistration)) {
                    // Sanity checks to make sure only brand name was sent
                    if(!empty($name) || !empty($email) || !empty($pass)) {
                        // This should never happen unless somebody is purposely messing with the registration
                        throw new \Exception("Please make sure that you are using up to date browser, such as Google Chrome and "
                            ."start the registration again. If you still have trouble registering, please contact us.");
                    }
                    // Set the flag saying that we are already at the brand confirmation step
                    $confirmBrand = true;

                    // Make sure user is logged in (the previous step logs them in)

                    if(!$loggedInUser) {
                        // This should never happen if the user follows normal registration steps
                        $brandnameError = 'Please make sure that you can see your name in the top right corner. If you'
                            . ' cannot see your name, please log in and try again. If you have trouble registering, please contact us.';
                        break;
                    }

                    if(empty($brand))
                        { $brandnameError = "Please enter the brand name."; break; }

                    if(strlen($brand) > 100)
                        { $brandnameError = "Brand name cannot exceed 100 characters."; break; }

                    $brandUrl = $brand;
                    $brand = htmlspecialchars($brand);

                    /** @var BrandTable $brandTable */
                    $brandTable = $this->getServiceLocator()->get('BrandTable');

                    // Check to make sure that they are not trying to re-register the brand which they have already created
                    $oldBrand = $brandTable->getBrandByExactName($brand);
                    if($oldBrand) {
                        if ($oldBrand->ownerId == $loggedInUser->id) {
                            $brandnameError = "You have already registered brand called \"" . $oldBrand->name . "\".";
                            break;
                        } else {
                            $brandnameError = "There is already a brand called \"" . $oldBrand->name . "\". For help please contact our support.";
                            break;
                        }
                    }

                    // Leave only letters and digits in the brand name, replace everything else with a dash
                    $brandUrl = preg_replace('/[^a-z0-9\-]/i', '-', $brandUrl);
                    // Remove multiple dashes
                    $brandUrl = preg_replace('/-+/', '-', $brandUrl);
                    // Remove dashes from the beginning and the end
                    $brandUrl = preg_replace('/^-+/', '', $brandUrl);
                    $brandUrl = preg_replace('/-+$/', '', $brandUrl);

                    // In case the brand name consists only of non-alpha characters:
                    if(empty(str_replace('-', '', $brandUrl))) {
                        $brandUrl = 'brand';
                        $isRepeat = TRUE;
                    }else {
                        $isRepeat = $brandTable->getBrandByUrl($brandUrl) != null;
                    }

                    // Check to make sure that they are not trying to register an existing brand. Fuzzy check
                    if($brandUrl != 'brand' && strlen($brandUrl) >= 3) {
                        $oldBrand = $brandTable->getBrandByUrl($brand);
                        if ($oldBrand) {
                            if ($oldBrand->ownerId == $loggedInUser->id) {
                                $brandnameError = "You have already registered brand called \"" . $oldBrand->name . "\".";
                                break;
                            } else {
                                $brandnameError = "There is already a brand called \"" . $oldBrand->name . "\". For help please contact our support.";
                                break;
                            }
                        }
                    }

                    $tries = 0;
                    while($isRepeat && $tries < 40) {
                        // Start at 100..999, every 5 fails, add one zero
                        $min = pow(10, 2 + (int)$tries/5);
                        $max = $min * 10 - 1;
                        $suffix = rand($min, $max);
                        $tries++;
                        $isRepeat = $brandTable->getBrandByUrl($brandUrl . '-' . $suffix) != null;
                        if(!$isRepeat) {
                            $brandUrl = $brandUrl . '-' . $suffix;
                            break;
                        }
                    }
                    if($isRepeat) {
                        // This should never happen
                        $brandnameError = "Please try again, if the error persists, please contact us.";
                        break;
                    }

                    // Store new brand in the DB
                    $brandObj = new Brand();
                    $brandObj->name = $brand;
                    $brandObj->strippedName = strtolower(preg_replace('/[^a-zA-Z0-9]*/', '', $brand));
                    $brandObj->ownerId = $loggedInUser->id;
                    $brandObj->url = $brandUrl;
                    $brandObj->public = false;
                    $brandObj->description = '';
                    $brandObj->brandType = ($brandType == 'pro') ? 1 : 0;
                    $brandTable->saveBrand($brandObj);



                    /** @var CategoryTable $categoryTable */
                    $categoryTable = $this->getServiceLocator()->get('CategoryTable');
                    if($brandType == 'brand') {
                        // Default category structure
                        $aboutUs = $categoryTable->saveCategory(new Category('About Us', 'about-us', $brandObj->id, 100000, false, null, true));

                        //    $categoryTable->saveCategory(new Category('Our Vision', 'our-vision', $brandObj->id, 110000, false, $aboutUs, true));
                        //    $categoryTable->saveCategory(new Category('Our Heritage', 'our-heritage', $brandObj->id, 120000, false, $aboutUs));
                        //    $categoryTable->saveCategory(new Category('Our management', 'our-management', $brandObj->id, 130000, false, $aboutUs));
                        //    $categoryTable->saveCategory(new Category('Contact Us', 'contact-us', $brandObj->id, 140000, false, $aboutUs, true, true));

                        $categoryTable->saveCategory(new Category('Our Stories', 'our-stories', $brandObj->id, 200000, true));
                        $categoryTable->saveCategory(new Category('Latest News', 'latest-news', $brandObj->id, 300000, true, null, true));
                        $categoryTable->saveCategory(new Category('Innovation Journey', 'innovation-journey', $brandObj->id, 400000, true));
                    }elseif($brandType == 'pro') {
                        // Default category structure
                        $categoryTable->saveCategory(new Category('About me', 'about-me', $brandObj->id, 100000, false, null, true));
                        $categoryTable->saveCategory(new Category('Blog', 'blog', $brandObj->id, 200000, true));
                        $categoryTable->saveCategory(new Category('My Stories', 'my-stories', $brandObj->id, 200000, true));
                    } // for custom/invalid don't create any categories

                    $this->redirect()->toRoute("sign-up-brand-welcome", ['brandUrl' => $brandUrl]);

                    break;
                }

                // Handle user registration form
                if(empty($name)) { break; } // Don't make it an error - user will be notified by javascript anyway
                                            // now we know that the form wasn't filled at all, so we skip processing the post
                if(empty($email)) { $emailError = "Email is required."; break; }
                if(empty($pass)) { $passwordError = "Password is required."; break; }

                if(strlen($name) < 2 || strlen($name) > 120)
                    { $usernameError = "Name must be between 2 and 120 characters long."; break; }

                if(strlen($email) < 5 || strlen($email) > 120)
                    { $emailError = "Email must be between 5 and 120 characters long."; break; }

                if(preg_match("/^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$/i", $email) == 0)
                    { $emailError = "Given email is not valid. Please re-check the entered address."; break; }

                if(preg_match("/^\\s^/i", $email) == 1)
                    { $emailError = "Email address cannot contain spaces. Please re-check the entered address."; break; }

                if(strlen($pass) < 7 || strlen($pass) > 50)
                    { $passwordError = "Password must be between 7 and 50 characters long."; break; }

                $contains_lowercase = (preg_match("/[a-z]/", $pass) === 1) ? 1 : 0;
                $contains_uppercase = (preg_match("/[A-Z]/", $pass) === 1) ? 1 : 0;
                $contains_numbers   = (preg_match("/[0-9]/", $pass) === 1) ? 1 : 0;
                $contains_special   = (preg_match("/[^a-zA-Z0-9]/", $pass) === 1) ? 1 : 0;
                $contains_spaces    = preg_match('/\s/', $pass) === 1;

                if($contains_spaces) { $passwordError = "Password cannot contain spaces."; break; }
                if($contains_lowercase + $contains_numbers + $contains_uppercase + $contains_special < 2) {
                    $passwordError = "Password must be made up of at least two of the following: "
                        . "Uppercase letters, lowercase letters, numbers or special characters (?!.*)";
                    break;
                }


                $user = $userTable->getUserByEmail($email);
                $userRegistered = false;
                if($user) {
                    // User already exists
                    if($user->email == $email && $user->fullName == $name && $user->checkPassword($pass)) {
                        // If all the details match, allow user to continue registering brand
                        // This happens when the user refreshes the page when on the "register brand" stage
                        $userRegistered = true;
                    }else{
                        $emailError = 'This e-mail is already registered. If you have registered before you can <a href="'
                                        . $this->url()->fromRoute('forgot-password').'">reset your password</a>.';
                    }
                }else{
                    // Register new user
                    $user = new User();
                    $user->id = 0; // new user
                    $user->fullName = htmlspecialchars($name);
                    $user->email = $email;
                    $user->registerDate = time();
                    $user->userType = 1;
                    $user->changePassword($pass);
                    $userTable->saveUser($user);

                    $userRegistered = true;
                }

                if($userRegistered) {
                    // Log the user in
                    $userTable->overrideLoggedInUser($user);
                    $cookie = new SetCookie("sid", $user->sessionId . '.' . $user->sessionKey, time() + 3600 * 24 * 7, "/");
                    $cookie->setHttponly(true);

                    /** @var Response $resp */
                    $resp = $this->getResponse();
                    $resp->getHeaders()->addHeader($cookie);

                    // registration successful, go to "confirm brand" screen
                    $confirmBrand = true;
                }
            }

        } while(false);

        $errorList = "";

        if($usernameError)  { if($errorList) $errorList .= "<br />"; $errorList .= "* " . $usernameError; }
        if($emailError)     { if($errorList) $errorList .= "<br />"; $errorList .= "* " . $emailError; }
        if($passwordError)  { if($errorList) $errorList .= "<br />"; $errorList .= "* " . $passwordError; }

        if($brandnameError) { if($errorList) $errorList .= "<br />"; $errorList .= $brandnameError; }

        $vm = new ViewModel([
            'fixedEmail'    => null,
            'registrationErrors' => $errorList,
            'username'      => $name,
            'email'         => $email,
            'brandName'     => $brandNameGET,
            'confirmBrand'  => $confirmBrand,
            'createAnotherBrand' => $createAnotherBrand,
            'redirect' => $redirect,
            'options' => $this->getOptions(),
        ]);

        if($confirmBrand) {
            $vm->setTemplate('lab-base/home-page/register-brand');

        }elseif($routePage == 'email') {
            $vm->setTemplate('lab-base/home-page/register-email');

        }elseif($routePage == 'social') {
            $vm->setTemplate('lab-base/home-page/register-social');
        }

        return $vm;
    }

    public function registerWelcomeAction() {
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($this->params()->fromRoute('brandUrl'));

        if($thisBrand->brandType) {
            $this->redirect()->toRoute('user', ['url' => $this->params()->fromRoute('brandUrl')]);
            //$brandUrl = $this->url()->fromRoute('user', ['url' => $this->params()->fromRoute('brandUrl')]);
        }else{
            $this->redirect()->toRoute('brand', ['url' => $this->params()->fromRoute('brandUrl')]);
            //$brandUrl = $this->url()->fromRoute('brand', ['url' => $this->params()->fromRoute('brandUrl')]);
        }
        return [];

        return [ 'RedirectURL' => $brandUrl ];
    }

    public function signinAction() {

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Sign In');

        /** @var Request $req */
        $req = $this->getRequest();
        $invalidEmailOrPassword = FALSE;
        $errorMessage = FALSE;
        $email = null;

        if($req->isPost()) {
            $email = $req->getPost('email');
            $pass  = $req->getPost('password');

            /** @var UserTable $userTable */
            $userTable = $this->getServiceLocator()->get('UserTable');
            $user = $userTable->getUserByEmail($email);
            
            if(!$user) {
                $invalidEmailOrPassword = true;
                usleep(rand(40000,60000)); // prevent "blind" email checks
            }else{
                usleep(rand(0,10000));
                if($user->checkPassword($pass)) {
                    if($user->isSuspended()) {
                        $errorMessage = "Your account is suspended. If you believe this is an error, please contact us.";
                    }else{
                        $user->updateSession();
                        $userTable->saveUser($user);

                        $cookie = new SetCookie("sid", $user->sessionId . '.' . $user->sessionKey, time() + 3600 * 24 * 7, "/");
                        $cookie->setHttponly(true);
                        /** @var Response $resp */
                        $resp = $this->getResponse();
                        $resp->getHeaders()->addHeader($cookie);
                        // todo: redirect to right page
                        $brandTable = $this->getServiceLocator()->get('BrandTable');
                        $brands = $brandTable->getBrandsByOwner($user->id);
                        if(count($brands) > 0) {
                            if($brands[0]->brandType)
                            {
                                $this->redirect()->toRoute("not-implemented", ['brandUrl' => $brands[0]->url]);
                            }
                            else
                            {
                                $this->redirect()->toRoute("brand", ['url' => $brands[0]->url]);
                            }
                        }else{
                            $this->redirect()->toRoute("root");
                        }
                    }
                }else{
                    $invalidEmailOrPassword = true;
                }
            }

        }

        return [
            'InvalidEmailOrPassword' => $invalidEmailOrPassword,
            'ErrorMessage' => $errorMessage,
            'Email' => $email,
        ];
    }
    
    public function signinAjaxAction() {

        /** @var Request $req */
        $req = $this->getRequest();
        $invalidEmailOrPassword = FALSE;
        $errorMessage = FALSE;

        if($req->isPost()) {
          
            $email = $req->getPost('email');
            $pass  = $req->getPost('password');

            /** @var UserTable $userTable */
            $userTable = $this->getServiceLocator()->get('UserTable');
            $user = $userTable->getUserByEmail($email);
            if(!$user) {
                $invalidEmailOrPassword = true;
                usleep(rand(40000,60000)); // prevent "blind" email checks
            }else{
                usleep(rand(0,10000));
                if($user->checkPassword($pass)){
                    if($user->isSuspended()) {
                        $errorMessage = "Your account is suspended. If you believe this is an error, please contact us.";
                    }else{
                        $user->updateSession();
                        $userTable->saveUser($user);

                        $cookie = new SetCookie("sid", $user->sessionId . '.' . $user->sessionKey, time() + 3600 * 24 * 7, "/");
                        $cookie->setHttponly(true);
                        /** @var Response $resp */
                        $resp = $this->getResponse();
                        $resp->getHeaders()->addHeader($cookie);
                        // todo: redirect to right page
                        $brandTable = $this->getServiceLocator()->get('BrandTable');
                        $brands = $brandTable->getBrandsByOwner($user->id);
                        if(count($brands) > 0){
                            return new JsonModel([
                                        "status" => "ok",
                                        'user' => $user,
                                        'isBrand' => true,
                                        'url' => $this->url("brand", ['url' => $brands[0]->url]),
                                        ]);
                        }else{
                            return new JsonModel([
                                        "status" => "ok",
                                        'isBrand' => false,
                                        'user' => $user,
                                        'url' => $this->url("homepage"),
                                        ]);
                        }
                    }
                }else{
                    $invalidEmailOrPassword = true;
                    
                }
            }
        }
           if($invalidEmailOrPassword){
               $errorMessage = 'Invalid username or password';
           }
        return new JsonModel([
                    "status" => "error",
                    'invalidEmailOrPassword' => $invalidEmailOrPassword,
                    'errorMessage' => $errorMessage,
                    'user' => $user
                    ]);
    }

    public function goToMyBrandAction() {
        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('UserTable');
        $user = $userTable->getLoggedInUser();
        if(!$user) {
            $this->redirect()->toRoute("sign-in");
            return [];
        }

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('BrandTable');

        if($user->lastVisitedBrandId > 0) {
            $brand = $brandTable->getBrandById($user->lastVisitedBrandId);
            if($brand) {
                $this->redirect()->toRoute("brand", ['url' => $brand->url]);
                return [];
            }
        }
        $brands = $brandTable->getBrandsByOwner($user->id);
        if(count($brands) > 0) {
            $this->redirect()->toRoute("brand", ['url' => $brands[0]->url]);
        }else{
            $this->redirect()->toRoute("user-settings");
        }

        return [];
    }

    // Handles the part where user types the e-mail and the e-mail is generated
    public function forgotPasswordAction() {
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Password Recovery');

        // todo: implement when we have sorted out e-mail

        return [];
    }

    // Handles the part after the user clicked in the password recovery link
    public function recoverPasswordAction() {
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Password Recovery');

        // todo: implement when we have sorted out e-mail

        return [];
    }

    // Handles the part after the user clicked in the password recovery link
    public function logoutAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $user = $userTable->getLoggedInUser();
        if(!$user)
            return ['userLoggedIn' => FALSE];

        $routeSessionId = $this->params()->fromRoute("sessionId");

        if(empty($routeSessionId) || $routeSessionId != $user->sessionId) {
            return [
                'userLoggedIn' => TRUE,
                'userSessionId' => $user->sessionId
            ];
        }

        // Unset the session cookie
        $cookie = new SetCookie("sid", '0.0', time() - 5, "/");
        $cookie->setHttponly(true);

        /** @var Response $resp */
        $resp = $this->getResponse();
        $resp->getHeaders()->addHeader($cookie);

        $this->redirect()->toRoute("sign-out");
        return [];
    }

    public function notImplementedAction() {
        return [];
    }

    public function deactivatedAction() {
        return [];
    }

    /** Old code for brand-based notifications */
    public function getNotificationsAction_OLD() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        $user = $userTable->getLoggedInUser();

        // don't error out since the user won't know what to do with it anyway, just return empty list
        if(!$user) return ['status' => 'ok', 'notifications' => []];

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('BrandTable');

        /** @var NotificationTable $notificationTable */
        $notificationTable = $this->getServiceLocator()->get('NotificationTable');


        $brands = $brandTable->getBrandsManagedByUser($user->id);
        $notifications = [];
        foreach($brands as $brand) {
            $notifications[] = $notificationTable->getNotificationsByBrand($brand->id);
        }
        if(count($notifications) > 0) {
            $notifications = call_user_func_array("array_merge", $notifications);
            // todo: smart notification count per brand
            // ie. max 5 for current brand, less for other brands if too many notifications in total
            usort($notifications, function(Notification $a, Notification $b) {
                return $b->date - $a->date;
            });
        }else{
            $notifications = [];
        }

        return new JsonModel([
            'status' => 'ok',
            'notifications' => $notifications
        ]);
    }

    public function getNotificationsAction() {
        $userTable = $this->getServiceLocator()->get('UserTable');
        /** @var BrandRelationTable $brandRelation */
        $brandRelation = $this->getServiceLocator()->get('BrandRelationTable');
        /** @var User $user */
        $user = $userTable->getLoggedInUser();
        $brandRelation->setFromTime($user->lastReadPostTimestamp);
        $brands = $brandRelation->getCountByUser($user->id);


        // don't error out since the user won't know what to do with it anyway, just return empty list
        if(!$user) {
            return new JsonModel(['status' => 'ok', 'notifications' => []]);
        }

        /** @var NotificationTable $notificationTable */
        $notificationTable = $this->getServiceLocator()->get('NotificationTable');

        $notifications = $notificationTable->getNotificationsByUser($user->id);

        // todo: remove after testing
        $test = new Notification($user->id, 'Test notification', '#');
        $test->date = 1471879900;
        $notifications[] = $test;
        $test = new Notification($user->id, 'Another test notification', '#');
        $test->date = 1471880400;
        $notifications[] = $test;

        return new JsonModel([
            'status' => 'ok',
            'notifications' => $notifications,
            'brandRelation' => $brands
        ]);
    }

     /**
     * set options
     *
     * @param  ModuleOptions  $options
     * @return UserController
     */
    public function setOptions(ModuleOptions $options)
    {
        $this->options = $options;

        return $this;
    }
    
    /**
     * get options
     *
     * @return ModuleOptions
     */
    public function getOptions()
    {
        if (!$this->options instanceof ModuleOptions) {
            $this->setOptions($this->getServiceLocator()->get('ScnSocialAuth-ModuleOptions'));
        }

        return $this->options;
    }
    
    public function checkUserLoginedAction()
    {
        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('UserTable');
        $loggedInUser =  $userTable->getLoggedInUser();
        if($loggedInUser){
            return new JsonModel([
                'status' => 'ok',
                'user' => $loggedInUser
            ]);
        }else{
            return new JsonModel([
                'status' => 'No logined'
            ]);
        }
    }
    
} 