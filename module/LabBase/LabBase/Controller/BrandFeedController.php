<?php
/**
 * Author: Sebi
 * Date: 2014-10-27
 * Time: 21:42
 */

namespace LabBase\Controller;

use LabBase\Model\Brand;
use LabBase\Model\BrandAboutTable;
use LabBase\Model\BrandPost;
use LabBase\Model\BrandRelation;
use LabBase\Model\Comment;
use LabBase\Model\BrandPostLoved;
use LabBase\Model\BrandPostTable;
use LabBase\Model\BrandPostLovedTable;
use LabBase\Model\BrandProductsCategoryTable;
use LabBase\Model\BrandProductsCollectionTable;
use LabBase\Model\BrandRelationTable;
use LabBase\Model\BrandServiceMetaTable;
use LabBase\Model\BrandTable;
use LabBase\Model\BrandService;
use LabBase\Model\BrandServiceMeta;
use LabBase\Model\BrandAbout;
use LabBase\Model\BrandTeamMember;
use LabBase\Model\BrandTeamMemberTable;
use LabBase\Model\BrandTeamMeta;
use LabBase\Model\BrandProductsCategory;
use LabBase\Model\BrandProductsCollection;
use LabBase\Model\BrandTeamMetaTable;
use LabBase\Model\Aboutme;
use LabBase\Model\AboutmeTable;
use LabBase\Model\PortfolioProject;
use LabBase\Model\PortfolioProjectTable;
use LabBase\Model\Category;
use LabBase\Model\CategoryTable;
use LabBase\Model\CommentTable;
use LabBase\Model\Image;
use LabBase\Model\ImageSaver;
use LabBase\Model\ImageTable;
use LabBase\View\ImageLinker;
use LabBase\Model\User;
use LabBase\Model\UserTable;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\View\View;

class BrandFeedController extends AbstractActionController {

    /*
     *  note: These helper members will be initialised in indexAction and
     *        available ONLY in getXYZContent() methods!
     */

    /** @var Brand $brand  */
    private $brand  = null;
    /** @var User $user */
    private $user   = null;
    /** @var ViewModel $view */
    private $view   = null;
    private $userIsBrandAdmin = false;

    /** @var BrandTable $brandTable */
    private $brandTable = null;
    /** @var UserTable $userTable */
    private $userTable  = null;
    /** @var ImageTable $imageTable */
    private $imageTable  = null;

    /** @var ServiceLocatorInterface $sl */
    private $sl      = null;
    /** @var Request $httpRequest */
    private $httpRequest = null;

    private $url     = null;
    private $param1  = null;
    private $param2  = null;
    private $param3  = null;
    private $param4  = null;
    private $segment = null;

    /*
     * End of helper members
     */
    /**
     * @param string $name Name of table
     * @return array|object
     */
    private function getTable($name)
    {
        return $this->getServiceLocator()->get($name.'Table');
    }


    public function manageCommentsAction() {
        $brandId = $this->params()->fromRoute("brandId");
        $commentsType = $this->params()->fromRoute("commentsType");
        return [
            'brandId' => $brandId,
            'commentsType' => $commentsType
        ];
    }

    public function getHomeContent() {
        $this->view->setTemplate('lab-base/brand-feed/home');

        $brandPostTable = $this->sl->get('BrandPostTable');
        $postList = $brandPostTable->getPostStoryByBrand($this->brand->id, 'all', $this->brand->isPersonalPage());

        $loggedInUser = (isset($this->user) && !empty($this->user)) ? $this->user : array();
        $brandPostLovedTable = $this->getServiceLocator()->get('BrandPostLovedTable');
        $commentTable = $this->sl->get('CommentTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        foreach($postList as $post)
        {
            /** @var BrandPost $post */
            $post->unescapePostContents();
            $post->dateFormatted = $this->time_elapsed_string($post->dateCreated);
            
            $loveCnt = $brandPostLovedTable->postLoveCount($post->id);
            $post->loveCnt = $loveCnt;
            if(isset($loggedInUser->id))
            {
                $loveOrNot = $brandPostLovedTable->getByUserBrandPost($loggedInUser->id, $this->brand->id, $post->id);
            }
            $post->loveOrNot = (isset($loveOrNot->loved)) ? $loveOrNot->loved : 0;
            
            $comments = $commentTable->getCommentsByPost($post->id, -1, -1, 1, 3);
            $post->commentsCnt = $commentTable->getTotalCommentsByPost($post->id, -1, -1, 1);
            
            $clientComments = [];
            foreach($comments as $comment) {
                $id = $comment->id;
                $date = $comment->dateCreated;
                $text = $comment->content;
                $displayName = $comment->posterDisplayName;
                $brandUrl = false;
                $seen     = $comment->seen === true;
                $isPublic = $comment->approved === true;
                $rejected = $comment->rejected === true;
                $userImage = $this->getImageObject('system-user-avatar-default');

                if($comment->posterBrandId) {
                    $postingBrand = $brandTable->getBrandById($comment->posterBrandId);
                    if(!$postingBrand) // Brand removed?
                        continue;
                    $displayName = $postingBrand->name;
                    $brandUrl =  $this->url()->fromRoute("root/brandFeed", ['brandUrl' => $postingBrand->url]);

                }elseif($comment->posterId) {
                    $poster = $userTable->getUserById($comment->posterId);
                    if(!$poster)
                        continue; // User removed
                    $displayName = $poster->fullName;

                    $imageAvatar = $this->getImageObject('user-avatar-'.$comment->posterId);

                    if(!empty($imageAvatar))
                    {
                        $userImage = $imageAvatar;
                    }
                    
                    $commentpost = $brandPostTable->getBrandPostById($comment->postId);
                    $postingBrand = $brandTable->getBrandById($commentpost->brandId);
                }

                    $userAvatar = '';
                    if(!empty($userImage))
                    {
                        $userAvatar = $this->getImageLink($userImage). '?' .$userImage->dateCreated;
                    }
                    else
                    {
                        $userInitails = ImageLinker::getAvatarInitials($postingBrand->name);
                    }

                $clientComments[] = [
                    'id' => $id,
                    'text' => $text,
                    'name' => $displayName,
                    'nameUrl' => $brandUrl,
                    'date' => $date,
                    'seen' => $seen,
                    'public' => $isPublic,
                    'rejected' => $rejected,
                    'userAvatar' => $userAvatar,
                    'userInitails' => $userInitails,
                    'postId' => $post->id, 
                ];
            }
            
            $post->comments = $clientComments;
        }

        /*  var BrandProductTable $brandProductTable */
        /*$brandProductTable = $this->sl->get('BrandProductTable');

        $products = $brandProductTable->getBrandProductsByBrand($this->brand->id, 15);

        foreach($products as $product) {
            $postList[] = BrandPost::fromProduct($product);
        }*/

        usort($postList, function ($a, $b) {
            return $b->dateCreated - $a->dateCreated;
        });


        return [
            'posts' => $postList
        ];
    }

    public function getStoriesContent() {

        $this->view->setTemplate('lab-base/brand-feed/stories');

        // todo: we don't need categories anymore?
/*        $categoryUrl = $this->param1;*/
        $action = $this->param1;
        $postId = $this->param2;

        /** @var BrandPostTable $brandPostTable */
        $brandPostTable = $this->sl->get('BrandPostTable');
        $categoryTable = $this->sl->get('CategoryTable');
        $commentTable = $this->sl->get('CommentTable');

        /*$categoryList = $categoryTable->getCategoriesByBrand($this->brand->id);
        usort($categoryList, function(Category $a, Category $b) {
            return $a->position - $b->position;
        });
        foreach($categoryList as $cat) {
            usort($cat->children, function (Category $a, Category $b) {
                return $a->position - $b->position;
            });
        }*/

        //$cat = $categoryTable->getCategoryByUrlAndBrand($categoryUrl, $this->brand->id);

        // Disallow selecting a category with children (only children can be selected)
        /*if(!empty($cat->children)) {
            $cat = reset($cat->children);
        }*/

        // Fix the url, if it has changed
        /*if($cat) {
            $categoryUrl = $cat->url;
        }*/

        // for now force disable categories
/*        $cat = null;*/
        // todo: remove it properly

        /*if($cat) {
            $postList = $brandPostTable->getBrandPostsByBrandAndCategory($this->brand->id, $cat->id);
        }else{*/
            if($this->userIsBrandAdmin)
            {
                $postList = $brandPostTable->getAllPostBrandOwnerByBrand($this->brand->id);
            }
            else
            {
                $postList = $brandPostTable->getPostStoryByBrand($this->brand->id);
            }
/*        }*/

        $editPost = false;
        if($action == 'edit' && $postId > 0)
        {
            $editPost = $brandPostTable->getBrandPostById($postId);
        }

        $loggedInUser = (isset($this->user) && !empty($this->user)) ? $this->user : array();
        $brandPostLovedTable = $this->getServiceLocator()->get('BrandPostLovedTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        foreach ($postList as $post) {

            /** @var BrandPost $post */
            /*echo "<pre>"; print_r($post); die;*/
            // caching done in getCategoryById
            
            $post->unescapePostContents();
            
            $loveCnt = $brandPostLovedTable->postLoveCount($post->id);
            $post->loveCnt = $loveCnt;
            if(isset($loggedInUser->id))
            {
                $loveOrNot = $brandPostLovedTable->getByUserBrandPost($loggedInUser->id, $this->brand->id, $post->id);
            }
            $post->loveOrNot = (isset($loveOrNot->loved)) ? $loveOrNot->loved : 0;
            
            $comments = $commentTable->getCommentsByPost($post->id, -1, -1, 1, 3);
            $post->commentsCnt = $commentTable->getTotalCommentsByPost($post->id, -1, -1, 1);
            
            $clientComments = [];
            foreach($comments as $comment) {
                $id = $comment->id;
                $date = $comment->dateCreated;
                $text = $comment->content;
                $displayName = $comment->posterDisplayName;
                $brandUrl = false;
                $seen     = $comment->seen === true;
                $isPublic = $comment->approved === true;
                $rejected = $comment->rejected === true;
                $userImage = $this->getImageObject('system-user-avatar-default');

                if($comment->posterBrandId) {
                    $postingBrand = $brandTable->getBrandById($comment->posterBrandId);
                    if(!$postingBrand) // Brand removed?
                        continue;
                    $displayName = $postingBrand->name;
                    $brandUrl =  $this->url()->fromRoute("root/brandFeed", ['brandUrl' => $postingBrand->url]);

                }elseif($comment->posterId) {
                    $poster = $userTable->getUserById($comment->posterId);
                    if(!$poster)
                        continue; // User removed
                    $displayName = $poster->fullName;

                    $imageAvatar = $this->getImageObject('user-avatar-'.$comment->posterId);

                    if(!empty($imageAvatar))
                    {
                        $userImage = $imageAvatar;
                    }
                    
                    $commentpost = $brandPostTable->getBrandPostById($comment->postId);
                    $postingBrand = $brandTable->getBrandById($commentpost->brandId);
                }

                    $userAvatar = '';
                    if(!empty($userImage))
                    {
                        $userAvatar = $this->getImageLink($userImage). '?' .$userImage->dateCreated;
                    }
                    else
                    {
                        $userInitails = ImageLinker::getAvatarInitials($postingBrand->name);
                    }

                $clientComments[] = [
                    'id' => $id,
                    'text' => $text,
                    'name' => $displayName,
                    'nameUrl' => $brandUrl,
                    'date' => $date,
                    'seen' => $seen,
                    'public' => $isPublic,
                    'rejected' => $rejected,
                    'userAvatar' => $userAvatar,
                    'userInitails' => $userInitails, 
                    'postId' => $post->id, 
                ];
            }
            
            $post->comments = $clientComments;
/*            echo "<pre>"; print_r($post); die;*/
        }
        
        return [
            'posts' => $postList,
            'action' => $action,
            'postId' => $postId,
            'editPost' => $editPost,
        ];
    }
    
    public function getDraftsContent() {

        $this->view->setTemplate('lab-base/brand-feed/drafts');

        /** @var BrandPostTable $brandPostTable */
        $brandPostTable = $this->sl->get('BrandPostTable');
        $categoryTable = $this->sl->get('CategoryTable');
        $commentTable = $this->sl->get('CommentTable');
        
        $postList = $brandPostTable->getBrandFeedDraftsByUserAndBrand($this->brand->id, $this->user->id);
        $loggedInUser = (isset($this->user) && !empty($this->user)) ? $this->user : array();
        
        foreach ($postList as $post) {
            /*echo "<pre>"; print_r($post); die;*/
            // caching done in getCategoryById
            /** @var BrandPost $post */
            
            $post->unescapePostContents();
        }

        return [
            'posts' => $postList
        ];
    }

    public function getGalleryContent() {

        $this->view->setTemplate('lab-base/brand-feed/gallery'); // name of template for segment (tab) contents

        $brandPostTable = $this->sl->get('BrandPostTable');
        
        $action = $this->param1;
        $singleId = $this->param2;
        
        $editPhotos = array();
        /** @var BrandPost|array $editPhotos */
        if($action == 'edit' && $singleId > 0)
        {
            $editPhotos = $brandPostTable->getBrandPostById($singleId);
            $editPhotos->unescapePostContents();
        }
        
        $photoPosts = $brandPostTable->getBrandFeedPostsByTypeByBrand('photo', $this->brand->id);

        $photoAlbumesCnt = $brandPostTable->getBrandFeedCntPhotoAndAlbums($this->brand->id);
        $postPhotoCnt = (isset($photoAlbumesCnt['photo_cnt']) && $photoAlbumesCnt['photo_cnt'] != '') ? $photoAlbumesCnt['photo_cnt'] : 0;
        $postAlbumCnt = (isset($photoAlbumesCnt['album_cnt']) && $photoAlbumesCnt['album_cnt'] != '') ? $photoAlbumesCnt['album_cnt'] : 0;
        
        $postVideoCnt = $brandPostTable->getBrandFeedCntVideos($this->brand->id);
        $postVideoCnt = (isset($postVideoCnt) && $postVideoCnt != "") ? $postVideoCnt : 0;
        
        $loggedInUser = (isset($this->user) && !empty($this->user)) ? $this->user : array();
        $brandPostLovedTable = $this->getServiceLocator()->get('BrandPostLovedTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $commentTable = $this->sl->get('CommentTable');
        
        foreach ($photoPosts as $post) {
            /*echo "<pre>"; print_r($post); die;*/
            // caching done in getCategoryById
            /** @var BrandPost $post */
            $post->unescapePostContents();
            $loveCnt = $brandPostLovedTable->postLoveCount($post->id);
            $post->loveCnt = $loveCnt;
            if(isset($loggedInUser->id))
            {
                $loveOrNot = $brandPostLovedTable->getByUserBrandPost($loggedInUser->id, $this->brand->id, $post->id);
            }
            $post->loveOrNot = (isset($loveOrNot->loved)) ? $loveOrNot->loved : 0;
            
            $comments = $commentTable->getCommentsByPost($post->id, -1, -1, 1, 3);
            $post->commentsCnt = $commentTable->getTotalCommentsByPost($post->id, -1, -1, 1);
            
            $clientComments = [];
            foreach($comments as $comment) {
                $id = $comment->id;
                $date = $comment->dateCreated;
                $text = $comment->content;
                $displayName = $comment->posterDisplayName;
                $brandUrl = false;
                $seen     = $comment->seen === true;
                $isPublic = $comment->approved === true;
                $rejected = $comment->rejected === true;
                $userImage = $this->getImageObject('system-user-avatar-default');

                if($comment->posterBrandId) {
                    $postingBrand = $brandTable->getBrandById($comment->posterBrandId);
                    if(!$postingBrand) // Brand removed?
                        continue;
                    $displayName = $postingBrand->name;
                    $brandUrl =  $this->url()->fromRoute("root/brandFeed", ['brandUrl' => $postingBrand->url]);

                }elseif($comment->posterId) {
                    $poster = $userTable->getUserById($comment->posterId);
                    if(!$poster)
                        continue; // User removed
                    $displayName = $poster->fullName;

                    $imageAvatar = $this->getImageObject('user-avatar-'.$comment->posterId);

                    if(!empty($imageAvatar))
                    {
                        $userImage = $imageAvatar;
                    }
                    
                    $commentpost = $brandPostTable->getBrandPostById($comment->postId);
                    $postingBrand = $brandTable->getBrandById($commentpost->brandId);
                }

                    $userAvatar = '';
                    if(!empty($userImage))
                    {
                        $userAvatar = $this->getImageLink($userImage). '?' .$userImage->dateCreated;
                    }
                    else
                    {
                        $userInitails = ImageLinker::getAvatarInitials($postingBrand->name);
                    }

                $clientComments[] = [
                    'id' => $id,
                    'text' => $text,
                    'name' => $displayName,
                    'nameUrl' => $brandUrl,
                    'date' => $date,
                    'seen' => $seen,
                    'public' => $isPublic,
                    'rejected' => $rejected,
                    'userAvatar' => $userAvatar,
                    'userInitails' => $userInitails, 
                    'postId' => $post->id, 
                ];
            }
            
            $post->comments = $clientComments;
        }
        
        return [
            'posts'         => $photoPosts,
            'total_photos'  => $postPhotoCnt,
            'total_albums'  => $postAlbumCnt,
            'total_videos'  => $postVideoCnt,
            
            'editPhoto'    => $editPhotos,
            'action'        => $action,
        ];
    }
    
    public function getAlbumContent() {

        $this->view->setTemplate('lab-base/brand-feed/album'); // name of template for segment (tab) contents

        $brandPostTable = $this->sl->get('BrandPostTable');
        
        $albumID = 0;
        $action = '';
        $editId = 0;
        if($this->param1 != 'edit')
        {
            $albumID = ($this->param1 > 0) ? $this->param1 : 0;
        }
        else
        {
            $action = $this->param1;
            $editId = $this->param2;
        }
        
        $albumPosts = $brandPostTable->getBrandFeedPostsAlbums($this->brand->id, $albumID);
        
        $editAlbum = [];
        /** @var BrandPost|array $editAlbum */
        if($action == 'edit' && $editId > 0)
        {
            $editAlbum = $brandPostTable->getBrandPostById($editId);
            if(!empty($editAlbum))
            {
                $editAlbum->unescapePostContents();
            }
        }

        $photoAlbumesCnt = $brandPostTable->getBrandFeedCntPhotoAndAlbums($this->brand->id);
        $postPhotoCnt = (isset($photoAlbumesCnt['photo_cnt']) && $photoAlbumesCnt['photo_cnt'] != '') ? $photoAlbumesCnt['photo_cnt'] : 0;
        $postAlbumCnt = (isset($photoAlbumesCnt['album_cnt']) && $photoAlbumesCnt['album_cnt'] != '') ? $photoAlbumesCnt['album_cnt'] : 0;
        
        $postVideoCnt = $brandPostTable->getBrandFeedCntVideos($this->brand->id);
        $postVideoCnt = (isset($postVideoCnt) && $postVideoCnt != "") ? $postVideoCnt : 0;
        
         $loggedInUser = (isset($this->user) && !empty($this->user)) ? $this->user : array();
        $brandPostLovedTable = $this->getServiceLocator()->get('BrandPostLovedTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $commentTable = $this->sl->get('CommentTable');
        
        foreach ($albumPosts as $post) {
            /*echo "<pre>"; print_r($post); die;*/
            // caching done in getCategoryById
            /** @var BrandPost $post */
            $post->unescapePostContents();
            $loveCnt = $brandPostLovedTable->postLoveCount($post->id);
            $post->loveCnt = $loveCnt;
            if(isset($loggedInUser->id))
            {
                $loveOrNot = $brandPostLovedTable->getByUserBrandPost($loggedInUser->id, $this->brand->id, $post->id);
            }
            $post->loveOrNot = (isset($loveOrNot->loved)) ? $loveOrNot->loved : 0;
            
            $comments = $commentTable->getCommentsByPost($post->id, -1, -1, 1, 3);
            $post->commentsCnt = $commentTable->getTotalCommentsByPost($post->id, -1, -1, 1);
            
            $clientComments = [];
            foreach($comments as $comment) {
                $id = $comment->id;
                $date = $comment->dateCreated;
                $text = $comment->content;
                $displayName = $comment->posterDisplayName;
                $brandUrl = false;
                $seen     = $comment->seen === true;
                $isPublic = $comment->approved === true;
                $rejected = $comment->rejected === true;
                $userImage = $this->getImageObject('system-user-avatar-default');

                if($comment->posterBrandId) {
                    $postingBrand = $brandTable->getBrandById($comment->posterBrandId);
                    if(!$postingBrand) // Brand removed?
                        continue;
                    $displayName = $postingBrand->name;
                    $brandUrl =  $this->url()->fromRoute("root/brandFeed", ['brandUrl' => $postingBrand->url]);

                }elseif($comment->posterId) {
                    $poster = $userTable->getUserById($comment->posterId);
                    if(!$poster)
                        continue; // User removed
                    $displayName = $poster->fullName;

                    $imageAvatar = $this->getImageObject('user-avatar-'.$comment->posterId);

                    if(!empty($imageAvatar))
                    {
                        $userImage = $imageAvatar;
                    }
                    
                    $commentpost = $brandPostTable->getBrandPostById($comment->postId);
                    $postingBrand = $brandTable->getBrandById($commentpost->brandId);
                }

                    $userAvatar = '';
                    if(!empty($userImage))
                    {
                        $userAvatar = $this->getImageLink($userImage). '?' .$userImage->dateCreated;
                    }
                    else
                    {
                        $userInitails = ImageLinker::getAvatarInitials($postingBrand->name);
                    }

                $clientComments[] = [
                    'id' => $id,
                    'text' => $text,
                    'name' => $displayName,
                    'nameUrl' => $brandUrl,
                    'date' => $date,
                    'seen' => $seen,
                    'public' => $isPublic,
                    'rejected' => $rejected,
                    'userAvatar' => $userAvatar,
                    'userInitails' => $userInitails, 
                    'postId' => $post->id, 
                ];
            }
            
            $post->comments = $clientComments;
        }
        
        return [
            'posts'         => $albumPosts,
            'total_photos'  => $postPhotoCnt,
            'total_albums'  => $postAlbumCnt,
            'total_videos'  => $postVideoCnt,
            'albumID'       => $albumID,
            'action'        => $action,
            'editAlbum'     => $editAlbum,
        ];
    }
    
    public function getVideoContent() {

        $this->view->setTemplate('lab-base/brand-feed/video'); // name of template for segment (tab) contents
        
        $action = (isset($this->param1) && $this->param1 != "") ? $this->param1 : '';
        $videoId = (isset($this->param2) && $this->param2 > 0) ? $this->param2 : 0;

        $brandPostTable = $this->sl->get('BrandPostTable');

        $photoAlbumesCnt = $brandPostTable->getBrandFeedCntPhotoAndAlbums($this->brand->id);
        $postPhotoCnt = (isset($photoAlbumesCnt['photo_cnt']) && $photoAlbumesCnt['photo_cnt'] != '') ? $photoAlbumesCnt['photo_cnt'] : 0;
        $postAlbumCnt = (isset($photoAlbumesCnt['album_cnt']) && $photoAlbumesCnt['album_cnt'] != '') ? $photoAlbumesCnt['album_cnt'] : 0;
        
        $postVideoCnt = $brandPostTable->getBrandFeedCntVideos($this->brand->id);
        $postVideoCnt = (isset($postVideoCnt) && $postVideoCnt != "") ? $postVideoCnt : 0;
        
        $videoPosts = $brandPostTable->getBrandFeedPostsByTypeByBrand('video', $this->brand->id);
        
        foreach ($videoPosts as $post) {
            /** @var BrandPost $post */
            $post->unescapePostContents();
            if($post->videoData != "")
            {
                $videoTags = $this->convertVideoURLToTags($post->videoData);
                if($videoTags != false)
                {
                    $post->videoDataTag = $videoTags;
                }
            }
        }
        
        return [
            'posts'         => $videoPosts,
            'total_photos'  => $postPhotoCnt,
            'total_albums'  => $postAlbumCnt,
            'total_videos'  => $postVideoCnt,
            
            'action'        => $action,
            'videoId'       => $videoId,
        ];
    }

    public function indexAction() {
        $this->url = $this->params()->fromRoute("url");
        $this->param1 = $this->params()->fromRoute("param1");
        $this->param2 = $this->params()->fromRoute("param2");
        $this->param3 = $this->params()->fromRoute("param3");
        $this->param4 = $this->params()->fromRoute("param4");

        $postIdOrSegment = $this->params()->fromRoute("segmentOrId");

        $outputJson = $this->params()->fromRoute("format") == 'json';
        $postId = (int)$postIdOrSegment;

        $segment = 'post';

        if(!($postId > 0)) {
            $postId = null;
            $segment = (string)$postIdOrSegment;
        }else{
            $postIdOrSegment = $postId;
        }
        $this->segment = $segment;

        $this->httpRequest = $this->getRequest();
        $this->sl = $this->getServiceLocator();

        $brandPostTable = $this->sl->get('BrandPostTable');
        $brandPostLovedTable = $this->sl->get('BrandPostLovedTable');

        $this->brandTable = $this->sl->get('BrandTable');
        $this->brand = $this->brandTable->getBrandByUrl($this->url);

        // Supplied brand URL doesn't exist?
        if($this->brand == null) {
            return $this->notFoundAction();
        }

        $this->sl->get('ViewHelperManager')->get('HeadTitle')->set($this->brand->name);

        $this->userTable = $this->sl->get('UserTable');
        $this->imageTable = $this->sl->get('ImageTable');
        $brTable = $this->sl->get('BrandRelationTable');
        $this->user = $this->userTable->getLoggedInUser();

        $this->view = new ViewModel();
        // Shared properties set.

        $isBrandOwner = false;
        // Logged in user is this brand's manager?
        if($this->user != null) {
            // todo: delegation
            if ($this->brand->ownerId == $this->user->id) {
                $isBrandOwner = true;

                // Update last visited brand
                if($this->user->lastVisitedBrandId != $this->brand->id) {
                    $this->user->lastVisitedBrandId = $this->brand->id;
                    $this->userTable->saveUser($this->user);
                }
            }
        }

        $this->userIsBrandAdmin = $isBrandOwner;

        if(!$this->brand->public && !$isBrandOwner) {
            // todo: Display 'not public' message here
        }

        $brandLovedByUser = false;
        if($this->user) {
            /** @var BrandRelationTable $brTable */
            $brandLovedByUser = (bool)$brTable->getByUserBrandType($this->user->id, $this->brand->id, 1);
        }

        $brandInfo = $this->brand->getClientArray();

        $renderInnerContentsOnly = $this->httpRequest->isXmlHttpRequest();

        $bannerUrl = false;
        if($this->brand->hasBanner) {
            /** @var Image $bannerImage */
            $bannerImage = $this->imageTable->getImageByUrl(ImageSaver::GetImageS3Path("brand-banner-" . $this->brand->id));

            if($bannerImage) {
                $bannerUrl = $bannerImage->getURL() . '?' . $bannerImage->dateCreated;
            }
        }

        $storiesCount = $brandPostTable->getBrandStoriesCountByBrand($this->brand->id);
        $lovedStoriesCount = $brandPostLovedTable->getCountLovedPostByBrand($this->brand->id);
        $brabdLovedCount = $brTable->getBrandLovedCountByBrand($this->brand->id);
        
        $draftsCount = 0;
        if(isset($this->user->id) && $this->user->id > 0)
        {
            $draftsCount = $brandPostTable->getDraftPostsByBrandAndUser($this->brand->id, $this->user->id);
        }
        
        $this->brand->counts = array('totalStories' => $storiesCount,
            'totalLovedStories' => $lovedStoriesCount,
            'totalLoveingUs' => $brabdLovedCount,
            'totalConnect' => $this->brand->connectCount,
            'totalDrafts' => $draftsCount);

        // Variables for ALL pages
        $feedVariables = [
            'BrandName' => $this->brand->name,
            'BrandUrl'  => $this->brand->url,
            'brand' => $this->brand,
            'user' => $this->user,
            'brandInfo' => $brandInfo,
            'bannerUrl' => $bannerUrl,
            'segment' => $segment,
            'brandLoved' => $brandLovedByUser,
            'isBrandAdmin' => $isBrandOwner,

            // todo: clean up below (don't use, replace with ones above for consistency)
            'Brand' => $this->brand,
            'thisBrand' => $this->brand,
            'brandUrl' => $this->brand->url,
            'userCanEditBrand' => $isBrandOwner,
            'isBrandOwner' => $isBrandOwner,
            
            //counter vaiables 
            'totalStories' => $storiesCount,
            'totalLovedStories' => $lovedStoriesCount,
            'totalLoveingUs' => $brabdLovedCount,
            'totalConnect' => $this->brand->connectCount,
            'totalDrafts' => $draftsCount,
        ];

        $this->view->setVariables($feedVariables);

        $segmentContent = null;

        switch($segment) {
            case 'stories':
                $segmentContent = $this->getStoriesContent();
                break;

            case 'gallery':
                $segmentContent = $this->getGalleryContent();
                break;

            case 'album':
                $segmentContent = $this->getAlbumContent();
                break;
            
            case 'video':
                $segmentContent = $this->getVideoContent();
                break;
                
            case 'products':
                $segmentContent = $this->getProductsContent();
                break;

            case 'services':
                $segmentContent = $this->getServicesContent();
                break;

            case 'about':
                $segmentContent = $this->getAboutContent();
                break;

            case 'about-management':
                $segmentContent = $this->getAboutManagementContent();
                break;

            case 'aboutme':
                $segmentContent = $this->getAboutmeContent();
                break;
            
            case 'portfolio':
                $segmentContent = $this->getPortfolioContent();
                break;
                
            case 'drafts':
                $segmentContent = $this->getDraftsContent();
                break;

            case 'mybrands':
                if($this->user) {
                    $segmentContent = $this->getMyBrandsContent();
                    break;
                }
            default:
                $segment = 'landing';

            case 'landing':
            case 'home':
                $segmentContent = $this->getHomeContent();
                break;
        }
        // Update in case it changed in the switch
        $this->view->setVariable('segment', $segment);

        // append segment data - override
        $this->view->setVariables($segmentContent);

        if($renderInnerContentsOnly) {
            $this->view->setTerminal(true);
        }

        if($outputJson) {
            $segmentURL = $this->url()->fromRoute('brand', [
                'url' => $this->brand->url,
                'segmentOrId' => $postIdOrSegment,
                'param1' => $this->param1,
                'param2' => $this->param2,
                'param3' => $this->param3,
                'param4' => $this->param4
            ]);

            $content = $this->sl->get('viewrenderer')->render($this->view);
            return new JsonModel([
                'content' => $content,
                'segment' => $segment,
                'segmentURL' => $segmentURL,
                'status' => 'ok',
            ]);
        }else{
            $this->layout()->setTemplate('lab-base/feed-layout');
            $this->layout()->setVariables($feedVariables);
            $this->layout()->setVariables($segmentContent);
            $this->sl->get('ViewHelperManager')->get('HeadTitle')->set($this->brand->name);
            return $this->view;
        }
    }
    
    public function manageBrandAction()
    {
        $brandId = $this->params()->fromRoute("id");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        $sl = $this->getServiceLocator();
        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $sl->get('BrandTable');
        $userTable = $sl->get('UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandById($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        $type = $req->getPost('type', '');
        $value = $req->getPost('value', '');
        /*if($value != "")
        {*/
            switch($type)
            {
                case 'loaction' : 
                        $thisBrand->location = trim($value);
                    break;
                
                case 'web' : 
                        $parsed = parse_url($value);
                        if (empty($parsed['scheme'])) {
                            $value = 'http://' . ltrim($value, '/');
                        }
                        $thisBrand->web = trim($value);
                    break;
                    
                case 'phone' : 
                        $thisBrand->phone = trim($value);
                    break;
            }
/*        }*/
        
        $brandTable->saveBrand($thisBrand);

        return new JsonModel([
            'status' => 'ok',
            'thisBrand' => $thisBrand,
        ]);
    }

    /* Post data:
     *  title
     *  content
     *  cat - category url
     *  imageData - image names separated by a semicolon
     *  postType - 'quote', 'simple' or 'advanced' for now.
     *  allowComments
     */
    public function addPostAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        $postType = $req->getPost('postType');
        if($postType == 'photo' || $postType == 'video')
        {
            if(!$req->isPost() || empty($req->getPost('content')) || empty($postType)) {
                return new JsonModel([
                    'status' => 'Please enter all fields.'
                ]);
            }
        }
        else
        {
            if(!$req->isPost() || empty($req->getPost('content')) || empty($postType)
                || ($postType != 'quote' && empty($req->getPost('title'))) /*|| empty($req->getPost('cat'))*/) {
                return new JsonModel([
                    'status' => 'Please enter all fields.'
                ]);
            }
        }

        $postDraft = ($req->getPost('draft') != "" && $req->getPost('draft') == 'draft') ? 1 : 0;

        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $categoryTable = $this->getServiceLocator()->get('CategoryTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        /*
        $category = $categoryTable->getCategoryByUrlAndBrand($req->getPost('cat'), $thisBrand->id);
        if(!$category) {
            if($thisBrand->brandType)
            {
                $category = $categoryTable->getCategoryByUrlAndBrand('my-stories', $thisBrand->id);
            }
            else
            {
                $category = $categoryTable->getCategoryByUrlAndBrand('our-stories', $thisBrand->id);
            }

            if(!$category) {
                return new JsonModel([
                    'status' => 'You cannot post into this category.'
                ]);
            }
        }*/

        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title', '');
        $videoURL = $req->getPost('videoURL', '');
        $buyNow = $req->getPost('buyNow', 0);
        $radio = $req->getPost('radio', 0);
        $price = $req->getPost('price', 0);

        // todo: proper image sanitation
        //if($imageData !== null) {
            //$imageData = addslashes($imageData);
        //}


        $post = new BrandPost($loggedInUser->id, $thisBrand->id, /*$category->id*/ 0, $title, $content, $req->getPost('postType', 'advanced'), $imageData, $videoURL, NULL, $postDraft, $buyNow, $radio, $price);
        $post->commentsEnabled = (bool)$req->getPost("allowComments", FALSE);

        $brandPostTable->saveBrandPost($post);

        return new JsonModel([
            'status' => 'ok',
            'post' => $post
        ]);
    }
    
    public function addPostImageAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        $sl = $this->getServiceLocator();
        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $sl->get('BrandTable');
        $userTable = $sl->get('UserTable');
        $imageSaver = $sl->get('ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $sl->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);


        //$image = $imageSaver->uploadImage($image, 'post-image-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);
        $image = $imageSaver->uploadImageSimple($image, 'post', /*random*/null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }
    
    public function deletePostAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $postId = $this->params()->fromRoute("postId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        
        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        //$brandProductTable = $this->getServiceLocator()->get('BrandProductTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $postType = $req->getPost('postType', '');

        $loggedInUser = $userTable->getLoggedInUser();

        $thisPost = $brandPostTable->getBrandPostById($postId);
        if(!$thisPost || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisPost->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.',
                'thisPost' => $thisPost,
                'loggedInUser' => $loggedInUser,
            ]);
        }

        $isAlbumImage = $req->getPost('album', 0);

        //todo: check what this does
        if(in_array($thisPost->postType, array(4)) && empty($thisPost->postTitle))
        {
            $newImages = [];
            $image = $req->getPost('image');
            $images = json_decode($thisPost->postImageData);
            foreach($images as $img)
            {
                if(basename($img->url) != $image)
                {
                    $newImages[] = $img;
                }
            }

            if(!empty($newImages))
            {
                $thisPost->postImageData = json_encode($newImages);
                $brandPostTable->saveBrandPost($thisPost);
            }
            else
            {
                $brandPostTable->deleteBrandPost($thisPost->id);
            }
        }
        else if($isAlbumImage == 1)
        {
            $newImages = [];
            $image = $req->getPost('image');
            $images = json_decode($thisPost->postImageData);
            foreach($images as $img)
            {
                if(basename($img->url) != $image)
                {
                    $newImages[] = $img;
                }
            }

            if(!empty($newImages))
            {
                $thisPost->postImageData = json_encode($newImages);
                $brandPostTable->saveBrandPost($thisPost);
            }
        }
        else
        {
            $brandPostTable->deleteBrandPost($thisPost->id);
        }

        return new JsonModel(['status' => 'ok']);
    }

    public function getPostDataAction()
    {
        $postId = $this->params()->fromRoute("postId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisPost = $brandPostTable->getBrandPostById($postId);

        if(!$thisPost || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisPost->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.',
                'thisPost' => $thisPost,
                'loggedInUser' => $loggedInUser,
            ]);
        }

        $singlePost = $brandPostTable->getBrandPostById($postId);
        $thisBrand = $brandTable->getBrandById($thisPost->brandId);
        
        if($singlePost->postType == 5 && !empty($singlePost->videoData))
        {
            $singlePost->videoDataTag = $this->convertVideoURLToTags($singlePost->videoData);
        }
/*       echo "<pre>"; print_r($singlePost); die; */
        $variables = [
            'singlePost' => $singlePost,
            'brand' => $thisBrand,
        ];
        
        $viewModel = new ViewModel( $variables );
        $viewModel->setTemplate('lab-base/brand-feed/get-post-data');
        return $viewModel;
    }
    
    public function editPostAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $postId = $this->params()->fromRoute("postId");
        

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        
        $postType = $req->getPost("postType");
        
        if(in_array($postType, array(1, 5, 4)))
        {
            if(!$req->isPost() || empty($req->getPost('content'))) {
                return new JsonModel([
                    'status' => 'Please enter all fields.'
                ]);
            }
        }
        else
        {
            if(!$req->isPost() || empty($req->getPost('content')) || empty($req->getPost('title'))) {
                return new JsonModel([
                    'status' => 'Please enter all fields.'
                ]);
            }
        }
        
        $isDraft = $req->getPost('drafts', 0);
        
        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisPost = $brandPostTable->getBrandPostById($postId);
        

        if(!$thisPost || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisPost->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        $thisPost->setPostContents($req->getPost('content'));
        $thisPost->setPostTitle($req->getPost('title'));
        $thisPost->setPostDraftsStatus($isDraft);
        $imageData = $req->getPost('imageData');
        $thisPost->postImageData = $imageData;
        
        $video = $req->getPost('video', '');
        $thisPost->videoData = $video;
        
/*        echo "<pre>"; print_r($thisPost); die;*/
        $brandPostTable->saveBrandPost($thisPost);


        return new JsonModel(['status' => 'ok']);
    }
    
    public function setPostLovedAction() {
        $postId = $this->params()->fromRoute("postId");
        $loveOrNot = $this->params()->fromRoute("loveOrNot");

        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        $brandPostLovedTable = $this->getServiceLocator()->get('BrandPostlovedTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');

        if(!$postId) {
            return new JsonModel(["status" => "An error has occurred."]);
        }

        $loggedInUser = $userTable->getLoggedInUser();
        if(!$loggedInUser) {
            return new JsonModel(["status" => "ok", 'login' => true]);
        }


        $thisPost = $brandPostTable->getBrandPostById($postId);
        $brand = $brandTable->getBrandById($thisPost->brandId);
        if(!$brand) {
            return new JsonModel(["status" => "An error has occurred, this brand does not exist."]);
        }

        $lovePostData = $brandPostLovedTable->getByUserBrandPost($loggedInUser->id, $thisPost->brandId, $postId);

        $loved = new BrandPostLoved($loggedInUser->id, $thisPost->brandId, $postId, $loveOrNot);

        $loved->id = isset($lovePostData->id) ? $lovePostData->id : 0;
        $lovedTbl = $brandPostLovedTable->save($loved);
        $isLove = false;
        if($loveOrNot)
        {
            $isLove = true;
        }
        $loveCnt = $brandPostLovedTable->postLoveCount($postId);

        $lovedStoriesCount = $brandPostLovedTable->getCountLovedPostByBrand($thisPost->brandId);
        
        return new JsonModel(['status' => 'ok', 'loveCount' => $loveCnt, 'is_love' => $isLove, 'story_loved_count' => $lovedStoriesCount]);
    }

     public function postCommentAction() {
        /**
         * @var $commentTable CommentTable
         * @var $brandPostTable BrandPostTable
         * @var $brandTable BrandTable
         * @var $notificationTable NotificationTable
         * @var UserTable $userTable
         */
        $postId = $this->params()->fromRoute("postId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost() ||
            empty($req->getPost('text'))
        ) {
            return new JsonModel([
                'status' => 'Please enter the required fields.'
            ]);
        }


        $commentTable = $this->getServiceLocator()->get('CommentTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $notificationTable = $this->getServiceLocator()->get('NotificationTable');

        $post = $brandPostTable->getBrandPostById($postId);
        if(!$post) {
            return new JsonModel([
                'status' => 'The post doesn\'t exist.'
            ]);
        }
        if(!$post->commentsEnabled) {
            return new JsonModel([
                'status' => 'The post doesn\'t allow for comments.'
            ]);
        }

        $name = NULL;
        $email = NULL;
        $commentBrandId = NULL;
        $autoApprove = false;

        $loggedInUser = $userTable->getLoggedInUser();
        if($loggedInUser) {
            $loggedInUserId = $loggedInUser->id;
            if($brandTable->isBrandManagedByUser($post->brandId, $loggedInUser->id)) {
                $autoApprove = true;
                if($req->getPost('postAs', 'user') === 'brand') {
                    $commentBrandId = $post->brandId;
                }
            }
        }else{
            if(
                empty($req->getPost('name')) ||
                empty($req->getPost('email'))
            ) {
                return new JsonModel([
                    'status' => 'You need to provide a name and an email address, or sign in.'
                ]);
            }
            $name = $req->getPost("name");
            $email = $req->getPost("email");
            $loggedInUserId = NULL;
        }

        $content = $req->getPost('text');
        $comment_id = $req->getPost('comment_id', 0);
        $content = htmlspecialchars($content);

        if($comment_id > 0)
        {
            $comment = $commentTable->getCommentById($comment_id);
            $comment->content = $content;
        }
        else
        {
            $comment = new Comment($loggedInUserId, $commentBrandId, $postId, $content, $name, $email);

            if($autoApprove) $comment->approved = true;
        }

        $commentTable->saveComment($comment);


        // Notification
        $displayName = $comment->posterDisplayName;
        if($comment->posterBrandId) {
            $displayName = $brandTable->getBrandById($comment->posterBrandId)->name;
        }elseif($comment->posterId) {
            $poster = $userTable->getUserById($comment->posterId);
            if(!$poster)
                $displayName = '<i>User removed</i>';
            else
                $displayName = $poster->fullName;
        }

        $title = $post->postTitle;
        if(strlen($title) > 40) {
            $i = strpos($title, ' ', 30);
            if($i > 0) {
                $title = substr($title, 0, $i) . '...';
            }
        }
        //$n = new Notification('<span class="author">' . $displayName . '</span><span class="action"> has commented on </span><span class="post">'.$title.'</span>');
        //$comment->id, $post->id, $post->brandId
        //$n->relatedComment = $comment->id;
        //$n->relatedPost = $post->id;
        //$n->relatedBrand = $post->brandId;

        //$notificationTable->saveNotification($n);

        return new JsonModel([
            'status' => 'ok',
            'postId' => $postId,
            'comments' => [$comment]
        ]);
    }

    public function getCommentsAction() {
        $req = $this->getRequest();
        $commentId = $req->getPost('commentId', 0);
        if($commentId > 0)
        {
            $commentTable = $this->getServiceLocator()->get('CommentTable');
            $comment = $commentTable->getCommentById($commentId);
            
            $ret = [
                'status' => 'ok',
                'comments' => $comment
            ];
            return new JsonModel($ret);
        }
        else
        {
            return $this->getCommentsCommon();
        }
    }

    private function getCommentsCommon($brandId = null) {
        /**
         * @var $commentTable CommentTable
         * @var $brandTable BrandTable
         * @var UserTable $userTable
         * @var BrandPostTable $brandPostTable
         * @var $req \Zend\Http\Request
         */

        $allBrandComments = $brandId !== null;

        $req = $this->getRequest();
        $firstId = $req->getPost('firstCommentId', -1);
        $lastId = $req->getPost('lastCommentId', -1);
        $limit = $req->getPost('limit', -1);

        $commentTable = $this->getServiceLocator()->get('CommentTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        $imageTable = $this->getServiceLocator()->get('ImageTable');
/*        $imageLinker = new ImageLinker($imageTable);*/

        $loggedInUser = $userTable->getLoggedInUser();
        $isManager = false;
        if($loggedInUser && $brandTable->isBrandManagedByUser($brandId, $loggedInUser->id)) {
            $isManager = true;
        }

        $totalComments = 0;
        if(!$allBrandComments) {
            $postId = $this->params()->fromRoute("postId");
            $post = $brandPostTable->getBrandPostById($postId);
            if (!$post) {
                return new JsonModel(['status' => 'The post you tried to comment has been removed.']);
            }

            if (!$post->commentsEnabled) {
                return new JsonModel(['status' => 'Comments are disabled for this post.']);
            }
            $brandId = $post->brandId;
            
            $comments = $commentTable->getCommentsByPost($postId, $firstId, $lastId, 1, $limit);
            $totalComments = $commentTable->getTotalCommentsByPost($postId, $firstId, $lastId, 1);
        }else{
            if(!$isManager) {
                return new JsonModel(['status' => 'Only brand managers can see this']);
            }

            $type = $this->params()->fromRoute('commentsType');
            $filterSeen = null;
            $filterApproved = null;
            $filterRejected = null;
            if($type == 'rejected') {
                $filterRejected = true;
            }elseif($type == 'approved') {
                $filterApproved = true;
            }elseif($type == 'latest') {
                $filterRejected = false;
            }

            $comments = $commentTable->getCommentsByBrand($brandId, $firstId, $lastId, $filterSeen, $filterApproved, $filterRejected);
            $commentTable->markBrandCommentsAndNotificationsAsSeen($brandId);
        }

        $clientComments = [];

        foreach($comments as $comment) {
            /** @var Comment $comment */
            if(!$isManager && $comment->approved !== true)
                continue;

            $id = $comment->id;
            $date = $comment->dateCreated;
            $text = $comment->content;
            $displayName = $comment->posterDisplayName;
            $brandUrl = false;
            $seen     = $comment->seen === true;
            $isPublic = $comment->approved === true;
            $rejected = $comment->rejected === true;
            $userImage = $this->getImageObject('system-user-avatar-default');

            if($comment->posterBrandId) {
                $postingBrand = $brandTable->getBrandById($comment->posterBrandId);
                if(!$postingBrand) // Brand removed?
                    continue;
                $displayName = $postingBrand->name;
                $brandUrl =  $this->url()->fromRoute("root/brandFeed", ['brandUrl' => $postingBrand->url]);

            }elseif($comment->posterId) {
                $poster = $userTable->getUserById($comment->posterId);
                if(!$poster)
                    continue; // User removed
                $displayName = $poster->fullName;

                $imageAvatar = $this->getImageObject('user-avatar-'.$comment->posterId);

                if(!empty($imageAvatar))
                {
                    $userImage = $imageAvatar;
                }
                
                $commentpost = $brandPostTable->getBrandPostById($comment->postId);
                $postingBrand = $brandTable->getBrandById($commentpost->brandId);
            }

                $userAvatar = '';
                if(!empty($userImage))
                {
                    $userAvatar = $this->getImageLink($userImage). '?' .$userImage->dateCreated;
                }
                else
                {
                    $userInitails = ImageLinker::getAvatarInitials($postingBrand->name);
                }

            $clientComments[] = [
                'id' => $id,
                'text' => $text,
                'name' => $displayName,
                'nameUrl' => $brandUrl,
                'date' => $date,
                'seen' => $seen,
                'public' => $isPublic,
                'rejected' => $rejected,
                'userAvatar' => $userAvatar,
                'userInitails' => $userInitails, 
                'postId' => $comment->postId, 
            ];
        }

        $ret = [
            'status' => 'ok',
            'comments' => $clientComments,
            'totalComments' => $totalComments
        ];

        if(isset($postId)) {
            $ret['postId'] = $postId;
        }
        return new JsonModel($ret);
    }

    function time_elapsed_string($ptime)
    {
        $etime = time() - $ptime;

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array( 365 * 24 * 60 * 60  =>  'year',
                     30 * 24 * 60 * 60  =>  'month',
                          24 * 60 * 60  =>  'day',
                               60 * 60  =>  'hour',
                                    60  =>  'minute',
                                     1  =>  'second'
                    );
        $a_plural = array( 'year'   => 'years',
                           'month'  => 'months',
                           'day'    => 'days',
                           'hour'   => 'hours',
                           'minute' => 'minutes',
                           'second' => 'seconds'
                    );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str);
            }
        }
    }

     public function getImageObject($imagePath) {
        $imageTable = $this->getServiceLocator()->get('ImageTable');

        $targetName = md5($imagePath);
        $targetName = 'i/' . $targetName . '.jpg';
        return $imageTable->getImageByUrl($targetName);
    }

    public function getImageLink(Image $img) {
        return ImageSaver::GetImageS3Link($img->url);
    }

    public function rejectCommentAction() {
        /**
         * @var $brand Brand
         * @var $user User
         * @var $comment Comment
         * @var $commentTable CommentTable
         */
        $brand = null;
        $user = null;
        $comment = null;
        $commentTable = null;

        $ret = $this->manageCommentPreamble($brand, $user, $comment, $commentTable);
        if($ret !== true) return $ret;

        $comment->approved = false;
        $comment->rejected = true;
        $commentTable->saveComment($comment);

        return new JsonModel(['status' => 'ok']);
    }

    private function manageCommentPreamble( &$brand, &$user, &$comment, &$commentTable ) {
        $commentId = (int)$this->params()->fromRoute("commentId");

        /**
         * @var $commentTable CommentTable
         * @var $brandTable BrandTable
         * @var $userTable UserTable
         */
        $userTable = $this->getServiceLocator()->get('UserTable');
        $user = $userTable->getLoggedInUser();
        if(!$user)
            return new JsonModel(['status' => 'You need to log in again.']);

        $commentTable = $this->getServiceLocator()->get('CommentTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $comment = $commentTable->getCommentById($commentId);
        if(!$comment)
            return new JsonModel(['status' => 'This comment doesn\'t exist or has been removed.']);

        $brand = $brandTable->getBrandByPostId($comment->postId);

        if(!$brand)
            return new JsonModel(['status' => 'The brand has been removed. Please notify an administrator if you think this is an error.']);

        if(!$brandTable->isBrandManagedByUser($brand->id, $user->id))
            return new JsonModel(['status' => 'You don\'t have the permission to manage comments. If you believe this is an error, please contact an administrator.']);

        return true;

    }
    
    public function editAlbumPhotoAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        
        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        $imageData = $req->getPost('imageData');
        $albumId = $req->getPost('albumId');

        if(!empty($imageData))
        {
            $imageData = json_decode($imageData);
        }
        
        if(!$albumId)
        {
            return new JsonModel([
                    'status' => 'You cannot edit this album.'
                ]);
        }
        
        $album = $brandPostTable->getBrandPostById($albumId);
        
        $myImgData = (!empty($album->postImageData)) ? json_decode($album->postImageData) : '';
        if(!empty($imageData))
        {
            foreach($imageData as $imgd)
            {
                $myImgData[] = $imgd;
            }
        }
        
        $album->postImageData = json_encode($myImgData);
        $brandPostTable->saveBrandPost($album);

        return new JsonModel([
            'status' => 'ok',
            'post' => $album
        ]);
    }   

    public function oldIndexAction() {
        /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("url");
        //$categoryUrl = $this->params()->fromRoute("categoryUrl");
        
        /*$postId = $this->params()->fromRoute("postId");*/
        $postIdOrType = $this->params()->fromRoute("segmentOrId");
        $albums = $this->params()->fromRoute("albums");
        $albumId = $this->params()->fromRoute("albumId");
        $postId = $postType = null;
        if($postIdOrType != "")
        {
            if((int)$postIdOrType > 0)
            {
                $postId = $postIdOrType;
            }
            else
            {
                $postType = $postIdOrType;
            }
        }
        
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);

        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);

        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        //$categoryTable = $this->getServiceLocator()->get('CategoryTable');
        $commentTable = $this->getServiceLocator()->get('CommentTable');

        /*
        $categoryList = $categoryTable->getCategoriesByBrand($thisBrand->id);
        usort($categoryList, function(Category $a, Category $b) {
            return $a->position - $b->position;
        });
        foreach($categoryList as $cat) {
            usort($cat->children, function (Category $a, Category $b) {
                return $a->position - $b->position;
            });
        }

        $cat = $categoryTable->getCategoryByUrlAndBrand($categoryUrl, $thisBrand->id);

        // Disallow selecting a category with children (only children can be selected)
        if(!empty($cat->children)) {
            $cat = reset($cat->children);
        }

        // Fix the url, if it has changed
        if($cat) {
            $categoryUrl = $cat->url;
        }*/


        $userTable = $this->getServiceLocator()->get('UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;
                /*if($cat)*/ $userCanPost = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }

        /*// Disallow non-enabled categories for public users
        if(!$userCanEditBrand && $cat && $cat->enabled == false) {
            $cat = null;
        }*/

        
        $thisPost = FALSE;
        $draftspostCnt = 0;
        if(isset($loggedInUser->id) && $loggedInUser->id > 0)
        {
            $draftspostCnt = $brandPostTable->getDraftPostsByBrandAndUser($thisBrand->id, $loggedInUser->id);
        }
        
        // A post has been requested?
        if($postId !== null) {
            // todo: Check if the brand matches the post
            $thisPost = $brandPostTable->getBrandPostById($postId);
            if($thisPost) {
                $postList = [$thisPost];
            }else{
                $postList = [];
            }
        }else if($postType !== null){
            // todo: Check if the brand matches the post type
            if($postType == 'drafts')
            {
                if(isset($loggedInUser->id) && $loggedInUser->id > 0)
                {
                    $thisPost = $brandPostTable->getBrandFeedDraftsByUserAndBrand($thisBrand->id, $loggedInUser->id);
                }
            }
            else
            {
                if(isset($albums) && $albums != "")
                {
                    if(isset($albumId) && $albumId > 0)
                    {
                        $thisPost = $brandPostTable->getBrandFeedPostsAlbums($thisBrand->id, $albumId);
                        $otherAlbums = $brandPostTable->getBrandFeedOtherAlbums($albumId, 3, $thisBrand->id);
                    }
                    else
                    {
                        $thisPost = $brandPostTable->getBrandFeedPostsAlbums($thisBrand->id, $albumId);
                    }
                }
                else
                {
                    $thisPost = $brandPostTable->getBrandFeedPostsByTypeByBrand($postType, $thisBrand->id);
                }
            }
            
            $postList = [];
            if($thisPost) {
                $postList = $thisPost;
            }
        }else{
            if($cat) {
                $postList = $brandPostTable->getBrandPostsByBrandAndCategory($thisBrand->id, $cat->id);
            }else{
                $postList = $brandPostTable->getBrandFeedPostsByBrand($thisBrand->id);
            }
        }
        $brandPostLovedTable = $this->getServiceLocator()->get('BrandPostLovedTable');
        $postPhotoCnt = 0;
        $postAlbumCnt = 0;
        $postVideoCnt = 0;
        foreach ($postList as $post) {
            /*echo "<pre>"; print_r($post); die;*/
            // caching done in getCategoryById
            $postCat = $categoryTable->getCategoryById($post->categoryId);
            $post->categoryName = $postCat->categoryName;
            $post->categoryUrl = $postCat->url;
            $loveCnt = $brandPostLovedTable->postLoveCount($post->id);
            $post->loveCnt = $loveCnt;
            if(isset($loggedInUser->id))
            {
                $loveOrNot = $brandPostLovedTable->getByUserBrandPost($loggedInUser->id, $thisBrand->id, $post->id);
            }
            $post->loveOrNot = (isset($loveOrNot->loved)) ? $loveOrNot->loved : 0;
            
            if($post->videoData != "")
            {
                $videoTags = $this->convertVideoURLToTags($post->videoData);
                if($videoTags != false)
                {
                    $post->videoDataTag = $videoTags;
                }
            }
         
             $allCmments = $commentTable->getCommentsByPost($post->id, -1, -1);
             $post->commentsCnt = count($allCmments);   
        }
        
        if($postType == 'photo' || $postType == 'video')
        {
            $photoAlbumesCnt = $brandPostTable->getBrandFeedCntPhotoAndAlbums($thisBrand->id);
            $postPhotoCnt = $photoAlbumesCnt['photo_cnt'];
            $postAlbumCnt = $photoAlbumesCnt['album_cnt'];
            
            $postVideoCnt = $brandPostTable->getBrandFeedCntVideos($thisBrand->id);
        }

        $brandLovedByUser = false;
        if($loggedInUser) {
            /** @var BrandRelationTable $brTable */
            $brTable = $this->getServiceLocator()->get('BrandRelationTable');
            $brandLovedByUser = $brTable->getByUserBrandType($loggedInUser->id, $thisBrand->id, 1);

        }
        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/feed-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);

        $view = new ViewModel([
            'thisBrand' => $thisBrand,
            'categoryList' => BrandSettingsController::categoryArrayToJson($categoryList),
            'currentCategory' => $cat,

            'singlePostView' => $thisPost !== FALSE,
            'thisPost' => $thisPost,
            'postList' => $postList,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,

            'brandLovedByUser' => $brandLovedByUser,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
            //'categoryUrl' => $categoryUrl,
            'postType' => $postType,
            'draftspostCnt' => $draftspostCnt,
            'postPhotoCnt' => $postPhotoCnt,
            'postAlbumCnt' => $postAlbumCnt,
            'postVideoCnt' => $postVideoCnt,
            
            'albums' => (isset($albums) && !empty($albums)) ? ($albumId > 0) ? FALSE : TRUE : FALSE,
            'albumId' => $albumId,
            'otherAlbums' => (isset($otherAlbums) && !empty($otherAlbums)) ? $otherAlbums : array(),
        ]);

        return $view;
    }

    private function convertVideoURLToTags($videoURL)
    {
        $pattern1 = "/(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/";
        $pattern2 = "/(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/";

        $replacement = false;

        if($videoURL != NULL)
        {
            $success1 = preg_match($pattern1, $videoURL, $match1);
            $success2 = preg_match($pattern2, $videoURL, $match2);

            if(!empty($match1))
            {
                if($match1[1] != "")
                {
                    $replacement = '<iframe width="420" height="345" src="//player.vimeo.com/video/'.$match1[1].'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                }
            }
            
            if(!empty($match2))
            {
                if($match2[1] != "")
                {
                    $replacement = '<iframe width="420" height="345" src="http://www.youtube.com/embed/'.$match2[1].'" frameborder="0" allowfullscreen></iframe>';
                }
            }
        }
        
        return $replacement;
    }

    /*
     * CONTENT PAGES
     */

    /* Services content */
    public function getServicesContent()
    {
        $brandServiceTable = $this->sl->get('BrandServiceTable');

        $serviceList = $brandServiceTable->getBrandServiceByBrand($this->brand->id);
        
        if($serviceList)
        {
            // todo: do this in js
            foreach($serviceList as $sl)
            {
                if(strlen($sl->serviceContent) > 60)
                {
                    $sl->serviceContentTrunc = $this->truncateString($sl->serviceContent, 60, true);
                }
            }
        }

        /** @var BrandServiceMetaTable $brandServiceMetaTable */
        $brandServiceMetaTable = $this->sl->get('BrandServiceMetaTable');
        $serviceMeta = $brandServiceMetaTable->getBrandServiceMetaByBrand($this->brand->id);

        if(!$serviceMeta) {
            $serviceMeta = new BrandServiceMeta($this->brand->id, 'n/a', 'n/a', 0, 0);
        }

        $this->view->setTemplate('lab-base/brand-feed/services');
        return [
            'serviceList' => $serviceList,
            'serviceMeta' => $serviceMeta,
        ];
    }
    
    private function truncateString($str, $chars, $to_space, $replacement="...") {
       if($chars > strlen($str)) return $str;

       $str = substr($str, 0, $chars);

       $space_pos = strrpos($str, " ");
       if($to_space && $space_pos >= 0) {
           $str = substr($str, 0, strrpos($str, " "));
       }

       return($str . $replacement);
    }
    
    public function addServiceAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
     
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $brandServiceTable = $this->getServiceLocator()->get('BrandServiceTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title');

        $service = new BrandService($loggedInUser->id, $thisBrand->id, $title, $content, $imageData);

        $brandServiceTable->saveBrandService($service);

        return new JsonModel([
            'status' => 'ok',
            'service' => $service
        ]);
    }
    
    public function getPopupServiceAction()
    {
        $serviceId = $this->params()->fromRoute("serviceId");
        
        $brandServiceTable = $this->getServiceLocator()->get('BrandServiceTable');
        $service = $brandServiceTable->getBrandServiceById($serviceId);
        
        $userCanPost = false;
        $userCanEditBrand = false;
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $thisBrand = $brandTable->getBrandById($service->brandId);
       
       // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }

        $userTable = $this->getServiceLocator()->get('UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }   
        }
     
        $variables = [
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,
            
            'service' => $service,
        ];
        
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);
        
        $viewModel = new ViewModel( $variables );
        $viewModel->setTemplate('lab-base/brand-feed/get-popup-service');
        return $viewModel;
    }
    
    public function editServiceAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        if(!$req->isPost() || empty($req->getPost('content')) 
            || (empty($req->getPost('title')) || $req->getPost('serviceId') < 1)) {
            return new JsonModel([
                'status' => 'Please enter all fields.'
            ]);
        }

        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $brandServiceTable = $this->getServiceLocator()->get('BrandServiceTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.'
            ]);
        }

        $serviceId = $req->getPost('serviceId');
        
        $serviceData = $brandServiceTable->getBrandServiceById($serviceId);
        
        if($serviceData->creatorId != $loggedInUser->id)
        {
            return new JsonModel([
                'status' => 'You do not have permission for update this service.'
            ]);
        }
        
        if(!$serviceData)
        {
            return new JsonModel([
                'status' => 'You can not update this service because this service not found in database.'
            ]);
        }
        
        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title');

        $service = new BrandService($loggedInUser->id, $thisBrand->id, $title, $content, $imageData);

        if($serviceId > 0)
        {
            $service->id = $serviceId;
        }
        
        $brandServiceTable->saveBrandService($service);

        return new JsonModel([
            'status' => 'ok',
            'service' => $service
        ]);
    }
    
    public function deleteServiceAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $serviceId = $this->params()->fromRoute("serviceId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $brandServiceTable = $this->getServiceLocator()->get('BrandServiceTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisService = $brandServiceTable->getBrandServiceById($serviceId);

        if(!$thisService || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisService->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.',
                'thisService' => $thisService,
                'loggedInUser' => $loggedInUser,
            ]);
        }
        
        if(!$thisService)
        {
            return new JsonModel([
                'status' => 'Service you select not found.',
            ]);
        }

        $brandServiceTable->deleteBrandService($thisService->id);

        return new JsonModel(['status' => 'ok']);
    }
    
    
    public function manageServiceSettingsAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $req = $this->getRequest();
        
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);

        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $userTable = $this->getServiceLocator()->get('UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
        
        $brandServiceMetaTable = $this->getServiceLocator()->get('BrandServiceMetaTable');
        
        $serviceMeta = $brandServiceMetaTable->getBrandServiceMetaByBrand( $thisBrand->id );
        
        if($userCanEditBrand)
        {
            $type = $req->getPost('type');
            $content = $req->getPost('content');
            if(!empty($serviceMeta))
            {
            switch($type)
            {
                case 'title' :
                        $serviceMeta->title = $content;
                    break;
                    
                case 'description' :
                        $serviceMeta->description = $content;
                    break;
                
                case 'public_title' :
                        $serviceMeta->publicTitle = $content;
                    break;
                
                case 'public_description' :
                        $serviceMeta->publicDescription = $content;
                    break;
            }
            }
            else
            {
                $title = ($type == 'title') ? $content : '';
                $description = ($type == 'description') ? $content : '';
                
                $serviceMeta = new BrandServiceMeta($thisBrand->id, $title, $description);
            }
            $brandServiceMetaTable->saveBrandServiceMeta($serviceMeta);
            return new JsonModel(['status' => 'ok', 'serviceMeta' => $serviceMeta]);
        }
        else
        {
            return new JsonModel(['status' => 'You have no access for this brand.']);
        }
    }
    
    public function addServiceImageAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $imageSaver = $this->getServiceLocator()->get('ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $this->getServiceLocator()->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);


        $image = $imageSaver->uploadImageSimple($image, 'service', /*random*/null, $loggedInUser->id, $thisBrand->id);
        //$image = $imageSaver->uploadImage($image, 'service-image-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }


    /* About content */
    public function getAboutContent() {
        $about = new BrandAbout();

        /** @var BrandAboutTable $brandAboutTable */
        $brandAboutTable = $this->getServiceLocator()->get('BrandAboutTable');

        $aboutHeaders = $about->getAboutHeaders();

        $aboutLists = $brandAboutTable->getBrandAboutByBrand($this->brand->id);
        
        $aboutHeaderTitle = $aboutHeaders[1];
        $aboutHeaderIndex = 1;
        
        if(!empty($aboutLists))
        {
            foreach($aboutLists as $aboutList)
            {
                $nextHrd = 0;
                if($aboutList->header > 0)
                {
                    $nextHrd = $this->get_next($aboutHeaders, $aboutList->header);
                }
                
                if(!empty($nextHrd))
                {
                    $aboutHeaderTitle = $nextHrd;
                    $aboutHeaderIndex = array_flip($aboutHeaders)[$nextHrd];
                }
                else
                {
                    $aboutHeaderTitle = 'Additional Content';
                    $aboutHeaderIndex = 0;
                }
            }
        }
    
        foreach($aboutLists as $aboutList)
        {
            if(strlen($aboutList->aboutDescription) > 50)
            {
                $aboutList->aboutFullDescription = $aboutList->aboutDescription;
                $aboutList->aboutDescription = $this->truncateString($aboutList->aboutDescription, 50, true);
            }
        }
    
        $managementTeamName = ($this->brand->managementTeamName != "") ? $this->brand->managementTeamName : 'Management Team';

        $this->view->setTemplate('lab-base/brand-feed/aboutus');
        return [
            'aboutLists' => $aboutLists,
            'aboutHeaderTitle' => $aboutHeaderTitle,
            'aboutHeaderIndex' => $aboutHeaderIndex,
            
            'managementTeamName' => $managementTeamName,
        ];
    }
    
    public function getAboutmeContent() {
        /** @var BrandAboutTable $brandAboutTable */
        $aboutmeTable = $this->getServiceLocator()->get('AboutmeTable');
        
        $aboutmeProfile = $aboutmeTable->getAboutmeByBrand($this->brand->id);
        
        $this->view->setTemplate('lab-base/brand-feed/aboutme');
        return [
            'aboutme' => $aboutmeProfile,
        ];
    }
    
    public function addAboutAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $brandAboutTable = $this->getServiceLocator()->get('BrandAboutTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to about us data.'
            ]);
        }
        
        $content = $req->getPost('content');
        $header = $req->getPost('header');
        $title = $req->getPost('title');

        $sortOrder = $brandAboutTable->getMaxSortOrder($thisBrand->id);
        
        $sort = (isset($sortOrder[0]->sortOrder)) ? ($sortOrder[0]->sortOrder + 1) : 0;
/*        $sort = ($header > 0 && $header < 6) ? $header : 6;*/
        $header = ($header != "") ? $header : 'Additional Content';
       
        
        $aboutUS = new BrandAbout($loggedInUser->id, $thisBrand->id, $header, $title, $content, $sort);
        $aboutUS->sortOrder = $sort;
        
        $aboutHeaders = $aboutUS->getAboutHeaders();
        
        $selHeader = array_flip($aboutHeaders)[$header];
        $nextHrd = '';
        if($selHeader > 0)
        {
            $nextHrd = $this->get_next($aboutHeaders, $selHeader);
        }
        
        if(!empty($nextHrd))
        {
            $aboutHeaderTitle = $nextHrd;
            $aboutHeaderIndex = array_flip($aboutHeaders)[$nextHrd];
        }
        else
        {
            $aboutHeaderTitle = 'Additional Content';
            $aboutHeaderIndex = 0;
        }

        $brandAboutTable->saveBrandAbout($aboutUS);
        
        /*$aboutUS->aboutFullDescription = $aboutUS->aboutDescription;
        $aboutUS->aboutDescription = $this->truncateString($aboutUS->aboutDescription, 50, true);*/
        if(strlen($aboutUS->aboutDescription) > 50)
        {
            $aboutUS->aboutFullDescription = $aboutUS->aboutDescription;
            $aboutUS->aboutDescription = $this->truncateString($aboutUS->aboutDescription, 50, true);
        }
    
        return new JsonModel([
            'status' => 'ok',
            'aboutUS' => $aboutUS,
            'headerTitle' => $aboutHeaderTitle,
            'headerIndex' => $aboutHeaderIndex,
        ]);
    }

    /* todo: evaluate this */
    private function get_next($array, $key) {
       $currentKey = key($array);
       while ($currentKey !== null && $currentKey != $key) {
           next($array);
           $currentKey = key($array);
       }
       return next($array);
    }
    
    public function getPopupAboutAction()
    {
        $aboutId = $this->params()->fromRoute("aboutId");
        
        $brandAboutTable = $this->getServiceLocator()->get('BrandAboutTable');
    
        $about = $brandAboutTable->getBrandAboutById($aboutId);
        
        $userCanPost = false;
        $userCanEditBrand = false;
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $thisBrand = $brandTable->getBrandById($about->brandId);
       
       // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }

        $userTable = $this->getServiceLocator()->get('UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }   
        }
        
        $aboutData = new BrandAbout();
        
        $aboutHeaders = $aboutData->getAboutHeaders();
        if($about->header > 0 && $about->header < 6)
        {
            $aboutHeaderTitle = $aboutHeaders[$about->header];
            $aboutHeaderIndex = $about->header;
        }
        else
        {
            $aboutHeaderTitle = 'Additional Contents';
            $aboutHeaderIndex = 0;
        }
     
        $variables = [
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,
            
            'about' => $about,
            'aboutHeaderTitle' => $aboutHeaderTitle,
            'aboutHeaderIndex' => $aboutHeaderIndex,
        ];
        
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);
        
        $viewModel = new ViewModel( $variables );
        $viewModel->setTemplate('lab-base/brand-feed/get-about');
        return $viewModel;
    }
    
    public function editAboutAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        if(!$req->isPost() || empty($req->getPost('content')) 
            || (empty($req->getPost('title')) || $req->getPost('aboutId') < 1)) {
            return new JsonModel([
                'status' => 'Please enter all fields.'
            ]);
        }

        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $brandAboutTable = $this->getServiceLocator()->get('BrandAboutTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to about us data.'
            ]);
        }

        $aboutId = $req->getPost('aboutId');
        
        $aboutData = $brandAboutTable->getBrandAboutById($aboutId);
        
        if($aboutData->creatorId != $loggedInUser->id)
        {
            return new JsonModel([
                'status' => 'You do not have permission for update this about us data.'
            ]);
        }
        
        if(!$aboutData)
        {
            return new JsonModel([
                'status' => 'You can not update this about us data because this data is not found in database.'
            ]);
        }
        
        $content = $req->getPost('content');
        $header = $req->getPost('header');
        $title = $req->getPost('title');

        $aboutData->aboutTitle = $title;
        $aboutData->aboutDescription = $content;
                
        $brandAboutTable->saveBrandAbout($aboutData);
        
        /*$aboutData->aboutFullDescription = $aboutData->aboutDescription;
        $aboutData->aboutDescription = $this->truncateString($aboutData->aboutDescription, 50, true);*/
        if(strlen($aboutData->aboutDescription) > 50)
        {
            $aboutData->aboutFullDescription = $aboutData->aboutDescription;
            $aboutData->aboutDescription = $this->truncateString($aboutData->aboutDescription, 50, true);
        }

        return new JsonModel([
            'status' => 'ok',
            'aboutData' => $aboutData
        ]);
    }
    
    public function deleteAboutAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $aboutId = $this->params()->fromRoute("aboutId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $brandAboutTable = $this->getServiceLocator()->get('BrandAboutTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisAbout = $brandAboutTable->getBrandAboutById($aboutId);

        if(!$thisAbout || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisAbout->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to about us.',
                'thisAbout' => $thisAbout,
                'loggedInUser' => $loggedInUser,
            ]);
        }
        
        if(!$thisAbout)
        {
            return new JsonModel([
                'status' => 'About us you select not found.',
            ]);
        }

        $brandAboutTable->deleteBrandAbout($thisAbout->id);

        return new JsonModel(['status' => 'ok']);
    }
    
    public function updateIndexAboutAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $brandAboutTable = $this->getServiceLocator()->get('BrandAboutTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to update about us data.'
            ]);
        }
        
        $sortOrders = $req->getPost('sortOrders');

        $sortOrders = json_decode($sortOrders);
        
        if(!empty($sortOrders))
        {
            foreach($sortOrders as $sortOrder)
            {
                if(is_array($sortOrder))
                {
                    foreach($sortOrder as $key => $so)
                    {
                        $aboutUsData = $brandAboutTable->getBrandAboutById($so->id);
                        $aboutUsData->sortOrder = $key;
                        $brandAboutTable->saveBrandAbout($aboutUsData);
                    }
                }
            }
        }
        
        return new JsonModel([
            'status' => 'ok',
        ]);
    }


    /* Management content */
    public function getAboutManagementContent() {

        /** @var BrandTeamMetaTable $brandTeamMetaTable */
        $brandTeamMetaTable = $this->getServiceLocator()->get('BrandTeamMetaTable');
        /** @var BrandTeamMemberTable $brandTeamMemberTable */
        $brandTeamMemberTable = $this->getServiceLocator()->get('BrandTeamMemberTable');

        $teamMetaKeys = array('TEAM_META_TITLE', 'TEAM_META_DESCRIPTION');
        $teamMetaArray = array();

        foreach($teamMetaKeys as $teamMetaKey)
        {
            $teamMetaList = $brandTeamMetaTable->getBrandTeamMetaByBrandAndKey($this->brand->id, $teamMetaKey);
            if(!empty($teamMetaList))
            {
                $teamMetaArray[$teamMetaKey]['content'] = $teamMetaList->teamValue;
                $teamMetaArray[$teamMetaKey]['public'] = $teamMetaList->public;
            }
        }
        
        $thisTeamMembers = $brandTeamMemberTable->getBrandTeamMemberByBrand($this->brand->id);

        $this->view->setTemplate('lab-base/brand-feed/management-team');
        return [
            'teamMetas' => $teamMetaArray,
            'teamMembers' => $thisTeamMembers,
        ];
    }
    
    public function addTeamMemberAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $brandTeamMemberTable = $this->getServiceLocator()->get('BrandTeamMemberTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to team member data.'
            ]);
        }
        
        $biography = $req->getPost('biography');
        $position = $req->getPost('position');
        $name = $req->getPost('name');
        $imageData = $req->getPost('imageData');

        $teamMember = new BrandTeamMember($loggedInUser->id, $thisBrand->id, $name, $biography, $position, $imageData);
        
        $brandTeamMemberTable->saveBrandTeamMember($teamMember);

        return new JsonModel([
            'status' => 'ok',
            'aboutUS' => $teamMember
        ]);
    }
    
    public function showTeamMemberDetailAction()
    {
        $tmId = $this->params()->fromRoute("tmId");
        
        if($tmId < 1)
        {
            return new JsonModel([
                'status' => 'Team member not found.'
            ]);
        }

        $req = $this->getRequest();
        
        $type = $req->getPost('type', 'show');
        
        $brandTeamMemberTable = $this->getServiceLocator()->get('BrandTeamMemberTable');
        
        $thisTeamMember = $brandTeamMemberTable->getBrandTeamMemberById($tmId);

        $variables = [
            'thisTeamMember' => $thisTeamMember,
            'type' => $type,
        ];
        
        $viewModel = new ViewModel( $variables );
        $viewModel->setTemplate('lab-base/brand-feed/show-team-member');
        return $viewModel;
    }
    
    public function editTeamMemberAction()
    {
        $tmId = $this->params()->fromRoute("tmId");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $brandTeamMemberTable = $this->getServiceLocator()->get('BrandTeamMemberTable');
        
        $tmData = $brandTeamMemberTable->getBrandTeamMemberById($tmId);
        
        if(!$tmData)
        {
            return new JsonModel([
                'status' => 'Team member not found.'
            ]);
        }
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandById($tmData->brandId);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to team member data.'
            ]);
        }
        
        $biography = $req->getPost('biography');
        $position = $req->getPost('position');
        $name = $req->getPost('name');
        $imageData = $req->getPost('imageData');

        $tmData->biography = $biography;
        $tmData->imageData = $imageData;
        $tmData->name = $name;
        $tmData->position = $position;
        
        $brandTeamMemberTable->saveBrandTeamMember($tmData);

        return new JsonModel([
            'status' => 'ok',
            'tmData' => $tmData
        ]);
    }
    
    public function deleteTeamMemberAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $tmId = $this->params()->fromRoute("tmId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $brandTeamMemberTable = $this->getServiceLocator()->get('BrandTeamMemberTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisTM = $brandTeamMemberTable->getBrandTeamMemberById($tmId);

        if(!$thisTM || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisTM->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.',
                'tmId' => $tmId,
                'loggedInUser' => $loggedInUser,
            ]);
        }
        
        if(!$thisTM)
        {
            return new JsonModel([
                'status' => 'Team member you select not found.',
            ]);
        }

        $brandTeamMemberTable->deleteBrandTeamMamber($thisTM->id);

        return new JsonModel(['status' => 'ok']);
    }
    
    public function addTeamMemberImageAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $imageSaver = $this->getServiceLocator()->get('ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $this->getServiceLocator()->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);


        $image = $imageSaver->uploadImageSimple($image, 'team-member', /*random*/null, $loggedInUser->id, $thisBrand->id);
        //$image = $imageSaver->uploadImage($image, 'team-member-image-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }
    
    public function manageBradsTeamMetaAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $req = $this->getRequest();
        
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);

        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $userTable = $this->getServiceLocator()->get('UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
        
        $brandTeamMetaTable = $this->getServiceLocator()->get('BrandTeamMetaTable');
        
        if($userCanEditBrand)
        {
            $type = $req->getPost('type');
            $content = $req->getPost('content');
            switch($type)
            {
                case 'title' :
                        $metaKey = 'TEAM_META_TITLE';
                        $thisTeamMeta = $brandTeamMetaTable->getBrandTeamMetaByBrandAndKey($thisBrand->id, $metaKey);
                        if(empty($thisTeamMeta))
                        {
                            $thisTeamMeta = new BrandTeamMeta($loggedInUser->id, $thisBrand->id, $metaKey, $content);
                        }
                        else
                        {
                            $thisTeamMeta->teamValue = $content;
                        }
                    break;
                    
                case 'description' :
                        $metaKey = 'TEAM_META_DESCRIPTION';
                        $thisTeamMeta = $brandTeamMetaTable->getBrandTeamMetaByBrandAndKey($thisBrand->id, $metaKey);
                        if(empty($thisTeamMeta))
                        {
                            $thisTeamMeta = new BrandTeamMeta($loggedInUser->id, $thisBrand->id, $metaKey, $content);
                        }
                        else
                        {
                            $thisTeamMeta->teamValue = $content;
                        }
                    break;
                
                case 'public_title' :
                        $metaKey = 'TEAM_META_TITLE';
                        $thisTeamMeta = $brandTeamMetaTable->getBrandTeamMetaByBrandAndKey($thisBrand->id, $metaKey);
                        
                        if(empty($thisTeamMeta))
                        {
                            $thisTeamMeta = new BrandTeamMeta($loggedInUser->id, $thisBrand->id, $metaKey, '');
                        }
                        $thisTeamMeta->public = $content;
                    break;
                
                case 'public_description' :
                        $metaKey = 'TEAM_META_DESCRIPTION';
                        $thisTeamMeta = $brandTeamMetaTable->getBrandTeamMetaByBrandAndKey($thisBrand->id, $metaKey);
                        if(empty($thisTeamMeta))
                        {
                            $thisTeamMeta = new BrandTeamMeta($loggedInUser->id, $thisBrand->id, $metaKey, '');
                        }
                        $thisTeamMeta->public = $content;
                    break;

                default:
                    return new JsonModel(['status' => 'Wrong value type']);
            }
            
            $brandTeamMetaTable->saveBrandTeamMeta($thisTeamMeta);
            return new JsonModel(['status' => 'ok', 'team_meta' => $thisTeamMeta]);
        }
        else
        {
            return new JsonModel(['status' => 'You have no access for this page.']);
        }
    }


    /* Products content; param1 -> categoryId; param2 -> collectionId */
    public function getProductsContent() {

        $categoryId = (int)$this->param1;
        
        $action = '';
        $collectionId = 0;
        $editProductId = 0;
        if($this->param2 == 'edit')
        {
            $action = $this->param2;
            $editProductId = $this->param3;
        }
        else
        {
            $collectionId = (int)$this->param2;
        }

        /** @var BrandProductsCategoryTable $productCategory */
        $productCategory = $this->sl->get('BrandProductsCategoryTable');
        /** @var BrandProductsCollectionTable $productCollection */
        $productCollection = $this->sl->get('BrandProductsCollectionTable');
        /** @var BrandPostTable $table */
        $table = $this->getServiceLocator()->get('BrandPostTable');

        $collections = array();
        if($this->userIsBrandAdmin)
        {
            $categoriesList = $productCategory->getCategoriesByBrand($this->brand->id);
            if($categoryId > 0)
            {
                $collections = $productCollection->getCollectionsByBrandByCategory($this->brand->id, $categoryId);
            }
        }
        else
        {
            $categoriesList = $productCategory->getCategoriesByBrandPublic($this->brand->id);
            if($categoryId > 0)
            {
                $collections = $productCollection->getCollectionsByBrandByCategoryPublic($this->brand->id, $categoryId);
            }
        }
        
        $categories = [];
        $defaultCategory = new BrandProductsCategory();
        $defaultCategory->id = 0;
        $defaultCategory->categoryName = "All Products";
        $defaultCategory->brandId = $this->brand->id;
        $defaultCategory->canDelete = 1;
        $defaultCategory->public = 1;
        $allProductsTab = array();
        
        if(!empty($categoriesList))
        {
            $categories[] = $defaultCategory;
            foreach($categoriesList as $cl)
            {
                $categories[] = $cl;
            }
        }
        
        if($collectionId > 0)
        {
            $collections = array($productCollection->getById($collectionId)); 
        }
        
        $productList = array();
        $allProducts = array();
        if($categoryId > 0 && $collectionId > 0)
        {
            $productList['collections_'.$collectionId] = $table->getBrandProductsByCategoryAndCollection($categoryId, $collectionId);
        }
        else if($categoryId > 0)
        {
            if(!empty($collections))
            {
                foreach($collections as $collection)
                {
                    $productList['collections_'.$collection->id] = $table->getBrandProductsByCategoryAndCollection($categoryId, $collection->id);
                }
            }
            else
            {
                $productList = $table->getBrandProductsByBrandAndCategory($this->brand->id, $categoryId);
            }
        }
        else
        {
            foreach($categories as $key => $category)
            {
                $catCollections = array();
                if($category->id > 0)
                {
                    $catCollections = $productCollection->getCollectionsByBrandByCategory($this->brand->id, $category->id);
                    $catProducts = $table->getBrandProductsByBrandAndCategory($this->brand->id, $category->id, 4);
                }
                else
                {
                    $catProducts = $table->getBrandProductsByBrandAndCategory($this->brand->id, 0);
                }
                
                $allProductsTab[$key] = $category;
                $allProductsTab[$key]->collections = $catCollections;
                $allProductsTab[$key]->products = $catProducts;
            }
        }
        /**
         * @var CommentTable $commentTable
         */
        $commentTable = $this->sl->get('CommentTable');
        if(!empty($productList))
        {
            foreach($productList as $key => $product)
            {
                $collectionBreak = explode("_", $key);
                if(count($collectionBreak) > 1 && $collectionBreak[0] == 'collections')
                {
                    if(!empty($product))
                    {
                        foreach($product as $pr)
                        {
                            $productCat = $productCategory->getById($pr->categoryId);
                            $pr->categoryName = $productCat->categoryName;
                            if($pr->collectionId > 0)
                            {
                                $productCollection1 = $productCollection->getById($pr->collectionId);
                                $pr->collectionName = $productCollection1->collectionName;
                            }
                            
                            $pr->postContent = $this->truncateString($pr->postContent, 80, true);
                        }
                    }
                }
                else
                {
                    $productCat = $productCategory->getById($product->categoryId);
                    $product->categoryName = ($product->categoryId > 0) ? $productCat->categoryName : "All Products";
                    if($product->collectionId > 0)
                    {
                        $productCollection1 = $productCollection->getById($product->collectionId);
                        $product->collectionName = $productCollection1->collectionName;
                    }
                    
                    $product->postContent = $this->truncateString($product->postContent, 80, true);

                }
            }
        }

        if(!empty($allProductsTab))
        {
            foreach($allProductsTab as $cat)
            {
                if(!empty($cat->products))
                {
                    foreach($cat->products as $product)
                    {
                        $product->commentsCnt = $commentTable->getTotalCommentsByPost($product->id, -1,-1,1);
                        //$product->postContent = $this->truncateString($product->postContent, 80, true);
                    }
                }
            }
        }
        
        $this->view->setTemplate('lab-base/brand-feed/products');

        return [
            'categories' => $categories,
            'collections' => $collections,
            'selCategory' => $categoryId,
            'selcollection' => $collectionId,
            
            'productList' => $productList,
            'allProducts' => $allProductsTab,
            
            'action' => $action,
            'editProductId' => $editProductId,
        ];
    }
    
    public function manageProductsCategoryAction()
    {
        $brandId = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $postDataCollection = $req->getPost('postData');
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $categoryTable = $this->getServiceLocator()->get('BrandProductsCategoryTable');
        $collectionTable = $this->getServiceLocator()->get('BrandProductsCollectionTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $action = $req->getPost('action');
        
        if($action == 'remove')
        {
            $categoryID = $req->getPost('categoryID');
            $categoryData = $categoryTable->getById($categoryID);
            if(isset($categoryData->canDelete) && $categoryData->canDelete)
            {
                return new JsonModel([
                    'status' => 'This is default category you can not delete this category.'
                ]);
            }
            else
            {
                $categoryTable->delete($categoryID);
            }
        }
        else if($action == 'public')
        {
            $categoryId = ($req->getPost('categoryId') > 0) ? $req->getPost('categoryId') : 0;
            $showOrNot = $req->getPost('showOrNot');
            $categoryTable->makeCategoryPublicOrNot($categoryId, $showOrNot);
        }
        else if($action == 'edit-category')
        {
            $categoryId = $req->getPost('categoryId', 0);
            
            $categoryData = $categoryTable->getById($categoryId);
            
            return new JsonModel([
                'status' => 'ok',
                'data' => $categoryData,
            ]);
        }
        else
        {
            $categoryName = $req->getPost('categoryName');
            $categoryId = ($req->getPost('categoryId') > 0) ? $req->getPost('categoryId') : 0;
            $category =  new BrandProductsCategory($thisBrand->id, $categoryName);
            if($categoryId > 0)
            {
                $category->id = $categoryId;
            }
            $categoryTable->save($category);
            
            if($action == 'add')
            {
                if(!empty($postDataCollection))
                {
                    foreach($postDataCollection as $pdCollection)
                    {
                        if(trim($pdCollection['value']) != "")
                        {
                            $collection =  new BrandProductsCollection($category->id, $thisBrand->id, $pdCollection['value']);
                            $collectionTable->save($collection);
                        }
                    }
                }
                /*$CollectionName = $req->getPost('collectionName');
                if($CollectionName != "")
                {
                    $collection =  new BrandProductsCollection($category->id, $thisBrand->id, $CollectionName);
                    $collectionTable->save($collection);
                }*/
            }
        }
        
        return new JsonModel([
            'status' => 'ok',
        ]);
    }
 
    public function manageProductsCollectionAction()
    {
        $brandId = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $categoryTable = $this->getServiceLocator()->get('BrandProductsCategoryTable');
        $collectionTable = $this->getServiceLocator()->get('BrandProductsCollectionTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $action = $req->getPost('action');
        
        if($action == 'remove')
        {
            $collectionID = $req->getPost('collectionID');
            $categoryData = $collectionTable->getById($collectionID);
            $collectionTable->delete($collectionID);
        }
        else if($action == 'public')
        {
            $collectionId = $req->getPost('collectionId', 0);
            $public = $req->getPost('public');
            $collectionTable->makeCollectionPublicOrNot($collectionId, $public);
        }
        else
        {
            $collectionId = $req->getPost('collectionId', 0);
            
            if($collectionId > 0)
            {
                $collectionName = $req->getPost('collectionName', '');
                $collectionData = $collectionTable->getById($collectionId);
                $collectionData->collectionName = $collectionName;
                $collection = $collectionData;
                $collectionTable->save($collection);
            }
            else
            {
                $categoryId = $req->getPost('categoryId', 0);
                $collections = $req->getPost('collections', array());
                
                $collection =  new BrandProductsCollection();
                foreach($collections as $single)
                {
                    $collection =  new BrandProductsCollection($categoryId, $thisBrand->id, $single);
                    $collectionTable->save($collection);
                }
            }            
            
        }
        
        return new JsonModel([
            'status' => 'ok',
        ]);
    }
    
    public function addProductImageAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $imageSaver = $this->getServiceLocator()->get('ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $this->getServiceLocator()->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);


        // uploadImageSimple($file, $imageClass, $imageId = null, $uploaderId, $brandId = null, $maxX = 4096, $maxY = 4096, $raw = false)
        $image = $imageSaver->uploadImageSimple($image, 'product', /*random*/null, $loggedInUser->id, $thisBrand->id);
        //$image = $imageSaver->uploadImage($image, 'product-image-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }
    
    public function addProductAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        if(!$req->isPost() || empty($req->getPost('content')) 
            || (empty($req->getPost('title')))) {
            return new JsonModel([
                'status' => 'Please enter all fields.'
            ]);
        }

        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        //$brandProductTable = $this->getServiceLocator()->get('BrandProductTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $categoryTable = $this->getServiceLocator()->get('BrandProductsCategoryTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.'
            ]);
        }

        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title', '');
        $categoryId = $req->getPost('category', 0);
        $collectionId = $req->getPost('collection', 0);
        $buyNow = $req->getPost('buyNow', 0);
        $radio = $req->getPost('radio', 0);
        $price = $req->getPost('price', 0);

        //$product = new BrandProduct($loggedInUser->id, $thisBrand->id, $categoryId, $collectionId, $title, $content, $imageData);
        /*$product = new BrandPost($loggedInUser->id, $thisBrand->id, $categoryId, $title, $content, $buyNow, $radio, $price, 'product', $imageData);*/
        $product = new BrandPost($loggedInUser->id, $thisBrand->id, $categoryId, $title, $content, 'product', $imageData, NULL, NULL, 0, $buyNow, $radio, $price);
        $product->collectionId = $collectionId;

        $brandPostTable->saveBrandPost($product);

        return new JsonModel([
            'status' => 'ok',
            'product' => $product
        ]);
    }
    
    public function editProductAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        if(!$req->isPost() || empty($req->getPost('content')) 
            || (empty($req->getPost('title'))) || $req->getPost('prodcutId') < 1) {
            return new JsonModel([
                'status' => 'Please enter all fields.'
            ]);
        }

        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        //$brandProductTable = $this->getServiceLocator()->get('BrandProductTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $categoryTable = $this->getServiceLocator()->get('BrandProductsCategoryTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.'
            ]);
        }

        $productId = $req->getPost('prodcutId');
        
        $productData = $brandPostTable->getBrandPostById($productId);
        
        if($productData->creatorId != $loggedInUser->id)
        {
            return new JsonModel([
                'status' => 'You do not have permission for update this product.'
            ]);
        }
        
        if(!$productData)
        {
            return new JsonModel([
                'status' => 'You can not update this product because this product not found in database.'
            ]);
        }
        
        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title', '');
        $categoryId = $req->getPost('category');
        $collectionId = $req->getPost('collection');
        $buyNow = $req->getPost('buyNow', 0);
        $radio = $req->getPost('radio', 0);
        $price = $req->getPost('price', 0);

        //$product = new BrandProduct($loggedInUser->id, $thisBrand->id, $categoryId, $collectionId, $title, $content, $imageData);
        $product = new BrandPost($loggedInUser->id, $thisBrand->id, $categoryId, $title, $content, $buyNow, $radio, $price, 'product', $imageData);
        $product->collectionId = $collectionId;

        if($productId > 0)
        {
            $product->id = $productId;
        }
        
        $brandPostTable->saveBrandPost($product);

        return new JsonModel([
            'status' => 'ok',
            'product' => $product
        ]);
    }
    
    public function getSingleProductAction()
    {
        $productId = $this->params()->fromRoute("productId");

        /** @var BrandPostTable $table */
        $table = $this->getServiceLocator()->get('BrandPostTable');
        $product = $table->getBrandPostById($productId);
        
        if($product)
        {
            return new JsonModel(['status' => 'ok', 'product' => $product]);
        }
        else
        {
            return new JsonModel(['status' => 'Product not found.']);
        }
    }
    
    public function deleteProductAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $productId = $this->params()->fromRoute("productId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisProduct = $brandPostTable->getBrandPostById($productId);

        if(!$thisProduct || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisProduct->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.',
                'thisPost' => $thisProduct,
                'loggedInUser' => $loggedInUser,
            ]);
        }

        $brandPostTable->deleteBrandPost($thisProduct->id);

        return new JsonModel(['status' => 'ok']);
    }
    
    /*END*/
    
     /***** *** Portfolio Section **** ****/
     
     public function getPortfolioContent() {
        /** @var BrandPostTable $table */
        $table = $this->getServiceLocator()->get('BrandPostTable');
        
        $portfolioProjectTable = $this->getServiceLocator()->get('PortfolioProjectTable');
        
        if(isset($this->user) && $this->user->id > 0)
        {
            $portfolioProjects = $portfolioProjectTable->getPortfolioProjectByBrand($this->brand->id);
        }
        else
        {
            $portfolioProjects = $portfolioProjectTable->getPortfolioProjectByBrand($this->brand->id, 1);
        }
        
        
        $portfolios = array();
        if(!empty($portfolioProjects))
        {
            foreach($portfolioProjects as $key => $singlePorject)
            {
                $portfolios[$key] = $singlePorject;
                
                if(isset($this->user) && $this->user->id > 0)
                {
                    $posts = $table->getBrandPortfolioByBrandAndCategory($this->brand->id, $singlePorject->id);
                }
                else
                {
                    $posts = $table->getBrandPortfolioByBrandAndCategory($this->brand->id, $singlePorject->id, 1);
                }
                
                foreach($posts as $post)
                {
                    $post->postContent = $this->truncateString($post->postContent, 250, true);
                }
                
                $portfolios[$key]->portfolio = $posts;
            }
        }
        //echo "<pre>"; print_r($portfolios); die;
        $this->view->setTemplate('lab-base/brand-feed/portfolio');

        return [
            'portfolio' => $portfolios
        ];
    }
    
    public function addPortfolioImageAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $imageSaver = $this->getServiceLocator()->get('ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $this->getServiceLocator()->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);


        // uploadImageSimple($file, $imageClass, $imageId = null, $uploaderId, $brandId = null, $maxX = 4096, $maxY = 4096, $raw = false)
        $image = $imageSaver->uploadImageSimple($image, 'portfolio', /*random*/null, $loggedInUser->id, $thisBrand->id);
        //$image = $imageSaver->uploadImage($image, 'product-image-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }
    
    public function addPortfolioAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $portfolioProjectTable = $this->getServiceLocator()->get('PortfolioProjectTable');
        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $projectName = $req->getPost('projectName');
        
        $projectId = 0;
        if(!is_numeric($projectName))
        {
            $portfolioProject = $this->createPortfolioProject($thisBrand->id, $projectName);
            $projectId = $portfolioProject->id;
        }
        else
        {
            $projectId = $projectName;
        }
        
        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title');
        $draft = $req->getPost('draft');

        $brandPost = new BrandPost($loggedInUser->id, $thisBrand->id, $projectId, $title, $content, 'portfolio', $imageData, NULL, NULL, $draft);

        $brandPostTable->saveBrandPost($brandPost);

        return new JsonModel([
            'status' => 'ok',
            'brandPost' => $brandPost
        ]);
    }
    
    public function deletePortfolioProjectAction()
    {
        $portfolioProjectTable = $this->getServiceLocator()->get('PortfolioProjectTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $projectId = $this->params()->fromRoute("projectId");
        
        $project = $portfolioProjectTable->getById($projectId);
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandById($project->brandId);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        if(empty($project))
        {
            return new JsonModel([
                'status' => 'Portfolio project not found.'
            ]);
        }
        
        $portfolioProjectTable->delete($project->id);
        
        return new JsonModel([
            'status' => 'ok',
        ]);
    }
    
    public function managePortfolioProjectAction()
    {
        $portfolioProjectTable = $this->getServiceLocator()->get('PortfolioProjectTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $projectId = $this->params()->fromRoute("projectId");
        
        $project = $portfolioProjectTable->getById($projectId);
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandById($project->brandId);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $req = $this->getRequest();
        $action = $req->getPost('action');
        
        switch($action)
        {
            case 'showHide' : 
                    $toggle = $req->getPost('toggle');
                    
                    $project->public = $toggle;
                    
                    $portfolioProjectTable->savePortfolioProject($project);
                break;
            
            case 'edit-portfolio' : 
                    $projectName = $req->getPost('projectName');
                    
                    $project->projectName = $projectName;
                    
                    $portfolioProjectTable->savePortfolioProject($project);
                break;
        }

     
        return new JsonModel([
            'status' => 'ok',
            'project' => $project
        ]);   
    }
    
    public function createPortfolioProject($brandId, $projectName)
    {
        $portfolioProjectTable = $this->getServiceLocator()->get('PortfolioProjectTable');
        
        $portfolioProject = new PortfolioProject($brandId, $projectName);
        
        $portfolioProjectTable->savePortfolioProject($portfolioProject);
        
        return $portfolioProject;
    }
    
     /* *** PORTFOLIO SECTION END *** */
    
    /*** ABOUT ME SECTION ***/
    public function addAboutmeImageAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $imageSaver = $this->getServiceLocator()->get('ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $this->getServiceLocator()->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);


        // uploadImageSimple($file, $imageClass, $imageId = null, $uploaderId, $brandId = null, $maxX = 4096, $maxY = 4096, $raw = false)
        $image = $imageSaver->uploadImageSimple($image, 'aboutme', /*random*/null, $loggedInUser->id, $thisBrand->id);
        //$image = $imageSaver->uploadImage($image, 'product-image-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }
    
    public function addAboutMeAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $aboutmeTable = $this->getServiceLocator()->get('AboutmeTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title');

        $aboutme = new Aboutme($loggedInUser->id, $thisBrand->id, $title, $content, $imageData);

        $aboutmeTable->saveAboutme($aboutme);

        return new JsonModel([
            'status' => 'ok',
            'service' => $aboutme
        ]);
    }
    
    public function editAboutMeAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        
        $aboutMeTable = $this->getServiceLocator()->get('AboutmeTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title');
        $id = $req->getPost('aboutMeId', 0);
        
        if($id < 0)
        {
            return new JsonModel([
                'status' => 'Invalid about me data.'
            ]);
        }
        
        $aboutmedata = $aboutMeTable->getAboutmeById($id);
        
        if(!$aboutmedata)
        {
            return new JsonModel([
                'status' => 'About me data not found.'
            ]);
        }
        
        $aboutmedata->id = $id;
        $aboutmedata->aboutmeTitle = $title;
        $aboutmedata->aboutmeDescription = $content;
        $aboutmedata->aboutmeImage = $imageData;

        $aboutMeTable->saveAboutme($aboutmedata);

        return new JsonModel([
            'status' => 'ok',
            'service' => $aboutmedata
        ]);
    }
    
    /** END **/
    
    /** Discover Brands **/
    public function discoveryAction() {
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('discovery');

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        
        $action = $this->params()->fromQuery('action');
        $action = (isset($action) && !empty(trim($action))) ? $action : 'popular';
        switch($action)
        {
            case 'popular' :
                    $brands = $brandTable->getPopularBrands();
                break;
            
            case 'latest' :
                    $brands = $brandTable->getLatestBrands();
                break;
                
            case 'new' :
                    $brands = $brandTable->getNewBrands();
                break;
            
            default : 
                    $brands = $brandTable->getAllBrands();
                break;
        }

        if(!empty($brands))
        {
            foreach($brands as $key => $brand)
            {
                $brandPost = $brandPostTable->getLetestStoryWithPhotoByBrand($brand->id, 1);
                if(!empty($brandPost))
                {
                    $brand->posts = $brandPost;
                }
                else
                {
                    unset($brands[$key]);
                }
            }
        }

        return ['brands' => $brands, 'action' => $action];
    }
    
    public function searchBrandsAction() {
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Search');

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        
        $q = trim($this->params()->fromQuery('q'));

        if(empty($q)) {
            $q = null;
            $brands = $brandTable->getAllBrands();
        }else {
            $brands = $brandTable->searchBrandsByName(trim($this->params()->fromQuery('q')));
        }

        if(!empty($brands))
        {
            foreach($brands as $key => $brand)
            {
                $brandPost = $brandPostTable->getLetestStoryWithPhotoByBrand($brand->id, 1);
                if(!empty($brandPost))
                {
                    $brand->posts = $brandPost;
                }
                else
                {
                    unset($brands[$key]);
                }
            }
        }
        
        if(count($brands) == 1){
            $this->redirect()->toRoute("brand", ['url' => $brands[0]->url]);
            return [];
        }elseif(count($brands) > 0) {
            return ['brands' => $brands, 'status' => 'ok', 'queryHtml' => $q ? htmlspecialchars($q) : $q];
        }else{
            $brands = $brandTable->getPopularBrands();

            if(!empty($brands))
            {
                foreach($brands as $key => $brand)
                {
                    $brandPost = $brandPostTable->getLetestStoryWithPhotoByBrand($brand->id, 1);
                    if(!empty($brandPost))
                    {
                        $brand->posts = $brandPost;
                    }
                    else
                    {
                        unset($brands[$key]);
                    }
                }
            }
            
            return ['brands' => $brands, 'status' => 'not found', 'queryHtml' => $q ? htmlspecialchars($q) : $q];
        }
    }
    public function autocompleteAction() {

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $q = trim($this->params()->fromPost('query'));

        if(empty($q)) {
            $q = null;
            $brands = [];
        }else {
            $brands = $brandTable->searchBrandsByName($q);
        }

        $items = [];
        $i = 0;
        foreach($brands as $brand) {
            if(++$i > 5) break;
            //if(!$brand->public) continue; // todo: standardise public
            $items[] = ['url' => $brand->url, 'text' => $brand->name];
        }

        return new JsonModel(['status' => 'ok', 'items' => $items]);
    }
    /* END */

    public function postModalAction()
    {
        /**
         * @var $brandPostTable BrandPostTable
         * @var $brandTable BrandTable
         */
        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $userTable = $this->getServiceLocator()->get('UserTable');
        $post_id= $this->params()->fromRoute("id");
        $post =  $brandPostTable->getBrandPostById($post_id);
        $brand = $brandTable->getBrandById($post->brandId);
        $user = $userTable->getLoggedInUser();

        if(!isset($post->loveOrNot)){
            $post->loveOrNot = 0;
        }
        if(!isset($post->loveCnt)){
            $post->loveCnt = 0;
        }
        $postImages = array();
        $sliderImages = "";
        $littleSliderIm = "";
        if(!empty($post->postImageData))
        {
            $postImages = json_decode($post->postImageData);
            $totalCount = count($postImages);

            $countPerRow = 2;
            if($totalCount == 1 || $totalCount == 2) {
                $countPerRow = 1;
            }
            $firstRowCount = 1;
            if($totalCount % 2 == 0) {
                $firstRowCount = $countPerRow;
            }
            $i = 0;
            foreach($postImages as $postImage)
            {
                if($i < $firstRowCount) {
                    $currMaxCount = $firstRowCount;
                    $currInRow = $i + 1;
                }else{
                    $currMaxCount = $countPerRow;
                    $currInRow = ($i - $firstRowCount) % $currMaxCount + 1;
                }
                $imageClass = "img-$currInRow-of-$currMaxCount";
                $imageUrl = $postImage->url;
                $lsiderActiveClass = ($i == 0) ? 'active' : '';
                $photoTitle = (isset($post->postTitle) && !empty($post->postTitle)) ? $post->postTitle : count($postImages).' photos uploaded';
                $PhotCpation = (isset($postImage->caption) && $postImage->caption != "") ? '<p>'.$postImage->caption.'</p>' : $post->postContent;
                $dateTimestamp = $post->dateCreated;
                $formattedTime = date("d-m-Y H:i", $dateTimestamp);

                $imgName = basename($postImage->url);

                $statusCSS='';
                $statusText='';
                switch ($post->radio){
                    case '1':
                        $statusCSS='background-color: #D9544F;';
                        $statusText='Sale';
                        break;
                    case '2':
                        $statusCSS='background-color: green;';
                        $statusText='New';
                        break;
                    case '3':
                        $statusCSS='background-color: #4169E1; width:120px;';
                        $statusText='Best Seller';
                        break;
                }
                $sliderImages .='<div style="left: 5%;top: 10px;color:black;visibility:visible;'.$statusCSS.'" class="preview-infogoods" align="center">'.$statusText.'</div>';


                $sliderImages .= '<div class="item '.$lsiderActiveClass.' item-active"  data-slide-number="'.$i.'" data-index="'.$i.'" data-post-id="'.$post->id.'">
                                            <img id="'.$i.'" src="'.$postImage->url.'" class="img-responsive">
                                            <input type="hidden" id="photoTitle_'.$i.'" value="'.$photoTitle.'"/>
                                            <input type="hidden" id="photoCaption_'.$i.'" value="'.$PhotCpation.'"/>
                                            <input type="hidden" id="photoLoveCnt_'.$i.'" value="'.$post->loveCnt.'"/>
                                            <input type="hidden" id="photoLove_'.$i.'" value="'.$post->loveOrNot.'"/>
                                            <input type="hidden" id="photoDateStamp_'.$i.'" value="'.$dateTimestamp.'"/>
                                            <input type="hidden" id="photoDateFormate_'.$i.'" value="'.$formattedTime.'"/>
                                            <input type="hidden" id="photoCommenstCnt_'.$i.'" value="'.(isset($post->commentsCnt) ? $post->commentsCnt : 0).'"/>
                                        </div>';

                $littleSliderIm .= '<li>
                                        <a id="carousel-selector-'.$i.'"  href="javascript:void(0)"  >
                                            <img src="'.$postImage->url.'" style="width:80px; height:60px;" data-post-id="'.$post->id.'" data-slider-item="'.$i.'" class="littleP img-responsive">
                                        </a>
                                  </li>';
                $i++;
            }
        }

        $this->view = new ViewModel();
        $this->view->setVariables([
            'post' => $post,
            'user' => $user,
            'brand' => $brand,
            'sliderImages' => $sliderImages,
            'littleSliderIm' => $littleSliderIm
        ]);
        $this->view->setTemplate('lab-base/brand-feed/post-modal');
        $content = $this->getServiceLocator()->get('viewrenderer')->render($this->view);
        /*echo $content;
        exit;*/
        return new JsonModel([
            'status' => 'ok',
            'content' => $content
        ]);
    }

    public function getMyBrandsContent()
    {
        $this->view->setTemplate('lab-base/brand-feed/mybrands');

        $request = $this->getRequest();
        $sl = $this->getServiceLocator();

        $page = $this->params()->fromRoute("param1", 0);
        $search = $this->params()->fromRoute("param2", 0);
        $filter = $this->params()->fromRoute("param3", 0);
        $last_read = $this->params()->fromRoute("param4", 0);

        /** @var BrandPostTable $brandPostTabl */
        $brandPostTabl = $this->getTable('BrandPost');
        /** @var UserTable $userTable */
        $userTable = $this->sl->get('UserTable');
        /** @var User $user */
        $user = $userTable->getLoggedInUser();
        $last_read_post = $user->lastReadPostTimestamp;
        $userTable->updateLastReadPostTimestamp($user->id);

        /** @var BrandTable $brandTable */
        $brandTable = $sl->get('BrandTable');
        /** @var BrandRelationTable $brandRelation */
        $brandRelation = $sl->get('BrandRelationTable');
        //filter page
        $brandRelation->setFromTime($last_read);
        if($page){
            $brandRelation->setLimit(6, ($page-1) * 6);
        }else{
            $brandRelation->setLimit(6);
        }
        //end filter page
        if($filter) {
            $brandRelation->setLimit(($page-1) * 6);
            $brandR = $brandRelation->getFilterByUser($user->id, $filter);
        }else if($search){
            $brandRelation->setLimit(($page-1) * 6);
            $brandR = $brandRelation->getSearchByUser($user->id, $search);
        }else{
            $brandR = $brandRelation->getByUser($user->id);
        }
        $brand = [];
        foreach ($brandR as $index => $v){ /** @var BrandRelation $v */
            $i = $v->brandId;
            $brand[$i] = $brandTable->getBrandById($v->brandId);
            $brand[$i]->brandRelation = $v;
            $brand[$i]->post = $brandPostTabl->getLastPostByBrand($v->brandId);
            $brand[$i]->post->shortContent = $this->truncateString($brand[$i]->post->postContent, 320, true);
            $brand[$i]->post->loveCnt = $this->getTable('BrandPostLoved')->postLoveCount($brand[$i]->post->id);
            $brand[$i]->post->commentsCnt = $this->getTable('Comment')->getTotalCommentsByPost($brand[$i]->post->id, -1, -1, 1);
            if($brand[$i]->hasBanner) {
                /** @var Image $bannerImage */
                $bannerImage = $this->getTable('Image')->getImageByUrl(ImageSaver::GetImageS3Path("brand-banner-" . $brand[$i]->id));

                if($bannerImage) {
                    $brand[$i]->bannerUrl = $bannerImage->getURL() . '?' . $bannerImage->dateCreated;
                }else{
                    $brand[$i]->bannerUrl = '';
                }
            }else{
                $brand[$i]->bannerUrl = '';
            }
        }

        return [
            'brands' => $brand,
            'page' => $page,
            'last_read_post' => $last_read_post
        ];
    }
}
