<?php
/**
 * Author: Sebi
 * Date: 2015-06-14
 * Time: 18:53
 */

namespace LabBase\Controller;


use LabBase\Model\BrandTable;
use LabBase\Model\ImageSaver;
use LabBase\Model\User;
use LabBase\Model\UserTable;
use LabBase\Model\Slim;
use Zend\Loader\Exception\SecurityException;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class UserSettingsController extends AbstractActionController {

    /** @var int Maximum avatar size in pixels */
    private static $USER_AVATAR_MAX_SIDE = 192;

    public function indexAction() {
        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');

        $user = $userTable->getLoggedInUser();
        if(!$user) {
            $this->redirect()->toRoute('root');
            return [];
        }

        $brandList = $brandTable->getBrandsByOwner($user->id);

        return [
            'brandList' => $brandList
        ];
    }

    public function changeAvatarAction() {
        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        $currUrl = $this->params('url', false);
        $isAsyncRequest = false;

        if($req->isPost()) {
            $avatarImage = $req->getPost('imageString', '');

            if(empty($avatarImage)) {
                $avatarImage = $req->getFiles('avatarImage');
            }else{
                $isAsyncRequest = true;
                $avatarImage = base64_decode($avatarImage);
            }

            // Try getting imageString from Slim
            if (empty($avatarImage)) {
                $images = Slim::getImages();
                if (empty(!$images)) {
                    $img = $images[0]['output'];
                    $avatarImage = $img['data'];
                }
            }

            if(empty($avatarImage)) {
                return new JsonModel(['status' => 'Image could not be uploaded.']);
            }
            /** @var $imageSaver ImageSaver */
            $imageSaver = $this->getServiceLocator()->get('LabBase\Model\ImageSaver');
            /** @var UserTable $userTable */
            $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

            $user = $userTable->getLoggedInUser();
            if (!$user) {
                return new JsonModel(['status' => 'An error has occurred. You are not logged in.']);
            }
            $img = $imageSaver->uploadImageSimple($avatarImage, 'user-avatar', $user->id, $user->id, null, null,
                UserSettingsController::$USER_AVATAR_MAX_SIDE, UserSettingsController::$USER_AVATAR_MAX_SIDE, false, true);
            //$img = $imageSaver->uploadImage($avatarImage, 'user-avatar-'.$user->id,
            //    UserSettingsController::$USER_AVATAR_MAX_SIDE, UserSettingsController::$USER_AVATAR_MAX_SIDE, $user->id);

            $user = $userTable->getLoggedInUser();
            if (!$user) {
                return new JsonModel(['status' => 'You are not logged in.']);
            }

            // todo: either check the return value or wrap in proper try/catch
            $imageSaver->uploadImage($avatarImage, 'user-avatar' . '-' . $user->id, UserSettingsController::$USER_AVATAR_MAX_SIDE, UserSettingsController::$USER_AVATAR_MAX_SIDE, $user->id);
        }elseif($this->params('param', false) == 'removeAvatar') {
            /** @var $imageSaver ImageSaver */
            $imageSaver = $this->getServiceLocator()->get('LabBase\Model\ImageSaver');
            /** @var UserTable $userTable */
            $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
            $user = $userTable->getLoggedInUser();
            if ($user) {
                $imageSaver->deleteImage('user-avatar-'.$user->id);
            }
        }
        //if($isAsyncRequest) {
            return new JsonModel(['status' => 'ok']);
        //}else{
        //    $this->redirect()->toRoute('root/userSettings');
        //    return [];
        //}
    }



    private function changeSettingsRequirePassword(User $user) {
        $pass = $this->params()->fromPost("password");
        if(!$user->checkPassword($pass)) {
            throw new SecurityException();
        }

    }

    public function changeSettingsAction() {
        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('UserTable');
        $user = $userTable->getLoggedInUser();
        if(!$user) {
            $this->redirect()->toRoute("signin");
            return null;
        }

        $setting = $this->params()->fromPost("setting", "");

        try {
            switch ($setting) {
                case "username": {
                    $this->changeSettingsRequirePassword($user);
                    $name = $this->params()->fromPost("username", "");
                    if(!is_string($name) || strlen($name) < 2) {
                        return new JsonModel(['status' => 'Your name needs to be at least 2 characters long']);
                    }
                    if(strlen($name) > 120) {
                        return new JsonModel(['status' => 'Your name needs to be under 120 characters long']);
                    }
                    $user->fullName = htmlspecialchars($name);
                    $userTable->saveUser($user);

                    break;
                }
                case "email": {
                    $this->changeSettingsRequirePassword($user);
                    $email = $this->params()->fromPost("email", "");

                    if(strlen($email) < 5) {
                        return new JsonModel(['status' => 'Your email needs to be at least 5 characters long']);
                    }
                    if(strlen($email) > 120) {
                        return new JsonModel(['status' => 'Your email cannot be longer than 120 characters.']);
                    }

                    if(preg_match("/^[^\\s@]+@[^\\s@]+\\.[^\\s@]+$/i", $email) == 0) {
                        return new JsonModel(['status' => "Given email is not valid. Please re-check the entered address."]);
                    }

                    $user->email = $email;
                    $user->confirmedEmail = $email;
                    //todo: email confirmation

                    $userTable->saveUser($user);

                    break;
                }
                case "password": {
                    $this->changeSettingsRequirePassword($user);

                    $pass = $this->params()->fromPost("newPassword", "");

                    if(strlen($pass) < 7 || strlen($pass) > 50) {
                        return new JsonModel(['status' => 'Your new password needs to be between 7 and 50 characters long.']);
                    }

                    $contains_lowercase = (preg_match("/[a-z]/", $pass) === 1) ? 1 : 0;
                    $contains_uppercase = (preg_match("/[A-Z]/", $pass) === 1) ? 1 : 0;
                    $contains_numbers   = (preg_match("/[0-9]/", $pass) === 1) ? 1 : 0;
                    $contains_special   = (preg_match("/[^a-zA-Z0-9]/", $pass) === 1) ? 1 : 0;
                    $contains_spaces    = preg_match('/\s/', $pass) === 1;

                    if($contains_spaces) {
                        return new JsonModel(['status' => 'Your new password cannot contain spaces']);
                    }
                    if($contains_lowercase + $contains_numbers + $contains_uppercase + $contains_special < 2) {
                        return new JsonModel(['status' => 'Your new password needs to contain at least two of the follwing: \n'
                            .'Uppercase letters, lowercase letters, numbers, or special characters (e.g. *,@!^)']);
                    }

                    $user->changePassword($pass);

                    $userTable->saveUser($user);

                    return new JsonModel(['status' => 'ok', 'redirect' => $this->url()->fromRoute('signin')]);
                }

                default:
                    return new JsonModel(['status' => 'An error has occured. Please try again or contact support if the problem persists.']);
            }
        }catch (SecurityException $ex) {
            return new JsonModel(['status' => 'Entered password is incorrect']);
        }


        return new JsonModel(['status' => 'ok']);
    }

    public function profileAction()
    {
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Profile');
        /**
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */
        $userTable = $this->getServiceLocator()->get('UserTable');
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $user = $userTable->getLoggedInUser();
        if(!$user){
            $this->redirect()->toRoute('sign-in');
        }

        return [
            'user' => $user,
            'brandList' => $brandTable->getBrandsByOwner($user->id)
        ];
    }

}