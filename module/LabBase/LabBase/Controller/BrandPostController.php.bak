<?php
/**
 * Author: Sebi
 * Date: 2014-12-05
 * Time: 20:34
 */

namespace LabBase\Controller;


use LabBase\Model\Brand;
use LabBase\Model\BrandPost;
use LabBase\Model\BrandPostTable;
use LabBase\Model\BrandPostLoved;
use LabBase\Model\BrandPostLovedTable;
use LabBase\Model\BrandTable;
use LabBase\Model\CategoryTable;
use LabBase\Model\Comment;
use LabBase\Model\CommentTable;
use LabBase\Model\ImageSaver;
use LabBase\Model\Notification;
use LabBase\Model\NotificationTable;
use LabBase\Model\User;
use LabBase\Model\UserTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class BrandPostController extends AbstractActionController {

    /**
     * @param Brand $brand
     * @param User $user
     * @param Comment $comment
     * @param CommentTable $commentTable
     * @return bool|JsonModel
     */
    private function manageCommentPreamble( &$brand, &$user, &$comment, &$commentTable ) {
        $commentId = (int)$this->params()->fromRoute("commentId");

        /**
         * @var $commentTable CommentTable
         * @var $brandTable BrandTable
         * @var $userTable UserTable
         */
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $user = $userTable->getLoggedInUser();
        if(!$user)
            return new JsonModel(['status' => 'You need to log in again.']);

        $commentTable = $this->getServiceLocator()->get('LabBase\Model\CommentTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $comment = $commentTable->getCommentById($commentId);
        if(!$comment)
            return new JsonModel(['status' => 'This comment doesn\'t exist or has been removed.']);

        $brand = $brandTable->getBrandByPostId($comment->postId);

        if(!$brand)
            return new JsonModel(['status' => 'The brand has been removed. Please notify an administrator if you think this is an error.']);

        if(!$brandTable->isBrandManagedByUser($brand->id, $user->id))
            return new JsonModel(['status' => 'You don\'t have the permission to manage comments. If you believe this is an error, please contact an administrator.']);

        return true;

    }

    public function approveCommentAction() {
        /**
         * @var $brand Brand
         * @var $user User
         * @var $comment Comment
         * @var $commentTable CommentTable
         */
        $brand = null;
        $user = null;
        $comment = null;
        $commentTable = null;

        $ret = $this->manageCommentPreamble($brand, $user, $comment, $commentTable);
        if($ret !== true) return $ret;

        $comment->rejected = false;
        $comment->approved = true;
        $commentTable->saveComment($comment);

        return new JsonModel(['status' => 'ok']);
    }

    public function rejectCommentAction() {
        /**
         * @var $brand Brand
         * @var $user User
         * @var $comment Comment
         * @var $commentTable CommentTable
         */
        $brand = null;
        $user = null;
        $comment = null;
        $commentTable = null;

        $ret = $this->manageCommentPreamble($brand, $user, $comment, $commentTable);
        if($ret !== true) return $ret;

        $comment->approved = false;
        $comment->rejected = true;
        $commentTable->saveComment($comment);

        return new JsonModel(['status' => 'ok']);
    }

    public function deleteCommentAction() {
        /**
         * @var $brand Brand
         * @var $user User
         * @var $comment Comment
         * @var $commentTable CommentTable
         */
        $brand = null;
        $user = null;
        $comment = null;
        $commentTable = null;

        $ret = $this->manageCommentPreamble($brand, $user, $comment, $commentTable);
        if($ret !== true) return $ret;

        $commentTable->deleteComment($comment->id);

        return new JsonModel(['status' => 'ok']);
    }


    private function getCommentsCommon($brandId = null) {
        /**
         * @var $commentTable CommentTable
         * @var $brandTable BrandTable
         * @var UserTable $userTable
         * @var BrandPostTable $brandPostTable
         * @var $req \Zend\Http\Request
         */

        $allBrandComments = $brandId !== null;

        $req = $this->getRequest();
        $firstId = $req->getPost('firstCommentId', -1);
        $lastId = $req->getPost('lastCommentId', -1);

        $commentTable = $this->getServiceLocator()->get('LabBase\Model\CommentTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $isManager = false;
        if($loggedInUser && $brandTable->isBrandManagedByUser($brandId, $loggedInUser->id)) {
            $isManager = true;
        }

        if(!$allBrandComments) {
            $postId = $this->params()->fromRoute("postId");
            $post = $brandPostTable->getBrandPostById($postId);
            if (!$post) {
                return new JsonModel(['status' => 'The post you tried to comment has been removed.']);
            }

            if (!$post->commentsEnabled) {
                return new JsonModel(['status' => 'Comments are disabled for this post.']);
            }
            $brandId = $post->brandId;
            $comments = $commentTable->getCommentsByPost($postId, $firstId, $lastId);
        }else{
            if(!$isManager) {
                return new JsonModel(['status' => 'Only brand managers can see this']);
            }

            $type = $this->params()->fromRoute('commentsType');
            $filterSeen = null;
            $filterApproved = null;
            $filterRejected = null;
            if($type == 'rejected') {
                $filterRejected = true;
            }elseif($type == 'approved') {
                $filterApproved = true;
            }elseif($type == 'latest') {
                $filterRejected = false;
            }

            $comments = $commentTable->getCommentsByBrand($brandId, $firstId, $lastId, $filterSeen, $filterApproved, $filterRejected);
            $commentTable->markBrandCommentsAndNotificationsAsSeen($brandId);
        }

        $clientComments = [];

        foreach($comments as $comment) {
            /** @var Comment $comment */
            if(!$isManager && $comment->approved !== true)
                continue;

            $id = $comment->id;
            $datePosted = $comment->dateCreated;
            $text = $comment->content;
            $displayName = $comment->posterDisplayName;
            $brandUrl = false;
            $seen     = $comment->seen === true;
            $isPublic = $comment->approved === true;
            $rejected = $comment->rejected === true;

            if($comment->posterBrandId) {
                $postingBrand = $brandTable->getBrandById($comment->posterBrandId);
                if(!$postingBrand) // Brand removed?
                    continue;
                $displayName = $postingBrand->name;
                $brandUrl =  $this->url()->fromRoute("root/brandFeed", ['brandUrl' => $postingBrand->url]);

            }elseif($comment->posterId) {
                $poster = $userTable->getUserById($comment->posterId);
                if(!$poster)
                    continue; // User removed
                $displayName = $poster->fullName;
            }

            $clientComments[] = [
                'id' => $id,
                'text' => $text,
                'name' => $displayName,
                'nameUrl' => $brandUrl,
                'datePosted' => $datePosted,
                'seen' => $seen,
                'public' => $isPublic,
                'rejected' => $rejected,
            ];
        }

        $ret = [
            'status' => 'ok',
            'comments' => $clientComments
        ];

        if(isset($postId)) {
            $ret['postId'] = $postId;
        }
        return new JsonModel($ret);
    }

    public function getBrandCommentsAction() {
        return $this->getCommentsCommon($this->params()->fromRoute("brandId"));
    }

    public function getCommentsAction() {
        return $this->getCommentsCommon();
    }

    public function postCommentAction() {
        /**
         * @var $commentTable CommentTable
         * @var $brandPostTable BrandPostTable
         * @var $brandTable BrandTable
         * @var $notificationTable NotificationTable
         * @var UserTable $userTable
         */
        $postId = $this->params()->fromRoute("postId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost() ||
            empty($req->getPost('text'))
        ) {
            return new JsonModel([
                'status' => 'Please enter the required fields.'
            ]);
        }


        $commentTable = $this->getServiceLocator()->get('LabBase\Model\CommentTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $notificationTable = $this->getServiceLocator()->get('LabBase\Model\NotificationTable');

        $post = $brandPostTable->getBrandPostById($postId);
        if(!$post) {
            return new JsonModel([
                'status' => 'The post doesn\'t exist.'
            ]);
        }
        if(!$post->commentsEnabled) {
            return new JsonModel([
                'status' => 'The post doesn\'t allow for comments.'
            ]);
        }

        $name = NULL;
        $email = NULL;
        $commentBrandId = NULL;
        $autoApprove = false;

        $loggedInUser = $userTable->getLoggedInUser();
        if($loggedInUser) {
            $loggedInUserId = $loggedInUser->id;
            if($brandTable->isBrandManagedByUser($post->brandId, $loggedInUser->id)) {
                $autoApprove = true;
                if($req->getPost('postAs', 'user') === 'brand') {
                    $commentBrandId = $post->brandId;
                }
            }
        }else{
            if(
                empty($req->getPost('name')) ||
                empty($req->getPost('email'))
            ) {
                return new JsonModel([
                    'status' => 'You need to provide a name and an email address, or sign in.'
                ]);
            }
            $name = $req->getPost("name");
            $email = $req->getPost("email");
            $loggedInUserId = NULL;
        }

        $content = $req->getPost('text');
        $content = htmlspecialchars($content);

        $comment = new Comment($loggedInUserId, $commentBrandId, $postId, $content, $name, $email);

        if($autoApprove) $comment->approved = true;

        $commentTable->saveComment($comment);


        // Notification
        $displayName = $comment->posterDisplayName;
        if($comment->posterBrandId) {
            $displayName = $brandTable->getBrandById($comment->posterBrandId)->name;
        }elseif($comment->posterId) {
            $poster = $userTable->getUserById($comment->posterId);
            if(!$poster)
                $displayName = '<i>User removed</i>';
            else
                $displayName = $poster->fullName;
        }

        $title = $post->postTitle;
        if(strlen($title) > 40) {
            $i = strpos($title, ' ', 30);
            if($i > 0) {
                $title = substr($title, 0, $i) . '...';
            }
        }
        $n = new Notification('<span class="author">' . $displayName . '</span><span class="action"> has commented on </span><span class="post">'.$title.'</span>', $comment->id, $post->id, $post->brandId);

        $notificationTable->saveNotification($n);

        return new JsonModel([
            'status' => 'ok',
            'postId' => $postId,
            'comments' => [$comment]
        ]);
    }

    /*
     * POST image - base64 string of the image data (POST)
     *  or
     * FILE image
     */
    public function addPostImageAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $imageSaver = $this->getServiceLocator()->get('LabBase\Model\ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $this->getServiceLocator()->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);


        $image = $imageSaver->uploadImage($image, 'post-image-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }

    /* Post data:
     *  title
     *  content
     *  cat - category url
     *  imageData - image names separated by a semicolon
     *  postType - 'quote', 'simple' or 'advanced' for now.
     *  allowComments
     */
    public function addPostAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        $postType = $req->getPost('postType');
        if(!$req->isPost() || empty($req->getPost('content')) || empty($postType)
            || ($postType != 'quote' && empty($req->getPost('title'))) || empty($req->getPost('cat'))) {
            return new JsonModel([
                'status' => 'Please enter all fields.'
            ]);
        }
        
        $postDraft = ($req->getPost('draft') != "" && $req->getPost('draft') == 'draft') ? 1 : 0;

        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\CategoryTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        $category = $categoryTable->getCategoryByUrlAndBrand($req->getPost('cat'), $thisBrand->id);
        if(!$category) {
            $category = $categoryTable->getCategoryByUrlAndBrand('our-stories', $thisBrand->id);
            if(!$category) {
                return new JsonModel([
                    'status' => 'You cannot post into this category.'
                ]);
            }
        }

        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title', '');

        // todo: proper image sanitation
        if($imageData !== null) {
            $imageData = addslashes($imageData);
        }


        $post = new BrandPost($loggedInUser->id, $thisBrand->id, $category->id, $title, $content, $req->getPost('postType', 'advanced'), $imageData, NULL, $postDraft);
        $post->commentsEnabled = (bool)$req->getPost("allowComments", FALSE);

        $brandPostTable->saveBrandPost($post);

        return new JsonModel([
            'status' => 'ok',
            'post' => $post
        ]);
    }

    public function editPostAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $postId = $this->params()->fromRoute("postId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost() || empty($req->getPost('content')) || empty($req->getPost('title'))) {
            return new JsonModel([
                'status' => 'Please enter all fields.'
            ]);
        }
        
        $isDraft = $req->getPost('drafts', 0);
        
        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisPost = $brandPostTable->getBrandPostById($postId);

        if(!$thisPost || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisPost->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        $thisPost->setPostContents($req->getPost('content'));
        $thisPost->setPostTitle($req->getPost('title'));
        $thisPost->setPostDraftsStatus($isDraft);
        

        $brandPostTable->saveBrandPost($thisPost);


        return new JsonModel(['status' => 'ok']);
    }

    public function deletePostAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $postId = $this->params()->fromRoute("postId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisPost = $brandPostTable->getBrandPostById($postId);

        if(!$thisPost || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisPost->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.',
                'thisPost' => $thisPost,
                'loggedInUser' => $loggedInUser,
            ]);
        }

        $brandPostTable->deleteBrandPost($thisPost->id);

        return new JsonModel(['status' => 'ok']);
    }
    
    public function setPostLovedAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var $brandPostLovedTable BrandPostlovedTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $postId = $this->params()->fromRoute("postId");
        $loveOrNot = $this->params()->fromRoute("loveOrNot");
        
        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandPostLovedTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostlovedTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        if(!$postId) {
            return new JsonModel(["status" => "An error has occurred."]);
        }
        
        $loggedInUser = $userTable->getLoggedInUser();
        if(!$loggedInUser) {
            return new JsonModel(["status" => "Please log in to love this post."]);
        }
        
        
        $thisPost = $brandPostTable->getBrandPostById($postId);
        $brand = $brandTable->getBrandById($thisPost->brandId);
        if(!$brand) {
            return new JsonModel(["status" => "An error has occurred, this brand does not exist."]);
        }
        
        $lovePostData = $brandPostLovedTable->getByUserBrandPost($loggedInUser->id, $thisPost->brandId, $postId);
        
        $loved = new BrandPostLoved($loggedInUser->id, $thisPost->brandId, $postId, $loveOrNot);
        
        $loved->id = isset($lovePostData->id) ? $lovePostData->id : 0;
        $lovedTbl = $brandPostLovedTable->save($loved);
        $isLove = false;
        if($loveOrNot)
        {
            $isLove = true;
        }
        $loveCnt = $brandPostLovedTable->postLoveCount($postId);

        return new JsonModel(['status' => 'ok', 'loveCount' => $loveCnt, 'is_love' => $isLove]);
    }
}
