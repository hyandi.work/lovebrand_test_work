<?php
/**
 * Author: Sebi
 * Date: 2015-06-14
 * Time: 18:53
 */

namespace LabBase\Controller;


use LabBase\Model\ImageSaver;
use LabBase\Model\UserTable;
use Zend\Mvc\Controller\AbstractActionController;

class StaticContentController extends AbstractActionController {

    public function brandOwnerHelpAction() {
        return [];
    }

    public function termsOfServiceAction() {
        return [];
    }

}