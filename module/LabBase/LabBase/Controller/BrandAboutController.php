<?php
/**
 * Author: Sebi
 * Date: 2014-12-05
 * Time: 20:34
 */

namespace LabBase\Controller;

use LabBase\Model\BrandTable;
use LabBase\Model\Brand;
use LabBase\Model\BrandAbout;
use LabBase\Model\BrandAboutTable;
use LabBase\Model\BrandTeamMeta;
use LabBase\Model\BrandTeamMetaTable;
use LabBase\Model\BrandTeamMember;
use LabBase\Model\BrandTeamMemberTable;
use LabBase\Model\UserTable;
use LabBase\Model\ImageSaver;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class BrandAboutController extends AbstractActionController {
    
    public function indexAction() {
          /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);

        $about = new BrandAbout();
        
        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $brandAboutTable = $this->getServiceLocator()->get('LabBase\Model\BrandAboutTable');

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);
        
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
    
        $aboutHeaders = $about->getAboutHeaders();        
        $aboutLists = $brandAboutTable->getBrandAboutByBrand($thisBrand->id);
        
        $aboutHeaderTitle = $aboutHeaders[1];
        $aboutHeaderIndex = 1;
        
        if(!empty($aboutLists))
        {
            foreach($aboutLists as $aboutList)
            {
                $nextHrd = 0;
                if($aboutList->header > 0)
                {
                    $nextHrd = $this->get_next($aboutHeaders, $aboutList->header);
                }
                
                if(!empty($nextHrd))
                {
                    $aboutHeaderTitle = $nextHrd;
                    $aboutHeaderIndex = array_flip($aboutHeaders)[$nextHrd];
                }
                else
                {
                    $aboutHeaderTitle = 'Additional Content';
                    $aboutHeaderIndex = 0;
                }
            }
        }
        
    
        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/feed-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);

        $managementTeamName = ($thisBrand->managementTeamName != "") ? $thisBrand->managementTeamName : 'Management Team';
        
        $view = new ViewModel([
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
            
            'aboutLists' => $aboutLists,
            'aboutHeaderTitle' => $aboutHeaderTitle,
            'aboutHeaderIndex' => $aboutHeaderIndex,
            
            'managementTeamName' => $managementTeamName,
        ]);

        return $view;
    }
    
    public function addAboutAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        
        $brandAboutTable = $this->getServiceLocator()->get('LabBase\Model\BrandAboutTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to about us data.'
            ]);
        }
        
        $content = $req->getPost('content');
        $header = $req->getPost('header');
        $title = $req->getPost('title');

        $sortOrder = $brandAboutTable->getMaxSortOrder($thisBrand->id);
        
        $sort = (isset($sortOrder[0]->sortOrder)) ? ($sortOrder[0]->sortOrder + 1) : 0;
/*        $sort = ($header > 0 && $header < 6) ? $header : 6;*/
        $header = ($header != "") ? $header : 'Additional Content';
       
        
        $aboutUS = new BrandAbout($loggedInUser->id, $thisBrand->id, $header, $title, $content, $sort);
        $aboutUS->sortOrder = $sort;
        
        $aboutHeaders = $aboutUS->getAboutHeaders();
        
        $selHeader = array_flip($aboutHeaders)[$header];
        $nextHrd = '';
        if($selHeader > 0)
        {
            $nextHrd = $this->get_next($aboutHeaders, $selHeader);
        }
        
        if(!empty($nextHrd))
        {
            $aboutHeaderTitle = $nextHrd;
            $aboutHeaderIndex = array_flip($aboutHeaders)[$nextHrd];
        }
        else
        {
            $aboutHeaderTitle = 'Additional Content';
            $aboutHeaderIndex = 0;
        }

        $brandAboutTable->saveBrandAbout($aboutUS);
    
        return new JsonModel([
            'status' => 'ok',
            'aboutUS' => $aboutUS,
            'headerTitle' => $aboutHeaderTitle,
            'headerIndex' => $aboutHeaderIndex,
        ]);
    } 
    
    public function updateIndexAboutAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        
        $brandAboutTable = $this->getServiceLocator()->get('LabBase\Model\BrandAboutTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to update about us data.'
            ]);
        }
        
        $sortOrders = $req->getPost('sortOrders');

        $sortOrders = json_decode($sortOrders);
        
        if(!empty($sortOrders))
        {
            foreach($sortOrders as $sortOrder)
            {
                if(is_array($sortOrder))
                {
                    foreach($sortOrder as $key => $so)
                    {
                        $aboutUsData = $brandAboutTable->getBrandAboutById($so->id);
                        $aboutUsData->sortOrder = $key;
                        $brandAboutTable->saveBrandAbout($aboutUsData);
                    }
                }
            }
        }
        
        return new JsonModel([
            'status' => 'ok',
        ]);
    }
    
    public function deleteAboutAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $aboutId = $this->params()->fromRoute("aboutId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $brandAboutTable = $this->getServiceLocator()->get('LabBase\Model\BrandAboutTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisAbout = $brandAboutTable->getBrandAboutById($aboutId);

        if(!$thisAbout || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisAbout->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to about us.',
                'thisAbout' => $thisAbout,
                'loggedInUser' => $loggedInUser,
            ]);
        }
        
        if(!$thisAbout)
        {
            return new JsonModel([
                'status' => 'About us you select not found.',
            ]);
        }

        $brandAboutTable->deleteBrandAbout($thisAbout->id);

        return new JsonModel(['status' => 'ok']);
    }
    
    public function getPopupAboutAction()
    {
        $aboutId = $this->params()->fromRoute("aboutId");
        
        $brandAboutTable = $this->getServiceLocator()->get('LabBase\Model\BrandAboutTable');
    
        $about = $brandAboutTable->getBrandAboutById($aboutId);
        
        $userCanPost = false;
        $userCanEditBrand = false;
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandById($about->brandId);
       
       // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }

        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }   
        }
        
        $aboutData = new BrandAbout();
        
        $aboutHeaders = $aboutData->getAboutHeaders();
        if($about->header > 0 && $about->header < 6)
        {
            $aboutHeaderTitle = $aboutHeaders[$about->header];
            $aboutHeaderIndex = $about->header;
        }
        else
        {
            $aboutHeaderTitle = 'Additional Contents';
            $aboutHeaderIndex = 0;
        }
     
        $this->layout()->setTemplate('lab-base/brand-about/get-popup-about'); 
       
         $view = new ViewModel([
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,
            
            'about' => $about,
            'aboutHeaderTitle' => $aboutHeaderTitle,
            'aboutHeaderIndex' => $aboutHeaderIndex,
        ]);

        return $view;
    }
    
    public function editAboutAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        if(!$req->isPost() || empty($req->getPost('content')) 
            || (empty($req->getPost('title')) || $req->getPost('aboutId') < 1)) {
            return new JsonModel([
                'status' => 'Please enter all fields.'
            ]);
        }

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        
        $brandAboutTable = $this->getServiceLocator()->get('LabBase\Model\BrandAboutTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to about us data.'
            ]);
        }

        $aboutId = $req->getPost('aboutId');
        
        $aboutData = $brandAboutTable->getBrandAboutById($aboutId);
        
        if($aboutData->creatorId != $loggedInUser->id)
        {
            return new JsonModel([
                'status' => 'You do not have permission for update this about us data.'
            ]);
        }
        
        if(!$aboutData)
        {
            return new JsonModel([
                'status' => 'You can not update this about us data because this data is not found in database.'
            ]);
        }
        
        $content = $req->getPost('content');
        $header = $req->getPost('header');
        $title = $req->getPost('title');

        $aboutData->aboutTitle = $title;
        $aboutData->aboutDescription = $content;
                
        $brandAboutTable->saveBrandAbout($aboutData);

        return new JsonModel([
            'status' => 'ok',
            'aboutData' => $aboutData
        ]);
    }
    
    public function managementTeamAction() {
          /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);

        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $brandTeamMetaTable = $this->getServiceLocator()->get('LabBase\Model\BrandTeamMetaTable');
        $brandTeamMemberTable = $this->getServiceLocator()->get('LabBase\Model\BrandTeamMemberTable');

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);
        
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
    
        $teamMetaKeys = array('TEAM_META_TITLE', 'TEAM_META_DESCRIPTION');
        
        $teamMetaArray = array();
        foreach($teamMetaKeys as $teamMetaKey)
        {
            $teamMetaList = $brandTeamMetaTable->getBrandTeamMetaByBrandAndKey($thisBrand->id, $teamMetaKey);
            if(!empty($teamMetaList))
            {
                $teamMetaArray[$teamMetaKey]['content'] = $teamMetaList->teamValue;
                $teamMetaArray[$teamMetaKey]['public'] = $teamMetaList->public;
            }
        }
        
        $thisTeamMembers = $brandTeamMemberTable->getBrandTeamMemberByBrand($thisBrand->id);

        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/feed-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);
        
        $managementTeamName = ($thisBrand->managementTeamName != "") ? $thisBrand->managementTeamName : 'Management Team';
        
        $view = new ViewModel([
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
            
            'managementTeamName' => $managementTeamName,
            'teamMetas' => $teamMetaArray,
            'teamMembers' => $thisTeamMembers,
        ]);

        return $view;
    }
    
    public function updateTeamNameAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $userCanPost = false;
        $userCanEditBrand = false;
        $req = $this->getRequest();
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);

        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
        }
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to update team name.'
            ]);
        }
        
        $teamName = $req->getPost('teamName');
        $thisBrand->managementTeamName = $teamName;
        
        $brandTable->saveBrand($thisBrand);
        
        return new JsonModel([
            'status' => 'ok',
            'teamName' => $teamName
        ]);
    }
    
    public function manageBradsTeamMetaAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $req = $this->getRequest();
        
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);

        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
        
        $brandTeamMetaTable = $this->getServiceLocator()->get('LabBase\Model\BrandTeamMetaTable');
        
        if($userCanEditBrand)
        {
            $type = $req->getPost('type');
            $content = $req->getPost('content');
            switch($type)
            {
                case 'title' :
                        $metaKey = 'TEAM_META_TITLE';
                        $thisTeamMeta = $brandTeamMetaTable->getBrandTeamMetaByBrandAndKey($thisBrand->id, $metaKey);
                        if(empty($thisTeamMeta))
                        {
                            $thisTeamMeta = new BrandTeamMeta($loggedInUser->id, $thisBrand->id, $metaKey, $content);
                        }
                        else
                        {
                            $thisTeamMeta->teamValue = $content;
                        }
                    break;
                    
                case 'description' :
                        $metaKey = 'TEAM_META_DESCRIPTION';
                        $thisTeamMeta = $brandTeamMetaTable->getBrandTeamMetaByBrandAndKey($thisBrand->id, $metaKey);
                        if(empty($thisTeamMeta))
                        {
                            $thisTeamMeta = new BrandTeamMeta($loggedInUser->id, $thisBrand->id, $metaKey, $content);
                        }
                        else
                        {
                            $thisTeamMeta->teamValue = $content;
                        }
                    break;
                
                case 'public_title' :
                        $metaKey = 'TEAM_META_TITLE';
                        $thisTeamMeta = $brandTeamMetaTable->getBrandTeamMetaByBrandAndKey($thisBrand->id, $metaKey);
                        
                        if(empty($thisTeamMeta))
                        {
                            $thisTeamMeta = new BrandTeamMeta($loggedInUser->id, $thisBrand->id, $metaKey, '');
                        }
                        $thisTeamMeta->public = $content;
                    break;
                
                case 'public_description' :
                        $metaKey = 'TEAM_META_DESCRIPTION';
                        $thisTeamMeta = $brandTeamMetaTable->getBrandTeamMetaByBrandAndKey($thisBrand->id, $metaKey);
                        if(empty($thisTeamMeta))
                        {
                            $thisTeamMeta = new BrandTeamMeta($loggedInUser->id, $thisBrand->id, $metaKey, '');
                        }
                        $thisTeamMeta->public = $content;
                    break;

                default:
                    return new JsonModel(['status' => 'Wrong value type']);
            }
            
            $brandTeamMetaTable->saveBrandTeamMeta($thisTeamMeta);
            return new JsonModel(['status' => 'ok']);
        }
        else
        {
            return new JsonModel(['status' => 'You have no access for this page.']);
        }
    }
    
    public function addTeamMemberAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        
        $brandTeamMemberTable = $this->getServiceLocator()->get('LabBase\Model\BrandTeamMemberTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to team member data.'
            ]);
        }
        
        $biography = $req->getPost('biography');
        $position = $req->getPost('position');
        $name = $req->getPost('name');
        $imageData = $req->getPost('imageData');

        $teamMember = new BrandTeamMember($loggedInUser->id, $thisBrand->id, $name, $biography, $position, $imageData);
        
        $brandTeamMemberTable->saveBrandTeamMember($teamMember);

        return new JsonModel([
            'status' => 'ok',
            'aboutUS' => $teamMember
        ]);
    }
    
    public function editTeamMemberAction()
    {
        $tmId = $this->params()->fromRoute("tmId");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        
        $brandTeamMemberTable = $this->getServiceLocator()->get('LabBase\Model\BrandTeamMemberTable');
        
        $tmData = $brandTeamMemberTable->getBrandTeamMemberById($tmId);
        
        if(!$tmData)
        {
            return new JsonModel([
                'status' => 'Team member not found.'
            ]);
        }
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandById($tmData->brandId);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to team member data.'
            ]);
        }
        
        $biography = $req->getPost('biography');
        $position = $req->getPost('position');
        $name = $req->getPost('name');
        $imageData = $req->getPost('imageData');

        $tmData->biography = $biography;
        $tmData->imageData = $imageData;
        $tmData->name = $name;
        $tmData->position = $position;
        
        $brandTeamMemberTable->saveBrandTeamMember($tmData);

        return new JsonModel([
            'status' => 'ok',
            'tmData' => $tmData
        ]);
    }
    
    public function addTeamMemberImageAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $imageSaver = $this->getServiceLocator()->get('LabBase\Model\ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $this->getServiceLocator()->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);

        $image = $imageSaver->uploadImageSimple($image, 'team-member', /*random*/null, $loggedInUser->id, $thisBrand->id);
        //$image = $imageSaver->uploadImage($image, 'team-member-image-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }
    
    public function showTeamMemberDetailAction()
    {
        $tmId = $this->params()->fromRoute("tmId");
        
        if($tmId < 1)
        {
            return new JsonModel([
                'status' => 'Team member not found.'
            ]);
        }
        
        $req = $this->getRequest();
        
        $type = $req->getPost('type', 'show');
        
        $brandTeamMemberTable = $this->getServiceLocator()->get('LabBase\Model\BrandTeamMemberTable');
        
        $thisTeamMember = $brandTeamMemberTable->getBrandTeamMemberById($tmId);
        
        $view = new ViewModel([
            'thisTeamMember' => $thisTeamMember,
            'type' => $type,
        ]);

        return $view;
    }
    
    public function deleteTeamMemberAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $tmId = $this->params()->fromRoute("tmId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $brandTeamMemberTable = $this->getServiceLocator()->get('LabBase\Model\BrandTeamMemberTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisTM = $brandTeamMemberTable->getBrandTeamMemberById($tmId);

        if(!$thisTM || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisTM->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.',
                'tmId' => $tmId,
                'loggedInUser' => $loggedInUser,
            ]);
        }
        
        if(!$thisTM)
        {
            return new JsonModel([
                'status' => 'Team member you select not found.',
            ]);
        }

        $brandTeamMemberTable->deleteBrandTeamMamber($thisTM->id);

        return new JsonModel(['status' => 'ok']);
    }
    
    
    private function truncateString($str, $chars, $to_space, $replacement="...") {
       if($chars > strlen($str)) return $str;

       $str = substr($str, 0, $chars);

       $space_pos = strrpos($str, " ");
       if($to_space && $space_pos >= 0) {
           $str = substr($str, 0, strrpos($str, " "));
       }

       return($str . $replacement);
    }
    
    private function get_next($array, $key) {
       $currentKey = key($array);
       while ($currentKey !== null && $currentKey != $key) {
           next($array);
           $currentKey = key($array);
       }
       return next($array);
    }
}
