<?php
/**
 * Author: Luksor
 * Date: 2015-10-29
 * Time: 14:50
 */

namespace LabBase\Controller;

use LabBase\Model\BrandTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class BrandSearchController extends AbstractActionController {

    public function indexAction() {
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Discover');

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $brands = $brandTable->getAllBrands();

        return ['brands' => $brands];
    }

    public function searchAction() {
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Search');

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $q = $this->params()->fromQuery('q');

        if(empty($q)) {
            $q = null;
            $brands = $brandTable->getAllBrands();
        }else {
            $brands = $brandTable->searchBrandsByName($this->params()->fromQuery('q'));
        }

        if(count($brands) == 1) {
            $this->redirect()->toRoute("root/brandFeed", ['brandUrl' => $brands[0]->url]);
            return [];
        }elseif(count($brands) > 0) {
                return ['brands' => $brands, 'status' => 'ok', 'queryHtml' => $q ? htmlspecialchars($q) : $q];
        }else{
            $brands = $brandTable->getNBrands(5);

            return ['brands' => $brands, 'status' => 'not found', 'queryHtml' => $q ? htmlspecialchars($q) : $q];
        }

    }

    public function autocompleteAction() {

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $q = $this->params()->fromPost('query');

        if(empty($q)) {
            $q = null;
            $brands = [];
        }else {
            $brands = $brandTable->searchBrandsByName($q);
        }

        $items = [];
        $i = 0;
        foreach($brands as $brand) {
            if(++$i > 5) break;
            //if(!$brand->public) continue; // todo: standardise public
            $items[] = ['url' => $brand->url, 'text' => $brand->name];
        }

        return new JsonModel(['status' => 'ok', 'items' => $items]);
    }

}