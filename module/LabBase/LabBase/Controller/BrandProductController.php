<?php
/**
 * Author: Sebi
 * Date: 2014-12-05
 * Time: 20:34
 */

namespace LabBase\Controller;

use LabBase\Model\BrandPost;
use LabBase\Model\BrandPostLoved;
use LabBase\Model\BrandPostTable;
use LabBase\Model\BrandProductLoved;
use LabBase\Model\BrandProductLovedTable;
use LabBase\Model\BrandProductsCategory;
use LabBase\Model\BrandProductsCategoryTable;
use LabBase\Model\BrandProductsCollection;
use LabBase\Model\BrandProductsCollectionTable;
use LabBase\Model\BrandProduct;
use LabBase\Model\BrandProductTable;
use LabBase\Model\ProductsComment;
use LabBase\Model\ProductsCommentTable;
use LabBase\Model\BrandPostLovedTable;
use LabBase\Model\BrandRelationTable;
use LabBase\Model\BrandTable;
use LabBase\Model\Category;
use LabBase\Model\CategoryTable;
use LabBase\Model\Notification;
use LabBase\Model\UserTable;
use LabBase\Model\ImageSaver;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class BrandProductController extends AbstractActionController {
    
    public function indexAction() {
          /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("brandUrl");
        $categoryId = $this->params()->fromRoute("categoryId", 0);
        $collectionId = $this->params()->fromRoute("collectionId", 0);
        
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);

        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $productCategory = $this->getServiceLocator()->get('LabBase\Model\BrandProductsCategoryTable');
        $productCollection = $this->getServiceLocator()->get('LabBase\Model\BrandProductsCollectionTable');

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);

        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandProductTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductTable');

        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
        
        $collections = array();
        if($userCanEditBrand)
        {
            $categoriesList = $productCategory->getCategoriesByBrand($thisBrand->id);
            if($categoryId > 0)
            {
                $collections = $productCollection->getCollectionsByBrandByCategory($thisBrand->id, $categoryId); 
            }
        }
        else
        {
            $categoriesList = $productCategory->getCategoriesByBrandPublic($thisBrand->id);
            if($categoryId > 0)
            {
                $collections = $productCollection->getCollectionsByBrandByCategoryPublic($thisBrand->id, $categoryId); 
            }
        }
        
        $categories = [];
        $defaultCategory = new BrandProductsCategory();
        $defaultCategory->id = 0;
        $defaultCategory->categoryName = "All Products";
        $defaultCategory->brandId = $thisBrand->id;
        $defaultCategory->canDelete = 1;
        $defaultCategory->public = 1;
        $categories[] = $defaultCategory;
        if(!empty($categoriesList))
        {
            foreach($categoriesList as $cl)
            {
                $categories[] = $cl;
            }
        }
        
        if($collectionId > 0)
        {
            $collections = array($productCollection->getById($collectionId)); 
        }
        
        $productList = array();
        $allProducts = array();
        if($categoryId > 0 && $collectionId > 0)
        {
            $productList['collections_'.$collectionId] = $brandProductTable->getBrandProductsByCategoryAndCollection($categoryId, $collectionId);
        }
        else if($categoryId > 0)
        {
            if(!empty($collections))
            {
                foreach($collections as $collection)
                {
                    $productList['collections_'.$collection->id] = $brandProductTable->getBrandProductsByCategoryAndCollection($categoryId, $collection->id);
                }
            }
            else
            {
                $productList = $brandProductTable->getBrandProductsByBrandAndCategory($thisBrand->id, $categoryId);
            }
        }
        else
        {
            foreach($categories as $category)
            {
                if($category->id > 0)
                {
                    $catCollections = $productCollection->getCollectionsByBrandByCategory($thisBrand->id, $category->id); 
                    
                    $collect = array();
                    $collectProducts = array();
                    if(!empty($catCollections))
                    {
                        $collProducts = array();
                        foreach($catCollections as $catCollect)
                        {
                            $collect[] = $catCollect;
                            $collectProducts = $brandProductTable->getBrandProductsByCategoryAndCollection($category->id, $catCollect->id);
                            
                            if(!empty($collectProducts))
                            {
                                foreach($collectProducts as $collectProduct)
                                {
                                    $collectProduct->productContent = $this->truncateString($collectProduct->productContent, 80, true);
                                }
                            }
                            $collProducts['collections_'.$catCollect->id] = $collectProducts;
                        }
                    }
                    
                    $allProducts[$category->id]['collections'] = $collect;
                    $allProducts[$category->id]['products'] = $collProducts;
                }
                else
                {
                    $productList = $brandProductTable->getBrandProductsByBrandAndCategory($thisBrand->id, $category->id);
                }
            }
        }
        
/*        echo "<pre>"; print_r($allProducts); die;*/
        
        if(!empty($productList))
        {
            foreach($productList as $key => $product)
            {
                $collectionBreak = explode("_", $key);
                if(count($collectionBreak) > 1 && $collectionBreak[0] == 'collections')
                {
                    if(!empty($product))
                    {
                        foreach($product as $pr)
                        {
                            $productCat = $productCategory->getById($pr->categoryId);
                            $pr->categoryName = $productCat->categoryName;
                            if($pr->collectionId > 0)
                            {
                                $productCollection1 = $productCollection->getById($pr->collectionId);
                                $pr->collectionName = $productCollection1->collectionName;
                            }
                            
                            $pr->productContent = $this->truncateString($pr->productContent, 80, true);
                        }
                    }
                }
                else
                {
                    $productCat = $productCategory->getById($product->categoryId);
                    $product->categoryName = ($product->categoryId > 0) ? $productCat->categoryName : "All Products";
                    if($product->collectionId > 0)
                    {
                        $productCollection1 = $productCollection->getById($product->collectionId);
                        $product->collectionName = $productCollection1->collectionName;
                    }
                    
                    $product->productContent = $this->truncateString($product->productContent, 80, true);
                }
            }
        }
        
        
        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/feed-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);

        $view = new ViewModel([
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
            'categories' => $categories,
            'collections' => $collections,
            'selCategory' => $categoryId,
            'selcollection' => $collectionId,
            
            'productList' => $productList,
            'allProducts' => $allProducts,
        ]);

        return $view;
    }
    
    public function manageProductsCategoryAction()
    {
        $brandId = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $postDataCollection = $req->getPost('postData');
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductsCategoryTable');
        $collectionTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductsCollectionTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $action = $req->getPost('action');
        
        if($action == 'remove')
        {
            $categoryID = $req->getPost('categoryID');
            $categoryData = $categoryTable->getById($categoryID);
            if(isset($categoryData->canDelete) && $categoryData->canDelete)
            {
                return new JsonModel([
                    'status' => 'This is default category you can not delete this category.'
                ]);
            }
            else
            {
                $categoryTable->delete($categoryID);
            }
        }
        else if($action == 'public')
        {
            $categoryId = ($req->getPost('categoryId') > 0) ? $req->getPost('categoryId') : 0;
            $showOrNot = $req->getPost('showOrNot');
            $categoryTable->makeCategoryPublicOrNot($categoryId, $showOrNot);
        }
        else
        {
            $categoryName = $req->getPost('categoryName');
            $categoryId = ($req->getPost('categoryId') > 0) ? $req->getPost('categoryId') : 0;
            $category =  new BrandProductsCategory($thisBrand->id, $categoryName);
            if($categoryId > 0)
            {
                $category->id = $categoryId;
            }
            $categoryTable->save($category);
            
            if($action == 'add')
            {
                if(!empty($postDataCollection))
                {
                    foreach($postDataCollection as $pdCollection)
                    {
                        if(trim($pdCollection['value']) != "")
                        {
                            $collection =  new BrandProductsCollection($category->id, $thisBrand->id, $pdCollection['value']);
                            $collectionTable->save($collection);
                        }
                    }
                }
                /*$CollectionName = $req->getPost('collectionName');
                if($CollectionName != "")
                {
                    $collection =  new BrandProductsCollection($category->id, $thisBrand->id, $CollectionName);
                    $collectionTable->save($collection);
                }*/
            }
        }
        
        return new JsonModel([
            'status' => 'ok',
        ]);
    }
    
    public function manageProductsCollectionAction()
    {
        $brandId = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductsCategoryTable');
        $collectionTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductsCollectionTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $action = $req->getPost('action');
        
        if($action == 'remove')
        {
            $collectionID = $req->getPost('collectionID');
            $categoryData = $collectionTable->getById($collectionID);
            $collectionTable->delete($collectionID);
        }
        else if($action == 'public')
        {
            $collectionId = $req->getPost('collectionId', 0);
            $public = $req->getPost('public');
            $collectionTable->makeCollectionPublicOrNot($collectionId, $public);
        }
        else
        {
            $collectionName = $req->getPost('collectionName');
            $collectionId = $req->getPost('collectionId', 0);
            if($collectionId > 0)
            {
                $collectionData = $collectionTable->getById($collectionId);
                $collectionData->collectionName = $collectionName;
                $collection = $collectionData;
            }
            else
            {
                $categoryId = $req->getPost('categoryId', 0);
                $collection =  new BrandProductsCollection($categoryId, $thisBrand->id, $collectionName);
            }            
            
            $collectionTable->save($collection);
        }
        
        return new JsonModel([
            'status' => 'ok',
        ]);
    }
    
    /*
     * POST image - base64 string of the image data (POST)
     *  or
     * FILE image
     */
    public function addProductImageAction() {
        // Todo: remove this function if it is not needed (replaced by brandFeedController?)
        throw new \Exception("This method is used and not to be removed!");

        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $imageSaver = $this->getServiceLocator()->get('LabBase\Model\ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $this->getServiceLocator()->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);


        $image = $imageSaver->uploadImage($image, 'product-image-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }
    
    public function addProductAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        if(!$req->isPost() || empty($req->getPost('content')) 
            || (empty($req->getPost('title')))) {
            return new JsonModel([
                'status' => 'Please enter all fields.'
            ]);
        }

        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandProductTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductsCategoryTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.'
            ]);
        }

        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title', '');
        $categoryId = $req->getPost('category');
        $collectionId = $req->getPost('collection', 0);

        $product = new BrandProduct($loggedInUser->id, $thisBrand->id, $categoryId, $collectionId, $title, $content, $imageData);

        $brandProductTable->saveBrandProduct($product);

        return new JsonModel([
            'status' => 'ok',
            'product' => $product
        ]);
    }
    
    public function editProductAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        if(!$req->isPost() || empty($req->getPost('content')) 
            || (empty($req->getPost('title'))) || $req->getPost('prodcutId') < 1) {
            return new JsonModel([
                'status' => 'Please enter all fields.'
            ]);
        }

        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandProductTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductsCategoryTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.'
            ]);
        }

        $productId = $req->getPost('prodcutId');
        
        $productData = $brandProductTable->getBrandProductById($productId);
        
        if($productData->creatorId != $loggedInUser->id)
        {
            return new JsonModel([
                'status' => 'You do not have permission for update this product.'
            ]);
        }
        
        if(!$productData)
        {
            return new JsonModel([
                'status' => 'You can not update this product because this product not found in database.'
            ]);
        }
        
        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title', '');
        $categoryId = $req->getPost('category');
        $collectionId = $req->getPost('collection');

        $product = new BrandProduct($loggedInUser->id, $thisBrand->id, $categoryId, $collectionId, $title, $content, $imageData);

        if($productId > 0)
        {
            $product->id = $productId;
        }
        
        $brandProductTable->saveBrandProduct($product);

        return new JsonModel([
            'status' => 'ok',
            'product' => $product
        ]);
    }
    
    public function deleteProductAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $productId = $this->params()->fromRoute("productId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandProductTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisProduct = $brandProductTable->getBrandProductById($productId);

        if(!$thisProduct || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisProduct->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.',
                'thisPost' => $thisProduct,
                'loggedInUser' => $loggedInUser,
            ]);
        }

        $brandProductTable->deleteBrandProduct($thisProduct->id);

        return new JsonModel(['status' => 'ok']);
    }
    
    public function getSingleProductAction()
    {
        $productId = $this->params()->fromRoute("productId");
        
        $brandProductTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductTable');
        $product = $brandProductTable->getBrandProductById($productId);
        
        if($product)
        {
            return new JsonModel(['status' => 'ok', 'product' => $product]);
        }
        else
        {
            return new JsonModel(['status' => 'Product not found.']);
        }
    }
    
    public function getProductCommentsAction() {
        return $this->getProductCommentsCommon();
    }
    
    public function postProdcutsCommentAction() {
        /**
         * @var $commentTable CommentTable
         * @var $brandPostTable BrandPostTable
         * @var $brandTable BrandTable
         * @var $notificationTable NotificationTable
         * @var UserTable $userTable
         */
        $productId = $this->params()->fromRoute("productId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost() ||
            empty($req->getPost('text'))
        ) {
            return new JsonModel([
                'status' => 'Please enter the required fields.'
            ]);
        }


        $productsCommentTable = $this->getServiceLocator()->get('LabBase\Model\ProductsCommentTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $brandProductTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $notificationTable = $this->getServiceLocator()->get('LabBase\Model\NotificationTable');

        $product = $brandProductTable->getBrandProductById($productId);
        
        if(!$product) {
            return new JsonModel([
                'status' => 'The product doesn\'t exist.'
            ]);
        }
        
        $name = NULL;
        $email = NULL;
        $commentBrandId = NULL;
        $autoApprove = false;

        $loggedInUser = $userTable->getLoggedInUser();
        if($loggedInUser) {
            $loggedInUserId = $loggedInUser->id;
            if($brandTable->isBrandManagedByUser($product->brandId, $loggedInUser->id)) {
                $autoApprove = true;
                if($req->getPost('postAs', 'user') === 'brand') {
                    $commentBrandId = $product->brandId;
                }
            }
        }else{
            if(
                empty($req->getPost('name')) ||
                empty($req->getPost('email'))
            ) {
                return new JsonModel([
                    'status' => 'You need to provide a name and an email address, or sign in.'
                ]);
            }
            $name = $req->getPost("name");
            $email = $req->getPost("email");
            $loggedInUserId = NULL;
        }

        $content = $req->getPost('text');
        $content = htmlspecialchars($content);

        $comment = new ProductsComment($loggedInUserId, $commentBrandId, $productId, $content, $name, $email);

        if($autoApprove) $comment->approved = true;

        $productsCommentTable->saveProductsComment($comment);


        // Notification
        $displayName = $comment->posterDisplayName;
        if($comment->posterBrandId) {
            $displayName = $brandTable->getBrandById($comment->posterBrandId)->name;
        }elseif($comment->posterId) {
            $poster = $userTable->getUserById($comment->posterId);
            if(!$poster)
                $displayName = '<i>User removed</i>';
            else
                $displayName = $poster->fullName;
        }

        $title = $product->postTitle;
        if(strlen($title) > 40) {
            $i = strpos($title, ' ', 30);
            if($i > 0) {
                $title = substr($title, 0, $i) . '...';
            }
        }

        //$n = new Notification('<span class="author">' . $displayName . '</span><span class="action"> has commented on </span><span class="post">'.$title.'</span>', 0, 0, $comment->id, $product->id, $product->brandId);
        //$notificationTable->saveNotification($n);

        return new JsonModel([
            'status' => 'ok',
            'postId' => $productId,
            'comments' => [$comment]
        ]);
    }
    
    
    private function getProductCommentsCommon($brandId = null) {
        /**
         * @var $commentTable CommentTable
         * @var $brandTable BrandTable
         * @var UserTable $userTable
         * @var BrandPostTable $brandPostTable
         * @var $req \Zend\Http\Request
         */

        $allBrandComments = $brandId !== null;

        $req = $this->getRequest();
        $firstId = $req->getPost('firstCommentId', -1);
        $lastId = $req->getPost('lastCommentId', -1);

        $commentTable = $this->getServiceLocator()->get('LabBase\Model\ProductsCommentTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $brandProductTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $isManager = false;
        if($loggedInUser && $brandTable->isBrandManagedByUser($brandId, $loggedInUser->id)) {
            $isManager = true;
        }

        if(!$allBrandComments) {
            $productId = $this->params()->fromRoute("productId");
            $product = $brandProductTable->getBrandProductById($productId);
            if (!$product) {
                return new JsonModel(['status' => 'The product you tried to comment has been removed.']);
            }

            $brandId = $product->brandId;
            $comments = $commentTable->getProductsCommentsByProduct($productId, $firstId, $lastId);
        }else{
            if(!$isManager) {
                return new JsonModel(['status' => 'Only brand managers can see this']);
            }

            $type = $this->params()->fromRoute('commentsType');
            $filterSeen = null;
            $filterApproved = null;
            $filterRejected = null;
            if($type == 'rejected') {
                $filterRejected = true;
            }elseif($type == 'approved') {
                $filterApproved = true;
            }elseif($type == 'latest') {
                $filterRejected = false;
            }

            $comments = $commentTable->getProductsCommentsByBrand($brandId, $firstId, $lastId, $filterSeen, $filterApproved, $filterRejected);
            $commentTable->markBrandProductCommentsAndNotificationsAsSeen($brandId);
        }

        $clientComments = [];

        foreach($comments as $comment) {
            /** @var Comment $comment */
            if(!$isManager && $comment->approved !== true)
                continue;

            $id = $comment->id;
            $datePosted = $comment->dateCreated;
            $text = $comment->content;
            $displayName = $comment->posterDisplayName;
            $brandUrl = false;
            $seen     = $comment->seen === true;
            $isPublic = $comment->approved === true;
            $rejected = $comment->rejected === true;

            if($comment->posterBrandId) {
                $postingBrand = $brandTable->getBrandById($comment->posterBrandId);
                if(!$postingBrand) // Brand removed?
                    continue;
                $displayName = $postingBrand->name;
                $brandUrl =  $this->url()->fromRoute("root/brandFeed", ['brandUrl' => $postingBrand->url]);

            }elseif($comment->posterId) {
                $poster = $userTable->getUserById($comment->posterId);
                if(!$poster)
                    continue; // User removed
                $displayName = $poster->fullName;
            }

            $clientComments[] = [
                'id' => $id,
                'text' => $text,
                'name' => $displayName,
                'nameUrl' => $brandUrl,
                'datePosted' => $datePosted,
                'seen' => $seen,
                'public' => $isPublic,
                'rejected' => $rejected,
            ];
        }

        $ret = [
            'status' => 'ok',
            'comments' => $clientComments
        ];

        if(isset($productId)) {
            $ret['productId'] = $productId;
        }
        return new JsonModel($ret);
    }
    
    
    private function manageCommentPreamble( &$brand, &$user, &$comment, &$commentTable ) {
        $commentId = (int)$this->params()->fromRoute("commentId");

        /**
         * @var $commentTable CommentTable
         * @var $brandTable BrandTable
         * @var $userTable UserTable
         */
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $user = $userTable->getLoggedInUser();
        if(!$user)
            return new JsonModel(['status' => 'You need to log in again.']);

        $commentTable = $this->getServiceLocator()->get('LabBase\Model\ProductsCommentTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $comment = $commentTable->getProductsCommentById($commentId);
        
        if(!$comment)
            return new JsonModel(['status' => 'This comment doesn\'t exist or has been removed.']);

        $brand = $brandTable->getBrandByProductId($comment->productId);

        if(!$brand)
            return new JsonModel(['status' => 'The brand has been removed. Please notify an administrator if you think this is an error.']);

        if(!$brandTable->isBrandManagedByUser($brand->id, $user->id))
            return new JsonModel(['status' => 'You don\'t have the permission to manage comments. If you believe this is an error, please contact an administrator.']);

        return true;

    }

    public function approveCommentAction() {
        /**
         * @var $brand Brand
         * @var $user User
         * @var $comment Comment
         * @var $commentTable CommentTable
         */
        $brand = null;
        $user = null;
        $comment = null;
        $commentTable = null;

        $ret = $this->manageCommentPreamble($brand, $user, $comment, $commentTable);
        if($ret !== true) return $ret;

        $comment->rejected = false;
        $comment->approved = true;
        $commentTable->saveProductsComment($comment);

        return new JsonModel(['status' => 'ok']);
    }

    public function rejectCommentAction() {
        /**
         * @var $brand Brand
         * @var $user User
         * @var $comment Comment
         * @var $commentTable CommentTable
         */
        $brand = null;
        $user = null;
        $comment = null;
        $commentTable = null;

        $ret = $this->manageCommentPreamble($brand, $user, $comment, $commentTable);
        if($ret !== true) return $ret;

        $comment->approved = false;
        $comment->rejected = true;
        $commentTable->saveProductsComment($comment);

        return new JsonModel(['status' => 'ok']);
    }

    public function deleteCommentAction() {
        /**
         * @var $brand Brand
         * @var $user User
         * @var $comment Comment
         * @var $commentTable CommentTable
         */
        $brand = null;
        $user = null;
        $comment = null;
        $commentTable = null;

        $ret = $this->manageCommentPreamble($brand, $user, $comment, $commentTable);
        if($ret !== true) return $ret;

        $commentTable->deleteProductsComment($comment->id);

        return new JsonModel(['status' => 'ok']);
    }
    
    
    public function getPopupProductAction()
    {
        $productId = $this->params()->fromRoute("productId");
        
        $brandProductTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductTable');
        $product = $brandProductTable->getBrandProductById($productId);
        
        $userCanPost = false;
        $userCanEditBrand = false;
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandById($product->brandId);
       
       // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $productCategory = $this->getServiceLocator()->get('LabBase\Model\BrandProductsCategoryTable');
        $productCollection = $this->getServiceLocator()->get('LabBase\Model\BrandProductsCollectionTable');

        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandProductTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductTable');

        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }   
        }
        
        $brandProductLovedTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductLovedTable');
        
        if(!empty($product))
        {
            $productCat = $productCategory->getById($product->categoryId);
            $product->categoryName = $productCat->categoryName;
            if($product->collectionId > 0)
            {
                $productCollection1 = $productCollection->getById($product->collectionId);
                $product->collectionName = $productCollection1->collectionName;
            }
            
            $loveCnt = $brandProductLovedTable->productLoveCount($product->id);
            $product->loveCnt = $loveCnt;
            if(isset($loggedInUser->id))
            {
                $loveOrNot = $brandProductLovedTable->getByUserProduct($loggedInUser->id, $product->id);
            }
            $product->loveOrNot = (isset($loveOrNot->loved)) ? $loveOrNot->loved : 0;
        }
       
        $this->layout()->setTemplate('lab-base/brand-product/get-popup-product'); 
       
         $view = new ViewModel([
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,
            
            'product' => $product,
        ]);

        return $view;
    }
    
    private function truncateString($str, $chars, $to_space, $replacement="...") {
       if($chars > strlen($str)) return $str;

       $str = substr($str, 0, $chars);

       $space_pos = strrpos($str, " ");
       if($to_space && $space_pos >= 0) {
           $str = substr($str, 0, strrpos($str, " "));
       }

       return($str . $replacement);
    }
    
    public function setProductLovedAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var $brandPostLovedTable BrandPostlovedTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $productId = $this->params()->fromRoute("productId");
        $loveOrNot = $this->params()->fromRoute("loveOrNot");
        
        $brandProductTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductTable');        
        $brandProductLovedTable = $this->getServiceLocator()->get('LabBase\Model\BrandProductlovedTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        if(!$productId) {
            return new JsonModel(["status" => "An error has occurred."]);
        }
        
        $loggedInUser = $userTable->getLoggedInUser();
        if(!$loggedInUser) {
            return new JsonModel(["status" => "Please log in to love this post."]);
        }
        
        
        $thisPost = $brandProductTable->getBrandProductById($productId);
        $brand = $brandTable->getBrandById($thisPost->brandId);
        if(!$brand) {
            return new JsonModel(["status" => "An error has occurred, this brand does not exist."]);
        }
        
        $loveProductData = $brandProductLovedTable->getByUserProduct($loggedInUser->id, $productId);
        
        $loved = new BrandProductLoved($loggedInUser->id, $productId, $loveOrNot);
        
        $loved->id = isset($loveProductData->id) ? $loveProductData->id : 0;
        $lovedTbl = $brandProductLovedTable->save($loved);
        $isLove = false;
        if($loveOrNot)
        {
            $isLove = true;
        }
        $loveCnt = $brandProductLovedTable->productLoveCount($productId);

        return new JsonModel(['status' => 'ok', 'loveCount' => $loveCnt, 'is_love' => $isLove]);
    }

}
