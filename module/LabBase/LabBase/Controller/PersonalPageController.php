<?php
/**
 * Author: Sebi
 * Date: 2014-12-05
 * Time: 20:34
 */

namespace LabBase\Controller;

use LabBase\Model\BrandTable;
use LabBase\Model\Brand;
use LabBase\Model\BrandAbout;
use LabBase\Model\BrandAboutTable;
use LabBase\Model\AboutMe;
use LabBase\Model\AboutMeTable;
use LabBase\Model\BrandTeamMeta;
use LabBase\Model\BrandTeamMetaTable;
use LabBase\Model\BrandTeamMember;
use LabBase\Model\BrandTeamMemberTable;
use LabBase\Model\Category;
use LabBase\Model\CategoryTable;
use LabBase\Model\UserTable;
use LabBase\Model\ImageSaver;
use LabBase\Model\Notification;
use LabBase\Model\NotificationTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class PersonalPageController extends AbstractActionController {
    
    public function homeAction()
    {
        /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("brandUrl");
        $categoryUrl = $this->params()->fromRoute("categoryUrl");
        
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
       
        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);
        
        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\CategoryTable');
        $commentTable = $this->getServiceLocator()->get('LabBase\Model\CommentTable');

        $categoryList = $categoryTable->getCategoriesByBrand($thisBrand->id);
        
        usort($categoryList, function(Category $a, Category $b) {
            return $a->position - $b->position;
        });
        foreach($categoryList as $cat) {
            usort($cat->children, function (Category $a, Category $b) {
                return $a->position - $b->position;
            });
        }

        $cat = $categoryTable->getCategoryByUrlAndBrand($categoryUrl, $thisBrand->id);

        // Disallow selecting a category with children (only children can be selected)
        if(!empty($cat->children)) {
            $cat = reset($cat->children);
        }

        // Fix the url, if it has changed
        if($cat) {
            $categoryUrl = $cat->url;
        }


        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;
                if($cat) $userCanPost = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }

        // Disallow non-enabled categories for public users
        if(!$userCanEditBrand && $cat && $cat->enabled == false) {
            $cat = null;
        }
        
        $draftspostCnt = 0;
        if(isset($loggedInUser->id) && $loggedInUser->id > 0)
        {
            $draftspostCnt = $brandPostTable->getDraftPostsByBrandAndUser($thisBrand->id, $loggedInUser->id);
        }
        
        $postList = [];
        $thisPost = false;
        
        if($cat) {
            $postList = $brandPostTable->getBrandPostsByBrandAndCategory($thisBrand->id, $cat->id);
        }else{
            $postList = $brandPostTable->getBrandFeedPostsByBrand($thisBrand->id);
        }
        
        $brandPostLovedTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostLovedTable');
        $postPhotoCnt = 0;
        $postAlbumCnt = 0;
        $postVideoCnt = 0;
        foreach ($postList as $post) {
            /*echo "<pre>"; print_r($post); die;*/
            // caching done in getCategoryById
            $postCat = $categoryTable->getCategoryById($post->categoryId);
            $post->categoryName = $postCat->categoryName;
            $post->categoryUrl = $postCat->url;
            $loveCnt = $brandPostLovedTable->postLoveCount($post->id);
            $post->loveCnt = $loveCnt;
            if(isset($loggedInUser->id))
            {
                $loveOrNot = $brandPostLovedTable->getByUserBrandPost($loggedInUser->id, $thisBrand->id, $post->id);
            }
            $post->loveOrNot = (isset($loveOrNot->loved)) ? $loveOrNot->loved : 0;
            
            if($post->videoData != "")
            {
                $videoTags = $this->convertVideoURLToTags($post->videoData);
                if($videoTags != false)
                {
                    $post->videoDataTag = $videoTags;
                }
            }
            
            $allCmments = $commentTable->getCommentsByPost($post->id, -1, -1);
            $post->commentsCnt = count($allCmments);
        }
        
        $brandLovedByUser = false;
        if($loggedInUser) {
            /** @var BrandRelationTable $brTable */
            $brTable = $this->getServiceLocator()->get('LabBase\Model\BrandRelationTable');
            $brandLovedByUser = $brTable->getByUserBrandType($loggedInUser->id, $thisBrand->id, 1);

        }
        
        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/user-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);
        $this->layout()->setVariable('thisBrand', $thisBrand);

        $view = new ViewModel([
            'thisBrand' => $thisBrand,
            
            'categoryList' => BrandSettingsController::categoryArrayToJson($categoryList),
            'currentCategory' => $cat,
            'categoryUrl' => $categoryUrl,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,
            
            'brandLovedByUser' => $brandLovedByUser,
            
            'singlePostView' => $thisPost !== FALSE,
            'thisPost' => $thisPost,
            'postList' => $postList,
            'draftspostCnt' => $draftspostCnt,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
        ]);

        return $view;
    }
    
    
    public function photosAction()
    {
        /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\CategoryTable');
        $commentTable = $this->getServiceLocator()->get('LabBase\Model\CommentTable');
        
        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);
        
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
    
        $thisPhotos = FALSE;
        $photoPostLists = [];
        
        $thisPhotoPost = $brandPostTable->getBrandFeedPostsByTypeByBrand('photo', $thisBrand->id);
        
        if(!empty($thisPhotoPost))
        {
            $photoPostLists = $thisPhotoPost;
        }
        
        $brandPostLovedTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostLovedTable');
        
        $loveCnt = 0;
        if(!empty($photoPostLists))
        {
            foreach ($photoPostLists as $post) {
                $postCat = $categoryTable->getCategoryById($post->categoryId);
                $post->categoryName = $postCat->categoryName;
                $post->categoryUrl = $postCat->url;
                $loveCnt = $brandPostLovedTable->postLoveCount($post->id);
                $post->loveCnt = $loveCnt;
                if(isset($loggedInUser->id))
                {
                    $loveOrNot = $brandPostLovedTable->getByUserBrandPost($loggedInUser->id, $thisBrand->id, $post->id);
                }
                $post->loveOrNot = (isset($loveOrNot->loved)) ? $loveOrNot->loved : 0;
                
                $allCmments = $commentTable->getCommentsByPost($post->id, -1, -1);
                $post->commentsCnt = count($allCmments);
                
                if(!empty($post->postImageData))
                {
                    $post->postImageData = json_decode($post->postImageData);
                }
            }
        }
        
        $photoAlbumesCnt = $brandPostTable->getBrandFeedCntPhotoAndAlbums($thisBrand->id);
        $postPhotoCnt = (isset($photoAlbumesCnt['photo_cnt']) && $photoAlbumesCnt['photo_cnt'] != "") ? $photoAlbumesCnt['photo_cnt'] : 0;
        $postAlbumCnt = (isset($photoAlbumesCnt['album_cnt']) && $photoAlbumesCnt['album_cnt'] != "") ? $photoAlbumesCnt['album_cnt'] : 0;
        
        $postVideoCnt = $brandPostTable->getBrandFeedCntVideos($thisBrand->id);
        $postVideoCnt = (isset($postVideoCnt) && $postVideoCnt != "") ? $postVideoCnt : 0;
        
        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/user-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);

        $view = new ViewModel([
            'thisBrand' => $thisBrand,

            'singlePostView' => $thisPhotos !== FALSE,
            'thisPost' => $thisPhotos,
            'postList' => $photoPostLists,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
            'postPhotoCnt' => $postPhotoCnt,
            'postAlbumCnt' => $postAlbumCnt,
            'postVideoCnt' => $postVideoCnt,            
        ]);

        return $view;
    }
    
    public function videosAction()
    {
        /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\CategoryTable');
        
        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);
        
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
    
        $thisVideo = FALSE;
        $videoPostLists = [];
        
        $thisVideoPost = $brandPostTable->getBrandFeedPostsByTypeByBrand('video', $thisBrand->id);        
        
        if(!empty($thisVideoPost))
        {
            $videoPostLists = $thisVideoPost;
        }
        
        
        if(!empty($videoPostLists))
        {
            foreach ($videoPostLists as $post) {
                $postCat = $categoryTable->getCategoryById($post->categoryId);
                $post->categoryName = $postCat->categoryName;
                $post->categoryUrl = $postCat->url;
                
                if(!empty(trim($post->videoData)))
                {
                    $post->videoData = $this->convertVideoURLToTags(trim($post->videoData));
                }
            }
        }
        
        $photoAlbumesCnt = $brandPostTable->getBrandFeedCntPhotoAndAlbums($thisBrand->id);
        $postPhotoCnt = (isset($photoAlbumesCnt['photo_cnt']) && $photoAlbumesCnt['photo_cnt'] != "") ? $photoAlbumesCnt['photo_cnt'] : 0;
        $postAlbumCnt = (isset($photoAlbumesCnt['album_cnt']) && $photoAlbumesCnt['album_cnt'] != "") ? $photoAlbumesCnt['album_cnt'] : 0;
        
        $postVideoCnt = $brandPostTable->getBrandFeedCntVideos($thisBrand->id);
        $postVideoCnt = (isset($postVideoCnt) && $postVideoCnt != "") ? $postVideoCnt : 0;
        
        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/user-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);

        $view = new ViewModel([
            'thisBrand' => $thisBrand,

            'singlePostView' => $thisVideo !== FALSE,
            'thisPost' => $thisVideo,
            'postList' => $videoPostLists,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
            'postPhotoCnt' => $postPhotoCnt,
            'postAlbumCnt' => $postAlbumCnt,
            'postVideoCnt' => $postVideoCnt,            
        ]);

        return $view;
    }
    
    
    public function albumsAction()
    {
        /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("brandUrl");
        $albumId = $this->params()->fromRoute("albumId", 0);
       
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\CategoryTable');
        $commentTable = $this->getServiceLocator()->get('LabBase\Model\CommentTable');
        
        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);
        
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
    
        $thisPhotos = FALSE;
        $photoPostLists = [];
        
        $thisPhotoPost = $brandPostTable->getBrandFeedPostsAlbums($thisBrand->id, $albumId);
        
        if(!empty($thisPhotoPost))
        {
            $photoPostLists = $thisPhotoPost;
        }
        
        $brandPostLovedTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostLovedTable');
        
        $loveCnt = 0;
        if(!empty($photoPostLists))
        {
            foreach ($photoPostLists as $post) {
                $postCat = $categoryTable->getCategoryById($post->categoryId);
                $post->categoryName = $postCat->categoryName;
                $post->categoryUrl = $postCat->url;
                $loveCnt = $brandPostLovedTable->postLoveCount($post->id);
                $post->loveCnt = $loveCnt;
                if(isset($loggedInUser->id))
                {
                    $loveOrNot = $brandPostLovedTable->getByUserBrandPost($loggedInUser->id, $thisBrand->id, $post->id);
                }
                $post->loveOrNot = (isset($loveOrNot->loved)) ? $loveOrNot->loved : 0;
                
                $allCmments = $commentTable->getCommentsByPost($post->id, -1, -1);
                $post->commentsCnt = count($allCmments);
                
                if(!empty($post->postImageData))
                {
                    $post->postImageData = json_decode($post->postImageData);
                }
            }
        }
        
        $otherAlbums = FALSE;
        if($albumId > 0)
        {
            $photoPostLists = $photoPostLists[0];
            $otherAlbums = $brandPostTable->getBrandFeedOtherAlbums($albumId, 3, $thisBrand->id);
        }
        
        
        $photoAlbumesCnt = $brandPostTable->getBrandFeedCntPhotoAndAlbums($thisBrand->id);
        
        $postPhotoCnt = (isset($photoAlbumesCnt['photo_cnt']) && $photoAlbumesCnt['photo_cnt'] != "") ? $photoAlbumesCnt['photo_cnt'] : 0;
        $postAlbumCnt = (isset($photoAlbumesCnt['album_cnt']) && $photoAlbumesCnt['album_cnt'] != "") ? $photoAlbumesCnt['album_cnt'] : 0;
        
        $postVideoCnt = $brandPostTable->getBrandFeedCntVideos($thisBrand->id);
        $postVideoCnt = (isset($postVideoCnt) && $postVideoCnt != "") ? $postVideoCnt : 0;
        
        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/user-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);

        $view = new ViewModel([
            'thisBrand' => $thisBrand,

            'singlePostView' => $thisPhotos !== FALSE,
            'thisPost' => $thisPhotos,
            'postList' => $photoPostLists,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
            'postPhotoCnt' => $postPhotoCnt,
            'postAlbumCnt' => $postAlbumCnt,
            'postVideoCnt' => $postVideoCnt,            
            
            'otherAlbums' => $otherAlbums,            
            
            'albumId' => $albumId,            
        ]);

        return $view;
    }
    
    
    public function storyAction()
    {
        /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("brandUrl");
        $categoryUrl = $this->params()->fromRoute("categoryUrl");
        
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
       
        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);
        
        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $categoryTable = $this->getServiceLocator()->get('LabBase\Model\CategoryTable');
        $commentTable = $this->getServiceLocator()->get('LabBase\Model\CommentTable');

        $categoryList = $categoryTable->getCategoriesByBrand($thisBrand->id);
        
        usort($categoryList, function(Category $a, Category $b) {
            return $a->position - $b->position;
        });
        foreach($categoryList as $cat) {
            usort($cat->children, function (Category $a, Category $b) {
                return $a->position - $b->position;
            });
        }

        $cat = $categoryTable->getCategoryByUrlAndBrand($categoryUrl, $thisBrand->id);

        // Disallow selecting a category with children (only children can be selected)
        if(!empty($cat->children)) {
            $cat = reset($cat->children);
        }

        // Fix the url, if it has changed
        if($cat) {
            $categoryUrl = $cat->url;
        }


        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;
                if($cat) $userCanPost = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }

        // Disallow non-enabled categories for public users
        if(!$userCanEditBrand && $cat && $cat->enabled == false) {
            $cat = null;
        }
        
        $draftspostCnt = 0;
        if(isset($loggedInUser->id) && $loggedInUser->id > 0)
        {
            $draftspostCnt = $brandPostTable->getDraftPostsByBrandAndUser($thisBrand->id, $loggedInUser->id);
        }
        
        $thisPost = $brandPostTable->getBrandFeedPostsByTypesByBrand(array(2, 3), $thisBrand->id);
        
        $postList = [];
        if($thisPost) {
            $postList = $thisPost;
        }        
        
        $brandPostLovedTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostLovedTable');
        $postPhotoCnt = 0;
        $postAlbumCnt = 0;
        $postVideoCnt = 0;
        foreach ($postList as $post) {
            /*echo "<pre>"; print_r($post); die;*/
            // caching done in getCategoryById
            $postCat = $categoryTable->getCategoryById($post->categoryId);
            $post->categoryName = $postCat->categoryName;
            $post->categoryUrl = $postCat->url;
            $loveCnt = $brandPostLovedTable->postLoveCount($post->id);
            $post->loveCnt = $loveCnt;
            if(isset($loggedInUser->id))
            {
                $loveOrNot = $brandPostLovedTable->getByUserBrandPost($loggedInUser->id, $thisBrand->id, $post->id);
            }
            $post->loveOrNot = (isset($loveOrNot->loved)) ? $loveOrNot->loved : 0;
            
            $allCmments = $commentTable->getCommentsByPost($post->id, -1, -1);
            $post->commentsCnt = count($allCmments);
        }
        
        $brandLovedByUser = false;
        if($loggedInUser) {
            /** @var BrandRelationTable $brTable */
            $brTable = $this->getServiceLocator()->get('LabBase\Model\BrandRelationTable');
            $brandLovedByUser = $brTable->getByUserBrandType($loggedInUser->id, $thisBrand->id, 1);

        }
        
        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/user-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);
        $this->layout()->setVariable('thisBrand', $thisBrand);

        $view = new ViewModel([
            'thisBrand' => $thisBrand,
            
            'categoryList' => BrandSettingsController::categoryArrayToJson($categoryList),
            'currentCategory' => $cat,
            'categoryUrl' => $categoryUrl,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,
            
            'brandLovedByUser' => $brandLovedByUser,
            
            'singlePostView' => $thisPost !== FALSE,
            'thisPost' => $thisPost,
            'postList' => $postList,
            'draftspostCnt' => $draftspostCnt,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
        ]);

        return $view;
    }
    
    public function portfolioAction()
    {
        /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);

        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);
        
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
    
        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/user-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);
        $this->layout()->setVariable('thisBrand', $thisBrand);

        $view = new ViewModel([
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
        ]);

        return $view;
    }
    
    
    public function addPersonalPagePhotoImageAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $imageSaver = $this->getServiceLocator()->get('LabBase\Model\ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $this->getServiceLocator()->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);


        // todo: change to simple
        $image = $imageSaver->uploadImage($image, 'personal-page-photo-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }
    
    
    private function truncateString($str, $chars, $to_space, $replacement="...") {
       if($chars > strlen($str)) return $str;

       $str = substr($str, 0, $chars);

       $space_pos = strrpos($str, " ");
       if($to_space && $space_pos >= 0) {
           $str = substr($str, 0, strrpos($str, " "));
       }

       return($str . $replacement);
    }
    
    private function get_next($array, $key) {
       $currentKey = key($array);
       while ($currentKey !== null && $currentKey != $key) {
           next($array);
           $currentKey = key($array);
       }
       return next($array);
    }
    
    private function convertVideoURLToTags($videoURL)
    {
        $pattern1 = "/(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/";
        $pattern2 = "/(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/";
        
        if($videoURL != NULL)
        {
            $success1 = preg_match($pattern1, $videoURL, $match1);
            $success2 = preg_match($pattern2, $videoURL, $match2);
            
            $replacement = false;
            
            if(!empty($match1))
            {
                if($match1[1] != "")
                {
                    $replacement = '<iframe width="420" height="345" src="//player.vimeo.com/video/'.$match1[1].'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                }
            }
            
            if(!empty($match2))
            {
                if($match2[1] != "")
                {
                    $replacement = '<iframe width="420" height="345" src="http://www.youtube.com/embed/'.$match2[1].'" frameborder="0" allowfullscreen></iframe>';
                }
            }
        }
        
        return $replacement;
    }
    
    public function getSingleAlbumAction()
    {
        $albumId = $this->params()->fromRoute("albumId");
        
        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $album = $brandPostTable->getBrandPostById($albumId);
        
        $userCanPost = false;
        $userCanEditBrand = false;
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandById($album->brandId);
       
       $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }   
        }
       
       // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $this->layout()->setTemplate('lab-base/personal-page/get-single-album'); 
       
         $view = new ViewModel([
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,
            
            'album' => $album,
        ]);

        return $view;
    }
    
    public function getSinglePostAction()
    {
        $postId = $this->params()->fromRoute("postId");
        
        $brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $thisPost = $brandPostTable->getBrandPostById($postId);
        
        $userCanPost = false;
        $userCanEditBrand = false;
       
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandById($thisPost->brandId);
       
       $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }   
        }
       
       // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }

        $this->layout()->setTemplate('lab-base/personal-page/get-single-post'); 
       
         $view = new ViewModel([
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,
            
            'thisPost' => $thisPost,
        ]);

        return $view;
    }
}
