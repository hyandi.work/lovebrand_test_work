<?php
/**
 * Author: Sebi
 * Date: 2014-12-05
 * Time: 20:34
 */

namespace LabBase\Controller;

use LabBase\Model\BrandTable;
use LabBase\Model\Brand;
use LabBase\Model\BrandAbout;
use LabBase\Model\BrandAboutTable;
use LabBase\Model\AboutMe;
use LabBase\Model\AboutMeTable;
use LabBase\Model\BrandTeamMeta;
use LabBase\Model\BrandTeamMetaTable;
use LabBase\Model\BrandTeamMember;
use LabBase\Model\BrandTeamMemberTable;
use LabBase\Model\UserTable;
use LabBase\Model\ImageSaver;
use LabBase\Model\Notification;
use LabBase\Model\NotificationTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class AboutMeController extends AbstractActionController {
    
    public function indexAction() {
          /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        $aboutMeTable = $this->getServiceLocator()->get('LabBase\Model\AboutMeTable');

        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);
        
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
    
        $aboutMeData = $aboutMeTable->getAboutMeByBrand($thisBrand->id);
        
        $abData = [];
        if(!empty($aboutMeData))
        {
            foreach($aboutMeData as $am)
            {
                $am->imageData = (!empty($am->imageData)) ? json_decode($am->imageData) : '';
                if(!empty($am->imageData))
                {
                    foreach($am->imageData as $imaqge)
                    {
                        $am->imageData = $imaqge;
                    }
                }
                $abData = $am;
            }
        }
        
        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/user-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);

        $view = new ViewModel([
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
            
            'aboutMe' => $abData,
        ]);

        return $view;
    }
    
    
     /*
     * POST image - base64 string of the image data (AbouMe)
     *  or
     * FILE image
     */
    public function addAboutMeImageAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $imageSaver = $this->getServiceLocator()->get('LabBase\Model\ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $this->getServiceLocator()->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);


        $image = $imageSaver->uploadImageSimple($image, 'about-me-profile', /*random*/null, $loggedInUser->id, $thisBrand->id);
        //$image = $imageSaver->uploadImage($image, 'about-me-profile-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }
    
    
    public function addAboutMeAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        
        $aboutMeTable = $this->getServiceLocator()->get('LabBase\Model\AboutMeTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title');

        $aboutMe = new AboutMe($loggedInUser->id, $thisBrand->id, $title, $content, $imageData);

        $aboutMeTable->saveAboutMe($aboutMe);

        return new JsonModel([
            'status' => 'ok',
            'service' => $aboutMe
        ]);
    }
    
    public function editAboutMeAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $aboutMe = new AboutMe();
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        
        $aboutMeTable = $this->getServiceLocator()->get('LabBase\Model\AboutMeTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title');
        $id = $req->getPost('aboutMeId', 0);
        
        if($id < 0)
        {
            return new JsonModel([
                'status' => 'Invalid about me data.'
            ]);
        }
        
        $aboutmedata = $aboutMeTable->getAboutMeById($id);
        
        if(!$aboutmedata)
        {
            return new JsonModel([
                'status' => 'About me data not found.'
            ]);
        }

        $content = preg_replace('/<\s*br\s*\/?\s*>/', '</p><p>', $content);
        
        $aboutmedata->id = $id;
        $aboutmedata->name = $title;
        $aboutmedata->description = $content;
        $aboutmedata->imageData = $imageData;

        $aboutMeTable->saveAboutMe($aboutmedata);

        return new JsonModel([
            'status' => 'ok',
            'service' => $aboutmedata
        ]);
    }
    
    
    private function truncateString($str, $chars, $to_space, $replacement="...") {
       if($chars > strlen($str)) return $str;

       $str = substr($str, 0, $chars);

       $space_pos = strrpos($str, " ");
       if($to_space && $space_pos >= 0) {
           $str = substr($str, 0, strrpos($str, " "));
       }

       return($str . $replacement);
    }
    
    private function get_next($array, $key) {
       $currentKey = key($array);
       while ($currentKey !== null && $currentKey != $key) {
           next($array);
           $currentKey = key($array);
       }
       return next($array);
    }
}
