<?php

namespace LabBase\Controller;

use LabBase\Model\BrandRelation;
use LabBase\Model\BrandRelationTable;
use LabBase\Model\BrandTable;
use LabBase\Model\UserTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class InteractionController extends AbstractActionController {

    // todo: remove
    public function loveBrandAction() {

        $brandId = (int)$this->params()->fromRoute("param");
        $loveBool = $this->params()->fromRoute("param2") == "true";

        if(!$brandId) {
            return new JsonModel(["status" => "An error has occurred."]);
        }

        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        $user = $userTable->getLoggedInUser();
        if(!$user) {
            return new JsonModel(["status" => "Please log in to love this brand."]);
        }

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');

        $brand = $brandTable->getBrandById($brandId);
        if(!$brand) {
            return new JsonModel(["status" => "An error has occurred, this brand does not exist."]);
        }

        /** @var BrandRelationTable $brTable */
        $brTable = $this->getServiceLocator()->get('LabBase\Model\BrandRelationTable');

        if($loveBool) {
            $rel = new BrandRelation($user->id, $brand->id, 1);
            $brTable->save($rel);
        }else{
            $brTable->deleteUserBrandType($user->id, $brand->id, 1);
        }
        $brandTable->updateRelationCount($brand);
        $brandTable->saveBrand($brand);

        return new JsonModel(['status' => 'ok', 'newValue' => $loveBool]);
    }

    public function submitRelationAction() {

        $url = $this->params()->fromRoute("url");
        $relationType = (int)$this->params()->fromRoute("relationType");
        $value = $this->params()->fromRoute("value");
        if($value === true || $value == "true" || $value > 0) {
            $value = true;
        }else{
            $value = false;
        }

        if(!$url) {
            return new JsonModel(["status" => "An error has occurred."]);
        }

        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('UserTable');

        $user = $userTable->getLoggedInUser();
        if(!$user) {
            return new JsonModel(["status" => "Please log in to love this brand."]);
        }

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('BrandTable');

        $brand = $brandTable->getBrandByUrl($url);
        if(!$brand) {
            return new JsonModel(["status" => "An error has occurred, this brand does not exist."]);
        }

        /** @var BrandRelationTable $brTable */
        $brTable = $this->getServiceLocator()->get('BrandRelationTable');

        if($value) {
            $rel = new BrandRelation($user->id, $brand->id, $relationType);
            $brTable->save($rel);
        }else{
            $brTable->deleteUserBrandType($user->id, $brand->id, $relationType);
        }
        $brandTable->updateRelationCount($brand);
        $brandTable->saveBrand($brand);

        $totalCount = $brTable->getBrandLovedCountByBrand($brand->id);
        
        return new JsonModel(['status' => 'ok', 'value' => $value, 'total' => $totalCount]);
    }

}