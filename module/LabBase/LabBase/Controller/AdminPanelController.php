<?php
/**
 * Author: Sebi
 * Date: 2014-10-27
 * Time: 21:42
 */

namespace LabBase\Controller;

use LabBase\Model\BrandPost;
use LabBase\Model\BrandPostTable;
use LabBase\Model\BrandTable;
use LabBase\Model\DBModel;
use LabBase\Model\ImageSaver;
use LabBase\Model\ImageTable;
use LabBase\Model\UserTable;
use Zend\Http\Header\SetCookie;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;

class AdminPanelController extends AbstractActionController {

    protected function checkAdminStatus($superAdmin = true) {
        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('UserTable');
        $user = $userTable->getLoggedInUser();
        if(!$user) {
            return false;
        }
        if($superAdmin) {
            return $user->isSuperAdmin() ? $user : false;
        }else{
            return $user->isAdmin() ? $user : false;
        }
    }

    public function indexAction() {
        if(!$this->checkAdminStatus())
            $this->redirect()->toRoute("homepage");
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Admin Panel');

        return [];
    }

    public function loginAsAction() {
        if(!$this->checkAdminStatus(true))
            $this->redirect()->toRoute("root");

        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('UserTable');

        $userId = (int)$this->params()->fromRoute('param');
        $user = $userTable->getUserById($userId);

        // Log the user in
        $userTable->overrideLoggedInUser($user);
        $cookie = new SetCookie("sid", $user->sessionId . '.' . $user->sessionKey, time() + 3600 * 24 * 7, "/");
        $cookie->setHttponly(true);

        /** @var Response $resp */
        $resp = $this->getResponse();
        $resp->getHeaders()->addHeader($cookie);

        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $brands = $brandTable->getBrandsByOwner($user->id);
        if(count($brands) > 0) {
            $this->redirect()->toRoute("brand", ['url' => $brands[0]->url]);
        }else{
            $this->redirect()->toRoute("homepage");
        }
    }

    public function brandsAction() {
        if(!$this->checkAdminStatus())
            $this->redirect()->toRoute("root");
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Admin Panel');


        /** @var BrandTable $brandTable */
        $brandTable = $this->getServiceLocator()->get('BrandTable');
        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('UserTable');
        /** @var BrandPostTable $brandPostTable */
        $brandPostTable = $this->getServiceLocator()->get('BrandPostTable');

        $brands = $brandTable->getAllBrands();

        foreach($brands as $brand) {
            $brand->owner = $userTable->getUserById($brand->ownerId);
            $brand->postCount = $brandPostTable->getBrandPostCountBrand($brand->id);
        }

        return ['brands' => $brands];
    }

    public function usersAction() {
        if(!$this->checkAdminStatus())
            $this->redirect()->toRoute("homepage");
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Admin Panel');

        /** @var UserTable $userTable */
        $userTable = $this->getServiceLocator()->get('UserTable');

        $users = $userTable->getAllUsers();

        return ['users' => $users];
    }


    private function findRoutes($node, array &$ret, $parentRoute = null) {
        foreach ($node as $k => $v) {
            if(isset($v['options']['defaults'])) {
                $controller = isset($v['options']['defaults']['controller']) ? $v['options']['defaults']['controller'] : 'n/a';
                $action = isset($v['options']['defaults']['action']) ? $v['options']['defaults']['action'] : 'n/a';
            }else{
                $controller = $action = 'n/a';
            }

            if (substr($controller, 0, strlen('LabBase\Controller\\')) == 'LabBase\Controller\\') {
                $controller = substr($controller, strlen('LabBase\Controller\\'));
            }

            $meta = isset($v['metadata']) ? $v['metadata'] : [];


            $route = [
                'routeName' => $parentRoute == null ? $k : $parentRoute['routeName'] . '/' . $k,
                'route' => $parentRoute == null ? $v['options']['route'] : $parentRoute['route'] . $v['options']['route'],
                'controller' => $controller,
                'action' => $action,
                'id' => isset($meta['id']) ? $meta['id'] : 'n/a',
                'description' => isset($meta['description']) ? $meta['description'] : 'n/a',
                'longdesc' => isset($meta['longdesc']) ? $meta['longdesc'] : (isset($meta['description']) ? $meta['description'] : 'n/a'),
                'params' => '',
                'returns' => '',
            ];

            if(isset($meta['returns'])) {
                $str = '';
                foreach($meta['returns'] as $item) {
                    $e = explode(' ', $item, 3);
                    $str .= '<li> <span>'.$e[0].'</span> <span>'.$e[1].'</span> <span>'.$e[2].'</span> </li>';
                }
                $route['returns'] = '<ul>'.$str.'</ul>';
            }
            if(isset($meta['params'])) {
                $str = '';
                foreach($meta['params'] as $item) {
                    $e = explode(' ', $item, 3);
                    $str .= '<li> <span>'.$e[0].'</span> <span>'.$e[1].'</span> <span>'.$e[2].'</span> </li>';
                }
                $route['params'] = '<ul>'.$str.'</ul>';
            }

            $ret[] = $route;

            if(isset($v['child_routes']) && !empty($v['child_routes'])) {
                $this->findRoutes($v['child_routes'], $ret, $route);
            }
        }

    }

    public function sitePageListAction() {
        if(!$this->checkAdminStatus())
            $this->redirect()->toRoute("root");
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Admin Panel');

        $config = $this->getServiceLocator()->get('Config');
        $cfgRoutes = $config['router']['routes'];
        $routes = [];

        $this->findRoutes($cfgRoutes, $routes);

        usort($routes, function($a, $b) { return $a['id'] > $b['id'] ? 1 : -1;  });

        error_log(print_r($routes, true));

        return ['routes' => $routes];
    }

    public function siteDocsAction() {

        // no auth, is it ok?

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Admin Panel');

        $config = $this->getServiceLocator()->get('Config');
        $cfgRoutes = $config['router']['routes'];
        $routes = [];

        $this->findRoutes($cfgRoutes, $routes);

        usort($routes, function($a, $b) { return $a['id'] > $b['id'] ? 1 : -1;  });

        error_log(print_r($routes, true));

        // Generate SQL definitions for the database
        $cfgModels = $config['db_models'];
        $sql = [];

        if(defined('_DEVELOPMENT_SERVER')) {
            foreach($cfgModels as $modelName) {
                $fqModelName = '\\LabBase\\Model\\' . $modelName;
                /** @var DBModel $obj */
                $obj = new $fqModelName();
                $sql[$modelName] = $obj->generateSQLDefinition($modelName);
            }
        }

        return [
            'sql' => $sql,
            'routes' => $routes
        ];
    }

    public function imageManagerAction() {
        $user = $this->checkAdminStatus(true);
        if(!$user)
            $this->redirect()->toRoute("root");
        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set('Admin Panel');

        /** @var ImageTable $imageTable */
        $imageTable = $this->getServiceLocator()->get('LabBase\Model\ImageTable');

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $errors = '';

        if($req->isPost() && $req->getPost('imageName') !== null) {
            $imageName = $req->getPost('imageName');
            if(preg_match('/^[a-zA-Z0-9_\-]+$/', $imageName) !== 1) {
                $errors .= 'The image ID can only be made up of letters, digits, underscore and a minus sign. It cannot contain spaces.<br />';
            } else {
                $uploadedImage = $req->getFiles('uploadImageFile');
                if (!empty($uploadedImage)) {
                    if(empty($uploadedImage['tmp_name'])) {
                        $errors .= "No image selected<br />";
                    } else {
                        /** @var $imageSaver ImageSaver */
                        $imageSaver = $this->getServiceLocator()->get('LabBase\Model\ImageSaver');

                        $uploadRaw = !empty($req->getPost('uploadRaw', false));
                        error_log("RAW: ".$uploadRaw);

                        $img = $imageSaver->uploadImageSimple($uploadedImage, 'system', $imageName, $user->id, null, null, 8192, 8192, $uploadRaw);
                        //$img = $imageSaver->uploadImage($uploadedImage, 'system-'.$imageName, 8192, 8192, null, null, $uploadRaw);
                        if ($img) {
                            $errors .= 'Image uploaded<br />';
                        } else {
                            $errors .= 'Couldn\'t upload the image<br />';
                        }

                        error_log("ERRORS: ".$errors);
                    }
                }
            }
        }elseif(!empty($req->getPost('removeImage'))) {
            /** @var $imageSaver ImageSaver */
            $imageSaver = $this->getServiceLocator()->get('LabBase\Model\ImageSaver');
            $imageSaver->deleteImage($req->getPost('removeImage'));
            $errors .= 'Image removed<br />';
        }


        $sysImages = $imageTable->getAllSystemImages();

        return [
            'errors' => $errors,
            'images' => $sysImages
        ];
    }


}