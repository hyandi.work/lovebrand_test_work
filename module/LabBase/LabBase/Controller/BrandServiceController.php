<?php
/**
 * Author: Sebi
 * Date: 2014-12-05
 * Time: 20:34
 */

namespace LabBase\Controller;

use LabBase\Model\BrandPost;
use LabBase\Model\BrandPostLoved;
use LabBase\Model\BrandPostTable;
use LabBase\Model\BrandPostLovedTable;
use LabBase\Model\BrandRelationTable;
use LabBase\Model\BrandTable;
use LabBase\Model\Brand;
use LabBase\Model\BrandService;
use LabBase\Model\BrandServiceTable;
use LabBase\Model\Category;
use LabBase\Model\CategoryTable;
use LabBase\Model\UserTable;
use LabBase\Model\ImageSaver;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class BrandServiceController extends AbstractActionController {
    
    public function indexAction() {
          /**
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         * @var BrandPostTable $brandPostTable
         * @var BrandPostLovedTable $brandPostlovedTable
         * @var UserTable $userTable
         * @var BrandPost $post
         * @var BrandPostLoved $loved
         */

        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);

        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $brandServiceTable = $this->getServiceLocator()->get('BrandServiceTable');

        $this->getServiceLocator()->get('ViewHelperManager')->get('HeadTitle')->set($thisBrand->name);
        
        $userTable = $this->getServiceLocator()->get('UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
    
        $serviceList = $brandServiceTable->getBrandServiceByBrand($thisBrand->id);
        
        if($serviceList)
        {
            foreach($serviceList as $sl)
            {
                if(strlen($sl->serviceContent) > 60)
                {
                    $sl->serviceContentTrunc = $this->truncateString($sl->serviceContent, 60, true);
                }
            }
        }
    
        // todo: check if brand is public

        $this->layout()->setTemplate('lab-base/feed-layout');
        $this->layout()->setVariable('BrandName', $thisBrand->name);
        $this->layout()->setVariable('BrandUrl', $thisBrand->url);
        $this->layout()->setVariable('BrandDescription', $thisBrand->description);

        $view = new ViewModel([
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,

            // todo: remove those after finishing brand feed template:
            'brandUrl' => $brandUrl,
            
            'serviceList' => $serviceList,
        ]);

        return $view;
    }
    
    public function manageBradsSettingsAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
       
        $req = $this->getRequest();
        
        $userCanPost = false;
        $userCanEditBrand = false;

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);

        // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }
        
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }
            
        }
        
        if($userCanEditBrand)
        {
            $type = $req->getPost('type');
            $content = $req->getPost('content');
            switch($type)
            {
                case 'title' :
                        $thisBrand->name = $content;
                    break;
                    
                case 'description' :
                        $thisBrand->description = $content;
                    break;
                
                case 'public_title' :
                        $thisBrand->public_name = $content;
                    break;
                
                case 'public_description' :
                        $thisBrand->public_description = $content;
                    break;
            }
            
            $brandTable->saveBrand($thisBrand);
            return new JsonModel(['status' => 'ok']);
        }
        else
        {
            return new JsonModel(['status' => 'You have no access for this brand.']);
        }
    }
    
    public function addServiceAction()
    {
        $brandUrl = $this->params()->fromRoute("brandUrl");
        
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        
        $brandServiceTable = $this->getServiceLocator()->get('LabBase\Model\BrandServiceTable');
        
        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandUrl);
        
        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }
        
        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title');

        $service = new BrandService($loggedInUser->id, $thisBrand->id, $title, $content, $imageData);

        $brandServiceTable->saveBrandService($service);

        return new JsonModel([
            'status' => 'ok',
            'service' => $service
        ]);
    }
    
    
    public function editServiceAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var CategoryTable $categoryTable
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        if(!$req->isPost() || empty($req->getPost('content')) 
            || (empty($req->getPost('title')) || $req->getPost('serviceId') < 1)) {
            return new JsonModel([
                'status' => 'Please enter all fields.'
            ]);
        }

        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        
        $brandServiceTable = $this->getServiceLocator()->get('LabBase\Model\BrandServiceTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.'
            ]);
        }

        $serviceId = $req->getPost('serviceId');
        
        $serviceData = $brandServiceTable->getBrandServiceById($serviceId);
        
        if($serviceData->creatorId != $loggedInUser->id)
        {
            return new JsonModel([
                'status' => 'You do not have permission for update this service.'
            ]);
        }
        
        if(!$serviceData)
        {
            return new JsonModel([
                'status' => 'You can not update this service because this service not found in database.'
            ]);
        }
        
        $content = $req->getPost('content');
        $imageData = $req->getPost('imageData');
        $title = $req->getPost('title');

        $service = new BrandService($loggedInUser->id, $thisBrand->id, $title, $content, $imageData);

        if($serviceId > 0)
        {
            $service->id = $serviceId;
        }
        
        $brandServiceTable->saveBrandService($service);

        return new JsonModel([
            'status' => 'ok',
            'service' => $service
        ]);
    }
    
    private function truncateString($str, $chars, $to_space, $replacement="...") {
       if($chars > strlen($str)) return $str;

       $str = substr($str, 0, $chars);

       $space_pos = strrpos($str, " ");
       if($to_space && $space_pos >= 0) {
           $str = substr($str, 0, strrpos($str, " "));
       }

       return($str . $replacement);
    }
    
        /*
     * POST image - base64 string of the image data (POST)
     *  or
     * FILE image
     */
    public function addServiceImageAction() {
        // Todo: remove this function if it is not needed (replaced by brandFeedController?)
        throw new \Exception("This method is used and not to be removed!");
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         * @var ImageSaver $imageSaver
         */
        $brandId = $this->params()->fromRoute("brandUrl");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();
        if(!$req->isPost()) {
            return new JsonModel([
                'status' => 'Only post requests are accepted.'
            ]);
        }

        //$brandPostTable = $this->getServiceLocator()->get('LabBase\Model\BrandPostTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $imageSaver = $this->getServiceLocator()->get('LabBase\Model\ImageSaver');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisBrand = $brandTable->getBrandByUrl($brandId);

        if(!$thisBrand || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisBrand->id, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to post.'
            ]);
        }

        if(($image = ImageSaver::GetImageFromClient($this)) === FALSE) {
            return new JsonModel([
                'status' => 'Invalid image.'
            ]);
        }

        $this->getServiceLocator()->get('Application')->getEventManager()->attach(\Zend\Mvc\MvcEvent::EVENT_RENDER, function($event){
            $event->getResponse()->getHeaders()->addHeaderLine('Content-Type', 'text/plain');
        }, -10000);


        $image = $imageSaver->uploadImage($image, 'service-image-'.$thisBrand->id.'-'.mt_rand().rand(), null, null, $loggedInUser->id, $thisBrand->id);


        return new JsonModel([
            'status' => 'ok',
            'imageName' => $image->stringIdentifier,
            'imageLink' => $image->getURL(),
        ]);

    }
    
    public function deleteServiceAction() {
        /**
         * @var $brandPostTable BrandPostTable
         * @var UserTable $userTable
         * @var BrandTable $brandTable
         */

        $serviceId = $this->params()->fromRoute("serviceId");

        /** @var \Zend\Http\Request $req */
        $req = $this->getRequest();

        $brandServiceTable = $this->getServiceLocator()->get('LabBase\Model\BrandServiceTable');
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');

        $loggedInUser = $userTable->getLoggedInUser();
        $thisService = $brandServiceTable->getBrandServiceById($serviceId);

        if(!$thisService || !$loggedInUser || !$brandTable->isBrandManagedByUser($thisService->brandId, $loggedInUser->id)) {
            return new JsonModel([
                'status' => 'You have to log in as the site manager to product.',
                'thisService' => $thisService,
                'loggedInUser' => $loggedInUser,
            ]);
        }
        
        if(!$thisService)
        {
            return new JsonModel([
                'status' => 'Service you select not found.',
            ]);
        }

        $brandServiceTable->deleteBrandService($thisService->id);

        return new JsonModel(['status' => 'ok']);
    }
    
    public function getPopupServiceAction()
    {
        $serviceId = $this->params()->fromRoute("serviceId");
        
        $brandServiceTable = $this->getServiceLocator()->get('LabBase\Model\BrandServiceTable');
        $service = $brandServiceTable->getBrandServiceById($serviceId);
        
        $userCanPost = false;
        $userCanEditBrand = false;
        
        $brandTable = $this->getServiceLocator()->get('LabBase\Model\BrandTable');
        $thisBrand = $brandTable->getBrandById($service->brandId);
       
       // Supplied brand URL doesn't exist?
        if($thisBrand == null) {
            return $this->notFoundAction();
        }

        $userTable = $this->getServiceLocator()->get('LabBase\Model\UserTable');
        $loggedInUser = $userTable->getLoggedInUser();
        // Logged in user is this brand's manager?
        if($loggedInUser != null) {
            // todo: delegation
            if ($thisBrand->ownerId == $loggedInUser->id) {
                $userCanEditBrand = true;

                // Update last visited brand
                if($loggedInUser->lastVisitedBrandId != $thisBrand->id) {

                    $loggedInUser->lastVisitedBrandId = $thisBrand->id;
                    $userTable->saveUser($loggedInUser);
                }
            }   
        }
     
        $this->layout()->setTemplate('lab-base/brand-service/get-popup-service'); 
       
         $view = new ViewModel([
            'thisBrand' => $thisBrand,

            // using 2 different variables in template, in case of future changes
            'userCanPost' => $userCanPost,
            'userCanEditBrand' => $userCanEditBrand,
            
            'service' => $service,
        ]);

        return $view;
    }
}
