<?php

namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class BrandTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return Brand[]
     */
    public function getAllBrands() {
        $rowset = $this->tableGateway->select();
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * @param int $count
     * @return Brand[]
     */
    public function getNBrands($count) {
        $rowset = $this->tableGateway->select(function(Select $select) use($count) {
            $select->limit($count);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    public function getPopularBrands($count = 0)
    {
        $rowset = $this->tableGateway->select(function(Select $select) use($count) {
            if($count > 0)
            {
                $select->limit($count);
            }
            $select->order('loveCount DESC');
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    
    public function getLatestBrands($count = 0)
    {
        $sql = new Sql($this->tableGateway->adapter);
        
        $currentTime = strtotime('-1 hour');
        
        $limit = '';
        if($count > 0)
        {
            $limit = 'LIMIT '.$count;
        }
        
        $statement = $this->tableGateway->adapter->createStatement(<<<SQL
SELECT Brand.id FROM Brand INNER JOIN BrandPost ON BrandPost.brandId = Brand.id WHERE BrandPost.dateCreated < $currentTime GROUP BY Brand.id ORDER BY BrandPost.dateCreated DESC $limit
SQL
);
        $rowset = $statement->execute();
        
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $this->getBrandById($row['id']);
        }
        return $ret;
    }
    
    public function getNewBrands($count = 0)
    {
        $sql = new Sql($this->tableGateway->adapter);
        
        $currentTime = strtotime('-1 hour');
        
        $limit = '';
        if($count > 0)
        {
            $limit = 'LIMIT '.$count;
        }
        
        $statement = $this->tableGateway->adapter->createStatement(<<<SQL
SELECT Brand.id FROM Brand INNER JOIN BrandPost ON BrandPost.brandId = Brand.id WHERE BrandPost.dateCreated >= $currentTime GROUP BY Brand.id ORDER BY BrandPost.dateCreated DESC $limit
SQL
);
        $rowset = $statement->execute();
        
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $this->getBrandById($row['id']);
        }
        return $ret;
    }
    
    /**
     * @param $id int
     * @return Brand|null
     */
    public function getBrandById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $url string
     * @return Brand|null
     */
    public function getBrandByUrl($url) {
        $rowset = $this->tableGateway->select(['url' => $url]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $name string
     * @return Brand|null
     */
    public function getBrandByExactName($name) {
        $rowset = $this->tableGateway->select(['name' => $name]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $name string
     * @return Brand[]
     */
    public function searchBrandsByName($name) {
        $where = new Where();
        $name = str_replace('%', '\\%', $name);
        $where->like('name', '%'.$name.'%');
        // todo: order by lastest post
        $rowset = $this->tableGateway->select($where);

        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * @param $id
     * @return Brand|null
     */
    public function getBrandByPostId($id) {
        $sql = new Sql($this->tableGateway->adapter);
        $statement = $this->tableGateway->adapter->createStatement(<<<SQL
SELECT Brand.* FROM Brand INNER JOIN BrandPost ON BrandPost.brandId = Brand.id WHERE BrandPost.id = ${id}
SQL
);
        $rowset = $statement->execute();

        /*
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->join('posts', 'posts.brandId = brands.id');
            $select->where('posts.id', $id);
            error_log($select->getSqlString());
        });*/
        $row = $rowset->current();

        if (!$row) {
            return null;
        }

        // todo: fix sql mess
        $ret = new Brand();
        $ret->exchangeArray($row);

        return $ret;
    }
    
    public function getBrandByProductId($id) {
        return $this->getBrandByPostId($id);
        // todo: remove below if works
        $sql = new Sql($this->tableGateway->adapter);
        $statement = $this->tableGateway->adapter->createStatement(<<<SQL
SELECT Brand.* FROM Brand INNER JOIN BrandProduct ON BrandProduct.brandId = Brand.id WHERE BrandProduct.id = ${id}
SQL
);
        $rowset = $statement->execute();

        /*
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->join('posts', 'posts.brandId = brands.id');
            $select->where('posts.id', $id);
            error_log($select->getSqlString());
        });*/
        $row = $rowset->current();

        if (!$row) {
            return null;
        }

        // todo: fix sql mess
        $ret = new Brand();
        $ret->exchangeArray($row);

        return $ret;
    }

    /**
     * @param $ownerId int
     * @return Brand[]
     */
    public function getBrandsByOwner($ownerId) {
        $rowset = $this->tableGateway->select(['ownerId' => (int)$ownerId]);
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * @param $managerUserId int
     * @return Brand[]
     */
    public function getBrandsManagedByUser($managerUserId) {
        // todo: delegation
        $rowset = $this->tableGateway->select(['ownerId' => (int)$managerUserId]);
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * @param Brand $brand
     */
    public function updateRelationCount(Brand $brand) {
        $broken = true;
        if($broken) return; // todo: fix SQL procedure

        $id = $brand->id;
        if(!$id) return;
        $statement = $this->tableGateway->adapter->createStatement("SELECT updateBrandRelations(".$brand->id.") AS loveCount");
        $rowset = $statement->execute();
        $row = $rowset->current();
        if (!$row) {
            return;
        }
        $brand->loveCount = (int)$row['loveCount'];
    }

    /**
     * @param $brandId int
     * @param $managerUserId int
     * @return bool
     */
    public function isBrandManagedByUser($brandId, $managerUserId) {
        // todo: delegation
        $rowset = $this->tableGateway->select(['id' => $brandId, 'ownerId' => (int)$managerUserId]);
        return $rowset->current() ? true : false;
    }

    /**
     * @param Brand $brand Brand to save
     * @throws \Exception If brand id != 0 but it's not in the db
     */
    public function saveBrand(Brand $brand)
    {
        $data = $brand->toTable();

        $id = (int)$brand->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $brand->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getBrandById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Brand id does not exist');
            }
        }
    }

    /**
     * @param $id int
     */
    public function deleteBrand($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}