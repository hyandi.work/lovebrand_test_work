<?php

namespace LabBase\Model;

class CommentProductInfo extends ProductsComment {

    public $productTitle;


    public function exchangeArray($data) {
        $this->id           = (!empty($data['id']))             ? (int) $data['id'] : null;
        $this->posterId     = (!empty($data['posterId']))       ? (int) $data['posterId'] : null;
        $this->posterBrandId= (!empty($data['posterBrandId']))  ?       $data['posterBrandId'] : null;
        $this->posterDisplayName = (!empty($data['posterDisplayName'])) ? $data['posterDisplayName'] : null;
        $this->posterEmail  = (!empty($data['posterEmail']))    ?       $data['posterEmail'] : null;
        $this->productId       = (!empty($data['productId']))         ?       $data['productId'] : null;
        $this->dateCreated  = (!empty($data['dateCreated']))    ? (int) $data['dateCreated'] : null;
        $this->content      = (!empty($data['content']))        ?       $data['content'] : null;
        $this->approved     = (!empty($data['approved']))       ? (bool)$data['approved'] : null;
        $this->rejected     = (!empty($data['rejected']))       ? (bool)$data['rejected'] : null;
        $this->seen         = (!empty($data['seen']))           ? (bool)$data['seen'] : null;

        $this->productTitle     = (!empty($data['productTitle']))             ? $data['productTitle'] : null;
    }

}