<?php

namespace LabBase\Model;

class User extends DBModel {
    public $id;
    public $fullName;
    public $email;
    public $confirmedEmail;
    public $password;
    public $registerDate;
    public $hasAvatarTimestamp;
    public $lastVisitedBrandId;
    public $sessionId;
    public $sessionKey;
    public $sessionExpires;
    public $userType;
    public $lastReadPostTimestamp;

    public static $sessionLength = 144000; // 40 hours in seconds

    public function getClientArray() {
        return [
            'id'        => (int)$this->id,
            'name'      => $this->fullName,
            'userType'  => (int)$this->userType,
            'hasAvatar' => (bool)$this->hasAvatarTimestamp
        ];
    }

    protected function databaseMappings() {
        return [
            [ 'id',                 DBModel::TYPE_INT , DBModel::DB_PKEY ],
            [ 'fullName',           DBModel::TYPE_VARCHAR_64 , DBModel::STR_NO_HTML ],
            [ 'email',              DBModel::TYPE_VARCHAR_128, DBModel::STR_TRIM | DBModel::DB_INDEX ],
            [ 'confirmedEmail',     DBModel::TYPE_VARCHAR_128, DBModel::STR_TRIM | DBModel::DB_NULL ],
            [ 'password',           DBModel::TYPE_VARCHAR_64 , DBModel::INTERP_STRING ],
            [ 'registerDate',       DBModel::TYPE_INT , DBModel::INTERP_TIMESTAMP ],
            [ 'hasAvatarTimestamp', DBModel::TYPE_INT , DBModel::DB_NULL | DBModel::INTERP_TIMESTAMP ],
            [ 'lastVisitedBrandId', DBModel::TYPE_INT , DBModel::DB_NULL ],
            [ 'sessionId',          DBModel::TYPE_INT , DBModel::DB_NULL | DBModel::DB_INDEX ],
            [ 'sessionKey',         DBModel::TYPE_INT , DBModel::DB_NULL ],
            [ 'sessionExpires',     DBModel::TYPE_INT , DBModel::DB_NULL | DBModel::INTERP_TIMESTAMP ],
            [ 'userType',           DBModel::TYPE_INT , DBModel::INTERP_DEFAULT ],
            [ 'lastReadPostTimestamp',DBModel::TYPE_INT , DBModel::DB_NULL | DBModel::INTERP_TIMESTAMP ],
        ];
    }

    // todo: migrate fully to image linker
    public function getAvatarInitials() {
        $name = $this->fullName;
        $name = trim(str_replace("  ", " ", $name));
        $words = explode(' ', $name);
        if(!$words || count($words) < 2) {
            return substr($name, 0, 2);
        }
        return strtoupper(substr($words[0], 0, 1)) . strtolower(substr($words[count($words)-1], 0, 1));
    }

    public function getAvatarLinkOrNull() {
        if($this->hasAvatarTimestamp) {
            return ImageSaver::GetImageS3Link(ImageSaver::GetImageS3Path('user-avatar-' . $this->id));
        }else{
            return null;
        }
    }

    /**
     * Changes password and session keys
     * @param $raw string Raw password to set
     * @throws \Exception If for some reason the password couldn't be set
     */
    public function changePassword($raw) {
        $hash = User::hashPassword($raw);
        if($hash === FALSE) { // password_hash might return FALSE, retry once.
            $hash = User::hashPassword($raw); // Now we may have more entropy
            if($hash === FALSE) {
                throw new \Exception("Couldn't generate password hash");
            }
        }
        $this->password = $hash;
        // Invalidate sessions on password change
        $this->generateSession();
    }

    /**
     * @param $raw string Raw password to check against
     * @return bool TRUE if password is valid
     */
    public function checkPassword($raw) {
        return password_verify($raw, $this->password);
    }

    protected static function hashPassword($raw) {
        return password_hash($raw, PASSWORD_BCRYPT, ['cost' => 10]);
    }

    /**
     * Create new session key, invalidating the old one
     */
    public function generateSession() {
        // mask out negative ints
        $this->sessionId = (0x7ffffffe & User::getRandomInt()) + 1;
        $this->sessionKey = (0x7ffffffe & User::getRandomInt()) + 1;
        $this->sessionExpires = time() + User::$sessionLength;
    }

    private function isSessionValid()
    {
        return  $this->sessionId > 0
                && $this->sessionKey > 0
                && $this->sessionExpires > time();
    }

    /**
     * Update session expiry date
     */
    public function updateSession() {
        $this->sessionExpires = time() + User::$sessionLength; // make session valid for the next $sessionLength hours

        if(!$this->isSessionValid()) {
            $this->generateSession();
        }
    }


    protected function getRandomInt() {
        $rand = openssl_random_pseudo_bytes(4, $strong);
        if($rand === FALSE) {
            $rand = 0;
            $strong = FALSE;
        }else{
            $rand = hexdec(bin2hex($rand));
        }

        if(!$strong) {
            list($usec, $sec) = explode(' ', microtime());
            mt_srand($rand ^ mt_rand() ^ ((float) $sec + ((float) $usec * 100000)));
            $rand = $rand ^ mt_rand(0,0xffffffff);
            $rand = $rand ^ mt_rand(0,0xffffffff);
            $rand = $rand ^ mt_rand(0,0xffffffff);
        }
        return $rand;
    }

    public function isSuspended() {
        return $this->userType == 0;
    }

    public function isAdmin() {
        return $this->userType >= 2;
    }

    public function isSuperAdmin() {
        return $this->userType >= 3;
    }

    public function setSuspended($isSuspended) {
        $this->userType = $isSuspended ? 0 : 1;
    }

    /**
     * @return bool Does the user have a confirmed email
     */
    public function hasValidEmail() {
        return !empty($this->confirmedEmail);
    }


    /**
     * @return bool True if the user hasn't confirmed their email (either because it's the first e-mail,
     * or the user has changed the email and has not yet confirmed the new one)
     */
    public function emailNeedsConfirmation() {
        return $this->confirmedEmail !== $this->email;
    }

    // todo: setAdmin / setRank / something like this

}