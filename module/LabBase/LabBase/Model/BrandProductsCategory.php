<?php

namespace LabBase\Model;

class BrandProductsCategory extends DBModel {
    public $id;
    public $brandId;
    public $categoryName;
    public $public;
    public $canDelete; // 0 -> Can Delete, 1 -> Can not delete

    /**
     * @param int $userId
     * @param int $brandId
     * @param int $relationType
     */
    public function __construct($brandId = 0, $categoryName = '', $canDelete = 0, $public = 1) {
        $this->brandId      = $brandId;
        $this->categoryName = $categoryName;
        $this->canDelete    = $canDelete;
        $this->public       = $public;
    }
    
    protected function databaseMappings() {
        return [
            DBModel::Field('id',                DBModel::TYPE_INT,          DBModel::DB_PKEY),
            DBModel::Field('categoryName',      DBModel::TYPE_VARCHAR_255,  DBModel::DB_NULL),
            
            DBModel::Field('public',            DBModel::TYPE_BYTE,         DBModel::INTERP_BOOL),
            DBModel::Field('canDelete',         DBModel::TYPE_BYTE,          DBModel::INTERP_BOOL),
            
            DBModel::Field('brandId',           DBModel::TYPE_INT,   DBModel::INTERP_FKEY | DBModel::DB_NULL, [
                DBModel::ForeignKey( 'brand', 'Brand', 'id', DBModel::FK_NOT_ENFORCED)
            ])
        ];
    }

}