<?php

namespace LabBase\Model;

class BrandRelation extends DBModel {
    public $id;
    public $userId;
    public $brandId;
    public $relationType;
    public $date;

    /**
     * @param int $userId
     * @param int $brandId
     * @param int $relationType
     */
    public function __construct($userId = 0, $brandId = 0, $relationType = 0) {
        $this->userId       = $userId;
        $this->brandId      = $brandId;
        $this->relationType = $relationType;
        $this->date         = time();
    }

    /**
     * [ fieldName, fieldType, interpType|dbAttribs ]
     * [ 'name', DBModel::VARCHAR_64, DBModel::INTERP_DEFAULT | DBModel::DB_NULL ]
     * @return array of arrays each describing a database field
     */
    protected function databaseMappings() {
        // todo: add composite key
        return [
            DBModel::Field('id',                DBModel::TYPE_INT,          DBModel::DB_PKEY),
            DBModel::Field('userId',            DBModel::TYPE_INT,          DBModel::INTERP_FKEY, [
                DBModel::ForeignKey( 'user', 'User', 'id', DBModel::FK_NOT_ENFORCED)
            ]),
            DBModel::Field('brandId',           DBModel::TYPE_INT,   DBModel::INTERP_FKEY | DBModel::DB_NULL, [
                DBModel::ForeignKey( 'brand', 'Brand', 'id', DBModel::FK_NOT_ENFORCED)
            ]),
            DBModel::Field('relationType',      DBModel::TYPE_INT,          DBModel::DB_NULL),
            DBModel::Field('date',              DBModel::TYPE_INT,          DBModel::DB_NULL | DBModel::INTERP_TIMESTAMP),
        ];
    }
}