<?php

namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class AboutmeTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandPost|null
     */
    public function getAboutmeById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }
    
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
    public function getAboutmeByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select
                ->where->equalTo('brandId', $brandId);
        });
        $row = $rowset->current();
        if (!$row) {
            return null;
        }

        return $row;
    }
    
    /**
     * @param BrandPost $post Brand to save
     * @throws \Exception If brand id != 0 but it's not in the db
     */
    public function saveAboutme(Aboutme $aboutme)
    {
        $data = $aboutme->toTable();

        $id = (int)$aboutme->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $aboutme->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getAboutmeById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Brand id does not exist');
            }
        }
    }

    public function deleteAboutme($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}