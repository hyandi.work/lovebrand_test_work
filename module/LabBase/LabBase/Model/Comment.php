<?php

namespace LabBase\Model;

class Comment extends DBModel {
    public $id;
    public $posterId;
    public $posterBrandId;
    public $posterDisplayName;
    public $posterEmail;
    public $postId;
    public $dateCreated;
    public $content;
    public $approved;
    public $rejected;
    public $seen;

    public $posterCachedName = false;

    /**
     * @param $posterId int
     * @param $posterBrandId int
     * @param $postId int
     * @param $content string
     * @param $posterName string
     * @param $posterEmail string
     */
    public function __construct($posterId = null, $posterBrandId = null, $postId = null, $content = null, $posterName = null, $posterEmail = null) {
        $this->posterId = $posterId;
        $this->posterBrandId = $posterBrandId;
        $this->postId = $postId;
        $this->content = $content;
        $this->posterDisplayName = $posterName;
        $this->posterEmail = $posterEmail;

        $this->dateCreated = time();
        $this->approved = false;
        $this->rejected = false;
        $this->seen     = false;
    }

    /*public function exchangeArray($data) {
        $this->id           = (!empty($data['id']))             ? (int) $data['id'] : null;
        $this->posterId     = (!empty($data['posterId']))       ? (int) $data['posterId'] : null;
        $this->posterBrandId= (!empty($data['posterBrandId']))  ? (int) $data['posterBrandId'] : null;
        $this->posterDisplayName = (!empty($data['posterDisplayName'])) ? $data['posterDisplayName'] : null;
        $this->posterEmail  = (!empty($data['posterEmail']))    ?       $data['posterEmail'] : null;
        $this->postId       = (!empty($data['postId']))         ? (int) $data['postId'] : null;
        $this->dateCreated  = (!empty($data['dateCreated']))    ? (int) $data['dateCreated'] : null;
        $this->content      = (!empty($data['content']))        ?       $data['content'] : null;
        $this->approved     = (!empty($data['approved']))       ? (bool)$data['approved'] : null;
        $this->rejected     = (!empty($data['rejected']))       ? (bool)$data['rejected'] : null;
        $this->seen         = (!empty($data['seen']))           ? (bool)$data['seen'] : null;
    }*/
    
    protected function databaseMappings() {
        return [
            [ 'id',                 DBModel::TYPE_INT        , DBModel::DB_PKEY ],
            [ 'posterId',           DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'posterBrandId',      DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],            
            
            [ 'postId',             DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'dateCreated',        DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            
            [ 'posterDisplayName',  DBModel::TYPE_VARCHAR_255, DBModel::INTERP_DEFAULT ],
            [ 'posterEmail',        DBModel::TYPE_VARCHAR_255, DBModel::INTERP_DEFAULT ],
            
            [ 'content',            DBModel::TYPE_TEXT       , DBModel::STR_NO_HTML ],
            
            [ 'approved',           DBModel::TYPE_BYTE       , DBModel::INTERP_BOOL ],
            [ 'rejected',           DBModel::TYPE_BYTE       , DBModel::INTERP_BOOL ],
            [ 'seen',               DBModel::TYPE_BYTE       , DBModel::INTERP_BOOL ],            
        ];
    }

}