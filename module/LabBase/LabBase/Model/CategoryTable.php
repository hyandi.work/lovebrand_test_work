<?php

namespace LabBase\Model;

use Zend\Db\TableGateway\TableGateway;

class CategoryTable {
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    /**
     * @var array
     */
    protected $categoryCache;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->categoryCache = [];
    }

    public function generateCategoryUrl($categoryName, $brandId) {

        /* NUMERIC IDS
        // Leave only letters and digits in the category name, replace everything else with a dash
        $url = preg_replace('/[^a-z0-9\-]/i', '-', $categoryName);
        // Remove multiple dashes
        $url = preg_replace('/-+/', '-', $url);
        // Remove dashes from the beginning and the end
        $url = preg_replace('/^-+/', '', $url);
        $url = preg_replace('/-+$/', '', $url);

        // In case the brand name consists only of non-alpha characters:
        if(empty(str_replace('-', '', $url))) {
            $url = 'category';
            $isRepeat = TRUE;
        }else {
            $isRepeat = $this->getCategoryByUrlAndBrand($url, $brandId) != null;
        }
        */

        $isRepeat = true;
        $url = '';

        $tries = 0;
        while($isRepeat && $tries < 40) {
            // Start at 1000..9999, every 5 fails, add one zero
            $min = pow(10, 3 + (int)$tries/5);
            $max = $min * 10 - 1;
            $suffix = rand($min, $max);
            $tries++;
//            $isRepeat = $this->getCategoryByUrlAndBrand($url . '-' . $suffix, $brandId) != null;
            $isRepeat = $this->getCategoryByUrlAndBrand($suffix, $brandId) != null;
            if(!$isRepeat) {
                //$url = $url . '-' . $suffix;
                $url = $suffix;
                break;
            }
        }
        if($isRepeat) {
            throw new \Exception("Couldn't generate category URL.");
        }
        return $url;
    }

    /**
     * @param $id int
     * @return Category|null
     */
    public function getCategoryById($id) {
        $id = (int)$id;

        // try cache first
        if(array_key_exists($id, $this->categoryCache)) {
            return $this->categoryCache[$id];
        }

        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        // todo: maybe optimize this bit, join it with getCategoriesByBrand
        $row->children = $this->getCategoriesByParent($row->id);
        // cache it
        $this->categoryCache[$id] = $row;
        return $row;
    }

    /**
     * @param $url string
     * @param $brandId int
     * @return Category|null
     */
    public function getCategoryByUrlAndBrand($url, $brandId) {
        /**
         * @var $row Category
         */
        $rowset = $this->tableGateway->select(['url' => $url, 'brandId' => $brandId]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }

        // cached?
        if(array_key_exists($row->id, $this->categoryCache)) {
            $row = $this->categoryCache[$row->id];
        }else {
            $row->children = $this->getCategoriesByParent($row->id);
            // cache it
            $this->categoryCache[$row->id] = $row;
        }

        return $row;
    }

    /**
     * @param $brandId int
     * @return array Category[]
     */
    public function getCategoriesByBrand($brandId) {
        $rowset = $this->tableGateway->select(['brandId' => $brandId, 'parentId' => null]);
        $ret = [];
        foreach ($rowset as $row) {
            /**
             * @var $row Category
             */
            // todo: optimize this bit (do one query, process children-parent relationship in php)

            // Check the cache to skip the children search
            if(array_key_exists($row->id, $this->categoryCache)) {
                $row = $this->categoryCache[$row->id];
            } else {
                $row->children = $this->getCategoriesByParent($row->id);
                $this->categoryCache[$row->id] = $row;
            }

            $ret[] = $row;
        }

        return $ret;
    }

    protected function getCategoriesByParent($parentId) {
        $rowset = $this->tableGateway->select(['parentId' => $parentId]);
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }

    /*protected function categoryToTable(Category $cat) {
        return [
            'brandId'       => $cat->brandId,
            'categoryName'  => $cat->categoryName,
            'url'           => $cat->url,
            'position'      => $cat->position,

            'displayInFeed' => $cat->displayInFeed,
            'enabled'       => $cat->enabled,
            'parentId'      => $cat->parentId,
            'system'        => $cat->isSystemCategory,
            'singlePostCategory' => $cat->isSinglePostCategory,
        ];
    }*/

    /**
     * @param Category $cat Category to save
     * @returns int Saved brand ID
     * @throws \Exception If brand id != 0 but it's not in the db
     */
    public function saveCategory(Category $cat)
    {
        $data = $cat->toTable();

        $id = (int)$cat->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $cat->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getCategoryById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Category id does not exist');
            }
        }

        return $cat->id;
    }

    public function deleteCategory($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}