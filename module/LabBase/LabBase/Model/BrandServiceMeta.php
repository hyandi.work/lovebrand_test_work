<?php

namespace LabBase\Model;

class BrandServiceMeta extends DBModel {

    public $id;
    public $brandId;
    
    public $title;
    public $description;

    public $publicTitle; // 1 -> public, 0 ->  not
    public $publicDescription; // 1 -> public, 0 ->  not

    public function __construct($brandId = NULL, $title = NULL, $description = NULL, $publicTitle = 1, $publicDescription = 1) {
        if($brandId === NULL)
            return;

        $this->brandId = $brandId;
        $this->title = htmlspecialchars($title);

        $this->publicTitle = $publicTitle;
        $this->publicDescription = $publicDescription;
        
        $this->setServiceMetaContents($description);
    }

    public function setServiceMetaTitle($titleRaw) {
        $this->title = htmlspecialchars($titleRaw);
    }

    public function setServiceMetaContents($contentRaw) {
        $this->description = $this->escapeServiceMetaContent($contentRaw);
/*        $this->productContent = "<p>" . $this->escapeProductContent($contentRaw) . "</p>";*/
    }
    
    private function escapeServiceMetaContent($raw) {
        // todo: check tag parameters [important before live]
        $out = '';
        $raw = preg_replace('/<\s*br\s*\/?\s*>/', '</p><p>', $raw);
        /*
        preg_match_all('/(.*?)<\s*?(\/?)\s*?(\w+)\s*?([^>]+)?([\/ ]*)>/', $raw, $htmlNodes, PREG_SET_ORDER);

        foreach ($htmlNodes as $node) {
            if(in_array($node[3], BrandPost::$allowedTags)) {
                $out = $out . $node[0];
            } else {
                $out = $out . htmlspecialchars($node[0]);
            }
        }
        */

        return $raw;
    }

    /*public function exchangeArray($data) {
        $this->id               = (!empty($data['id']))                 ? (int)$data['id']           : null;
        $this->creatorId        = (!empty($data['creatorId']))          ? (int)$data['creatorId']    : null;
        $this->brandId          = (!empty($data['brandId']))            ? (int)$data['brandId']      : null;

        $this->dateCreated      = (!empty($data['dateCreated']))        ? (int)$data['dateCreated']  : null;
        
        $this->serviceTitle     = (!empty($data['serviceTitle']))       ? $data['serviceTitle']      : null;        
        $this->serviceContent   = (!empty($data['serviceContent']))     ? $data['serviceContent']    : null;
        $this->serviceImageData = (!empty($data['serviceImageData']))   ? $data['serviceImageData']  : null;
        
        $this->public           = (!empty($data['public']))             ? (int)$data['public']       : 1;
    }*/
    
    protected function databaseMappings() {
        return [
            DBModel::Field('id',                    DBModel::TYPE_INT,          DBModel::DB_PKEY),
            
            DBModel::Field('title',                 DBModel::TYPE_VARCHAR_128,  DBModel::DB_NULL),
            DBModel::Field('description',           DBModel::TYPE_TEXT,         DBModel::STR_NO_HTML),
            
            DBModel::Field('publicTitle',           DBModel::TYPE_BYTE,         DBModel::INTERP_BOOL),
            DBModel::Field('publicDescription',     DBModel::TYPE_BYTE,         DBModel::INTERP_BOOL),
            
            DBModel::Field('brandId',               DBModel::TYPE_INT,          DBModel::INTERP_FKEY | DBModel::DB_NULL, [
                DBModel::ForeignKey( 'brand', 'Brand', 'id', DBModel::FK_NOT_ENFORCED)
            ])
        ];
    }
}
