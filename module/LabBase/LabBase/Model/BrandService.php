<?php

namespace LabBase\Model;

class BrandService extends DBModel {

    public $id;
    public $creatorId;
    public $brandId;

    public $dateCreated;

    public $serviceImageData;
    public $serviceTitle;
    public $serviceContent;

    public $public; // 1 -> public, 0 ->  not

    private static $allowedTags = [ 'p', 'strong', 'a', 'span', 'em', 'img', 'br' ];

    public function __construct($userId = NULL, $brandId = NULL, $titleRaw = NULL, $contentRaw = NULL, $imageData = NULL, $public = 1) {
        if($userId === NULL && $brandId === NULL)
            return;

        $this->creatorId = $userId;
        $this->brandId = $brandId;
        $this->dateCreated = time();
        $this->serviceTitle = htmlspecialchars($titleRaw);

        $this->serviceImageData = $imageData;

        $this->public = $public;
        
        $this->setServiceContents($contentRaw);
    }

    public function setServiceTitle($titleRaw) {
        $this->serviceTitle = htmlspecialchars($titleRaw);
    }

    public function setServiceContents($contentRaw) {
        $this->serviceContent = $this->escapeServiceContent($contentRaw);
/*        $this->productContent = "<p>" . $this->escapeProductContent($contentRaw) . "</p>";*/
    }
    
    private function escapeServiceContent($raw) {
        // todo: check tag parameters [important before live]
        $out = '';
        $raw = preg_replace('/<\s*br\s*\/?\s*>/', '</p><p>', $raw);
        /*
        preg_match_all('/(.*?)<\s*?(\/?)\s*?(\w+)\s*?([^>]+)?([\/ ]*)>/', $raw, $htmlNodes, PREG_SET_ORDER);

        foreach ($htmlNodes as $node) {
            if(in_array($node[3], BrandPost::$allowedTags)) {
                $out = $out . $node[0];
            } else {
                $out = $out . htmlspecialchars($node[0]);
            }
        }
        */

        return $raw;
    }

    /**
     * @return string[] Array of links to attached images in simple editor mode
     */
    public function getImageLinkArray() {
        if(!$this->serviceImageData) return [];
        $images = explode(";", $this->serviceImageData);
        if (count($images) < 1) return [];
        return $images;
    }

    /*public function exchangeArray($data) {
        $this->id               = (!empty($data['id']))                 ? (int)$data['id']           : null;
        $this->creatorId        = (!empty($data['creatorId']))          ? (int)$data['creatorId']    : null;
        $this->brandId          = (!empty($data['brandId']))            ? (int)$data['brandId']      : null;

        $this->dateCreated      = (!empty($data['dateCreated']))        ? (int)$data['dateCreated']  : null;
        
        $this->serviceTitle     = (!empty($data['serviceTitle']))       ? $data['serviceTitle']      : null;        
        $this->serviceContent   = (!empty($data['serviceContent']))     ? $data['serviceContent']    : null;
        $this->serviceImageData = (!empty($data['serviceImageData']))   ? $data['serviceImageData']  : null;
        
        $this->public           = (!empty($data['public']))             ? (int)$data['public']       : 1;
    }*/
    
    protected function databaseMappings() {
        return [
            DBModel::Field('id',                    DBModel::TYPE_INT,          DBModel::DB_PKEY),
            
            DBModel::Field('serviceTitle',          DBModel::TYPE_VARCHAR_128,  DBModel::DB_NULL),
            DBModel::Field('serviceContent',        DBModel::TYPE_TEXT,         DBModel::STR_NO_HTML),
            DBModel::Field('serviceImageData',      DBModel::TYPE_TEXT,         DBModel::STR_NO_HTML),
            
            DBModel::Field('dateCreated',           DBModel::TYPE_INT,          DBModel::DB_NULL | DBModel::INTERP_TIMESTAMP),
            
            DBModel::Field('public',                DBModel::TYPE_BYTE,         DBModel::INTERP_BOOL),
            
            DBModel::Field('creatorId',             DBModel::TYPE_INT,          DBModel::INTERP_FKEY, [
                DBModel::ForeignKey( 'creator', 'User', 'id', DBModel::FK_NOT_ENFORCED)
            ]),
            DBModel::Field('brandId',               DBModel::TYPE_INT,          DBModel::INTERP_FKEY | DBModel::DB_NULL, [
                DBModel::ForeignKey( 'brand', 'Brand', 'id', DBModel::FK_NOT_ENFORCED)
            ])
        ];
    }
}
