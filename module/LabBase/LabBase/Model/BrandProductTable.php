<?php

namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class BrandProductTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandPost|null
     */
    public function getBrandProductById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
   /* public function getBrandProductsByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select->where->equalTo('brandId', $brandId);
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }*/

    /**
     * @param $brandId int
     * @return int
     */
    public function getBrandProductCountBrand($brandId) {
        $id = (int)$brandId;
        // todo: count properly
        $rowset = $this->tableGateway->select(['id' => $id]);

        if(!$rowset) return 0;

        return $rowset->count();
    }

    /**
     * @param $categoryId
     * @param $collectionId
     * @return array|null
     */
    public function getBrandProductsByCategoryAndCollection($categoryId, $collectionId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($categoryId, $collectionId) {
            $select->where->equalTo( 'categoryId' , $categoryId )->AND->equalTo( 'collectionId' , $collectionId );
                
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }

    /**
     * @param $brandId int
     * @param int $limit
     * @return array|null
     */
    public function getBrandProductsByBrand($brandId, $limit = 10) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $limit) {
            $select
                ->where->equalTo('brandId', $brandId);
                $select->order('dateCreated DESC');
                if($limit > 0) {
                    $select->limit($limit);
                }
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }

    /**
     * @param $brandId int
     * @param $categoryId int
     * @return array BrandPost|null
     */
    public function getBrandProductsByBrandAndCategory($brandId, $categoryId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $categoryId) {
            $select->where->equalTo('brandId', $brandId)->AND->equalTo('categoryId', $categoryId)->AND->equalTo('collectionId', 0);
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    public function getBrandProductsByBrandAndCategoryWithLimit($brandId, $categoryId, $limit = 0) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $categoryId, $limit) {
            $select->where->equalTo('brandId', $brandId)->AND->equalTo('categoryId', $categoryId);
            if($limit > 0)
            {
                $select->limit($limit);
            }
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }


    /**
     * @param BrandProduct $product
     * @throws \Exception If brand id != 0 but it's not in the db
     * @internal param BrandPost $post Brand to save
     */
    public function saveBrandProduct(BrandProduct $product)
    {
        $data = $product->toTable();

        $id = (int)$product->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $product->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getBrandProductById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Brand id does not exist');
            }
        }
    }

    public function deleteBrandProduct($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}