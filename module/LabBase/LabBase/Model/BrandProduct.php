<?php

namespace LabBase\Model;

class BrandProduct extends DBModel {

    public $id;
    public $creatorId;
    public $brandId;
    public $categoryId;

    public $collectionId;

    public $dateCreated;

    public $productImageData;
    public $productTitle;
    public $productContent;

    public $public; // 1 -> public, 0 ->  not

    private static $allowedTags = [ 'p', 'strong', 'a', 'span', 'em', 'img', 'br' ];

    public function __construct($userId = NULL, $brandId = 0, $categoryId = 0, $collectionId = 0, $titleRaw = NULL, $contentRaw = NULL, $imageData = NULL, $public = 1) {
        if($userId === NULL && $brandId === NULL)
            return;

        $this->creatorId = $userId;
        $this->brandId = $brandId;
        $this->categoryId = $categoryId;
        $this->collectionId = $collectionId;
        $this->dateCreated = time();
        $this->productTitle = htmlspecialchars($titleRaw);

        $this->productImageData = $imageData;

        $this->public = $public;
        
        $this->setProductContents($contentRaw);
    }

    public function setProductTitle($titleRaw) {
        $this->productTitle = htmlspecialchars($titleRaw);
    }

    public function setProductContents($contentRaw) {
        $this->productContent = $this->escapeProductContent($contentRaw);
/*        $this->productContent = "<p>" . $this->escapeProductContent($contentRaw) . "</p>";*/
    }
    
    private function escapeProductContent($raw) {
        // todo: check tag parameters [important before live]
        $out = '';
        $raw = preg_replace('/<\s*br\s*\/?\s*>/', '</p><p>', $raw);
        /*
        preg_match_all('/(.*?)<\s*?(\/?)\s*?(\w+)\s*?([^>]+)?([\/ ]*)>/', $raw, $htmlNodes, PREG_SET_ORDER);

        foreach ($htmlNodes as $node) {
            if(in_array($node[3], BrandPost::$allowedTags)) {
                $out = $out . $node[0];
            } else {
                $out = $out . htmlspecialchars($node[0]);
            }
        }
        */

        return $raw;
    }

    /**
     * @return string[] Array of links to attached images in simple editor mode
     */
    public function getImageLinkArray() {
        if(!$this->productImageData) return [];
        $images = explode(";", $this->productImageData);
        if (count($images) < 1) return [];
        return $images;
    }

    protected function databaseMappings() {
        return [
            DBModel::Field('id',                DBModel::TYPE_INT,          DBModel::DB_PKEY),
            
            DBModel::Field('categoryId',        DBModel::TYPE_INT,          DBModel::DB_NULL),
            DBModel::Field('collectionId',      DBModel::TYPE_INT,          DBModel::DB_NULL),
            
            DBModel::Field('productTitle',      DBModel::TYPE_VARCHAR_255,  DBModel::STR_TRIM),
            DBModel::Field('productContent',    DBModel::TYPE_TEXT,         DBModel::STR_NO_HTML),
            DBModel::Field('productImageData',  DBModel::TYPE_TEXT,         DBModel::STR_NO_HTML),
            
            DBModel::Field('dateCreated',       DBModel::TYPE_INT,          DBModel::DB_NULL | DBModel::INTERP_TIMESTAMP),
            DBModel::Field('public',            DBModel::TYPE_BYTE,          DBModel::INTERP_BOOL),
            
            DBModel::Field('creatorId',        DBModel::TYPE_INT,          DBModel::INTERP_FKEY, [
                DBModel::ForeignKey( 'creator', 'User', 'id', DBModel::FK_NOT_ENFORCED)
            ]),
            DBModel::Field('brandId',           DBModel::TYPE_INT,   DBModel::INTERP_FKEY | DBModel::DB_NULL, [
                DBModel::ForeignKey( 'brand', 'Brand', 'id', DBModel::FK_NOT_ENFORCED)
            ])
        ];
    }
}
