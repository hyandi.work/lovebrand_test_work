<?php
namespace LabBase\Model;

use Zend\Db\Sql\Delete;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;

class BrandProductsCategoryTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandProductsCategory|null
     */
    public function getById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $id int
     * @return BrandProductsCategory[]
     */
    public function getCategoriesByBrand($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo('brandId', $id);
            $select->limit(50);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    
    /**
     * @param $id int
     * @return BrandProductsCategory[]
     */
    public function getCategoriesByBrandPublic($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo('brandId', $id)->AND->equalTo('public', 1);
            $select->limit(50);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /*protected function toTable(BrandProductsCategory $obj) {
        return [
            'brandId'       => $obj->brandId,
            'categoryName'  => $obj->categoryName,
            'public'        => $obj->public,
            'canDelete'     => $obj->canDelete
        ];
    }*/

    public function save(BrandProductsCategory $obj)
    {
        $data = $obj->toTable();

        $id = (int)$obj->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $obj->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Given id does not exist');
            }
        }
    }

    /**
     * @param $id int
     */
    public function delete($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }

    public function makeCategoryPublicOrNot($id, $showOrNot)
    {
        $category = $this->getById($id);
        if(!empty($category))
        {
            $category->public = $showOrNot;
            $this->save($category);
        }
    }
}