<?php

namespace AwsModule\Filter\File;

use Aws\S3\S3Client;
use Zend\Stdlib\ErrorHandler;
use Zend\Stdlib\Exception\RuntimeException;

class ImageS3UploadFilter extends S3RenameUpload
{
    /**
     * @param S3Client $client
     * @param array    $options
     */
    public function __construct(S3Client $client, $options = [])
    {
        parent::__construct($client, $options);
    }

    /**
     * @param array|string $fileArray Array with name, tmp_name and data - the data from the data key will be used
     * instead of the tmp_name file.
     */
    public function filter($fileArray) {
        if(!isset($fileArray['data']))
            throw new RuntimeException();

            $targetFile = $this->getFinalTarget($fileArray);

        $this->checkFileExists($targetFile);
        $this->moveUploadedFile($fileArray['data'], $targetFile);
    }

    protected function moveUploadedFile($sourceData, $targetFile)
    {
        ErrorHandler::start();
        $result = file_put_contents($targetFile, $sourceData);
        $warningException = ErrorHandler::stop();
        if (!$result || null !== $warningException) {
            throw new RuntimeException(
                sprintf("The file could not be uploaded. An error occurred while processing the file."),
                0,
                $warningException
            );
        }

        return $result;
    }

}
