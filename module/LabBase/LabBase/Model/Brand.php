<?php

namespace LabBase\Model;

class Brand extends DBModel {
    public $id;
    public $name;
    public $strippedName;
    public $imageId;
    public $ownerId;
    public $public;
    public $url;
    public $description;
    public $brandType;
    public $hasBanner;
    public $loveCount;
    public $connectCount;
    public $storyCount;
    public $lovedStoryCount;
    public $managementTeamName;
    
    public $location;
    public $phone;
    public $web;

    /** @var User $owner */
    public $owner;
    public $postCount;

    public function getClientArray() {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'isPublic'  => (bool)$this->public,
            'url'       => $this->url,
            'loveCount'         => (int)$this->loveCount,
            'connectCount'      => (int)$this->connectCount,
            'storyCount'        => (int)$this->storyCount,
            'lovedStoryCount'   => (int)$this->lovedStoryCount,
            'owner'   => (int)$this->ownerId,

        ];
    }

    protected function databaseMappings() {
        return [
            [ 'id',                 DBModel::TYPE_INT        , DBModel::DB_PKEY ],
            [ 'name',               DBModel::TYPE_VARCHAR_128, DBModel::STR_NO_HTML ],
            [ 'strippedName',       DBModel::TYPE_VARCHAR_128, DBModel::INTERP_DEFAULT ],
            [ 'imageId',            DBModel::TYPE_VARCHAR_32 , DBModel::INTERP_DEFAULT ],
            [ 'ownerId',            DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'url',                DBModel::TYPE_VARCHAR_128, DBModel::INTERP_DEFAULT ],
            [ 'description',        DBModel::TYPE_TEXT       , DBModel::STR_NO_HTML ],
            [ 'brandType',          DBModel::TYPE_BYTE       , DBModel::INTERP_DEFAULT ],
            [ 'public',             DBModel::TYPE_BYTE       , DBModel::INTERP_BOOL ],
            [ 'hasBanner',          DBModel::TYPE_BYTE       , DBModel::INTERP_BOOL ],
            [ 'loveCount',          DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'connectCount',       DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'storyCount',         DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'lovedStoryCount',    DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'managementTeamName', DBModel::TYPE_VARCHAR_128, DBModel::DB_NULL ],
            [ 'location',           DBModel::TYPE_VARCHAR_255, DBModel::DB_NULL ],
            [ 'phone',              DBModel::TYPE_VARCHAR_128, DBModel::DB_NULL ],
            [ 'web',                DBModel::TYPE_VARCHAR_255, DBModel::DB_NULL ],
        ];
    }

    /**
     * Returns avatar with size $minSize or greter, unless only samller is available
     * @param int $minSize Minimum side dimension in px
     * @return string Absolute URL to the avatar image
     */
    public function getAvatarLink($minSize = 128) {
        // todo: avatar scaling
        if(!$this->hasBanner) {
            return null;
        }
        return ImageSaver::GetImageS3LinkFromName('brand-avatar-'.$this->id);
    }
    
    public function isPersonalPage()
    {
        $isPersonalPage = false;
        if($this->brandType === 1)
        {
            $isPersonalPage = true;
        }
        
        return $isPersonalPage;
    }
}