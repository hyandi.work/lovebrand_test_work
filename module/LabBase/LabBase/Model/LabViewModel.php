<?php
/**
 * Author: Sebi
 * Date: 2014-10-28
 * Time: 16:24
 */

namespace LabBase\Model;


use Zend\View\Model\ViewModel;

class LabViewModel extends ViewModel {

    public function __construct($vars) {
        parent::__construct();

        $vm = new ViewModel();
        $vm->setTemplate("lab-base/feed-layout");

    }

} 