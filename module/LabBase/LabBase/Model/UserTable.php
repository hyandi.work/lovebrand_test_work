<?php

namespace LabBase\Model;

use Zend\Db\Adapter\Exception\InvalidQueryException;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class UserTable {

    protected $tableGateway;
    protected $loggedInUser = FALSE;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function overrideLoggedInUser($user) {
        $this->loggedInUser = $user;
    }

    /**
     * @return User|null Logged in user, or null
     */
    public function getLoggedInUser() {
        if($this->loggedInUser === FALSE) {
            if (isset($_COOKIE) && isset($_COOKIE['sid'])) {
                list($sid, $key) = explode('.', $_COOKIE['sid']);
                $this->loggedInUser = $this->getUserBySession($sid, $key);
                if($this->loggedInUser) {

                    // Check if the user is suspended
                    if($this->loggedInUser->isSuspended()) {
                        $this->loggedInUser = null;

                    // check if we should update session
                    }elseif($this->loggedInUser->sessionExpires < time() + User::$sessionLength - 600) {
                        // If it was not updated for 5 minutes, update now
                        $this->loggedInUser->updateSession();
                        $this->saveUser($this->loggedInUser);
                    }
                }
            }else{
                $this->loggedInUser = null;
            }
        }
        return $this->loggedInUser;
    }

    /**
     * @return User[]
     */
    public function getAllUsers() {
        $rowset = $this->tableGateway->select();
        $ret = [];

        foreach($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }

    /**
     * @param $id int
     * @return User|null
     */
    public function getUserById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $email string
     * @return User|null
     */
    public function getUserByEmail($email) {
        $rowset = $this->tableGateway->select(['email' => $email]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $sid int Session ID
     * @param $key int Session Key
     * @return User|null User, if succeeded
     */
    public function getUserBySession($sid, $key) {
        $id = (int)$sid;
        $rowset = $this->tableGateway->select(['sessionId' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        if ($row->sessionKey !== (int)$key) {
            return null;
        }
        if ($row->sessionExpires < time()) {
            return null;
        }

        return $row;
    }

    /**
     * @param User $user User to save
     * @throws \Exception If user id != 0 but it's not in the db
     */
    public function saveUser(User $user)
    {
        $data = $user->toTable();

        $id = (int)$user->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $user->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getUserById($id)) {
                try {
                    $this->tableGateway->update($data, ['id' => $id]);
                }catch (InvalidQueryException $ex) {
                    // This might happen if session_id already exists in the db
                    // In this rare case, generate new ID
                    $user->generateSession();
                    $data = $user->toTable();
                    $this->tableGateway->update($data, ['id' => $id]);
                }

            } else {
                throw new \Exception('User id does not exist');
            }
        }
    }

    public function deleteUser($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
    public function updateLastReadPostTimestamp($id)
    {
        $this->tableGateway->update(['lastReadPostTimestamp' => time()], ['id' => $id]);
    }
}