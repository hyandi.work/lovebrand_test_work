<?php

namespace LabBase\Model;

class ProductsComment {
    public $id;
    public $posterId;
    public $posterBrandId;
    public $posterDisplayName;
    public $posterEmail;
    public $productId;
    public $dateCreated;
    public $content;
    public $approved;
    public $rejected;
    public $seen;

    public $posterCachedName = false;

    /**
     * @param $posterId int
     * @param $posterBrandId int
     * @param $postId int
     * @param $content string
     * @param $posterName string
     * @param $posterEmail string
     */
    public function __construct($posterId = null, $posterBrandId = null, $productId = null, $content = null, $posterName = null, $posterEmail = null) {
        $this->posterId = $posterId;
        $this->posterBrandId = $posterBrandId;
        $this->productId = $productId;
        $this->content = $content;
        $this->posterDisplayName = $posterName;
        $this->posterEmail = $posterEmail;

        $this->dateCreated = time();
        $this->approved = false;
        $this->rejected = false;
        $this->seen     = false;
    }

    public function exchangeArray($data) {
        $this->id           = (!empty($data['id']))             ? (int) $data['id'] : null;
        $this->posterId     = (!empty($data['posterId']))       ? (int) $data['posterId'] : null;
        $this->posterBrandId= (!empty($data['posterBrandId']))  ? (int) $data['posterBrandId'] : null;
        $this->posterDisplayName = (!empty($data['posterDisplayName'])) ? $data['posterDisplayName'] : null;
        $this->posterEmail  = (!empty($data['posterEmail']))    ?       $data['posterEmail'] : null;
        $this->productId    = (!empty($data['productId']))      ? (int) $data['productId'] : null;
        $this->dateCreated  = (!empty($data['dateCreated']))    ? (int) $data['dateCreated'] : null;
        $this->content      = (!empty($data['content']))        ?       $data['content'] : null;
        $this->approved     = (!empty($data['approved']))       ? (bool)$data['approved'] : null;
        $this->rejected     = (!empty($data['rejected']))       ? (bool)$data['rejected'] : null;
        $this->seen         = (!empty($data['seen']))           ? (bool)$data['seen'] : null;
    }

}