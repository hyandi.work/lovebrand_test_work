<?php

namespace LabBase\Model;

class BrandPostLoved extends DBModel {
    public $id;
    public $postId;
    public $brandId;
    public $userId;
    public $loved; // 1 => Loved, 0 => Un-loved
    public $date;

    /**
     * @param int $userId
     * @param int $brandId
     * @param int $relationType
     */
    public function __construct($userId = 0, $brandId = 0, $postId = 0, $loved = 0) {
        $this->userId       = $userId;
        $this->brandId      = $brandId;
        $this->postId       = $postId;
        $this->loved        = $loved;
        $this->date         = time();
    }

    /*public function exchangeArray($data) {
        $this->id           = (!empty($data['id']))        ? (int) $data['id']      : null;
        $this->userId       = (!empty($data['userId']))    ? (int) $data['userId']  : null;
        $this->brandId      = (!empty($data['brandId']))   ? (int) $data['brandId'] : null;
        $this->postId       = (!empty($data['postId']))    ? (int) $data['postId']  : null;
        $this->loved        = (!empty($data['loved']))     ? (int) $data['loved']   : null;
        $this->date         = (!empty($data['date']))      ? (int) $data['date']    : null;
    }*/
    
    protected function databaseMappings() {
        return [
            [ 'id',           DBModel::TYPE_INT        , DBModel::DB_PKEY ],
            [ 'userId',       DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'brandId',      DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'postId',       DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            
            [ 'loved',        DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'date',         DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
        ];
    }

}