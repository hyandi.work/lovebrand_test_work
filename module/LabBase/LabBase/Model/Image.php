<?php

namespace LabBase\Model;

use AwsModule\Filter\File\S3RenameUpload;

class Image extends DBModel {
    public $id;
    public $url;
    public $dateCreated;
    public $stringIdentifier;

    /**
     * @var int UserID who uploaded the image or 0 for system image
     */
    public $uploaderId;
    /**
     * @var int ID of the brand to which the image belongs
     */
    public $brandId;

    /**
     * @var $resX int Resolution X
     * @var $resY int Resolution Y
     */

    public $resX;
    public $resY;

    /**
     * @param $url string
     * @param string $resolutionString Resolution specified by string in format: 123x234
     * @param $uploaderId int|null UserID who uploaded the image
     * @throws \Exception on invalid string
     */
    public function __construct($url = null, $resolutionString = null, $uploaderId = null)
    {
        $this->url = $url;
        $this->dateCreated = time();
        if($url != null) {
            $exp = explode('x', $resolutionString);
            if (count($exp) != 2) {
                throw new \Exception("Invalid image resolution");
            }
            $this->resX = (int)$exp[0];
            $this->resY = (int)$exp[1];
        }

        if ($uploaderId) {
            $this->uploaderId = $uploaderId;
        }else{
            $this->uploaderId = null;
        }
    }

    protected function databaseMappings() {
        return [
            DBModel::Field('id',                DBModel::TYPE_INT,          DBModel::DB_PKEY),
            DBModel::Field('stringIdentifier',  DBModel::TYPE_VARCHAR_128,  DBModel::DB_NULL),
            DBModel::Field('url',               DBModel::TYPE_VARCHAR_128,          DBModel::STR_TRIM),
            DBModel::Field('resX',              DBModel::TYPE_INT,          DBModel::DB_NULL),
            DBModel::Field('resY',              DBModel::TYPE_INT,          DBModel::DB_NULL),
            DBModel::Field('dateCreated',       DBModel::TYPE_INT,          DBModel::DB_NULL | DBModel::INTERP_TIMESTAMP),
            DBModel::Field('uploaderId',        DBModel::TYPE_INT,          DBModel::INTERP_FKEY, [
                DBModel::ForeignKey( 'uploader', 'User', 'id', DBModel::FK_NOT_ENFORCED)
            ]),
            DBModel::Field('brandId',           DBModel::TYPE_INT,   DBModel::INTERP_FKEY | DBModel::DB_NULL, [
                DBModel::ForeignKey( 'brand', 'Brand', 'id', DBModel::FK_NOT_ENFORCED)
            ])
        ];
    }

    public function getURL() {
        return ImageSaver::GetImageS3Link($this->url);
    }

}