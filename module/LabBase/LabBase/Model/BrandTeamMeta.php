<?php

namespace LabBase\Model;

class BrandTeamMeta extends DBModel {

    public $id;
    public $creatorId;
    public $brandId;

    public $teamKey;
    public $teamValue;

    public $public; // 1 -> public, 0 ->  not

    public function __construct($userId = NULL, $brandId = NULL, $teamKey = NULL, $teamValue = NULL, $public = 1) {
        if($userId === NULL && $brandId === NULL)
            return;

        $this->creatorId = $userId;
        $this->brandId = $brandId;
        
        $this->teamKey = $teamKey;
        $this->setTeamMetaContents($teamValue);
        
        $this->public = $public;
    }

    public function setTeamMetaContents($contentRaw) {
        $this->teamValue = $this->escapeTeamMetaContent($contentRaw);
    }
    
    private function escapeTeamMetaContent($raw) {
        // todo: check tag parameters [important before live]
        $out = '';
        $raw = preg_replace('/<\s*br\s*\/?\s*>/', '</p><p>', $raw);
        /*
        preg_match_all('/(.*?)<\s*?(\/?)\s*?(\w+)\s*?([^>]+)?([\/ ]*)>/', $raw, $htmlNodes, PREG_SET_ORDER);

        foreach ($htmlNodes as $node) {
            if(in_array($node[3], BrandPost::$allowedTags)) {
                $out = $out . $node[0];
            } else {
                $out = $out . htmlspecialchars($node[0]);
            }
        }
        */

        return $raw;
    }

   /* public function exchangeArray($data) {
        $this->id          = (!empty($data['id']))          ? (int)$data['id']          : null;
        $this->creatorId   = (!empty($data['creatorId']))   ? (int)$data['creatorId']   : null;
        $this->brandId     = (!empty($data['brandId']))     ? (int)$data['brandId']     : null;
        
        $this->teamKey     = (!empty($data['teamKey']))     ? $data['teamKey']          : null;        
        $this->teamValue   = (!empty($data['teamValue']))   ? $data['teamValue']        : null;
        
        $this->public      = (!empty($data['public']))      ? (int)$data['public']      : 0;
    }*/
    
    
    protected function databaseMappings() {
        return [
            DBModel::Field('id',                DBModel::TYPE_INT,          DBModel::DB_PKEY),
            
            DBModel::Field('teamKey',           DBModel::TYPE_VARCHAR_255,  DBModel::DB_NULL),
            DBModel::Field('teamValue',         DBModel::TYPE_TEXT,         DBModel::STR_NO_HTML),
            
            DBModel::Field('public',            DBModel::TYPE_BYTE,         DBModel::INTERP_BOOL),
            
            DBModel::Field('creatorId',         DBModel::TYPE_INT,          DBModel::INTERP_FKEY, [
                DBModel::ForeignKey( 'creator', 'User', 'id', DBModel::FK_NOT_ENFORCED)
            ]),
            DBModel::Field('brandId',           DBModel::TYPE_INT,   DBModel::INTERP_FKEY | DBModel::DB_NULL, [
                DBModel::ForeignKey( 'brand', 'Brand', 'id', DBModel::FK_NOT_ENFORCED)
            ])
        ];
    }
}
