<?php
namespace LabBase\Model;

use Zend\Db\Sql\Delete;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;

class BrandPostLovedTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandPostLoved|null
     */
    public function getById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $id int
     * @return BrandPostLoved[]
     */
    public function getByBrand($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo('brandId', $id);
            $select->order('date DESC');
            $select->limit(50);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * @param $id int
     * @return BrandPostLoved[]
     */
    public function getByUser($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo('userId', $id);
            $select->order('date DESC');
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    
    /**
     * @param $id int
     * @return BrandPostLoved[]
     */
    public function getByPost($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo('postId', $id);
            $select->order('date DESC');
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * @param $userId
     * @param $brandId
     * @return BrandPostLoved[]
     */
    public function getByUserBrand($userId, $brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($userId, $brandId) {
            $select->where->equalTo('brandId', $brandId)->AND->equalTo('userId', $userId);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    
    /**
     * @param $userId
     * @param $brandId
     * @param $postId
     * @return BrandPostLoved[]
     */
    public function getByUserBrandPost($userId, $brandId, $postId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($userId, $brandId, $postId) {
            $select->where->equalTo('brandId', $brandId)->AND->equalTo('userId', $userId)->AND->equalTo('postId', $postId);
        });
        
        $row = $rowset->current();
        
        if(empty($row))
        {
            return null;
        }
        
        return $row;
    }

    /**
     * @param $userId
     * @param $brandId
     * @param $relationType
     * @return BrandPostLoved|null
     */
    public function getByUserPostLoved($userId, $postId, $loved) {
        $rowset = $this->tableGateway->select(function (Select $select) use($userId, $postId, $loved) {
            $select->where->
                equalTo('brandId', $brandId)
                ->AND->equalTo('userId', $userId)
                ->AND->equalTo('loved', $loved);
        });
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    public function getCountLovedPostByBrand($brandId) {
        return 0;
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select->where->
                equalTo('brandId', $brandId)
                ->AND->equalTo('loved', 1);
            
            $select->group('postId');
        });
        
//        if(!$rowset) return 0;

//        return $rowset->count();
		
		return null;
    }
    

    /*protected function toTable(BrandPostLoved $obj) {
        return [
            'date'          => $obj->date,
            'userId'        => $obj->userId,
            'brandId'       => $obj->brandId,
            'postId'        => $obj->postId,
            'loved'         => $obj->loved
        ];
    }*/

    public function save(BrandPostLoved $obj)
    {
        $data = $obj->toTable();

        $id = (int)$obj->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $obj->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Given id does not exist');
            }
        }
    }

    /**
     * @param $id int
     */
    public function delete($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }

    
    /**
    * @param $postId
    */
    public function postLoveCount($postId)
    {
        $loveCnt = $this->tableGateway->select(function (Select $select) use($postId) {
            $select->where->equalTo('postId', $postId)->AND->equalTo('loved', 1);
/*            $select->columns(array('loveCnt' => new Expression('COUNT(id)')));*/
        });
        $ret = array();
        foreach($loveCnt as $row) {
            $ret[] = $row;
        }
        return count($ret);
    }
}