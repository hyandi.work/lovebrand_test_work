<?php
namespace LabBase\Model;

use Zend\Db\Sql\Delete;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;

class BrandProductLovedTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandPostLoved|null
     */
    public function getById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $id int
     * @return BrandPostLoved[]
     */
    public function getByUser($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo('creatorId', $id);
            $select->order('date DESC');
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    
    /**
     * @param $id int
     * @return BrandPostLoved[]
     */
    public function getByProduct($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo('productId', $id);
            $select->order('date DESC');
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    
    /**
     * @param $userId
     * @param $brandId
     * @param $postId
     * @return BrandPostLoved[]
     */
    public function getByUserProduct($userId, $productId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($userId, $productId) {
            $select->where->equalTo('creatorId', $userId)->AND->equalTo('productId', $productId);
        });
        
        $row = $rowset->current();
        
        if(empty($row))
        {
            return null;
        }
        
        return $row;
    }

    /**
     * @param $userId
     * @param $brandId
     * @param $relationType
     * @return BrandPostLoved|null
     */
    public function getByUserPostLoved($userId, $productId, $loved) {
        $rowset = $this->tableGateway->select(function (Select $select) use($userId, $productId, $loved) {
            $select->where->
                equalTo('productId', $productId)
                ->AND->equalTo('creatorId', $userId)
                ->AND->equalTo('loved', $loved);
        });
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }


    protected function toTable(BrandProductLoved $obj) {
        return [
            'date'          => $obj->date,
            'creatorId'     => $obj->creatorId,
            'productId'     => $obj->productId,
            'loved'         => $obj->loved
        ];
    }

    public function save(BrandProductLoved $obj)
    {
        $data = $this->toTable($obj);

        $id = (int)$obj->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $obj->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Given id does not exist');
            }
        }
    }

    /**
     * @param $id int
     */
    public function delete($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }

    
    /**
    * @param $postId
    */
    public function productLoveCount($productId)
    {
        $loveCnt = $this->tableGateway->select(function (Select $select) use($productId) {
            $select->where->equalTo('productId', $productId)->AND->equalTo('loved', 1);
/*            $select->columns(array('loveCnt' => new Expression('COUNT(id)')));*/
        });
        $ret = array();
        foreach($loveCnt as $row) {
            $ret[] = $row;
        }
        return count($ret);
    }
}