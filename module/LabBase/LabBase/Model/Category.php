<?php

namespace LabBase\Model;

class Category extends DBModel {
    public $id;
    public $brandId;
    public $categoryName;
    public $url;
    public $position;
    public $displayInFeed;
    public $enabled;
    public $parentId;
    public $isSystemCategory;
    public $isSinglePostCategory;

    /**
     * @var $children Category[]
     */
    public $children;


    /**
     * @param $catName string
     * @param $catUrl string
     * @param $brandId int
     * @param $position int
     * @param $displayInFeed bool
     * @param $parentId int
     * @param bool $system Cannot be removed by the user
     * @param bool $singlePost Only one post can exist in this category
     */
    public function __construct($catName = null, $catUrl = null, $brandId = null, $position = null, $displayInFeed = false, $parentId = null, $system = false, $singlePost = false) {
        $this->categoryName = $catName;
        $this->url = $catUrl;
        $this->brandId = $brandId;
        $this->position = $position;
        $this->displayInFeed = $displayInFeed;
        $this->parentId = $parentId;
        $this->enabled = true;
        $this->children = [];
        $this->isSinglePostCategory = $singlePost;
        $this->isSystemCategory = $system;
    }

    protected function databaseMappings() {
        return [
            [ 'id',                     DBModel::TYPE_INT           , DBModel::DB_PKEY ],
            [ 'brandId',                DBModel::TYPE_INT           , DBModel::INTERP_DEFAULT ],
           
            [ 'categoryName',           DBModel::TYPE_VARCHAR_255   , DBModel::INTERP_DEFAULT ],
            [ 'url',                    DBModel::TYPE_VARCHAR_128   , DBModel::INTERP_DEFAULT ],
            
            [ 'position',               DBModel::TYPE_INT           , DBModel::INTERP_DEFAULT ],
            [ 'parentId',               DBModel::TYPE_INT           , DBModel::INTERP_DEFAULT ],
            
            [ 'displayInFeed',          DBModel::TYPE_BYTE          , DBModel::INTERP_BOOL ],
            
            [ 'enabled',                DBModel::TYPE_INT           , DBModel::INTERP_DEFAULT ],
            [ 'isSystemCategory',       DBModel::TYPE_BYTE          , DBModel::INTERP_BOOL ],
            
            [ 'isSinglePostCategory',   DBModel::TYPE_BYTE          , DBModel::INTERP_BOOL ],          
        ];
    }
    
   /* public function exchangeArray($data) {
        $this->id               = (!empty($data['id']))         ? (int) $data['id'] : null;
        $this->brandId          = (!empty($data['brandId']))    ? (int) $data['brandId'] : null;
        $this->categoryName     = (!empty($data['categoryName'])) ?     $data['categoryName'] : null;
        $this->url              = (!empty($data['url']))        ?       $data['url'] : null;
        $this->position         = (!empty($data['position']))   ? (int) $data['position'] : null;
        $this->parentId         = (!empty($data['parentId']))   ? (int) $data['parentId'] : null;
        $this->displayInFeed    = (!empty($data['displayInFeed']))     ? (bool)$data['displayInFeed'] : null;
        $this->enabled          = (!empty($data['enabled']))    ? (bool)$data['enabled'] : null;
        $this->isSystemCategory = (!empty($data['system']))     ? (bool)$data['system'] : null;
        $this->isSinglePostCategory = (!empty($data['singlePostCategory'])) ? (bool)$data['singlePostCategory'] : null;
    }*/
}