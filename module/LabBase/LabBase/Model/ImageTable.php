<?php
namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class ImageTable {

    protected $tableGateway;
    protected $imageIdCache = [];
    protected $imageUrlCache = [];

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return Image|null
     */
    public function getImageById($id) {
        if(isset($this->imageIdCache[$id])) {
            return $this->imageIdCache[$id];
        }

        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }

        $this->imageUrlCache[$row->url] = $row;
        $this->imageIdCache[$row->id] = $row;

        return $row;
    }

    /**
     * @param $url
     * @return Image|null
     */
    public function getImageByUrl($url) {
        if(isset($this->imageUrlCache[$url])) {
            return $this->imageUrlCache[$url];
        }

        $rowset = $this->tableGateway->select(['url' => $url]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }

        $this->imageUrlCache[$row->url] = $row;
        $this->imageIdCache[$row->id] = $row;

        return $row;
    }

    /**
     * @param $userId
     * @return Image|null
     */
    public function getImagesByUploaderUserId($userId) {
        $rowset = $this->tableGateway->select(['uploaderId' => $userId]);
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * @return Image|null
     */
    public function getAllSystemImages() {
        return $this->getImagesByUploaderUserId(0);
    }


    /**
     * @param Image $image Image to save
     * @throws \Exception If image id != 0 but it's not in the db
     */
    public function saveImage(Image $image)
    {
        $data = $image->toTable();

        $id = (int)$image->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $image->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getImageById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Image id does not exist');
            }
        }
    }

    /**
     * @param $id int
     */
    public function deleteImageDBEntryRememberToRemoveFromS3($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}