<?php

namespace LabBase\Model;

class BrandProductsCollection extends DBModel {
    public $id;
    public $categoryId;
    public $brandId;
    public $collectionName;
    public $public; // 1 -> Public, 0-> Not

    /**
     * @param int $userId
     * @param int $brandId
     * @param int $relationType
     */
    public function __construct($categoryId = 0, $brandId = 0, $collectionName = '', $public = 1) {
        $this->categoryId       = $categoryId;
        $this->brandId          = $brandId;
        $this->collectionName   = $collectionName;
        $this->public           = $public;
    }

    protected function databaseMappings() {
        return [
            DBModel::Field('id',                DBModel::TYPE_INT,          DBModel::DB_PKEY),
            DBModel::Field('categoryId',        DBModel::TYPE_INT,          DBModel::DB_NULL),
            
            DBModel::Field('collectionName',    DBModel::TYPE_VARCHAR_255,          DBModel::STR_TRIM),
            DBModel::Field('public',            DBModel::TYPE_BYTE,          DBModel::INTERP_BOOL),
            
            DBModel::Field('brandId',           DBModel::TYPE_INT,   DBModel::INTERP_FKEY | DBModel::DB_NULL, [
                DBModel::ForeignKey( 'brand', 'Brand', 'id', DBModel::FK_NOT_ENFORCED)
            ])
        ];
    }

}