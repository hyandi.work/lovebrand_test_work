<?php

namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class BrandPostTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandPost|null
     */
    public function getBrandPostById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
    public function getBrandPostsByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select->where->equalTo('brandId', $brandId)->AND->notEqualTo('draft', 1);
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }

    /**
     * @param $brandId int
     * @return int
     */
    public function getBrandPostCountBrand($brandId) {
        $id = (int)$brandId;
        // todo: count properly
        $rowset = $this->tableGateway->select(['brandId' => $id]);

        if(!$rowset) return 0;

        return $rowset->count();
    }
    
    /**
     * @param $postType 
     * @return array(BrandPost)|null
     */
    public function getBrandFeedPostsByTypeByBrand($postType, $brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($postType, $brandId) {
            /*$select
                ->join('Category', 'Category.id = categoryId', ['displayInFeed']);*/
                switch($postType)
                {
                    case "photo" : 
                            $select->where->equalTo('BrandPost.postType', 4)->AND->equalTo('BrandPost.brandId', $brandId)/*->AND->equalTo('displayInFeed', 1)*/->AND->notEqualTo('BrandPost.draft', 1);
                        break;
                    case "video" : 
                            $select->where->equalTo('BrandPost.postType', 5)->AND->equalTo('BrandPost.brandId', $brandId)/*->AND->equalTo('displayInFeed', 1)*/->AND->notEqualTo('BrandPost.draft', 1);
                        break;
                    default :
                            $select->where->notEqualTo('BrandPost.postType', 4)->AND->equalTo('BrandPost.brandId', $brandId)/*->AND->equalTo('displayInFeed', 1)*/->AND->notEqualTo('BrandPost.draft', 1);
                        break;
                }
                
                $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    
    public function getBrandFeedPostsByTypesByBrand($postTypes, $brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($postTypes, $brandId) {
            $select
                ->join('Category', 'Category.id = categoryId', ['displayInFeed']);
                
                $select->where->in('BrandPost.postType', $postTypes)->AND->equalTo('BrandPost.brandId', $brandId)->AND->equalTo('displayInFeed', 1)->AND->notEqualTo('BrandPost.draft', 1);
                
                $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    /**
     * @param 
     * @return array(BrandPost)|null
     */
    public function getBrandFeedPostsAlbums($brandId, $albumId = 0) {
        $rowset = $this->tableGateway->select(function (Select $select) use($albumId, $brandId) {
            /*$select
                ->join('Category', 'Category.id = categoryId', ['displayInFeed']);*/
                if($albumId > 0)
                {
                    $select->where->equalTo('BrandPost.id', $albumId);
                }
                else
                {
                    $select->where->equalTo('BrandPost.postType', 4)->AND->equalTo('BrandPost.brandId', $brandId)/*->AND->equalTo('displayInFeed', 1)*/->AND->notEqualTo('BrandPost.draft', 1)->AND->notEqualTo('BrandPost.postTitle', '');
                    $select->order('dateCreated DESC');
                }    
        });
        
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }
        
        return $ret;
    }
    
    public function getBrandFeedOtherAlbums($albumId = 0, $limit = NULL, $brandId = 0)
    {
        $rowset = $this->tableGateway->select(function (Select $select) use($albumId, $limit, $brandId) {
            $select
                ->join('Category', 'Category.id = categoryId', ['displayInFeed']);
            $select->where->equalTo('BrandPost.postType', 4)->AND->equalTo('displayInFeed', 1)->AND->notEqualTo('BrandPost.draft', 1)->AND->notEqualTo('BrandPost.postTitle', '')->AND->notEqualTo('BrandPost.id', $albumId)->equalTo('BrandPost.brandId', $brandId);
            if($limit != NUll)
            {
                $select->limit($limit);
            }
            $select->order('dateCreated DESC');
        });
        
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    public function getBrandFeedCntPhotoAndAlbums($brandId) {
        $rowset = $this->getBrandFeedPostsByTypeByBrand('photo', $brandId);
        
        $photoCounters = $albumCounters = 0;
        foreach ($rowset as $row) {
            $photoImgaes = json_decode($row->postImageData);
            $photoCounters += count($photoImgaes);
            if(!empty($row->postTitle))
            {
                $albumCounters++;
            }
        }
        
        return array('photo_cnt' => $photoCounters, 'album_cnt' => $albumCounters);
    }
    
    public function getBrandFeedCntVideos($brandId) {
        $rowset = $this->getBrandFeedPostsByTypeByBrand('video', $brandId);
        
        $videoCounters = 0;
        foreach ($rowset as $row) {
            if(!empty($row->videoData))
            {
                $videoCounters++;
            }
        }
        
        return $videoCounters;
    }

    /**
     * @param $brandId
     * @param $userId 
     * @return array(BrandPost)|null
     */
    public function getBrandFeedDraftsByUserAndBrand($brandId, $userId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $userId) {
            //$select->join('Category', 'Category.id = categoryId', ['displayInFeed']);
                
            $select->where->notEqualTo('BrandPost.postType', 4)
                ->AND->equalTo('BrandPost.brandId', $brandId)
                ->AND->equalTo('BrandPost.creatorId', $userId)
                ->AND->equalTo('BrandPost.draft', 1);

            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
    public function getBrandFeedPostsByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select
                //->join('Category', 'Category.id = categoryId', ['displayInFeed'])
                ->where->equalTo('brandId', $brandId)
                //->AND->equalTo('displayInFeed', 1)
                ->AND->notEqualTo('draft', 1);
                $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }

    /**
     * @param $brandId int
     * @param $categoryId int
     * @return array BrandPost|null
     */
    public function getBrandPostsByBrandAndCategory($brandId, $categoryId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $categoryId) {
            $select->where->equalTo('brandId', $brandId)->AND->equalTo('categoryId', $categoryId)->AND->notEqualTo('draft', 1);
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }

    public function getAllPostBrandOwnerByBrand($brandId)
    {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select->where->equalTo('BrandPost.brandId', $brandId)->AND->in('BrandPost.postType', array('2', '3'));
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    public function getPostStoryByBrand($brandId, $include = '', $isPersonalPage = 0)
    {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $include, $isPersonalPage) {
            if($include == 'all')
            {
                if($isPersonalPage)
                {
                    $postTypeAry = array('6');
                    $select->where->equalTo('BrandPost.brandId', $brandId)->AND->notIn('BrandPost.postType', $postTypeAry)->AND->notEqualTo('draft', 1);
                }
                else
                {
                    $select->where->equalTo('BrandPost.brandId', $brandId)->AND->notEqualTo('draft', 1);
                }
            }else{
                $postTypeAry = array('2','3');
                $select->where->equalTo('BrandPost.brandId', $brandId)->AND->in('BrandPost.postType', $postTypeAry)->AND->notEqualTo('draft', 1);
            }
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    /**
     * @param $brandId int
     * @param $userId int
     * @return array BrandPost|null
     */
    public function getDraftPostsByBrandAndUser($brandId, $userId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $userId) {
            /*$select->where->equalTo('brandId', $brandId)->AND->equalTo('creatorId', $userId)->AND->equalTo('draft', 1);*/
            $select->where->notEqualTo('postType', 4)->AND->equalTo('brandId', $brandId)->AND->equalTo('creatorId', $userId)->AND->equalTo('draft', 1);
        });
        
        if(!$rowset) return 0;

        return $rowset->count();
    }
    
    public function getLetestStoryWithPhotoByBrand($brandId, $limit = 1) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $limit) {
            /*$select->where->equalTo('brandId', $brandId)->AND->equalTo('creatorId', $userId)->AND->equalTo('draft', 1);*/
            $select->where->equalTo('brandId', $brandId)->AND->in('postType', array('2', '3'))->AND->notEqualTo('draft', 1)->AND->notIn('postImageData', array('', '[]'));
            $select->limit((int)$limit);
            $select->order('dateCreated DESC');
        });
        
       $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    public function getBrandStoriesCountByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select->where->equalTo('brandId', $brandId)->AND->notEqualTo('draft', 1);
            $select->order('dateCreated DESC');
        });
        if(!$rowset) return 0;

        return $rowset->count();
    }

    public function getBrandProductsByCategoryAndCollection($categoryId, $collectionId, $limit = 0) {
        $rowset = $this->tableGateway->select(function (Select $select) use($categoryId, $collectionId, $limit) {
            $select
                ->where->equalTo('categoryId', $categoryId)
                ->AND->equalTo('collectionId', $collectionId)
                ->AND->equalTo('postType', 6); // product

            if($limit > 0)
            {
                $select->limit($limit);
            }
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    public function getBrandProductsByBrandAndCategory($brandId, $categoryId, $limit = 0) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $categoryId, $limit) {
            $select
                ->where->equalTo('brandId', $brandId)
                ->AND->equalTo('categoryId', $categoryId)
                ->AND->equalTo('postType', 6); // product

            if($limit > 0)
            {
                $select->limit($limit);
            }
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }

    public function getBrandPortfolioByBrandAndCategory($brandId, $categoryId, $drafts = '', $limit = 0) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $categoryId, $limit, $drafts) {
            $select
                ->where->equalTo('brandId', $brandId)
                ->AND->equalTo('categoryId', $categoryId)
                ->AND->equalTo('postType', 7); // portfolio

            if($drafts != '')
            {
                $drafts = ($drafts == 1) ? 0 : 1;
                $select->where->AND->equalTo('draft', $drafts);
            }
            if($limit > 0)
            {
                $select->limit($limit);
            }
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    /*protected function brandPostToTable(BrandPost $post) {
        return [
            'creatorId'     => $post->creatorId,
            'brandId'       => $post->brandId,
            'categoryId'    => $post->categoryId,

            'dateCreated'   => $post->dateCreated,
            'dateParsed'    => $post->dateParsed,

            'postType'          => (int)$post->getPostTypeInt(),
            'postTitle'         => $post->postTitle,
            'postContentRaw'    => $post->postContentRaw,
            'postImageData'     => $post->postImageData,
            'videoData'         => $post->videoData,
            'postMetadata'      => $post->postMetadata,
            'postContent'       => $post->postContent,
            'commentsEnabled'   => (int)$post->commentsEnabled,
            'draft'             => (int)$post->draft,
        ];
    }*/

    /**
     * @param BrandPost $post Brand to save
     * @throws \Exception If brand id != 0 but it's not in the db
     */
    public function saveBrandPost(BrandPost $post)
    {
        $data = $post->toTable();

        $id = (int)$post->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $post->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getBrandPostById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Brand id does not exist');
            }
        }
    }

    public function deleteBrandPost($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }

    public function getLastPostByBrand($brandId)
    {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select
                //->join('Category', 'Category.id = categoryId', ['displayInFeed'])
                ->where->equalTo('brandId', $brandId)
                //->AND->equalTo('displayInFeed', 1)
                ->AND->notEqualTo('draft', 1);
            $select->order('dateCreated DESC');
            $select->limit(1);
        });
        $ret = null;
        foreach ($rowset as $row) {
            $ret = $row;
        }

        return $ret;
    }
}
