<?php

namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class BrandTeamMemberTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandPost|null
     */
    public function getBrandTeamMemberById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
   /* public function getBrandProductsByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select->where->equalTo('brandId', $brandId);
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }*/

    /**
     * @param $brandId int
     * @return int
     */
    public function getBrandTeamMemberCountBrand($brandId) {
        $id = (int)$brandId;
        // todo: count properly
        $rowset = $this->tableGateway->select(['id' => $id]);

        if(!$rowset) return 0;

        return $rowset->count();
    }
    
    
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
    public function getBrandTeamMemberByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select
                ->where->equalTo('brandId', $brandId);
                $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }

    protected function brandTeamMemberToTable(BrandTeamMember $teamMember) {
        return [
            'creatorId'     => $teamMember->creatorId,
            'brandId'       => $teamMember->brandId,
            
            'dateCreated'   => $teamMember->dateCreated,

            'name'          => $teamMember->name,
            'position'      => $teamMember->position,
            'imageData'     => $teamMember->imageData,
            'biography'     => $teamMember->biography,
            'public'        => (int)$teamMember->public,
        ];
    }

    /**
     * @param BrandPost $post Brand to save
     * @throws \Exception If brand id != 0 but it's not in the db
     */
    public function saveBrandTeamMember(BrandTeamMember $teamMember)
    {
        $data = $teamMember->toTable();

        $id = (int)$teamMember->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $teamMember->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getBrandTeamMemberById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Brand id does not exist');
            }
        }
    }

    public function deleteBrandTeamMamber($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}