<?php
namespace LabBase\Model;

use Zend\Db\Sql\Delete;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class BrandRelationTable {

    const FILTER_PRODUCT = 2;
    const FILTER_STORIES = 3;

    protected $tableGateway;

    private $_limit = null;
    private $_offset = null;
    private $_lastReadPost = null;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function setLimit($limit, $offset = null)
    {
        $this->_limit = $limit;
        $this->_offset = $offset;
    }
    public function setFromTime($time)
    {
        $this->_lastReadPost = $time;
    }

    /**
     * @param $id int
     * @return BrandRelation|null
     */
    public function getById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $id int
     * @return BrandRelation[]
     */
    public function getByBrand($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo('brandId', $id);
            $select->order('date DESC');
            $select->limit(50);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    
    public function getBrandLovedCountByBrand($id) {
        return 0;
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo('brandId', $id);            
        });
        
//        if(!$rowset) return 0;

//        return $rowset->count();
		
		return null;
    }

    /**
     * @param $id int
     * @return BrandRelation[]
     */
    public function getByUser($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo($this->tableGateway->table.'.userId', $id);
            $select->group(''.$this->tableGateway->table.'.brandId');
            $select->order($this->tableGateway->table.'.date DESC');
            $select->join('BrandPost', 'BrandPost.brandId='.$this->tableGateway->table.'.brandId');
            if($this->_limit){
                $select->limit($this->_limit);
            }
            if($this->_offset){
                $select->offset($this->_offset);
            }
            if($this->_lastReadPost){
                $select->where->greaterThanOrEqualTo('BrandPost.dateParsed', $this->_lastReadPost);
            }
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    public function getCountByUser($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo($this->tableGateway->table.'.userId', $id);
            $select->group(''.$this->tableGateway->table.'.brandId');
            $select->order($this->tableGateway->table.'.date DESC');
            $select->join('BrandPost', 'BrandPost.brandId='.$this->tableGateway->table.'.brandId');
            if($this->_limit){
                $select->limit($this->_limit);
            }
            if($this->_offset){
                $select->offset($this->_offset);
            }
            if($this->_lastReadPost){
                $select->where->greaterThanOrEqualTo('BrandPost.dateParsed', $this->_lastReadPost);
            }
        });
        return $rowset->count();
    }
    public function getSearchByUser($id, $search) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id,$search) {
            $select->where->equalTo($this->tableGateway->table.'.userId', $id)->like('Brand.name',"%$search%");
            $select->group(''.$this->tableGateway->table.'.brandId');
            $select->order(''.$this->tableGateway->table.'.date DESC');
            $select->join('Brand', 'Brand.id='.$this->tableGateway->table.'.brandId');
            if($this->_limit){
                $select->limit($this->_limit);
            }
            if($this->_offset){
                $select->offset($this->_offset);
            }
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    public function getFilterByUser($id, $id_filter) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id,$id_filter) {
            $select->where->equalTo($this->tableGateway->table.'.userId', $id);
            $select->order(''.$this->tableGateway->table.'.date DESC');
            $select->group(''.$this->tableGateway->table.'.brandId');
            $select->join('BrandPost', 'BrandPost.brandId='.$this->tableGateway->table.'.brandId');
            if($this->_limit){
                $select->limit($this->_limit);
            }
            if($this->_offset){
                $select->offset($this->_offset);
            }
            $in = [];
            if($id_filter == self::FILTER_PRODUCT){
                array_filter(BrandPost::getPostTypes(), function($v, $k) use (&$in) {
                    if($v != 'product'){
                        $in[] = $k;
                    }
                }, ARRAY_FILTER_USE_BOTH);
            }else if($id_filter == self::FILTER_STORIES){
                array_filter(BrandPost::getPostTypes(), function($v, $k) use (&$in) {
                    if($v != 'advanced'){
                        $in[] = $k;
                    }
                }, ARRAY_FILTER_USE_BOTH);
            }
            $select->where->notIn('BrandPost.postType', $in);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * @param $userId
     * @param $brandId
     * @return BrandRelation[]
     */
    public function getByUserBrand($userId, $brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($userId, $brandId) {
            $select->where->equalTo('brandId', $brandId)->AND->equalTo('userId', $userId);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * @param $userId
     * @param $brandId
     * @param $relationType
     * @return BrandRelation|null
     */
    public function getByUserBrandType($userId, $brandId, $relationType) {
        return 0;
        $rowset = $this->tableGateway->select(function (Select $select) use($userId, $brandId, $relationType) {
            $select->where->
                equalTo('brandId', $brandId)
                ->AND->equalTo('userId', $userId)
                ->AND->equalTo('relationType', $relationType);
        });
        $row = $rowset->current();
//        if (!$row) {
//            return null;
//        }
//        return $row;
		return null;
    }


    public function save(BrandRelation $obj)
    {
        $data = $obj->toTable();

        $id = (int)$obj->id;
        if ($id == 0) {
            $adapter = $this->tableGateway->getAdapter();
            $fp = function($name) use ($adapter) { return $adapter->driver->formatParameterName($name); };
            // todo: table change
            $queryString = "INSERT IGNORE INTO brandrelations (userId, brandId, relationType, date) VALUES "
                . "(".$fp('userId').",".$fp('brandId').",".$fp('relationType').",".$fp('date').")";
            $queryData =[
                'userId' => $data['userId'],
                'brandId' => $data['brandId'],
                'relationType' => $data['relationType'],
                'date' => $data['date'],
            ];
            $resultset = $adapter->query($queryString, $queryData);
            $obj->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Given id does not exist');
            }
        }
    }

    /**
     * @param $id int
     */
    public function delete($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }

    /**
     * @param $userId
     * @param $brandId
     * @param $relationType
     */
    public function deleteUserBrandType($userId, $brandId, $relationType) {
        $rowset = $this->tableGateway->delete(function (Delete $q) use($userId, $brandId, $relationType) {
            $q->where->
                equalTo('brandId', $brandId)
                ->AND->equalTo('userId', $userId)
                ->AND->equalTo('relationType', $relationType);
        });
    }
}