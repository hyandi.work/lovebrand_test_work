<?php

namespace LabBase\Model;

class PortfolioProject extends DBModel {

    public $id;
    public $brandId;

    public $projectName;
    public $public;
   
    public function __construct($brandId = NULL, $name = NULL, $public = 1) {
        if($brandId === NULL)
            return;

        $this->brandId = $brandId;
        
        $this->projectName = htmlspecialchars($name);
        $this->public = $public;
    }

    protected function databaseMappings() {
        return [
            [ 'id',                     DBModel::TYPE_INT           , DBModel::DB_PKEY ],
            [ 'brandId',                DBModel::TYPE_INT           , DBModel::INTERP_DEFAULT ],
            [ 'projectName',            DBModel::TYPE_VARCHAR_255   , DBModel::STR_NO_HTML ],
            [ 'public',                 DBModel::TYPE_BYTE          , DBModel::INTERP_BOOL ],
        ];
    }
}
