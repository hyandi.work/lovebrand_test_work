<?php

namespace LabBase\Model;

class BrandProductLoved {
    public $id;
    public $productId;    
    public $creatorId;
    public $loved; // 1 => Loved, 0 => Un-loved
    public $date;

    /**
     * @param int $userId
     * @param int $brandId
     * @param int $relationType
     */
    public function __construct($userId = 0, $productId = 0, $loved = 0) {
        $this->creatorId    = $userId;
        $this->productId    = $productId;
        $this->loved        = $loved;
        $this->date         = time();
    }

    public function exchangeArray($data) {
        $this->id           = (!empty($data['id']))        ? (int) $data['id']          : null;
        $this->creatorId    = (!empty($data['creatorId'])) ? (int) $data['creatorId']   : null;        
        $this->productId    = (!empty($data['productId'])) ? (int) $data['productId']   : null;
        $this->loved        = (!empty($data['loved']))     ? (int) $data['loved']       : null;
        $this->date         = (!empty($data['date']))      ? (int) $data['date']        : null;
    }

}