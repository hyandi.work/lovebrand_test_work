<?php

namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class PortfolioProjectTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandPost|null
     */
    public function getById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }
    
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
    public function getPortfolioProjectByBrand($brandId, $public = 'all') {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $public) {
            $select
                ->where->equalTo('brandId', $brandId);
            
            if($public != 'all')
            {
                $select->where->AND->equalTo('public', $public);    
            }
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    /**
     * @param BrandPost $post Brand to save
     * @throws \Exception If brand id != 0 but it's not in the db
     */
    public function savePortfolioProject(PortfolioProject $portfolioProject)
    {
        $data = $portfolioProject->toTable();

        $id = (int)$portfolioProject->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $portfolioProject->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Brand id does not exist');
            }
        }
    }

    public function delete($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}