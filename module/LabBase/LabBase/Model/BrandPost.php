<?php

namespace LabBase\Model;


class BrandPost extends DBModel {

    public $id;
    public $creatorId;
    public $brandId;
    public $categoryId; // for product only
    public $collectionId; // for product only

    public $dateCreated;
    public $dateParsed;

    public $postTitle;
    public $postContentRaw;
    public $postContent;

    public $videoData;
    public $postMetadata;
    public $postImageData;

    public $postType;

    public $commentsEnabled;
    public $draft;

    public $buyNow;
    public $radio;
    public $price;

    // these don't relate to the brandPost table directly:
    public $categoryName;
    public $categoryUrl;

    private $_serviceLocator;

    private static $ALLOWED_TAGS = [
        'a' => ['href', 'target', 'title'],
        'b' => [],
        'br' => [],
        'em' => [],
        'i' => [],
        'img' => ['alt', 'src', 'title'],
        'li' => [],
        'ol' => [],
        'p' => [],
        'span' => [],
        'strong' => [],
        'sub' => [],
        'sup' => [],
        'u' => [],
        'ul' => [],
    ];

    private static $postTypes = [
        1 => 'quote',
        2 => 'simple',
        3 => 'advanced',
        4 => 'photo',
        5 => 'video',

        6 => 'product',
        7 => 'portfolio',
    ];
    private static $postTypeToSegment = [
        1 => 'home',
        2 => 'stories',
        3 => 'stories',
        4 => 'gallery',
        5 => 'video',

        6 => 'products',
        7 => 'portfolio',
    ];
    private static $postTypeToString = [
        1 => 'Home',
        2 => 'Stories',
        3 => 'Stories',
        4 => 'Gallery',
        5 => 'Videos',

        6 => 'Products',
        7 => 'Portfolio',
    ];

    /**
     * @return \Zend\ServiceManager\ServiceManager
     */
    public function getServiceLocator()
    {
        return ServiceLocatorFactory::getInstance();
    }

    /**
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $this->_serviceLocator = $serviceLocator;
    }

    public static function fromProduct(BrandProduct $p) {
        $post = new BrandPost($p->creatorId, $p->brandId, NULL, $p->productTitle, $p->productContent, 'product', $p->productImageData, NULL, NULL, !$p->public);
        $post->commentsEnabled = false;
        $post->id = $p->id;
        $post->dateCreated = $p->dateCreated;
        return $post;
    }

    public function __construct($userId = NULL, $brandId = NULL, $categoryId = NULL, $titleRaw = NULL, $contentRaw = NULL, $postType = 'advanced', $imageData = NULL, $videoData = NULL,  $metadata = NULL, $draft = 0, $buyNow = NULL, $radio = NULL, $price = NULL) {
        if($userId === NULL && $brandId === NULL)
            return;

        $this->creatorId = $userId;
        $this->brandId = $brandId;
        $this->categoryId = $categoryId;
        $this->dateCreated = time();
        $this->setPostContents($contentRaw, $this->dateCreated);
        $this->postTitle = htmlspecialchars($titleRaw);
        $this->buyNow = $buyNow;
        $this->radio = $radio;
        $this->price = $price;

        $this->postImageData = $imageData;
        $this->videoData = $videoData;
        $this->postMetadata = $metadata;

        $this->commentsEnabled = TRUE;
        
        $this->draft = $draft;

        $this->setPostType($postType);
    }

    public function getPostSegmentParam() {
        return $this->categoryId;
    }

    public function getPostSegment() {
        return $this->postType > 0 ? BrandPost::$postTypeToSegment[$this->postType] : null;
    }

    public function getPostTypeString() {
        return $this->postType > 0 ? BrandPost::$postTypeToString[$this->postType] : null;
    }

    /**
     * @return string one of: quote, simple, advanced
     */
    public function getPostType() {
        if($this->postType > 0) {
            return BrandPost::$postTypes[$this->postType];
        }
        return null;
    }

    /**
     * @param string $postType one of: quote, simple, advanced
     */
    public function setPostType($postType) {
        // todo: find a better way
        $this->postType = array_flip(BrandPost::$postTypes)[$postType];
    }

    /**
     * @return int internal ID of the post type
     */
    public function getPostTypeInt() {
        return $this->postType;
    }


    public function setPostTitle($titleRaw) {
        $this->postTitle = htmlspecialchars($titleRaw);
    }

    public function setPostContents($contentRaw, $parseDateOverride = NULL) {
        $this->postContentRaw = $contentRaw;

        $this->postContent = $this->escapePostContent($contentRaw);
        $this->dateParsed = $parseDateOverride ? $parseDateOverride : time();
    }

    // todo: fix this approach
    public function unescapePostContents() {
        $this->postContent = $this->postContentRaw;
    }

    public function getPostContentsEscaped() {
        return $this->postContent;
    }
    public function getPostContentsUnescaped() {
        return $this->postContentRaw;
    }
    
    public function setPostDraftsStatus($draftStatus) {
        $this->draft = $draftStatus ? 1 : 0;
    }
    private function escapePostContent($raw) {
        $out = '';
        //$raw = preg_replace('/<\s*br\s*\/?\s*>/', '</p><p>', $raw);

        $openTags = [];

        // $pieces contains all tags and non-tag content as strings
        $pieces = preg_split('@(\<[^<>]+\>)@i', $raw, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        for($i = 0; $i < count($pieces); $i++) {
            $str = $pieces[$i];

            $matched = preg_match_all('@^<\s*(/?)\s*([^/\s]+)\s*(.*?)\s*?>$@i', $str, $matches, PREG_SET_ORDER);
            if(!$matched) {
                $out .= htmlspecialchars($str);
            }else{
                // Tag matched.
                $matches = $matches[0];
                $tagCloser = $matches[1];
                $tagName = $matches[2];
                $parameters = empty($matches[3]) ? FALSE : $matches[3];

                // $key is the trimmed tag name to-lower'ed
                $key = strtolower($tagName);
                if(!key_exists($key, BrandPost::$ALLOWED_TAGS)) {
                    $out .= htmlspecialchars($str);
                    continue;
                }

                $parameterString = '';
                $closedByParam = FALSE;

                // If tag is NOT </closing> tag, allow its params
                if(empty($tagCloser)) {
                    $allowedParams = BrandPost::$ALLOWED_TAGS[$key];
                    if($parameters && !empty($allowedParams)) {
                        $matches = [];
                        $matched = preg_match_all('@\s*(?|([^=]+)()\s|([^=]+)=(?|(\'[^\']+\')|("[^"]+")|(\S+)))@i', $parameters, $matches, PREG_SET_ORDER);
                        if($matched) {
                            foreach($matches as $match) {
                                $name = $match[1];
                                $value = $match[2];

                                $closedByParam = FALSE; // reset if it was not last param
                                if($name === '/') {
                                    $closedByParam = TRUE;
                                    continue;
                                }

                                $key = strtolower($name);

                                if($key === "style") {
                                    // todo: maybe allow some styles?
                                    // for now discard
                                    continue;
                                }else{
                                    if(!in_array($key, $allowedParams)) {
                                        // skip parameter
                                        continue;
                                    }
                                }

                                if(!empty($value)) {
                                    $parameterString .= " $name=$value";
                                }else{
                                    $parameterString .= " $name";
                                }
                            }
                        }
                    }
                }

                $closed = $closedByParam;
                if(!$closed) {
                    if($key === 'br' || $key === 'hr') {
                        $closed = true;
                        $closedByParam = true;
                    }else {
                        $closed = !empty($tagCloser);
                    }
                }

                if($closedByParam) {
                    $parameterString .= ' /';
                }

                if(!empty($tagCloser)) {
                    $newTags = $openTags;
                    for($j = count($newTags) - 1; $j >= 0; $j--) {
                        $tag = array_pop($newTags);
                        if(strtolower($tag) == $key) {
                            $openTags = $newTags;
                            break;
                        }
                        $out .= "</$tag>";
                    }
                }

                $out .= "<$tagCloser$tagName$parameterString>";
                if(!$closed) {
                    array_push($openTags, $tagName);
                }
            }
        }
        for($j = count($openTags) - 1; $j >= 0; $j--) {
            $tag = $openTags[$j];
            $out .= "</$tag>";
        }
        return $out;
    }
    
    protected function databaseMappings() {
        return [
            [ 'id',                 DBModel::TYPE_INT,   DBModel::DB_PKEY ],
            [ 'creatorId',          DBModel::TYPE_INT,   DBModel::INTERP_DEFAULT ],
            [ 'brandId',            DBModel::TYPE_INT,   DBModel::INTERP_DEFAULT ],
            [ 'categoryId',         DBModel::TYPE_INT,   DBModel::INTERP_DEFAULT ], // for products only
            [ 'collectionId',       DBModel::TYPE_INT,   DBModel::INTERP_DEFAULT ], // for products only

            [ 'dateCreated',        DBModel::TYPE_INT,   DBModel::INTERP_TIMESTAMP ],
            [ 'dateParsed',         DBModel::TYPE_INT,   DBModel::INTERP_TIMESTAMP ],

            [ 'postTitle',          DBModel::TYPE_TEXT,  DBModel::STR_NO_HTML ],
            [ 'postContentRaw',     DBModel::TYPE_TEXT,  DBModel::STR_NO_HTML ],
            [ 'postContent',        DBModel::TYPE_TEXT,  DBModel::STR_NO_HTML ],
            [ 'buyNow',             DBModel::TYPE_TEXT, DBModel::STR_NO_HTML ],
            [ 'price',              DBModel::TYPE_FLOAT,  DBModel::INTERP_DEFAULT ],
            [ 'radio',              DBModel::TYPE_TEXT,  DBModel::STR_NO_HTML ],

            [ 'postMetadata',       DBModel::TYPE_TEXT,  DBModel::INTERP_STRING ],
            [ 'postImageData',      DBModel::TYPE_TEXT,  DBModel::INTERP_STRING ],
            [ 'videoData',          DBModel::TYPE_TEXT,  DBModel::INTERP_STRING ],

            [ 'commentsEnabled',    DBModel::TYPE_BYTE,  DBModel::INTERP_BOOL ],
            [ 'draft',              DBModel::TYPE_BYTE,  DBModel::INTERP_BOOL ],
            [ 'postType',           DBModel::TYPE_BYTE,  DBModel::INTERP_DEFAULT ],
        ];
    }

    /**
     * @return array
     */
    public static function getPostTypes()
    {
        return self::$postTypes;
    }

    /*public function exchangeArray($data) {
        $this->id               = (!empty($data['id']))             ? (int)$data['id']         : null;
        $this->creatorId        = (!empty($data['creatorId']))      ? (int)$data['creatorId']  : null;
        $this->brandId          = (!empty($data['brandId']))        ? (int)$data['brandId']    : null;
        $this->categoryId       = (!empty($data['categoryId']))     ? (int)$data['categoryId'] : null;

        $this->dateCreated      = (!empty($data['dateCreated']))    ? (int)$data['dateCreated']: null;
        $this->dateParsed       = (!empty($data['dateParsed']))     ? (int)$data['dateParsed'] : null;
        $this->postTitle        = (!empty($data['postTitle']))      ? $data['postTitle']       : null;
        $this->postContentRaw   = (!empty($data['postContentRaw'])) ? $data['postContentRaw']  : null;
        $this->postContent      = (!empty($data['postContent']))    ? $data['postContent']     : null;
        $this->postImageData    = (!empty($data['postImageData']))  ? $data['postImageData']   : null;
        $this->videoData        = (!empty($data['videoData']))      ? $data['videoData']       : null;
        $this->postType         = (!empty($data['postType']))       ? (int)$data['postType']   : 0;
        $this->postMetadata     = (!empty($data['postMetadata']))   ? $data['postMetadata']    : null;

        $this->commentsEnabled  = (!empty($data['commentsEnabled']))? ($data['commentsEnabled'] ? true : false) : null;
        
        $this->draft  = (!empty($data['draft']))? ($data['draft'] ? 1 : 0) : 0;
    }*/
}