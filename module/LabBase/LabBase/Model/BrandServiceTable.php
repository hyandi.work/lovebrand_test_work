<?php

namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class BrandServiceTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandPost|null
     */
    public function getBrandServiceById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
   /* public function getBrandProductsByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select->where->equalTo('brandId', $brandId);
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }*/

    /**
     * @param $brandId int
     * @return int
     */
    public function getBrandServiceCountBrand($brandId) {
        $id = (int)$brandId;
        // todo: count properly
        $rowset = $this->tableGateway->select(['id' => $id]);

        if(!$rowset) return 0;

        return $rowset->count();
    }
    
    
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
    public function getBrandServiceByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select
                ->where->equalTo('brandId', $brandId);
                $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }

   /* protected function brandServiceToTable(BrandService $service) {
        return [
            'creatorId'     => $service->creatorId,
            'brandId'       => $service->brandId,
            
            'dateCreated'   => $service->dateCreated,

            'serviceTitle'       => $service->serviceTitle,
            'serviceImageData'   => $service->serviceImageData,
            'serviceContent'     => $service->serviceContent,
            'public'             => (int)$service->public,
        ];
    }*/

    /**
     * @param BrandPost $post Brand to save
     * @throws \Exception If brand id != 0 but it's not in the db
     */
    public function saveBrandService(BrandService $service)
    {
/*        $data = $this->brandServiceToTable($service);*/
        $data = $service->toTable();

        $id = (int)$service->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $service->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getBrandServiceById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Brand id does not exist');
            }
        }
    }

    public function deleteBrandService($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}