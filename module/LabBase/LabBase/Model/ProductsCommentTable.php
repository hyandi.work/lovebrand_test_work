<?php
namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Update;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class ProductsCommentTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return Comment|null
     */
    public function getProductsCommentById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $postId int
     * @param $firstId int Start of excluded range (inclusive)
     * @param $lastId int End of excluded range (inclusive)
     * @return Comment[]
     */
    public function getProductsCommentsByProduct($productId, $firstId = -1, $lastId = -1) {
        $rowset = $this->tableGateway->select(function (Select $select) use($productId, $firstId, $lastId) {
            //$select->join('users', 'users.id = posterId', ['fullName']);
            if($lastId > 0 && $firstId > 0) {
                $select->where
                    ->equalTo('productId', $productId)
                    ->AND->NEST
                    ->lessThan('id', $firstId)
                    ->OR
                    ->greaterThan('id', $lastId)
                    ->UNNEST;
            }else{
                $select->where->equalTo('productId', $productId);
            }
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * @param $brandId int
     * @param $firstId int Start of excluded range (inclusive)
     * @param $lastId int End of excluded range (inclusive)
     * @param null|bool $filterSeen If not null, get only these comments with matching value
     * @param null|bool $filterApproved If not null, get only these comments with matching value
     * @param null|bool $filterRejected If not null, get only these comments with matching value
     * @return CommentPostInfo[]
     */
    public function getProductsCommentsByBrand($brandId, $firstId = -1, $lastId = -1, $filterSeen = null, $filterApproved = null, $filterRejected = null) {

        // CHANGE THE RS PROTOTYPE - is it the proper way to do it?
        $this->tableGateway->getResultSetPrototype()->setArrayObjectPrototype(new CommentProductInfo());

        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $firstId, $lastId, $filterSeen, $filterApproved, $filterRejected) {

            //$select->join('users', 'users.id = posterId', ['fullName']);
            $select->join('products', 'products.id = productId', ['brandId', 'productTitle']);
            if($lastId > 0 && $firstId > 0) {
                $select->where
                    ->equalTo('products.brandId', $brandId)
                    ->AND->NEST
                        ->lessThan('id', $firstId)
                        ->OR
                        ->greaterThan('id', $lastId)
                    ->UNNEST;
            }else{
                $select->where->equalTo('products.brandId', $brandId);
            }
            if($filterSeen !== null) {
                $w = new Where();
                $w->equalTo('seen', (bool)$filterSeen);
                $select->where->andPredicate($w);
            }
            if($filterApproved !== null) {
                $w = new Where();
                $w->equalTo('approved', (bool)$filterApproved);
                $select->where->andPredicate($w);
            }
            if($filterRejected !== null) {
                $w = new Where();
                $w->equalTo('rejected', (bool)$filterRejected);
                $select->where->andPredicate($w);
            }

            $select->order('dateCreated DESC');
        });

        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }

        // CHANGE THE RS PROTOTYPE BACK
        $this->tableGateway->getResultSetPrototype()->setArrayObjectPrototype(new ProductsComment());
        return $ret;
    }

    /**
     * @param int $brandId
     */
    public function markBrandProductCommentsAndNotificationsAsSeen($brandId) {
        // No data binding! Sanitize the data
        $brandId = (int)$brandId;
        $timestamp = time();

        $this->tableGateway->adapter->getDriver()->createStatement(<<<SQL
        UPDATE products_comments JOIN products ON products.id = products_comments.productId SET seen = 1 WHERE products.brandId = "${brandId}"
SQL
)->execute();
        $this->tableGateway->adapter->getDriver()->createStatement(<<<SQL
        UPDATE notifications JOIN products_comments ON notifications.relatedProductComment = products_comments.id SET notifications.dateSeen = "${timestamp}" WHERE products_comments.seen = 1
SQL
)->execute();
    }


    protected function productsCommentToTable(ProductsComment $comment) {
        return array(
            'posterId'          => $comment->posterId,
            'posterBrandId'     => $comment->posterBrandId,
            'posterDisplayName' => $comment->posterDisplayName,
            'posterEmail'       => $comment->posterEmail,
            'productId'         => $comment->productId,
            'dateCreated'       => $comment->dateCreated,
            'content'           => $comment->content,
            'approved'          => $comment->approved,
            'rejected'          => $comment->rejected,
            'seen'              => $comment->seen,
        );
    }

    /**
     * @param Comment $comment Comment to save
     * @throws \Exception If comment id != 0 but it's not in the db
     */
    public function saveProductsComment(ProductsComment $comment)
    {
        $data = $this->productsCommentToTable($comment);

        $id = (int)$comment->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $comment->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getProductsCommentById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Comment id does not exist');
            }
        }
    }

    /**
     * @param $id int
     */
    public function deleteProductsComment($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}