<?php

namespace LabBase\Model;

/**
 * Class DBModel
 * @package LabBase\Model
 *
 * INTERP_BOOL saves 1 or 0 and casts to boolean when reading
 * INTERP_TIMESTAMP will accept positive integers only, or store/retrieve 0 or null if nullable
 * INTERP_STRING will force cast to string
 * STR_TRIM implies INTERP_STRING and will trim whitespaces using trim()
 * STR_NO_HTML implies INTERP_STRING and will strip HTML tags0
 *
 * BYTE, SHORT, INT and LONG are TINYINT, SMALLINT, INT and BIGINT respectively.
 * VARCHAR_* constants for different field sizes.
 */
abstract class DBModel {
    /**
     * [ fieldName, fieldType, interpType|dbAttribs ]
     * [ 'name', DBModel::VARCHAR_64, DBModel::INTERP_DEFAULT | DBModel::DB_NULL ]
     * @return array of arrays each describing a database field
     */
    abstract protected function databaseMappings();

    const FK_ENFORCED          = 0x00000000;
    const FK_NOT_ENFORCED      = 0x01000000;
    const FK_ON_DELETE_CASCADE = 0x02000000;

    const DB_NULL       = 0x00100000;
    const DB_PKEY       = 0x00200000;
    const DB_INDEX      = 0x00400000;

    const INTERP_DEFAULT    = 0;
    const INTERP_BOOL       = 0x00100;
    const INTERP_TIMESTAMP  = 0x00200;
    const INTERP_STRING     = 0x00400;
    const STR_TRIM          = 0x00C00;
    const STR_NO_HTML       = 0x01400;
    const INTERP_FKEY       = 0x02000;

    // todo: floats?
    const TYPE_TEXT          = 0x80;
    const TYPE_VARCHAR_255   = 0x70;
    const TYPE_VARCHAR_192   = 0x60;
    const TYPE_VARCHAR_128   = 0x50;
    const TYPE_VARCHAR_96    = 0x40;
    const TYPE_VARCHAR_64    = 0x30;
    const TYPE_VARCHAR_32    = 0x20;
    const TYPE_VARCHAR_16    = 0x10;

    const TYPE_FLOAT          = 0x05;
    const TYPE_LONG          = 0x04;
    const TYPE_INT           = 0x03;
    const TYPE_SHORT         = 0x02;
    const TYPE_BYTE          = 0x01;
    const TYPE_BOOL          = 0x01;

    const MASK_TYPE_NUMERIC = 0x0F;
    const MASK_TYPE_TEXT    = 0xF0;

    const MASK_TYPE     = 0x000000FF;
    const MASK_INTERP   = 0x000FFF00;
    const MASK_DB       = 0x7FF00000;
    const MASK_INDEXED  = 0x00600000;

    private static $DB_NO_DEFAULTS = [
        DBModel::TYPE_TEXT => true,
    ];
    private static $TYPE_DEFAULTS = [
        DBModel::TYPE_TEXT => '',
        DBModel::TYPE_VARCHAR_255 => '',
        DBModel::TYPE_VARCHAR_192 => '',
        DBModel::TYPE_VARCHAR_128 => '',
        DBModel::TYPE_VARCHAR_96 => '',
        DBModel::TYPE_VARCHAR_64 => '',
        DBModel::TYPE_VARCHAR_32 => '',
        DBModel::TYPE_VARCHAR_16 => '',
        DBModel::TYPE_LONG => 0,
        DBModel::TYPE_INT => 0,
        DBModel::TYPE_SHORT => 0,
        DBModel::TYPE_BYTE => 0,
        DBModel::TYPE_FLOAT => 0.0,
    ];
    private static $DB_TYPES = [
        DBModel::TYPE_TEXT => 'TEXT',
        DBModel::TYPE_VARCHAR_255 => 'VARCHAR(255)',
        DBModel::TYPE_VARCHAR_192 => 'VARCHAR(192)',
        DBModel::TYPE_VARCHAR_128 => 'VARCHAR(128)',
        DBModel::TYPE_VARCHAR_96 => 'VARCHAR(96)',
        DBModel::TYPE_VARCHAR_64 => 'VARCHAR(64)',
        DBModel::TYPE_VARCHAR_32 => 'VARCHAR(32)',
        DBModel::TYPE_VARCHAR_16 => 'VARCHAR(16)',
        DBModel::TYPE_LONG => 'BIGINT',
        DBModel::TYPE_INT => 'INT',
        DBModel::TYPE_SHORT => 'SMALLINT',
        DBModel::TYPE_BYTE => 'TINYINT',
        DBModel::TYPE_FLOAT => 'FLOAT',
    ];

    private $loadCallbacks = [];
    private $saveCallbacks = [];

    protected function attachLoadCallback($key, $fn) {
        $this->loadCallbacks[$key] = $fn;
    }
    protected function attachSaveCallback($key, $fn) {
        $this->saveCallbacks[$key] = $fn;
    }

    public static function ForeignKey($alias, $table, $fieldName, $options = DBModel::FK_ENFORCED) {
        if(is_array($options)) {
            $ret = 0;
            foreach($options as $attribute) {
                $ret |= (int)$attribute;
            }
            $options = $ret;
        }
        return [ $alias, $table, $fieldName, $options ];
    }

    public static function Field($name, $type, $specialAttributes = DBModel::INTERP_DEFAULT, $foreignKeys = null) {
        if(is_array($specialAttributes)) {
            $ret = 0;
            foreach($specialAttributes as $attribute) {
                $ret |= (int)$attribute;
            }
            $specialAttributes = $ret;
        }
        if(!$foreignKeys) {
            $foreignKeys = [];
        }
        return [$name, $type, $specialAttributes, $foreignKeys];
    }


    public function generateSQLDefinition($tableName) {
        $sql = 'CREATE TABLE IF NOT EXISTS `'.$tableName."` (\n";
        $tableOptions = '';
        $foreignKeys = '';

        $mappings = $this->databaseMappings();
        foreach($mappings as $field) {
            $id = $field[0];
            $interp = $field[2] & DBModel::MASK_INTERP;
            $type = $field[1] & DBModel::MASK_TYPE;
            $canBeNull = ($field[2] & DBModel::DB_NULL) != 0;
            $isPrimary = ($field[2] & DBModel::DB_PKEY) != 0;
            $isIndexed = ($field[2] & DBModel::DB_INDEX) != 0;
            $hasForeign = !empty($field[3]);

            if($isPrimary) {
                $tableOptions .= "  PRIMARY KEY (`$id`),\n";
            }elseif($isIndexed) {
                $tableOptions .= "  INDEX `$id` (`$id`),\n";
            }

            if($hasForeign) {
                foreach($field[3] as $fk) {
                    //  [ $alias, $table, $fieldName, $options ]
                    $enforced = ($fk[3] & DBModel::FK_NOT_ENFORCED) == 0;
                    if($enforced) {
                        $onDeleteCascade = ($fk[3] & DBModel::FK_ON_DELETE_CASCADE) != 0;
                        $table = $fk[1];
                        $name = $fk[2];
                        $foreignKeys .= "  FOREIGN KEY ($id) REFERENCES $table($name)";
                        if($onDeleteCascade) {
                            $foreignKeys .= " ON DELETE CASCADE,\n";
                        }else{
                            $foreignKeys .= ",\n";
                        }
                    }
                }
            }

            $dbType = DBModel::$DB_TYPES[$type];
            if($isPrimary) {
                $extras = 'NOT NULL AUTO_INCREMENT';
            }else {
                if (isset(DBModel::$DB_NO_DEFAULTS[$type])) {
                    if ($canBeNull) {
                        $extras = 'NULL';
                    } else {
                        $default = DBModel::$TYPE_DEFAULTS[$type];
                        $extras = "NOT NULL";
                    }
                } else {
                    if ($canBeNull) {
                        $extras = 'DEFAULT NULL';
                    } else {
                        $default = DBModel::$TYPE_DEFAULTS[$type];
                        $extras = "NOT NULL DEFAULT '$default'";
                    }
                }
            }

            $sql .= "  `$id` $dbType $extras,\n";
        }

        $sql .= $tableOptions;
        $sql .= $foreignKeys;
        $sql = trim($sql, ",\n\r ");
        $sql .= "\n) ENGINE=InnoDB AUTO_INCREMENT=1234111 DEFAULT CHARSET=utf8;";
        return $sql;
    }

    public function exchangeArray($data) {
        $mappings = $this->databaseMappings();

        foreach($mappings as $field) {
            $id = $field[0];
            $type = $field[1] & DBModel::MASK_TYPE;
            $canBeNull = ($field[2] & DBModel::DB_NULL) != 0;
            $interp = $field[2] & DBModel::MASK_INTERP;

            if(count($field) >= 4) {
                $fkeys = $field[3];
                if (!is_array($fkeys)) {
                    $fkeys = [];
                }
            }

            $val = null;
            if(isset($data[$id])) {
                $val = $data[$id];
            }

            // if no value:
            if($val === null) {
                if($canBeNull) {
                    $this->$id = null;
                    continue;
                }else{
                    $this->$id = DBModel::$TYPE_DEFAULTS[$type];
                    continue;
                }
            }

            if($type & DBModel::MASK_TYPE_NUMERIC) {
                if($type & DBModel::TYPE_LONG) {
                    $val = (double)$val;
                }else{
                    $val = (int)$val;
                }
            }elseif($type & DBModel::MASK_TYPE_TEXT) {
                $val = (string)$val;
            }else{
                error_log("DBModel - wrong type, neither numeric nor text");
            }


            if($interp & DBModel::INTERP_STRING) {
                $val = (string)$val;
                if($interp & DBModel::STR_NO_HTML) {
                    $val = strip_tags($val);
                }
                if($interp & DBModel::STR_TRIM) {
                    $val = trim($val);
                }
            }elseif($interp & DBModel::INTERP_BOOL) {
                $val = (bool)$val;
            }elseif($interp & DBModel::INTERP_TIMESTAMP) {
                $val = (int) $val;
                if($val < 1) {
                    if($canBeNull) {
                        $val = null;
                    }else{
                        $val = 0;
                    }
                }
            }
            $this->$id = $val;
        }

        // Run callback after load
        foreach($this->loadCallbacks as $key => $callback) {
            $callback($this);
        }
    }

    public function toTable() {
        $mappings = $this->databaseMappings();

        // Run callback before save
        foreach($this->saveCallbacks as $key => $callback) {
            $callback($this);
        }

        $ret = [];

        foreach($mappings as $field) {
            $id = $field[0];
            $type = $field[1] & DBModel::MASK_TYPE;
            $canBeNull = ($field[2] & DBModel::DB_NULL) != 0;
            $interp = $field[2] & DBModel::MASK_INTERP;

            $val = null;
            if(isset($this->$id)) {
                $val = $this->$id;
                // if no value:
                if($val === null) {
                    if($canBeNull) {
                        $ret[$id] = null;
                        continue;
                    }else{
                        $ret[$id] = DBModel::$TYPE_DEFAULTS[$type];
                        continue;
                    }
                }
            }else{
                if($canBeNull) {
                    // don't add the field
                    continue;
                }else{
                    $ret[$id] = DBModel::$TYPE_DEFAULTS[$type];
                    continue;
                }

            }

            if($type & DBModel::MASK_TYPE_NUMERIC) {
                if($type & DBModel::TYPE_LONG) {
                    $val = (double)$val;
                }else{
                    $val = (int)$val;
                }
            }elseif($type & DBModel::MASK_TYPE_TEXT) {
                $val = (string)$val;
            }else{
                error_log("DBModel - wrong type, neither numeric nor text");
            }


            if($interp & DBModel::INTERP_STRING) {
                $val = (string)$val;
                if($interp & DBModel::STR_NO_HTML) {
                    $val = strip_tags($val);
                }
                if($interp & DBModel::STR_TRIM) {
                    $val = trim($val);
                }
            }elseif($interp & DBModel::INTERP_BOOL) {
                $val = $val ? 1 : 0;
            }elseif($interp & DBModel::INTERP_TIMESTAMP) {
                $val = (int) $val;
                if($val < 1) {
                    if($canBeNull) {
                        $val = null;
                    }else{
                        $val = 0;
                    }
                }
            }
            $ret[$id] = $val;
        }
        return $ret;
    }
}