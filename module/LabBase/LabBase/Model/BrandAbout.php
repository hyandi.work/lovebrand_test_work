<?php

namespace LabBase\Model;

class BrandAbout extends DBModel {

    public $id;
    public $creatorId;
    public $brandId;

    public $dateCreated;

    public $header;
    public $aboutTitle;
    public $aboutDescription;
    public $sortOrder;

    public $public; // 1 -> public, 0 ->  not

    private static $allowedTags = [ 'p', 'strong', 'a', 'span', 'em', 'img', 'br' ];
    
    private static $aboutHeaders = [
        0 => 'Additional Content',
        1 => 'Who we are',
        2 => 'Our Vision',
        3 => 'Our Mission',
        4 => 'Our Values',
        5 => 'Our Herritage',
    ];

    public function __construct($userId = NULL, $brandId = NULL, $header = NULL, $titleRaw = NULL, $contentRaw = NULL, $sortOrder = 0, $public = 1) {
        if($userId === NULL && $brandId === NULL)
            return;

        $this->creatorId = $userId;
        $this->brandId = $brandId;
        $this->dateCreated = time();
        
        $this->aboutTitle = htmlspecialchars($titleRaw);
        $this->sortOrder = (int)$sortOrder;

        $this->public = $public;
        
        $this->setAboutContents($contentRaw);
        $this->setAboutHeader($header);
    }

    
    /**
     * @return string one of: quote, simple, advanced
     */
    public function getAboutHeader() {
        return BrandAbout::$aboutHeaders[$this->header];
    }
    
    public function getAboutHeaders()
    {
        return BrandAbout::$aboutHeaders;
    }

    /**
     * @param string $header one of: quote, simple, advanced
     */
    public function setAboutHeader($header) {
        // todo: find a better way
        $this->header = array_flip(BrandAbout::$aboutHeaders)[$header];
    }

    /**
     * @return int internal ID of the header
     */
    public function getAboutHeaderInt() {
        return $this->header;
    }
    
    public function setAboutTitle($titleRaw) {
        $this->aboutTitle = htmlspecialchars($titleRaw);
    }

    public function setAboutContents($contentRaw) {
        $this->aboutDescription = $this->escapeAboutContent($contentRaw);
/*        $this->productContent = "<p>" . $this->escapeProductContent($contentRaw) . "</p>";*/
    }
    
    private function escapeAboutContent($raw) {
        // todo: check tag parameters [important before live]
        $out = '';
        $raw = preg_replace('/<\s*br\s*\/?\s*>/', '</p><p>', $raw);
        /*
        preg_match_all('/(.*?)<\s*?(\/?)\s*?(\w+)\s*?([^>]+)?([\/ ]*)>/', $raw, $htmlNodes, PREG_SET_ORDER);

        foreach ($htmlNodes as $node) {
            if(in_array($node[3], BrandPost::$allowedTags)) {
                $out = $out . $node[0];
            } else {
                $out = $out . htmlspecialchars($node[0]);
            }
        }
        */

        return $raw;
    }
/*
    // todo: fix and remove
    public function exchangeArrayXX($data) {
        $this->id                 = (!empty($data['id']))                 ? (int)$data['id']           : null;
        $this->creatorId          = (!empty($data['creatorId']))          ? (int)$data['creatorId']    : null;
        $this->brandId            = (!empty($data['brandId']))            ? (int)$data['brandId']      : null;

        $this->dateCreated        = (!empty($data['dateCreated']))        ? (int)$data['dateCreated']  : null;
        
        $this->aboutTitle         = (!empty($data['aboutTitle']))         ? $data['aboutTitle']        : null;        
        $this->aboutDescription   = (!empty($data['aboutDescription']))   ? $data['aboutDescription']  : null;
        $this->header             = (!empty($data['header']))             ? (int)$data['header']       : 0;
        $this->sortOrder          = (!empty($data['sortOrder']))          ? (int)$data['sortOrder']    : 0;
        
        $this->public             = (!empty($data['public']))             ? (int)$data['public']       : 1;
    }*/

    protected function databaseMappings() {
        return [
            [ 'id',                 DBModel::TYPE_INT        , DBModel::DB_PKEY ],
            [ 'strippedName',       DBModel::TYPE_VARCHAR_128, DBModel::INTERP_DEFAULT ],
            [ 'dateCreated',        DBModel::TYPE_INT        , DBModel::INTERP_TIMESTAMP ],
            [ 'creatorId',          DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'brandId',            DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'aboutTitle',         DBModel::TYPE_VARCHAR_255, DBModel::STR_NO_HTML ],
            [ 'aboutDescription',   DBModel::TYPE_TEXT       , DBModel::STR_NO_HTML ],
            [ 'header',             DBModel::TYPE_BYTE       , DBModel::INTERP_DEFAULT ],
            [ 'sortOrder',          DBModel::TYPE_BYTE       , DBModel::INTERP_DEFAULT ],
            [ 'public',             DBModel::TYPE_BOOL       , DBModel::INTERP_BOOL ],
        ];
    }
}
