<?php
namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Update;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class CommentTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return Comment|null
     */
    public function getCommentById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $postId int
     * @param $firstId int Start of excluded range (inclusive)
     * @param $lastId int End of excluded range (inclusive)
     * @return Comment[]
     */
    public function getCommentsByPost($postId, $firstId = -1, $lastId = -1, $approved = -1, $l = -1) {
        
        $rowset = $this->tableGateway->select(function (Select $select) use($postId, $firstId, $lastId, $approved, $l) {
            //$select->join('users', 'users.id = posterId', ['fullName']);
            if($lastId > 0 && $firstId > 0) {
                $select->where
                    ->equalTo('postId', $postId)
                    ->AND->NEST
                    ->lessThan('id', $firstId)
                    ->OR
                    ->greaterThan('id', $lastId)
                    ->UNNEST;
            }else if($approved >= 0){
                $select->where->equalTo('postId', $postId)->AND->equalTo('approved', 1);
            }else{
                $select->where->equalTo('postId', $postId);
            }
            if($l > 0)
            {
                $select->limit((int)$l);
            }
            $select->order('dateCreated DESC');
                       
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    
    public function getTotalCommentsByPost($postId, $firstId = -1, $lastId = -1, $approved = -1) {
        
        $rowset = $this->tableGateway->select(function (Select $select) use($postId, $firstId, $lastId, $approved) {
            //$select->join('users', 'users.id = posterId', ['fullName']);
            if($lastId > 0 && $firstId > 0) {
                $select->where
                    ->equalTo('postId', $postId)
                    ->AND->NEST
                    ->lessThan('id', $firstId)
                    ->OR
                    ->greaterThan('id', $lastId)
                    ->UNNEST;
            }else if($approved >= 0){
                $select->where->equalTo('postId', $postId)->AND->equalTo('approved', 1);
            }else{
                $select->where->equalTo('postId', $postId);
            }
            $select->order('dateCreated DESC');
                       
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return count($ret);
    }

    /**
     * @param $brandId int
     * @param $firstId int Start of excluded range (inclusive)
     * @param $lastId int End of excluded range (inclusive)
     * @param null|bool $filterSeen If not null, get only these comments with matching value
     * @param null|bool $filterApproved If not null, get only these comments with matching value
     * @param null|bool $filterRejected If not null, get only these comments with matching value
     * @return CommentPostInfo[]
     */
    public function getCommentsByBrand($brandId, $firstId = -1, $lastId = -1, $filterSeen = null, $filterApproved = null, $filterRejected = null) {

        // CHANGE THE RS PROTOTYPE - is it the proper way to do it?
        $this->tableGateway->getResultSetPrototype()->setArrayObjectPrototype(new CommentPostInfo());

        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $firstId, $lastId, $filterSeen, $filterApproved, $filterRejected) {

            //$select->join('users', 'users.id = posterId', ['fullName']);
            $select->join('posts', 'posts.id = postId', ['brandId', 'postTitle']);
            $select->join('categories', 'categories.id = categoryId', ['categoryName', 'categoryUrl' => 'url']);
            if($lastId > 0 && $firstId > 0) {
                $select->where
                    ->equalTo('posts.brandId', $brandId)
                    ->AND->NEST
                        ->lessThan('id', $firstId)
                        ->OR
                        ->greaterThan('id', $lastId)
                    ->UNNEST;
            }else{
                $select->where->equalTo('posts.brandId', $brandId);
            }
            if($filterSeen !== null) {
                $w = new Where();
                $w->equalTo('seen', (bool)$filterSeen);
                $select->where->andPredicate($w);
            }
            if($filterApproved !== null) {
                $w = new Where();
                $w->equalTo('approved', (bool)$filterApproved);
                $select->where->andPredicate($w);
            }
            if($filterRejected !== null) {
                $w = new Where();
                $w->equalTo('rejected', (bool)$filterRejected);
                $select->where->andPredicate($w);
            }

            $select->order('dateCreated DESC');
        });

        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }

        // CHANGE THE RS PROTOTYPE BACK
        $this->tableGateway->getResultSetPrototype()->setArrayObjectPrototype(new Comment());
        return $ret;
    }

    /**
     * @param int $brandId
     */
    public function markBrandCommentsAndNotificationsAsSeen($brandId) {
        // No data binding! Sanitize the data
        $brandId = (int)$brandId;
        $timestamp = time();

        $this->tableGateway->adapter->getDriver()->createStatement(<<<SQL
        UPDATE comments JOIN posts ON posts.id = comments.postId SET seen = 1 WHERE posts.brandId = "${brandId}"
SQL
)->execute();
        $this->tableGateway->adapter->getDriver()->createStatement(<<<SQL
        UPDATE notifications JOIN comments ON notifications.relatedComment = comments.id SET notifications.dateSeen = "${timestamp}" WHERE comments.seen = 1
SQL
)->execute();
    }


    /*protected function commentToTable(Comment $comment) {
        return array(
            'posterId'      => $comment->posterId,
            'posterBrandId' => $comment->posterBrandId,
            'posterDisplayName' => $comment->posterDisplayName,
            'posterEmail'   => $comment->posterEmail,
            'postId'        => $comment->postId,
            'dateCreated'   => $comment->dateCreated,
            'content'       => $comment->content,
            'approved'      => $comment->approved,
            'rejected'      => $comment->rejected,
            'seen'          => $comment->seen,
        );
    }*/

    /**
     * @param Comment $comment Comment to save
     * @throws \Exception If comment id != 0 but it's not in the db
     */
    public function saveComment(Comment $comment)
    {
        $data = $comment->toTable();

        $id = (int)$comment->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $comment->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getCommentById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Comment id does not exist');
            }
        }
    }

    /**
     * @param $id int
     */
    public function deleteComment($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}