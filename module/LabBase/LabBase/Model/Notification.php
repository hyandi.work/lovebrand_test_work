<?php

namespace LabBase\Model;

class Notification extends DBModel {
    public $id;
    public $userId;
    public $text;
    public $link;
    public $date;
    public $dateSeen;

    /**
     * @param int $userId
     * @param string $text
     * @param string $link
     */
    public function __construct($userId = null, $text = null, $link = null) {
        $this->userId = $userId;
        $this->text = $text;
        $this->link = $link;

        $this->date = time();
        $this->dateSeen = null;
    }

    protected function databaseMappings() {
        return [
            DBModel::Field('id',                DBModel::TYPE_INT,          DBModel::DB_PKEY),
            DBModel::Field('userId',            DBModel::TYPE_INT,           DBModel::INTERP_FKEY, [
                DBModel::ForeignKey( 'user', 'User', 'id',    DBModel::FK_NOT_ENFORCED)
            ]),
            DBModel::Field('text',              DBModel::TYPE_VARCHAR_255,  DBModel::INTERP_DEFAULT),
            DBModel::Field('link',              DBModel::TYPE_VARCHAR_255,  DBModel::DB_NULL),
            DBModel::Field('date',              DBModel::TYPE_INT,          DBModel::INTERP_TIMESTAMP),
            DBModel::Field('dateSeen',          DBModel::TYPE_INT,          DBModel::INTERP_TIMESTAMP | DBModel::DB_NULL)
        ];
    }

}