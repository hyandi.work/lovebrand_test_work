<?php

namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class BrandAboutTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandPost|null
     */
    public function getBrandAboutById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
   /* public function getBrandProductsByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select->where->equalTo('brandId', $brandId);
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }*/

    /**
     * @param $brandId int
     * @return int
     */
    public function getBrandAboutCountBrand($brandId) {
        $id = (int)$brandId;
        // todo: count properly
        $rowset = $this->tableGateway->select(['id' => $id]);

        if(!$rowset) return 0;

        return $rowset->count();
    }
    
    
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
    public function getBrandAboutByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select
                ->where->equalTo('brandId', $brandId);
                $select->order('sortOrder ASC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    public function getMaxSortOrder($brandId)
    {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select->where->equalTo('brandId', $brandId);
            $select->order('sortOrder desc');
        });
        
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }
        
        return $ret;
    }

    protected function brandAboutToTable(BrandAbout $about) {
        return [
            'creatorId'        => $about->creatorId,
            'brandId'          => $about->brandId,
            
            'dateCreated'      => $about->dateCreated,

            'aboutTitle'       => $about->aboutTitle,
            'aboutDescription' => $about->aboutDescription,
            'sortOrder'        => (int)$about->sortOrder,
            'header'           => (int)$about->header,
            'public'           => (int)$about->public,
        ];
    }

    /**
     * @param BrandPost $post Brand to save
     * @throws \Exception If brand id != 0 but it's not in the db
     */
    public function saveBrandAbout(BrandAbout $about)
    {
        $data = $about->toTable();
        
        $id = (int)$about->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $about->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getBrandAboutById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Brand id does not exist');
            }
        }
    }

    public function deleteBrandAbout($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}