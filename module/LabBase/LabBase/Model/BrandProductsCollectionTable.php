<?php
namespace LabBase\Model;

use Zend\Db\Sql\Delete;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;

class BrandProductsCollectionTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandProductsCollection|null
     */
    public function getById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $id int
     * @return BrandProductsCollection[]
     */
    public function getCollectionsByBrand($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id) {
            $select->where->equalTo('brandId', $id);
            $select->limit(50);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    
    public function getCollectionsByCategory($categoryId) {
        $id = (int)$categoryId;
        $rowset = $this->tableGateway->select(function (Select $select) use($categoryId) {
            $select->where->equalTo('categoryId', $categoryId);
            $select->limit(50);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    
    public function getCollectionsByBrandByCategory($id, $categoryId) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id, $categoryId) {
            $select->where->equalTo('categoryId', $categoryId)->AND->equalTo('brandId', $id);
            $select->limit(50);
        });
        
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }
    
    /**
     * @param $id int
     * @return BrandProductsCollection[]
     */
    public function getCollectionsByBrandByCategoryPublic($id, $categoryId) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id, $categoryId) {
            $select->where->equalTo('categoryId', $categoryId)->AND->equalTo('brandId', $id)->AND->equalTo('public', 1);
            $select->limit(50);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    public function save(BrandProductsCollection $obj)
    {
        $data = $obj->toTable();

        $id = (int)$obj->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $obj->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Given id does not exist');
            }
        }
    }

    /**
     * @param $id int
     */
    public function delete($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }

    public function makeCollectionPublicOrNot($id, $showOrNot)
    {
        $collection = $this->getById($id);
        if(!empty($collection))
        {
            $collection->public = $showOrNot;
            $this->save($collection);
        }
    }
}