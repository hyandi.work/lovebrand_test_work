<?php

namespace LabBase\Model;

class CommentPostInfo extends Comment {

    public $postTitle;
    public $postCategoryName;
    public $postCategoryUrl;


    public function exchangeArray($data) {
        $this->id           = (!empty($data['id']))             ? (int) $data['id'] : null;
        $this->posterId     = (!empty($data['posterId']))       ? (int) $data['posterId'] : null;
        $this->posterBrandId= (!empty($data['posterBrandId']))  ?       $data['posterBrandId'] : null;
        $this->posterDisplayName = (!empty($data['posterDisplayName'])) ? $data['posterDisplayName'] : null;
        $this->posterEmail  = (!empty($data['posterEmail']))    ?       $data['posterEmail'] : null;
        $this->postId       = (!empty($data['postId']))         ?       $data['postId'] : null;
        $this->dateCreated  = (!empty($data['dateCreated']))    ? (int) $data['dateCreated'] : null;
        $this->content      = (!empty($data['content']))        ?       $data['content'] : null;
        $this->approved     = (!empty($data['approved']))       ? (bool)$data['approved'] : null;
        $this->rejected     = (!empty($data['rejected']))       ? (bool)$data['rejected'] : null;
        $this->seen         = (!empty($data['seen']))           ? (bool)$data['seen'] : null;

        $this->postTitle     = (!empty($data['postTitle']))             ? $data['postTitle'] : null;
        $this->postCategoryName = (!empty($data['postCategoryName']))   ? $data['postCategoryName'] : null;
        $this->postCategoryUrl  = (!empty($data['postCategoryUrl']))    ? $data['postCategoryUrl'] : null;
    }

}