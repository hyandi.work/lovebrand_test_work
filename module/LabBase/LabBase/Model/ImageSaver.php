<?php
/**
 * User: sebi
 * Date: 28/06/15
 * Time: 18:03
 */

namespace LabBase\Model;


use Aws\S3\S3Client;
use Zend\Mvc\Controller\AbstractActionController;

class ImageSaver {

    // max image size in pixels
    private static $MAX_X = 4096;
    private static $MAX_Y = 10000;

    private static $_BUCKET = null;
    public static function getBucketName() {
        if(ImageSaver::$_BUCKET !== null) {
            return ImageSaver::$_BUCKET;
        }
        return defined('_DEVELOPMENT_SERVER') ? 'loveabrand-indev' : 'loveabrand';
    }

    public static function initBucketName($s3bucket) {
        if(ImageSaver::$_BUCKET === null) {
            ImageSaver::$_BUCKET = $s3bucket;
        }
    }


    /**
     * @var S3Client $s3
     */
    private $s3;
    /**
     * @var $imageTable ImageTable
     */
    private $imageTable;


    /**
     * @param S3Client $s3
     * @param ImageTable $imageTable
     */
    public function __construct(S3Client $s3, ImageTable $imageTable) {
        $this->s3 = $s3;
        $this->imageTable = $imageTable;
    }


    /**
     * @param AbstractActionController $controller
     * @param string $postName $_POST key to look for
     * @param string $filesArrayName $_FILES entry name to look for. Same as $postName if ommited
     * @return FALSE|string|array base64 image OR entry from $_FILES array OR FALSE on failure
     */
    public static function GetImageFromClient(AbstractActionController $controller, $postName = 'image', $filesArrayName = NULL) {
        if($filesArrayName === NULL) $filesArrayName = $postName;

        /** @var \Zend\Http\Request $req */
        $req = $controller->getRequest();

        $image = $req->getPost('image', '');
        if(empty($image)) {
            $image = $req->getFiles('image');
        }else{
            $image = base64_decode($image);
        }

        if(!is_array($image) && empty($image)) {
            return FALSE;
        }else{
            return $image;
        }
    }

    /**
     * @param $targetName string Image id/name
     * @param $suffix string Added separately to hash
     * @return string S3 Image path
     */
    public static function GetImageS3Path($targetName, $suffix = '') {
        $targetName = md5($targetName);
        return "i/$targetName$suffix.jpg";
    }

    /**
     * @param string $imagePath Image path in bucket
     * @return string S3 Image link
     */
    public static function GetImageS3Link($imagePath) {
        return 'https://'. ImageSaver::getBucketName() .'.s3.amazonaws.com/' . $imagePath;
    }

    /**
     * @param string $stringID Image string identifier
     * @return string S3 Image link
     */
    public static function GetImageS3LinkFromName($stringID) {
        return 'https://'. ImageSaver::getBucketName() .'.s3.amazonaws.com/' . ImageSaver::GetImageS3Path($stringID);
    }


    /**
     * Loads a GD image from data string, resizes if necessary
     * @param $string string Image data
     * @param null|int $maxX Image will be resized to this width, or to default max if null
     * @param null|int $maxY Image will be resized to this height, or to default max if null
     * @param bool $getRaw If true, the image will not be rescaled
     * @return resource GD Image Resource
     * @throws \Exception on failure
     */
    private static function LoadImage($string, $maxX = null, $maxY = null, $getRaw = false)
    {
        if($maxX === null) {
            $maxX = ImageSaver::$MAX_X;
        }
        if($maxY === null) {
            $maxY = ImageSaver::$MAX_Y;
        }
        $im = \imagecreatefromstring($string);

        if(!$im)
        {
            $im  = \imagecreatetruecolor(250, 50);
            $bg = \imagecolorallocate($im, 255, 255, 255);
            $txt  = \imagecolorallocate($im, 0, 0, 0);

            \imagefilledrectangle($im, 0, 0, 250, 50, $bg);
            \imagestring($im, 5, 10, 10, 'Image upload error', $txt);
        }elseif(!$getRaw) {
            ImageSaver::ResizeImage($im, $maxX, $maxY);
        }

        return $im;
    }
    private static function ResizeImage(&$im, $maxX, $maxY) {
        $w = \imagesx($im);
        $h = \imagesy($im);
        $resizeNeeded = $w > $maxX || $h > $maxY;
        if($resizeNeeded) {
            if($w > $maxX) {
                $newW = $maxX;
                $newH = $h * ($newW / $w);
            }else{
                $newH = $maxY;
                $newW = $w * ($newH / $h);
            }

            $newIm = \imagecreatetruecolor($newW, $newH);
            if(!\imagecopyresampled($newIm, $im, 0, 0, 0, 0, $newW, $newH, $w, $h))
                throw new \Exception("An error has occurred while resizing the image.");
            \imagedestroy($im);
            $im = $newIm;
        }
    }

    /**
     * @param $image Image|string Image object or identifier to delete
     */
    public function deleteImage($image) {
        if(is_string($image)) {
            $image = ImageSaver::GetImageS3Path($image);
        }else{
            /** @var Image $image */
            $image = $image->url;
        }

        $image = $this->imageTable->getImageByUrl($image);
        if(!$image)
            return;

        $this->s3->deleteObject(array(
            'Bucket' => ImageSaver::getBucketName(),
            'Key'    => $image->url
        ));

        $this->imageTable->deleteImageDBEntryRememberToRemoveFromS3($image->id);
    }

    /**
     * @param array|string $file A single entry from the $_FILES array OR string with image data.
     * @param string $imageClass Image class corresponding to source - brand-banner, user-avatar
     * @param string|null $imageId Unique ID in given class - null will get a random ID
     * @param $uploaderId
     * @param null|int $brandId ID of the brand which uploaded this image
     * @param null|int $postId ID of related post (optional)
     * @param null|int $maxX Image will be resized to this width, or to default max if null
     * @param null|int $maxY Image will be resized to this height, or to default max if null
     * @param bool $raw
     * @param bool $overwrite
     * @return Image Saved image
     * @throws \Exception On failure
     */
    public function uploadImageSimple($file, $imageClass, $imageId = null, $uploaderId, $brandId = null, $postId = null, $maxX = 4096, $maxY = 4096, $raw = false, $overwrite = false) {
        if($imageId === null) {
            $imageId = md5($imageClass . mt_rand() . rand());
            $image = $this->imageTable->getImageByUrl(ImageSaver::GetImageS3Path($imageId));
            if($image) {
                $imageId = md5($imageId . mt_rand() . rand());
                $image = $this->imageTable->getImageByUrl(ImageSaver::GetImageS3Path($imageId));
                if($image) {
                    throw new \Exception("Couldn't save the image. Please try again");
                }
            }

        }
        $name = "$imageClass-$imageId";
        return $this->uploadImage($file, $name, $maxX, $maxY, $uploaderId, $brandId, $raw, $imageClass, $overwrite, $postId);
    }

    /**
     * @param array|string $file A single entry from the $_FILES array OR string with image data.
     * @param string $targetName_a Target path where the image should be saved (random if null)
     * @param null|int $maxX Image will be resized to this width, or to default max if null
     * @param null|int $maxY Image will be resized to this height, or to default max if null
     * @param null|int $uploaderUserId ID of the user who is uploading the image
     * @param null|int $brandId ID of the brand which uploaded this image
     * @param bool $saveRaw If true, the image will never be resized or sanitized
     * @param string $imageClass Image class corresponding to source - brand-banner, user-avatar
     * @param bool $overwrite
     * @return Image Saved image
     * @throws \Exception On failure
     */
    public function uploadImage($file, $targetName_a = null, $maxX = null, $maxY = null, $uploaderUserId = null, $brandId = null, $saveRaw = false, $imageClass = null, $overwrite = false, $postId = null) {

        $random = false;
        if($targetName_a == null) {
            $overwrite = false;
            $random = true;
            $targetName_a = md5(mt_rand() . rand());
        }

        $targetName = ImageSaver::GetImageS3Path($targetName_a);
        // Are we updating existing image?
        $image = $this->imageTable->getImageByUrl($targetName);
        if(!$image) {
            $image = new Image();
        }else{
            if(!$overwrite) {
                if ($random) {
                    // and we shouldn't be updating it?

                    // try again
                    $targetName_a = md5($targetName_a . mt_rand() . rand());
                    $targetName = ImageSaver::GetImageS3Path($targetName_a);
                    $image = $this->imageTable->getImageByUrl($targetName);
                    if ($image) // Throw up if we cannot generate a new random id
                        throw new \Exception("Couldn't save the image. Please try again");
                } else {
                    throw new \Exception("Couldn't save the image. Image with the same name exists");
                }
            }
        }



        if(is_array($file)) {
            if(empty($file['tmp_name']))
                return null;
            $imgString = file_get_contents($file['tmp_name']);
        }else{
            $imgString = $file;
        }
        error_log("Input file length: " . strlen($imgString) . " bytes");
        $img = ImageSaver::LoadImage($imgString, $maxX, $maxY, $saveRaw);

        imageinterlace($img, true);

        if(!$saveRaw) {
            ob_start();
            $status = @imagejpeg($img, null, 92);
            $cleanImgString = ob_get_clean();

            if($status !== true) {
                throw new \Exception('The image could not be saved. Source image is not valid.');
            }
        }else{
            $cleanImgString = $imgString;
        }

        $resX = imagesx($img);
        $resY = imagesy($img);


        $image->dateCreated = time();
        $image->resX = $resX;
        $image->resY = $resY;
        $image->url = $targetName;
        $image->uploaderId = $uploaderUserId;
        $image->brandId = $brandId;
        $image->stringIdentifier = $targetName_a;


        $s3data = [
            'Bucket' => ImageSaver::getBucketName(),
            'Body'   => $cleanImgString,
            'ACL'    => 'public-read',
            'ContentType' => 'image/jpeg'
        ];

        $meta = [
            'id' => $targetName_a,
            'upload-date' => $image->dateCreated
        ];
        if($brandId) {
            $meta['page-id'] = $brandId;
        }
        if($uploaderUserId) {
            $meta['user-id'] = $uploaderUserId;
        }
        if($postId) {
            $meta['post-id'] = $postId;
        }
        if($imageClass) {
            $meta['image-class'] = $imageClass;
        }

        $s3data['Metadata'] = $meta;

        $veryLarge = $resX > 2500 || $resY > 2500*2;
        if($veryLarge) {
            $s3data['Key'] = ImageSaver::GetImageS3Path($targetName_a, '_orig');
            $this->s3->putObject($s3data);

            $s3data['Key'] = ImageSaver::GetImageS3Path($targetName_a);
            ImageSaver::ResizeImage($img, 2048, 4096);
            ob_start();
            $status = @imagejpeg($img, null, 92);
            $s3data['Body'] = ob_get_clean();
            if ($status !== true) {
                error_log("Image could not be resized for $targetName_a ($imageClass)");
            }
            $this->s3->putObject($s3data);
        }else{
            $s3data['Key'] = ImageSaver::GetImageS3Path($targetName_a);
            $this->s3->putObject($s3data);
        }

        $this->imageTable->saveImage($image);

        $s3data['Key'] = ImageSaver::GetImageS3Path($targetName_a, '_small_small');
        ImageSaver::ResizeImage($img, 256, 512);
        ob_start();
        $status = @imagejpeg($img, null, 92);
        $s3data['Body'] = ob_get_clean();
        if($status !== true) {
            error_log("Thumbnail small could not be saved for $targetName_a ($imageClass)");
        }
        $this->s3->putObject($s3data);

        return $image;
    }

}