<?php
namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class NotificationTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return Notification|null
     */
    public function getNotificationById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    /**
     * @param $id int
     * @return Notification[]
     */
    public function getNotificationsByUser($id, $limit = 8) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(function (Select $select) use($id, $limit) {
            $select->where->equalTo('userId', $id);
            $select->order('date DESC');
            $select->limit($limit);
        });
        $ret = [];
        foreach($rowset as $row) {
            $ret[] = $row;
        }
        return $ret;
    }


    /*protected function notificationToTable(Notification $notification) {
        return [
            'date'          => $notification->date,
            'dateSeen'      => $notification->dateSeen,
            'text'          => $notification->text,
            'relatedComment' => $notification->relatedComment,
            'relatedPost'   => $notification->relatedPost,
            'relatedBrand'  => $notification->relatedBrand,
        ];
    }*/

    /**
     * @param Notification $notification Comment to save
     * @throws \Exception If comment id != 0 but it's not in the db
     */
    public function saveNotification(Notification $notification)
    {
        $data = $notification->toTable();

        $id = (int)$notification->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $notification->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getNotificationById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Notification id does not exist');
            }
        }
    }

    /**
     * @param $id int
     */
    public function deleteNotification($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}