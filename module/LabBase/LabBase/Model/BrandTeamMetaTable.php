<?php

namespace LabBase\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class BrandTeamMetaTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param $id int
     * @return BrandPost|null
     */
    public function getBrandTeamMetaById($id) {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
   /* public function getBrandProductsByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select->where->equalTo('brandId', $brandId);
            $select->order('dateCreated DESC');
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }*/

    /**
     * @param $brandId int
     * @return int
     */
    public function getBrandTeamMetaCountBrand($brandId) {
        $id = (int)$brandId;
        // todo: count properly
        $rowset = $this->tableGateway->select(['id' => $id]);

        if(!$rowset) return 0;

        return $rowset->count();
    }
    
    
    /**
     * @param $brandId int
     * @return array(BrandPost)|null
     */
    public function getBrandTeamMetaByBrand($brandId) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId) {
            $select
                ->where->equalTo('brandId', $brandId);                
        });
        $ret = [];
        foreach ($rowset as $row) {
            $ret[] = $row;
        }

        return $ret;
    }
    
    public function getBrandTeamMetaByBrandAndKey($brandId, $metaKey) {
        $rowset = $this->tableGateway->select(function (Select $select) use($brandId, $metaKey) {
            $select
                ->where->equalTo('brandId', $brandId)->AND->equalTo('teamKey', $metaKey);
        });
        
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $row;
    }

    protected function brandTeamMetaToTable(BrandTeamMeta $teamMeta) {
        return [
            'creatorId'     => $teamMeta->creatorId,
            'brandId'       => $teamMeta->brandId,
            
            'teamKey'       => $teamMeta->teamKey,
            'teamValue'     => $teamMeta->teamValue,
            
            'public'        => (int)$teamMeta->public,
        ];
    }

    /**
     * @param BrandPost $post Brand to save
     * @throws \Exception If brand id != 0 but it's not in the db
     */
    public function saveBrandTeamMeta(BrandTeamMeta $teamMeta)
    {
/*        $data = $this->brandTeamMetaToTable($teamMeta);*/
        $data = $teamMeta->toTable();

        $id = (int)$teamMeta->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $teamMeta->id = $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getBrandTeamMetaById($id)) {
                $this->tableGateway->update($data, ['id' => $id]);
            } else {
                throw new \Exception('Brand id does not exist');
            }
        }
    }

    public function deleteBrandTeamMeta($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}