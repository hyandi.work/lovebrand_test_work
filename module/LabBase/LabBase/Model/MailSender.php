<?php
/**
 * Created by PhpStorm.
 * User: Sebi
 * Date: 22.01.2016
 * Time: 02:56
 */

namespace LabBase\Model;

use Swift_SmtpTransport;

class MailSender {

    private static $ADDRESSES = [
        "default"   => "noreply@loveabrand.co.uk",
        "noreply"   => "noreply@loveabrand.co.uk",
        "info"      => "info@loveabrand.co.uk",
        "support"   => "support@loveabrand.co.uk"
    ];

    public function __construct() {}

    public function sendMail($fromKeyword = "default", $toEmail, $toName, $subject, $content) {

        $from = MailSender::$ADDRESSES["default"];
        if($fromKeyword && isset(MailSender::$ADDRESSES[$fromKeyword])) {
            $from = MailSender::$ADDRESSES[$fromKeyword];
        }

        if(preg_match("/^[^@]+@[^.@]+\\.[^.@].*$/i", $toEmail) === 0) {
            throw new \Exception("Invalid email address.");
        }

        $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 25)
            ->setUsername('your username')
            ->setPassword('your password');


    }

}