<?php

namespace LabBase\Model;

class Aboutme extends DBModel {

    public $id;
    public $creatorId;
    public $brandId;

    public $aboutmeTitle;
    public $aboutmeDescription;
    public $aboutmeImage;
    
    private static $allowedTags = [ 'p', 'strong', 'a', 'span', 'em', 'img', 'br' ];
   
    public function __construct($userId = NULL, $brandId = NULL, $titleRaw = NULL, $contentRaw = NULL, $image = NULL) {
        if($userId === NULL && $brandId === NULL)
            return;

        $this->creatorId = $userId;
        $this->brandId = $brandId;
        
        $this->aboutmeTitle = htmlspecialchars($titleRaw);
        $this->aboutmeImage = $image;
        
        $this->setAboutmeContents($contentRaw);
    }

    public function setAboutmeTitle($titleRaw) {
        $this->aboutmeTitle = htmlspecialchars($titleRaw);
    }

    public function setAboutmeContents($contentRaw) {
        $this->aboutmeDescription = $this->escapeAboutmeContent($contentRaw);
/*        $this->productContent = "<p>" . $this->escapeProductContent($contentRaw) . "</p>";*/
    }
    
    private function escapeAboutmeContent($raw) {
        // todo: check tag parameters [important before live]
        $out = '';
        $raw = preg_replace('/<\s*br\s*\/?\s*>/', '</p><p>', $raw);
        /*
        preg_match_all('/(.*?)<\s*?(\/?)\s*?(\w+)\s*?([^>]+)?([\/ ]*)>/', $raw, $htmlNodes, PREG_SET_ORDER);

        foreach ($htmlNodes as $node) {
            if(in_array($node[3], BrandPost::$allowedTags)) {
                $out = $out . $node[0];
            } else {
                $out = $out . htmlspecialchars($node[0]);
            }
        }
        */

        return $raw;
    }
/*
    // todo: fix and remove
    public function exchangeArrayXX($data) {
        $this->id                 = (!empty($data['id']))                 ? (int)$data['id']           : null;
        $this->creatorId          = (!empty($data['creatorId']))          ? (int)$data['creatorId']    : null;
        $this->brandId            = (!empty($data['brandId']))            ? (int)$data['brandId']      : null;

        $this->dateCreated        = (!empty($data['dateCreated']))        ? (int)$data['dateCreated']  : null;
        
        $this->aboutTitle         = (!empty($data['aboutTitle']))         ? $data['aboutTitle']        : null;        
        $this->aboutDescription   = (!empty($data['aboutDescription']))   ? $data['aboutDescription']  : null;
        $this->header             = (!empty($data['header']))             ? (int)$data['header']       : 0;
        $this->sortOrder          = (!empty($data['sortOrder']))          ? (int)$data['sortOrder']    : 0;
        
        $this->public             = (!empty($data['public']))             ? (int)$data['public']       : 1;
    }*/

    protected function databaseMappings() {
        return [
            [ 'id',                     DBModel::TYPE_INT        , DBModel::DB_PKEY ],
            [ 'creatorId',              DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'brandId',                DBModel::TYPE_INT        , DBModel::INTERP_DEFAULT ],
            [ 'aboutmeTitle',           DBModel::TYPE_VARCHAR_255, DBModel::STR_NO_HTML ],
            [ 'aboutmeDescription',     DBModel::TYPE_TEXT       , DBModel::STR_NO_HTML ],
            [ 'aboutmeImage',           DBModel::TYPE_TEXT       , DBModel::STR_NO_HTML ],
        ];
    }
}
