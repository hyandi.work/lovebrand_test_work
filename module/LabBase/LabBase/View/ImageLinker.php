<?php

namespace LabBase\View;

use LabBase\Model\Brand;
use LabBase\Model\BrandTable;
use LabBase\Model\Image;
use LabBase\Model\ImageSaver;
use LabBase\Model\ImageTable;
use LabBase\Model\User;
use Zend\View\Helper\AbstractHelper;


class ImageLinker extends AbstractHelper {

    const DEFAULT_USER_AVATAR = '/images/square-avatar.png';
    const DEFAULT_BRAND_AVATAR = '/images/square-avatar.png';

    protected $imageTable;

    public function __construct(ImageTable $imageTable) {
        $this->imageTable = $imageTable;
    }

    /**
     * Echoes out the image link, given the S3 path
     * @param $imagePath string S3 Image path
     * @return ImageLinker Instance of self
     */
    public function __invoke($imagePath = null)
    {
        if(!empty($imagePath))
            echo ImageSaver::GetImageS3Link($imagePath);
        return $this;
    }

    /**
     * Returns the image link, given the s3 path
     * @param $img Image S3 Image path
     * @return string Image URL
     */
    public function getImageLink(Image $img) {
        return ImageSaver::GetImageS3Link($img->url);
    }

    /**
     * Returns <img> tag for given image id
     * @param $imageUrlId string
     * @param $properties string HTML attributes for img
     * @param string $maxWidthCSSValue Set inline CSS for max-width to this value
     * @return string Image URL
     */
    public function getImgHTML($imageUrlId, $properties = '', $maxWidthCSSValue = '') {
        if(!empty($maxWidthCSSValue)) {
            if($properties == null)
                $properties = '';
            if(stripos($properties, 'style="') !== false) {
                $properties = str_ireplace('style="', 'style="max-width: ' . $maxWidthCSSValue . ';', $properties);
            }else {
                $properties .= 'style="max-width: ' . $maxWidthCSSValue . '"';
            }
        }
        return '<img src="'.ImageSaver::GetImageS3Link(ImageSaver::GetImageS3Path($imageUrlId)).'?" '.$properties.' />';
    }

    /**
     * Returns the image link, given the S3 path
     * @param $imageUrlId string $imagePath Image Url-ID
     * @return string URL to the image
     */
    public function getImageLinkFromId($imageUrlId) {
        return ImageSaver::GetImageS3Link(ImageSaver::GetImageS3Path($imageUrlId));
    }

    /**
     * Gets the Image object from the database
     * @param $imagePath string S3 Image path
     * @return Image|null Image object
     */
    public function getImageObject($imagePath) {
        return $this->imageTable->getImageByUrl(ImageSaver::GetImageS3Path($imagePath));
    }

    /**
     * @param $name
     * @return string 1 or 2 letter initial string
     */
    static public function getAvatarInitials($name) {
        $name = trim(str_replace("  ", " ", $name));
        $words = explode(' ', $name);
        if(!$words || count($words) < 2) {
            return substr($name, 0, 2);
        }
        return strtoupper(substr($words[0], 0, 1)) . strtolower(substr($words[count($words)-1], 0, 1));
    }

    /**
     * @param Brand $brand
     * @return string HTML of brand's avatar or the default one
     */
    public function getBrandAvatarHTML(Brand $brand) {
        $avatarImage = $this->getImageObject('brand-avatar-'.$brand->id);
        if(!$avatarImage) {
            $initials = ImageLinker::getAvatarInitials($brand->name);
            
            //$html = '<div class="avatar page-avatar initials-avatar"><span>'.$initials.'</span></div>';
            $html = '<div class="avatar page-avatar image-avatar"><img src="'.ImageLinker::DEFAULT_BRAND_AVATAR.'" /></div>';
        }else{
            $html = '<div class="avatar page-avatar image-avatar"><img src="'.$avatarImage->getURL().'?'.$avatarImage->dateCreated.'" /></div>';
        }
        return $html;
    }

    /**
     * @param User $user User object
     * @return Image User's avatar or the default one
     */
    public function getUserAvatarImageOrDefault(User $user) {
        $avatarImage = $this->getImageObject('user-avatar-'.$user->id);
        if(!$avatarImage) {
            $avatarImage = $this->getImageObject('system-user-avatar-default');
        }
        return $avatarImage;
    }

    /**
     * @param User $user User object
     * @return string Link to the the user's avatar or the default one
     */
    public function getUserAvatarLinkOrDefault(User $user) {
        $avatarImage = $this->getImageObject('user-avatar-'.$user->id);
        if(!$avatarImage) {
            $avatarImage = $this->getImageObject('system-user-avatar-default');
        }
        if(!$avatarImage) {
            return ImageLinker::DEFAULT_USER_AVATAR; //ImageSaver::GetImageS3Link(ImageSaver::GetImageS3Path('system-user-avatar-default'));
        }
        return ImageSaver::GetImageS3Link($avatarImage->url);
    }

    /**
     * @param User $user User object
     * @return string|null Link to the the user's avatar or null
     */
    public function getUserAvatarLinkOrNull(User $user) {
        $avatarImage = $this->getImageObject('user-avatar-'.$user->id);
        if(!$avatarImage) {
            return null;
        }
        return ImageSaver::GetImageS3Link($avatarImage->url);
    }

    public function getIcon($iconId, $altText, $classList = '', $id = null, $inlineStyles = null) {
        $params = ' ';

        if(!empty($classList)) {
            $params .= 'class="'.$classList.' icon" ';
        }else{
            $params .= 'class="icon" ';
        }
        if(!empty($id)) {
            $params .= 'id="'.$id.'" ';
        }
        if(!empty($inlineStyles)) {
            $params .= 'style="'.$inlineStyles.'" ';
        }

        return <<<HTML
<svg class="icon">
    <use${params}xlink:href="/img/icons/${iconId}.svg"></use>
</svg>
HTML;
        //return '<img'.$params.'src="/img/icons/'.$iconId.'.svg" alt="'.$altText.'" />';
    }
    
    public function smart_resize_image($file,
                              $string             = null,
                              $width              = 0, 
                              $height             = 0, 
                              $proportional       = false, 
                              $output             = 'file', 
                              $delete_original    = true, 
                              $use_linux_commands = false,
                                $quality = 100
           ) {
      
        if ( $height <= 0 && $width <= 0 ) return false;
        if ( $file === null && $string === null ) return false;
     
        # Setting defaults and meta
        $info                         = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
        $image                        = '';
        $final_width                  = 0;
        $final_height                 = 0;
        list($width_old, $height_old) = $info;
        $cropHeight = $cropWidth = 0;
     
        # Calculating proportionality
        if ($proportional) {
          if      ($width  == 0)  $factor = $height/$height_old;
          elseif  ($height == 0)  $factor = $width/$width_old;
          else                    $factor = min( $width / $width_old, $height / $height_old );
     
          $final_width  = round( $width_old * $factor );
          $final_height = round( $height_old * $factor );
        }
        else {
          $final_width = ( $width <= 0 ) ? $width_old : $width;
          $final_height = ( $height <= 0 ) ? $height_old : $height;
          $widthX = $width_old / $width;
          $heightX = $height_old / $height;
          
          $x = min($widthX, $heightX);
          $cropWidth = ($width_old - $width * $x) / 2;
          $cropHeight = ($height_old - $height * $x) / 2;
        }
     
        # Loading image to memory according to type
        switch ( $info[2] ) {
          case IMAGETYPE_JPEG:  $file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);  break;
          case IMAGETYPE_GIF:   $file !== null ? $image = imagecreatefromgif($file)  : $image = imagecreatefromstring($string);  break;
          case IMAGETYPE_PNG:   $file !== null ? $image = imagecreatefrompng($file)  : $image = imagecreatefromstring($string);  break;
          default: return false;
        }
        
        
        # This is the resizing/resampling/transparency-preserving magic
        $image_resized = imagecreatetruecolor( $final_width, $final_height );
        if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
          $transparency = imagecolortransparent($image);
          $palletsize = imagecolorstotal($image);
     
          if ($transparency >= 0 && $transparency < $palletsize) {
            $transparent_color  = imagecolorsforindex($image, $transparency);
            $transparency       = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
            imagefill($image_resized, 0, 0, $transparency);
            imagecolortransparent($image_resized, $transparency);
          }
          elseif ($info[2] == IMAGETYPE_PNG) {
            imagealphablending($image_resized, false);
            $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
            imagefill($image_resized, 0, 0, $color);
            imagesavealpha($image_resized, true);
          }
        }
        imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);
        
        
        # Taking care of original, if needed
        if ( $delete_original ) {
          if ( $use_linux_commands ) exec('rm '.$file);
          else @unlink($file);
        }
     
        # Preparing a method of providing result
        switch ( strtolower($output) ) {
          case 'browser':
            $mime = image_type_to_mime_type($info[2]);
            header("Content-type: $mime");
            $output = NULL;
          break;
          case 'file':
            $output = $file;
          break;
          case 'return':
            return $image_resized;
          break;
          default:
          break;
        }
        
        # Writing image according to type to the output destination and image quality
        switch ( $info[2] ) {
          case IMAGETYPE_GIF:   imagegif($image_resized, $output);    break;
          case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output, $quality);   break;
          case IMAGETYPE_PNG:
            $quality = 9 - (int)((0.9*$quality)/10.0);
            imagepng($image_resized, $output, $quality);
            break;
          default: return false;
        }
     
        return true;
      }

}
