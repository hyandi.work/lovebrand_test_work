<?php
/**
 * Date: 20.10.16
 * Time: 14:25
 */

namespace LabBase\View;


use LabBase\Model\BrandTable;
use LabBase\Model\ImageTable;
use Zend\Json\Json;
use Zend\View\Helper\AbstractHelper;

class ProductHelper extends AbstractHelper
{
    protected $imageTable;
    protected $brandTable;
    protected $notificationTable;

    public function __construct(ImageTable $imageTable, BrandTable $brandTable) {
        $this->imageTable = $imageTable;
        $this->brandTable = $brandTable;
    }

    public function  jsonShortData($post)
    {
        /**
         * @var Brand $brand
         */
        if(!$post) return;
        $data = [];
        $data['id'] = $post->id;
        $data['postTitle'] = $post->postTitle;
        $data['postContent'] = $post->postContent;
        $data['buyNow'] = $post->buyNow;
        $data['radio'] = $post->radio;
        $data['price'] = $post->price;
        if(isset($post->commentsCnt)) {
            $data['commentsCnt'] = $post->commentsCnt;
        }else{
            $data['commentsCnt'] = 0;
        }
        if($post->postImageData) {
            $data['postImageData'] = Json::decode($post->postImageData);
        }else{
            $data['postImageData'] = [];
        }
        $imagelinker = $this->imageTable;
        $imagelinker = new ImageLinker($imagelinker);
        $brandTable = $this->brandTable;
        $brand = $brandTable->getBrandById($post->brandId);
        $data['brand'] = [];
        $data['brand']['name'] = $brand->name;
        $data['brand']['avatar'] = $imagelinker->getBrandAvatarHTML($brand);
        $segment = $post->getPostSegment();
        $segParam = $post->getPostSegmentParam();
        $data['brand']['segment'] = $segment;
        $data['brand']['segParam'] = $segParam;
        $data['brand']['postType'] = $post->getPostTypeString();

        if(!isset($post->loveOrNot)){
            $post->loveOrNot = 0;
        }
        if(!isset($post->loveCnt)){
            $post->loveCnt = 0;
        }
        $postImages = array();
        $sliderImages = "";
        $littleSliderIm = "";
        if(!empty($post->postImageData))
        {
            $postImages = json_decode($post->postImageData);
            $totalCount = count($postImages);

            $countPerRow = 2;
            if($totalCount == 1 || $totalCount == 2) {
                $countPerRow = 1;
            }
            $firstRowCount = 1;
            if($totalCount % 2 == 0) {
                $firstRowCount = $countPerRow;
            }
            $i = 0;
            foreach($postImages as $postImage)
            {
                if($i < $firstRowCount) {
                    $currMaxCount = $firstRowCount;
                    $currInRow = $i + 1;
                }else{
                    $currMaxCount = $countPerRow;
                    $currInRow = ($i - $firstRowCount) % $currMaxCount + 1;
                }
                $imageClass = "img-$currInRow-of-$currMaxCount";
                $imageUrl = $postImage->url;
                $lsiderActiveClass = ($i == 0) ? 'active' : '';
                $photoTitle = (isset($post->postTitle) && !empty($post->postTitle)) ? $post->postTitle : count($postImages).' photos uploaded';
                $PhotCpation = (isset($postImage->caption) && $postImage->caption != "") ? '<p>'.$postImage->caption.'</p>' : $post->postContent;
                $dateTimestamp = $post->dateCreated;
                $formattedTime = date("d-m-Y H:i", $dateTimestamp);

                $imgName = basename($postImage->url);

                $statusCSS='';
                $statusText='';
                switch ($post->radio){
                    case '1':
                        $statusCSS='background-color: #D9544F;';
                        $statusText='Sale';
                        break;
                    case '2':
                        $statusCSS='background-color: green;';
                        $statusText='New';
                        break;
                    case '3':
                        $statusCSS='background-color: #4169E1; width:120px;';
                        $statusText='Best Seller';
                        break;
                }
                $sliderImages .='<div style="left: 5%;color:black;visibility:visible;'.$statusCSS.'" class="preview-infogoods" align="center">'.$statusText.'</div>';


                $sliderImages .= '<div class="item '.$lsiderActiveClass.' item-active"  data-slide-number="'.$i.'" data-index="'.$i.'" data-post-id="'.$post->id.'">
                                            <img id="'.$i.'" src="'.$postImage->url.'" class="img-responsive">
                                            <input type="hidden" id="photoTitle_'.$i.'" value="'.$photoTitle.'"/>
                                            <input type="hidden" id="photoCaption_'.$i.'" value="'.$PhotCpation.'"/>
                                            <input type="hidden" id="photoLoveCnt_'.$i.'" value="'.$post->loveCnt.'"/>
                                            <input type="hidden" id="photoLove_'.$i.'" value="'.$post->loveOrNot.'"/>
                                            <input type="hidden" id="photoDateStamp_'.$i.'" value="'.$dateTimestamp.'"/>
                                            <input type="hidden" id="photoDateFormate_'.$i.'" value="'.$formattedTime.'"/>
                                            <input type="hidden" id="photoCommenstCnt_'.$i.'" value="'.(isset($post->commentsCnt) ? $post->commentsCnt : 0).'"/>
                                        </div>';

                $littleSliderIm .= '<li>
                                        <a id="carousel-selector-'.$i.'"  href="javascript:void(0)"  >
                                            <img src="'.$postImage->url.'" style="width:80px; height:60px;" data-post-id="'.$post->id.'" data-slider-item="'.$i.'" class="littleP img-responsive">
                                        </a>
                                  </li>';
                $i++;
            }
        }

        $data['sliderImages'] = $sliderImages;
        $data['littleSlider'] = $littleSliderIm ;

        //$data[''] = $post->;
        echo '<details style="display:none;" id="brand-post-data-'.$post->id.'">'.Json::encode( $data ).'</details>';
    }
}