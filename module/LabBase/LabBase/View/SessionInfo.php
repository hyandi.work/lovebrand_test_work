<?php

namespace LabBase\View;

use LabBase\Model\BrandTable;
use LabBase\Model\NotificationTable;
use LabBase\Model\UserTable;
use Zend\View\Helper\AbstractHelper;

class SessionInfo extends AbstractHelper {

    protected $userTable;
    protected $brandTable;
    protected $notificationTable;

    public function __construct(UserTable $userTable, BrandTable $brandTable) {
        $this->userTable = $userTable;
        $this->brandTable = $brandTable;
    }

    public function __invoke()
    {
        return $this;
    }

    public function getLoggedInUser() {
        return $this->userTable->getLoggedInUser();
    }

    public function getBrandById($id) {
        return $this->brandTable->getBrandById((int)$id);
    }

}