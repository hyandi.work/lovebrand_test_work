<?php
/**
 * Author: Sebi
 * Date: 2014-10-27
 * Time: 21:04
 */

namespace LabBase;
use Aws\Sdk;
use LabBase\Model\BrandTable;
use LabBase\Model\CommentTable;
use LabBase\Model\ImageSaver;
use LabBase\Model\UserTable;
use LabBase\View\ImageLinker;
use LabBase\View\ProductHelper;
use LabBase\View\SessionInfo;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\ServiceManager\ServiceManager;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ViewHelperProviderInterface {

    private static $_module_base_path = __DIR__;
    private $_module_config = null;


    /**
     * @return string Path to the module directory.
     */
    public static function getModulePath() {
        return Module::$_module_base_path;
    }


    public function onBootstrap(MvcEvent $e) {


        $app = $e->getApplication();

        $config = $app->getConfig();
        $stage = $config['stage'];

        if(defined('_DEVELOPMENT_SERVER') == false)
        {
            define('_DEVELOPMENT_SERVER', true);
        }

        $eventManager = $app->getEventManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        // attach to execute at low priority - this way controllers set the title first
        $eventManager->attach('render', array($this, 'setLayoutTitle'), -10);

        ImageSaver::initBucketName($config['aws']['s3bucket']);
    }


    public function setLayoutTitle(MvcEvent $e) {
        $viewHelperManager = $e->getApplication()->getServiceManager()->get('viewHelperManager');

        $headTitleHelper   = $viewHelperManager->get('headTitle');

        $headTitleHelper->setSeparator(' - ');

        $headTitleHelper->append($this->_module_config['site_title']);
    }


    public function getConfig()
    {
        if($this->_module_config === null) {
            $this->_module_config = include  __DIR__ . '/config/module.config.php';
        }
        return $this->_module_config;
    }


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        $tables = [
            'User',
            'Brand',
            'BrandPost',
            'BrandPostLoved',
            'BrandProductsCategory',
            'BrandProductsCollection',
            //'BrandProduct',
            'BrandProductLoved',
            'BrandService',
            'BrandServiceMeta',
            'BrandTeamMember',
            'BrandAbout',
            'BrandTeamMeta',
            'Category',
            'Comment',
            'ProductsComment',
            'Image',
            'Notification',
            'BrandRelation' => 'brandrelations', // todo: fix
            'AboutMe',
            'PortfolioProject',
        ];

        $generated = [];
        foreach($tables as $key => $table) {
            $dbTable = $table;
            $entityName = $table;
            if(is_string($key)) {
                $entityName = $key;
            }
            $generated[$entityName . 'TableGateway'] = function (ServiceManager $sm) use ($entityName, $dbTable) {
                /** @var Adapter $dbAdapter */
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                $resultSetPrototype = new ResultSet();
                $tableClass = '\\LabBase\\Model\\' . $entityName;
                $resultSetPrototype->setArrayObjectPrototype(new $tableClass());
                return new TableGateway($dbTable, $dbAdapter, null, $resultSetPrototype);
            };
            $generated[$entityName . 'Table'] =  function(ServiceManager $sm) use ($entityName) {
                /** @var TableGateway $tableGateway */
                $tableGateway = $sm->get($entityName . 'TableGateway');
                $tableClass = '\\LabBase\\Model\\' . $entityName . 'Table';
                $tableInstance = new $tableClass($tableGateway);
                return $tableInstance;
            };
        }

        $factories = [
            'ImageSaver' => function (ServiceManager $sm) {
                /** @var Sdk $aws */
                $aws = $sm->get(Sdk::class);

                ImageSaver::initBucketName($sm->get('config')['aws']['s3bucket']);
                return new ImageSaver(
                    $aws->createS3(),
                    $sm->get('ImageTable'));
            }
        ];
        return [
            'factories' => array_merge($generated, $factories),
        ];
    }

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'sessioninfo' => function($sm) {
                    /** @var UserTable $userTable */
                    $userTable = $sm->getServiceLocator()->get('UserTable');
                    /** @var CommentTable $commentTable */
                    $commentTable = $sm->getServiceLocator()->get('CommentTable');
                    /** @var BrandTable $brandTable */
                    $brandTable = $sm->getServiceLocator()->get('BrandTable');
                    return new SessionInfo($userTable, $brandTable, $commentTable);
                },
                'imageLinker' => function($sm) {
                    return new ImageLinker($sm->getServiceLocator()->get('ImageTable'), 'loveabrand');
                },
                'productHelper' => function($sm) {
                    $imageTable = $sm->getServiceLocator()->get('ImageTable');
                    /** @var BrandTable $brandTable */
                    $brandTable = $sm->getServiceLocator()->get('BrandTable');
                    return new ProductHelper($imageTable, $brandTable);
                }
            ]
        ];
    }
}