-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: lovebrand
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AboutMe`
--

DROP TABLE IF EXISTS `AboutMe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AboutMe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creatorId` int(11) NOT NULL DEFAULT '0',
  `brandId` int(11) NOT NULL DEFAULT '0',
  `aboutmeTitle` varchar(255) NOT NULL DEFAULT '',
  `aboutmeDescription` text NOT NULL,
  `aboutmeImage` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Brand`
--

DROP TABLE IF EXISTS `Brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `strippedName` varchar(128) NOT NULL DEFAULT '',
  `imageId` varchar(32) NOT NULL DEFAULT '',
  `ownerId` int(11) NOT NULL DEFAULT '0',
  `url` varchar(128) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `brandType` tinyint(4) NOT NULL DEFAULT '0',
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `hasBanner` tinyint(4) NOT NULL DEFAULT '0',
  `loveCount` int(11) NOT NULL DEFAULT '0',
  `connectCount` int(11) NOT NULL DEFAULT '0',
  `storyCount` int(11) NOT NULL DEFAULT '0',
  `lovedStoryCount` int(11) NOT NULL DEFAULT '0',
  `managementTeamName` varchar(128) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `phone` varchar(128) DEFAULT NULL,
  `web` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1234112 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BrandAbout`
--

DROP TABLE IF EXISTS `BrandAbout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrandAbout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `strippedName` varchar(128) NOT NULL DEFAULT '',
  `dateCreated` int(11) NOT NULL DEFAULT '0',
  `creatorId` int(11) NOT NULL DEFAULT '0',
  `brandId` int(11) NOT NULL DEFAULT '0',
  `aboutTitle` varchar(255) NOT NULL DEFAULT '',
  `aboutDescription` text NOT NULL,
  `header` tinyint(4) NOT NULL DEFAULT '0',
  `sortOrder` tinyint(4) NOT NULL DEFAULT '0',
  `public` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BrandPost`
--

DROP TABLE IF EXISTS `BrandPost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrandPost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creatorId` int(11) NOT NULL DEFAULT '0',
  `brandId` int(11) NOT NULL DEFAULT '0',
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `dateCreated` int(11) NOT NULL DEFAULT '0',
  `dateParsed` int(11) NOT NULL DEFAULT '0',
  `postTitle` varchar(255) NOT NULL DEFAULT '',
  `postContentRaw` text NOT NULL,
  `postContent` text NOT NULL,
  `postImageData` text NOT NULL,
  `videoData` text NOT NULL,
  `postMetadata` text NOT NULL,
  `commentsEnabled` tinyint(4) NOT NULL DEFAULT '0',
  `draft` tinyint(4) NOT NULL DEFAULT '0',
  `postType` tinyint(4) NOT NULL DEFAULT '0',
  `collectionId` int(11) NOT NULL DEFAULT '0',
  `radio` varchar(64) DEFAULT NULL,
  `price` text,
  `buyNow` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BrandPostLoved`
--

DROP TABLE IF EXISTS `BrandPostLoved`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrandPostLoved` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL DEFAULT '0',
  `brandId` int(11) NOT NULL DEFAULT '0',
  `postId` int(11) NOT NULL DEFAULT '0',
  `loved` int(11) NOT NULL DEFAULT '0',
  `date` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BrandProduct`
--

DROP TABLE IF EXISTS `BrandProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrandProduct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) DEFAULT NULL,
  `collectionId` int(11) DEFAULT NULL,
  `productTitle` varchar(255) NOT NULL DEFAULT '',
  `productContent` text NOT NULL,
  `productImageData` text NOT NULL,
  `dateCreated` int(11) DEFAULT NULL,
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `creatorId` int(11) NOT NULL DEFAULT '0',
  `brandId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BrandProductsCategory`
--

DROP TABLE IF EXISTS `BrandProductsCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrandProductsCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(255) DEFAULT NULL,
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `canDelete` tinyint(4) NOT NULL DEFAULT '0',
  `brandId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BrandProductsCollection`
--

DROP TABLE IF EXISTS `BrandProductsCollection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrandProductsCollection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) DEFAULT NULL,
  `collectionName` varchar(255) NOT NULL DEFAULT '',
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `brandId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BrandService`
--

DROP TABLE IF EXISTS `BrandService`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrandService` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serviceTitle` varchar(128) DEFAULT NULL,
  `serviceContent` text NOT NULL,
  `serviceImageData` text NOT NULL,
  `dateCreated` int(11) DEFAULT NULL,
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `creatorId` int(11) NOT NULL DEFAULT '0',
  `brandId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BrandServiceMeta`
--

DROP TABLE IF EXISTS `BrandServiceMeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrandServiceMeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `description` text NOT NULL,
  `publicTitle` tinyint(4) NOT NULL DEFAULT '0',
  `publicDescription` tinyint(4) NOT NULL DEFAULT '0',
  `brandId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BrandTeamMember`
--

DROP TABLE IF EXISTS `BrandTeamMember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrandTeamMember` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `imageData` text NOT NULL,
  `biography` text NOT NULL,
  `dateCreated` int(11) DEFAULT NULL,
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `creatorId` int(11) NOT NULL DEFAULT '0',
  `brandId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BrandTeamMeta`
--

DROP TABLE IF EXISTS `BrandTeamMeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BrandTeamMeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teamKey` varchar(255) DEFAULT NULL,
  `teamValue` text NOT NULL,
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `creatorId` int(11) NOT NULL DEFAULT '0',
  `brandId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Category`
--

DROP TABLE IF EXISTS `Category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brandId` int(11) NOT NULL DEFAULT '0',
  `categoryName` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(128) NOT NULL DEFAULT '',
  `position` int(11) NOT NULL DEFAULT '0',
  `parentId` int(11) NOT NULL DEFAULT '0',
  `displayInFeed` tinyint(4) NOT NULL DEFAULT '0',
  `enabled` int(11) NOT NULL DEFAULT '0',
  `isSystemCategory` tinyint(4) NOT NULL DEFAULT '0',
  `isSinglePostCategory` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1234115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Comment`
--

DROP TABLE IF EXISTS `Comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `posterId` int(11) NOT NULL DEFAULT '0',
  `posterBrandId` int(11) NOT NULL DEFAULT '0',
  `postId` int(11) NOT NULL DEFAULT '0',
  `dateCreated` int(11) NOT NULL DEFAULT '0',
  `posterDisplayName` varchar(255) NOT NULL DEFAULT '',
  `posterEmail` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `approved` tinyint(4) NOT NULL DEFAULT '0',
  `rejected` tinyint(4) NOT NULL DEFAULT '0',
  `seen` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Image`
--

DROP TABLE IF EXISTS `Image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stringIdentifier` varchar(128) DEFAULT NULL,
  `url` varchar(128) NOT NULL DEFAULT '',
  `resX` int(11) DEFAULT NULL,
  `resY` int(11) DEFAULT NULL,
  `dateCreated` int(11) DEFAULT NULL,
  `uploaderId` int(11) NOT NULL DEFAULT '0',
  `brandId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=455 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Notification`
--

DROP TABLE IF EXISTS `Notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL DEFAULT '0',
  `text` varchar(255) NOT NULL DEFAULT '',
  `link` varchar(255) DEFAULT NULL,
  `date` int(11) NOT NULL DEFAULT '0',
  `dateSeen` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PortfolioProject`
--

DROP TABLE IF EXISTS `PortfolioProject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PortfolioProject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brandId` int(11) NOT NULL DEFAULT '0',
  `projectName` varchar(255) NOT NULL DEFAULT '',
  `public` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullName` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `confirmedEmail` varchar(128) DEFAULT NULL,
  `registerDate` int(11) NOT NULL DEFAULT '0',
  `hasAvatarTimestamp` int(11) DEFAULT NULL,
  `password` varchar(64) NOT NULL DEFAULT '',
  `lastVisitedBrandId` int(11) DEFAULT NULL,
  `sessionId` int(11) DEFAULT NULL,
  `sessionKey` int(11) DEFAULT NULL,
  `sessionExpires` int(11) DEFAULT NULL,
  `userType` int(11) NOT NULL DEFAULT '0',
  `lastReadPostTimestamp` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `sessionId` (`sessionId`)
) ENGINE=InnoDB AUTO_INCREMENT=1234112 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brandrelations`
--

DROP TABLE IF EXISTS `brandrelations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brandrelations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `brandId` int(11) DEFAULT NULL,
  `relationType` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-02  9:10:14
