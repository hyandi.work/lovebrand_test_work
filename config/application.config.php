<?php
/**
 * Author: Sebi
 * Date: 2014-10-27
 * Time: 20:44
 */

return [

    'modules' => [
        'AwsModule',
        'LabBase',        
        'ScnSocialAuth',
        'ZfcBase',
        'ZfcUser',
    ],

    'module_listener_options' => [
        'module_paths' => [
            './module',
            './vendor',
        ],

        'config_glob_paths' => [
            'config/autoload/{,*.}{global,local}.php',
        ],
    ]


];