<?php
/**
 * Author: Sebi
 * Date: 2014-10-27
 * Time: 19:49
 */
$load_starttime = microtime(true);

// cd to the domain root
chdir(dirname(__DIR__));
define('ROOT_PATH', dirname(__DIR__));
// set the timezone
date_default_timezone_set("Europe/London");

// make sure we are running on development server (built-in php server or ENV[HTTP_TEST_ENVIRON] set)
if(isset($_COOKIE['indev']) || getenv("HTTP_TEST_ENVIRON") === "TRUE" || (isset($_SERVER['SERVER_SOFTWARE']) && strpos($_SERVER['SERVER_SOFTWARE'], 'Development Server') !== FALSE)) {
    error_reporting(E_ALL | E_STRICT);
    ini_set("display_errors", 1);
    ini_set("display_startup_errors", 1);

    if(defined('_DEVELOPMENT_SERVER') == false)
    {
        define('_DEVELOPMENT_SERVER', true);
    }


    if(isset($_GET['db_structure'])) {

        $sql = [];

        if(defined('_DEVELOPMENT_SERVER')) {
            require_once 'module/LabBase/LabBase/Model/DBModel.php';
            $config = (require 'module/LabBase/config/module.config.php');
            $cfgModels = $config['db_models'];
            foreach($cfgModels as $modelName) {
                $fqModelName = '\\LabBase\\Model\\' . $modelName;
                $path = 'module/LabBase/LabBase/Model/' . $modelName . '.php';
                /** @var \LabBase\Model\DBModel $obj */
                include_once $path;
                $obj = new $fqModelName();
                $sql[$modelName] = $obj->generateSQLDefinition($modelName);
            }
        }

    ?>
<?php if($_COOKIE['indev'] !== "puresql") { ?><pre><?php } ?>
<? foreach($sql as $model => $table) { ?>

-- <?= $model ?>

<?= $table ?>

<? } ?>
<?php if($_COOKIE['indev'] !== "puresql") { ?></pre><?php } ?>
    <?php
        die();
    }

    // for debug, allow phpinfo
    if(isset($_GET['do_phpinfo'])) {
        if(!isset($_COOKIE['devpass'])) {
            die("do_phpinfo not allowed. (see index for auth method)");
        }
        phpinfo();
        exit();
    }
}else{
    define('_PRODUCTION_SERVER', true);
}

$user_agent = $_SERVER['HTTP_USER_AGENT'];
if( preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $user_agent)
    || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',
        substr($user_agent,0,4)
    )
) {
    define('USER_AGENT_MOBILE', true);
}

if(isset($_COOKIE['ui'])) {
    if($_COOKIE['ui'] == 'desktop') {
        define('SERVE_DESKTOP', true);
    }elseif($_COOKIE['ui'] == 'mobile') {
        define('SERVE_MOBILE', true);
    }
}elseif(defined('USER_AGENT_MOBILE')) {
    define('SERVE_MOBILE', true);
}

// Load the ZF2
if (file_exists('vendor/autoload.php')) {
    $loader = include 'vendor/autoload.php';
}else{
    die("Composer (ZF2) autoloader not found");
}

// Make sure it's loaded
if (!class_exists('Zend\Loader\AutoloaderFactory')) {
    die('error: ZF2 not loaded.');
}

// Load the config and run
$config = require 'config/application.config.php';

Zend\Mvc\Application::init($config)->run();

if(defined('_DEVELOPMENT_SERVER')) {
    $headers = headers_list();
    foreach($headers as $h) {
        if(strpos(strtolower($h), 'content-type') === false)
            continue;
        if(strpos(strtolower($h), 'text/html') !== false) {
            // outputting html page (not ajax/json) - add load time
            $load_milliseconds = (int)((microtime(true) - $load_starttime) * 1000);
            echo 'Load time: '.$load_milliseconds.'ms';
        }
    }
}
