/*

 dragDropImageToURL(domEl, url)
    Attaches a drag-and-drop handler for given dom element which
    sends an image to the given URL (post 'image' <- base64 of image), expects json with {status: 'ok'}

 dragDropImageWithCallback(domEl, callback(base64img))
    Attaches a drag-and-drop handler for given dom element which
    fires the callback with a base64 of image as the argument

 */

var IMG_MAX_SIZE = 1920; // max resolution in any direction

function handleFileUploadStart(files, callback) {
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var imageType = /^image\//;

        // Check mime type for image
        if (!imageType.test(file.type)) {
            continue;
        }

        // Image object for the current file
        var img = document.createElement('img');
        img.onload = function() {
            this.onload = function() {};
            handleImageAdd(this, callback);
        };

        // Load the file
        var reader = new FileReader();
        reader.onload = function(img){return function(e) { img.src = e.target.result; }}(img);
        reader.readAsDataURL(file);
    }
}


function canvasError() {
    var err = $(".errorContainer");
    err.show();
    err.text("Error: Your web browser is outdated, please upgrade your browser in order to add images.");
    console.error(err);
}

// Canvas for resizing
var uploadCanvas = null;

// Handle the image adding on img load
function handleImageAdd(img, callback) {
    // img is an <img> element

    if(!uploadCanvas) {
        uploadCanvas = document.createElement('canvas');
        if(!uploadCanvas) {
            canvasError();
            return;
        }
    }
    var ctx;

    // Resize if necessary and possible
    if(img.width > IMG_MAX_SIZE || img.height > IMG_MAX_SIZE) {

        var width = img.width;
        var height = img.height;

        // Check if a lot of resizing is needed
        // (workaround for poor scaling algo in browsers)
        var firstResize = true;
        if(width > IMG_MAX_SIZE*2 || height > IMG_MAX_SIZE*2) {
            height = ~~(height * 0.5);
            width = ~~(width * 0.5);
            var tempCanvas = document.createElement('canvas');
            tempCanvas.height = height;
            tempCanvas.width = width;
            $('body').prepend(tempCanvas);
            var tempCtx = tempCanvas.getContext('2d');
            if(!tempCtx) {
                canvasError();
                return;
            }
            tempCtx.drawImage(img, 0, 0, width, height);

            // halve the resolution in each iteration
            while (width > IMG_MAX_SIZE * 2 || height > IMG_MAX_SIZE * 2) {
                height = ~~(height * 0.5);
                width = ~~(width * 0.5);

                tempCtx.drawImage(tempCanvas, 0, 0, width*2, height*2, 0, 0, width, height);
                firstResize = false;
            }
            var tempWidth = width,
                tempHeight = height;

            if (width > height) {
                height = ~~(height * IMG_MAX_SIZE / width);
                width = IMG_MAX_SIZE;
            }else{
                width = ~~(width * IMG_MAX_SIZE / height);
                height = IMG_MAX_SIZE;
            }
            uploadCanvas.width = width;
            uploadCanvas.height = height;

            ctx = uploadCanvas.getContext("2d");
            ctx.drawImage(tempCanvas, 0, 0, tempWidth, tempHeight, 0, 0, width, height);
            tempCanvas.remove();
        }else{
            if (width > height) {
                height = ~~(height * IMG_MAX_SIZE / width);
                width = IMG_MAX_SIZE;
            }else{
                width = ~~(width * IMG_MAX_SIZE / height);
                height = IMG_MAX_SIZE;
            }
            uploadCanvas.width = width;
            uploadCanvas.height = height;

            ctx = uploadCanvas.getContext("2d");
            if(!ctx) {
                canvasError();
                return;
            }
            ctx.drawImage(img, 0, 0, width, height);
        }

    // Resize not needed
    }else{
        uploadCanvas.width = img.width;
        uploadCanvas.height = img.height;

        ctx = uploadCanvas.getContext("2d");
        if(!ctx) {
            canvasError();
            return;
        }
        ctx.drawImage(img, 0, 0, img.width, img.height);
    }

    var dataUrl = uploadCanvas.toDataURL('image/jpeg', 0.9);
    img.src = dataUrl;
    if(dataUrl.indexOf('base64,') !== 0) {
        console.warn("toDataURL returned non-base64 string");
        callback(null);
        return;
    }
    img.datastr = dataUrl.split('base64,', 2)[1];

    callback(img);
}



function dummyEvent(e) {
    e.stopPropagation();
    e.preventDefault();
}

// Callback gets an <img> dom object, with datastr attribute, containing base64 string
window.dragDropImageWithCallback = function(domEl, callback) {
    domEl.addEventListener("dragenter", dummyEvent, false);
    domEl.addEventListener("dragover", dummyEvent, false);
    domEl.addEventListener("drop", function(e) {
        e.stopPropagation();
        e.preventDefault();

        if(!e.dataTransfer || !e.dataTransfer.files) {
            console.warn("Drop event doesn't have files");
            return;
        }

        var dt = e.dataTransfer;
        var files = dt.files;

        handleFileUploadStart(files, callback);
    }, false);
}

function uploadImageGetCallback(url, $loadingIndicator) {
    return function(img) {
        var finishFn = null;
        if($loadingIndicator !== void 0) {
            finishFn = window.loadingIndicator($loadingIndicator);
        }
        ajaxSubmit(url, {
            "image": img.datastr
        }, function (data) {
            if (data['status'] != 'ok') {
                console.error(data['status']);
                alert(data['status']);
            } else {
                document.location.reload();
            }
            if(finishFn) finishFn();
        });
    }
}

window.dragDropImageToURL = function(domEl, url) {
    dragDropImageWithCallback(domEl, uploadBannerImageGetCallback(url));
}