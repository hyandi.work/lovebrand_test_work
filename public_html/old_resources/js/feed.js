function saveCategories() {
    // todo: add delay timer

}

function prepareImageData(type) {
    var imageData = [];
    switch(type)
    {
        case 'upload-photo' : 
                var imageSet = window.uploadImageSetStoragePhoto.get();
            break;
        case 'upload-photo-album' : 
                var imageSet = window.uploadImageSetStoragePhotoAlbum.get();
            break;
        case 'upload-product' : 
                var imageSet = window.uploadImageSetStorageProduct.get();
            break;
        case 'upload-service' : 
                var imageSet = window.uploadImageSetStorageService.get();
            break;
        case 'upload-team-member' : 
                var imageSet = window.uploadImageSetStorageTM.get();
            break;
        case 'upload-aboutme' : 
                var imageSet = window.uploadImageSetStorageAboutme.get();
            break;
        default : 
                var imageSet = window.uploadImageSetStorage.get();
            break;
    }
    
    for(var i = 0; i < imageSet.images.length; i++) {
        var img = imageSet.images[i];
        if(img.loaded && img.url) {
            imageData.push(img);
        }
    }

    return window.ClassyJSON.jsonify(imageData);
}

function preparePhotoAlbumImageData(edit, uploadType) {
    var imageData = [];
    if(uploadType != "")
    {
        var editCls = (edit != "") ? '.'+edit : '';
    }
    else
    {
        var editCls = (edit) ? '.edit #photo_section .slider1' : '#photo_section .slider1';
    }
   
   var imageSet = window.uploadImageSetStoragePhotoAlbum.get();
   var imageCaptions = {};
    
    for(var i = 0; i < imageSet.images.length; i++) {
        var img = imageSet.images[i];
        if(img.loaded && img.url) {
            var imageName= GetFilename(img.url);
            var img_caption = $(editCls+' textarea.image-caption_'+imageName).val();
            img.caption = img_caption;
            
            imageData.push(img);
        }
    }   
/*    console.log(imageData); return false;*/
    return window.ClassyJSON.jsonify(imageData);
}

function postStory(storyClass, editorType, includeImages) {
    var draftsPost = (storyClass == 'draft-input') ? 'draft' : "";
    storyClass = (storyClass == 'draft-input') ? 'adv-story-input' : storyClass;
    var storySelector = '.editor-content .' + storyClass;
    var $titleField = $(storySelector + ' .story-input-title');
    if(editorType == 'video')
    {
        var $contentField = $(storySelector + ' textarea.video-input-text');
    }
    else
    {
        var $contentField = $(storySelector + ' textarea.story-input-text');
    }
    
    var contenID = $contentField.attr('id');
    // console.log(storySelector);

    var contents = '';
    if(editorType != 'advanced') {
        contents = $contentField.val().trim();
        /*contents = tinyMCE.get(contenID).getContent();*/
    }else{
        contents = tinyMCE.get(contenID).getContent();
        /*contents = tinyMCE.activeEditor.getContent();*/
        /*contents = tinyMCE.get('advanced-story').getContent();*/
    }
    //contents = tinyMCE.get(contenID).getContent();
    
    var title = $titleField.length ? $titleField.val().trim() : '';

    if(storyClass == 'upload-photo')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the photos.");
            return;
        }
    }
    else if(editorType == 'video')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the video.");
            return;
        }
    }
    else
    {
        if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
            var msg = (draftsPost == 'draft') ? 'saving draft' : (storyClass == 'upload-photo-album') ? 'submitting the posts' : 'submitting the post';
            labAlert("Please fill in the title and contents before "+msg+".");
            return;
        }
    }
    
    var videoURL = '';
    if(editorType == 'video')
    {
        videoURL = $(storySelector + ' .video-url-text').val();
    }

    var imageData = null;
    
    if(includeImages) {
        if(storyClass == 'photo-album')
        {
            imageData = preparePhotoAlbumImageData();
        }
        else
        {
            imageData = prepareImageData(storyClass);
        }
    }
    /*console.log(imageData); return false;*/

    ajaxSubmit('/$brandPost/' + g_thisBrand.url, {
            'title': title,
            'content': contents,
            'cat': g_thisBrand.category,
            'imageData': imageData,
            'postType': editorType,
            'allowComments': true,
            'draft' : draftsPost,
            'videoURL' : videoURL,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the post. " + error);
        }else{
            console.log("post submitted", data);
            if(includeImages) {
                if(storyClass == 'photo-album')
                {
                    var imgs = window.uploadImageSetStoragePhotoAlbum.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStoragePhotoAlbum.set(imgs);
                }
                else if(storyClass == 'upload-photo')
                {
                    var imgs = window.uploadImageSetStoragePhoto.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStoragePhoto.set(imgs);
                }
                else
                {
                    var imgs = window.uploadImageSetStorage.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStorage.set(imgs);
                }
            }
            var msg1 = (draftsPost == 'draft') ? 
                            'Draft saved sccessfully.' : 
                            (storyClass == 'upload-photo') ? 'Photos submitted successfully.' : 
                                (storyClass == 'upload-photo-album') ? 'Album submitted successfully.' : 
                                (storyClass == 'video-input') ? 'Video post submitted successfully.' : 'Post submitted successfully.';
            labAlert(msg1);
            window.location.href = window.location.href;
        }
    });
}

function editAllPostStory(storyClass, editorType, includeImages) {
    var draftsPost = (storyClass == 'draft-input') ? 'draft' : "";
    storyClass = (storyClass == 'draft-input') ? 'adv-story-input' : storyClass;
    var storySelector = '.editor-content .' + storyClass;
    var $titleField = $(storySelector + ' .story-input-title');
    
    var postId = $(storySelector + ' .story-id').val();
    
    if(postId <= 0)
    {
        labAlert("You can not edit this story.");
        return;
    }
    
    if(editorType == 'video')
    {
        var $contentField = $(storySelector + ' textarea.video-input-text');
    }
    else
    {
        var $contentField = $(storySelector + ' textarea.story-input-text');
    }
    
    var contenID = $contentField.attr('id');
    // console.log(storySelector);

    var contents = '';
    if(editorType != 'advanced') {
        contents = $contentField.val().trim();
        /*contents = tinyMCE.get(contenID).getContent();*/
    }else{
        contents = tinyMCE.get(contenID).getContent();
        /*contents = tinyMCE.activeEditor.getContent();*/
        /*contents = tinyMCE.get('advanced-story').getContent();*/
    }
    //contents = tinyMCE.get(contenID).getContent();
    
    var title = $titleField.length ? $titleField.val().trim() : '';

    if(storyClass == 'upload-photo')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the photos.");
            return;
        }
    }
    else if(editorType == 'video')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the video.");
            return;
        }
    }
    else
    {
        if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
            var msg = (draftsPost == 'draft') ? 'saving draft' : (storyClass == 'upload-photo-album') ? 'submitting the posts' : 'submitting the post';
            labAlert("Please fill in the title and contents before "+msg+".");
            return;
        }
    }
    
    var videoURL = '';
    if(editorType == 'video')
    {
        videoURL = $(storySelector + ' .video-url-text').val();
    }

    var imageData = null;
    var type = (storyClass == 'upload-photo-edit') ? 'upload-photo' : storyClass;
    if(includeImages) {
        if(storyClass == 'photo-album')
        {
            imageData = preparePhotoAlbumImageData();
        }
        else
        {
            imageData = prepareImageData(type);
        }
    }
    /*console.log(imageData); return false;*/

    ajaxSubmit('/$brandEditPostType/' + g_thisBrand.url, {
            'title': title,
            'content': contents,
            'cat': g_thisBrand.category,
            'imageData': imageData,
            'postType': editorType,
            'allowComments': true,
            'draft' : draftsPost,
            'videoURL' : videoURL,
            'editId' : postId,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the post. " + error);
        }else{
            console.log("post submitted", data);
            if(includeImages) {
                if(storyClass == 'photo-album')
                {
                    var imgs = window.uploadImageSetStoragePhotoAlbum.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStoragePhotoAlbum.set(imgs);
                }
                else if(type == 'upload-photo')
                {
                    var imgs = window.uploadImageSetStoragePhoto.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStoragePhoto.set(imgs);
                }
                else
                {
                    var imgs = window.uploadImageSetStorage.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStorage.set(imgs);
                }
            }
            var msg1 = (draftsPost == 'draft') ? 
                            'Draft saved sccessfully.' : 
                            (type == 'upload-photo') ? 'Photos submitted successfully.' : 
                                (storyClass == 'upload-photo-album') ? 'Album submitted successfully.' : 
                                (storyClass == 'video-input') ? 'Video post submitted successfully.' : 'Post submitted successfully.';
            labAlert(msg1);
            window.location.href = window.location.href;
        }
    });
}

function editPostType(storyClass, editorType, includeImages, albumId) {
    var draftsPost = (storyClass == 'draft-input') ? 'draft' : "";
    storyClass = (storyClass == 'draft-input') ? 'adv-story-input' : storyClass;
    var storySelector = '.edit .editor-content .' + storyClass;
    var $titleField = $(storySelector + ' .story-input-title');
        
    if(editorType == 'video')
    {
        var $contentField = $(storySelector + ' textarea.video-input-text');
    }
    else
    {
        var $contentField = $(storySelector + ' textarea.story-input-text');
    }
    
    var contenID = $contentField.attr('id');
    // console.log(storySelector);

    var contents = '';
    if(editorType != 'advanced') {
        contents = $contentField.val().trim();
        /*contents = tinyMCE.get(contenID).getContent();*/
    }else{
        contents = tinyMCE.get(contenID).getContent();
        /*contents = tinyMCE.activeEditor.getContent();*/
        /*contents = tinyMCE.get('advanced-story').getContent();*/
    }
    //contents = tinyMCE.get(contenID).getContent();
    
    var title = $titleField.length ? $titleField.val().trim() : '';

    if(storyClass == 'upload-photo')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the photos.");
            return;
        }
    }
    else if(editorType == 'video')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the video.");
            return;
        }
    }
    else
    {
        if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
            var msg = (draftsPost == 'draft') ? 'saving draft' : (storyClass == 'upload-photo-album') ? 'submitting the posts' : 'submitting the post';
            labAlert("Please fill in the title and contents before "+msg+".");
            return;
        }
    }
    
    var videoURL = '';
    if(editorType == 'video')
    {
        videoURL = $(storySelector + ' .video-url-text').val();
    }

    var imageData = null;
    
    if(includeImages) {
        if(storyClass == 'photo-album')
        {
            imageData = preparePhotoAlbumImageData(true);
        }
        else
        {
            imageData = prepareImageData(storyClass);
        }
    }
/*return false;*/
    ajaxSubmit('/$brandEditPostType/' + g_thisBrand.url, {
            'title': title,
            'content': contents,
            'cat': g_thisBrand.category,
            'imageData': imageData,
            'postType': editorType,
            'allowComments': true,
            'draft' : draftsPost,
            'videoURL' : videoURL,
            'editId' : albumId,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the post. " + error);
        }else{
            console.log("post submitted", data);
            if(includeImages) {
                if(storyClass == 'photo-album')
                {
                    var imgs = window.uploadImageSetStoragePhotoAlbum.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStoragePhotoAlbum.set(imgs);
                }
                else
                {
                    var imgs = window.uploadImageSetStorage.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStorage.set(imgs);
                }
            }
            var msg1 = (draftsPost == 'draft') ? 
                            'Draft saved sccessfully.' : 
                            (storyClass == 'upload-photo') ? 'Photos submitted successfully.' : 
                                (storyClass == 'upload-photo-album') ? 'Album submitted successfully.' : 
                                (storyClass == 'video-input') ? 'Video post submitted successfully.' : 'Post submitted successfully.';
            labAlert(msg1);
            window.location.href = window.location.href;
        }
    });
}

function upoloadAlbumPhoto(albumId) {
    var imageData = null;
    
    
    imageData = preparePhotoAlbumImageData('album_photo_new_container', 'edit-album-photo');
    console.log(imageData);

    ajaxSubmit('/$brandEditAlbumPhoto/' + g_thisBrand.url, {
            'imageData': imageData,
            'albumId' : albumId,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the post. " + error);
        }else{
            console.log("album photo submitted", data);
            
            var imgs = window.uploadImageSetStoragePhotoAlbum.get();
            imgs.images = [];
            imgs.imagesChanged();
            window.uploadImageSetStoragePhotoAlbum.set(imgs);
                
            var msg1 = 'Album photo submitted successfully.';
            labAlert(msg1);
            window.location.href = window.location.href;
        }
    });
}


function editPostStory(postID, drafts) {
    var $titleField = $('#edit_post_title_'+postID);
    var $contentField = $('#edit_post_content_'+postID);
    var contenID = $contentField.attr('id');
    
    contents = tinyMCE.get(contenID).getContent();
    
    var title = $titleField.length ? $titleField.val().trim() : '';

    if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
        labAlert("Please fill in the title and contents before submitting the post.");
        return;
    }
    window.loadingIndicator($('.modal-content'));
    ajaxSubmit('/$brandPostEdit/' + postID, {
            'title': title,
            'content': contents,
            'drafts' : drafts
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the post. " + error);
        }else{
            console.log("post submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorage.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorage.set(imgs);
            }
            window.location.reload();
        }
    });
}

function manageProduct(obj) {
    var type = $(obj).data('type');
    type = (type != "") ? type : 'add';
    includeImages = true;
    var $titleField = $('#productsManagement .product-input-title');
    var $contentField = $('#productsManagement textarea.product-input-text');
    var $categoryField = $('#productsManagement .product-category-text');
    var $collectionField = $('#productsManagement .product-collection-text');    

    var prodcutId = 0;
    if(type == 'edit')
    {
        prodcutId = $('#productsManagement .product-edit-id').val();
        
        if(prodcutId < 1)
        {
            labAlert("Product id not found.");
            return;
        }
    }
    
    var contents = $contentField.val().trim();
    
    var title = $titleField.length ? $titleField.val().trim() : '';
    
    if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
        labAlert("Please fill in the title and description before submitting the product.");
        return;
    }

    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-product');
    }

    var actionURL = '/$brandProductAdd/'+g_thisBrand.url;
    if(type == 'edit')
    {
        actionURL = '/$brandProductEdit/'+g_thisBrand.url;
    }
    
    ajaxSubmit(actionURL , {
            'title': title,
            'content': contents,
            'category': $categoryField.val(),
            'collection': $collectionField.val(),
            'imageData': imageData,
            'prodcutId': prodcutId,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            var msg = (type == 'edit') ? 'editing the product' : 'submitting the product';
            labAlert("An error has occured while "+msg+". " + error);
        }else{
            console.log("product submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageProduct.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageProduct.set(imgs);
            }
            var msg = (type == 'edit') ? 'Product updated successfully.' : 'Product submitted successfully.';
            labAlert(msg);
            window.location.href = window.location.href;
        }
    });
}

function addProductFromHome()
{
    includeImages = true;
    var $titleField = $('.product-input .product-input-title');
    var $contentField = $('.product-input textarea.product-input-text');
    var $categoryField = $('.product-input .product-category-text');

    var type = 'add';
    var prodcutId = 0;
    
    var contents = $contentField.val().trim();
    
    var title = $titleField.length ? $titleField.val().trim() : '';
    
    if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
        labAlert("Please fill in the title and description before submitting the product.");
        return;
    }

    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-product');
    }

    var actionURL = '/$brandProductAdd/'+g_thisBrand.url;
    
    ajaxSubmit(actionURL , {
            'title': title,
            'content': contents,
            'category': $categoryField.val(),
            'imageData': imageData,
            'prodcutId': prodcutId,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            var msg = (type == 'edit') ? 'editing the product' : 'submitting the product';
            labAlert("An error has occured while "+msg+". " + error);
        }else{
            console.log("product submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageProduct.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageProduct.set(imgs);
            }
            var msg = 'Product submitted successfully.';
            $titleField.val('');
            $contentField.val('');
            labAlert(msg);
            $('.editor-tabs span:first-child').trigger('click');
        }
    });
}

function addServiceData()
{
    var titleField = $('.service_add .service_add_title');
    var contentField = $('.service_add textarea.service_add_description');
    includeImages = true;
    if((titleField.length && titleField.val().trim().length < 1) || (contentField.length && contentField.val().trim().length < 1))
    {
        labAlert("Please fill in the title and description before submitting the service.");
        return;
    }
    
    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-service');
    }
    window.loadingIndicator($('body'));
    
    ajaxSubmit('/$serviceAdd/'+g_thisBrand.url , {
            'title': titleField.val().trim(),
            'content': contentField.val().trim(),
            'imageData': imageData,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the service. " + error);
        }else{
            console.log("service submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageService.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageService.set(imgs);
            }
            var msg = 'Service submitted successfully.';
            labAlert(msg);
            window.location.href = window.location.href;
        }
    });
}

function editServiceData()
{
    var titleField = $('#serviceEditPopup .service_add .service_add_title');
    var contentField = $('#serviceEditPopup .service_add textarea.service_add_description');
    var serviceId = $('#serviceEditPopup .service_add #hdnserviceId').val();
    includeImages = true;
    if((titleField.length && titleField.val().trim().length < 1) || (contentField.length && contentField.val().trim().length < 1))
    {
        labAlert("Please fill in the title and description before submitting the service.");
        return;
    }
    
    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-service');
    }
    window.loadingIndicator($('body'));
    
    ajaxSubmit('/$brandServiceEdit/'+g_thisBrand.url , {
            'title': titleField.val().trim(),
            'content': contentField.val().trim(),
            'serviceId': serviceId,
            'imageData': imageData,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the service. " + error);
        }else{
            console.log("service submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageService.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageService.set(imgs);
            }
            var msg = 'Service submitted successfully.';
            labAlert(msg);
            window.location.href = window.location.href;
        }
    });
}


function addTeamMemberData()
{
    var titleField = $('#addTeamMember .team_member_name');
    var position = $('#addTeamMember .team_member_position').val().trim();
    var biography = $('#addTeamMember textarea.team_member_description').val().trim();
    includeImages = true;
    if((titleField.length && titleField.val().trim().length < 1))
    {
        labAlert("Please fill in the name submitting the team member.");
        return;
    }
    
    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-team-member');
    }
    window.loadingIndicator($('body'));
    
    ajaxSubmit('/$addTeamMember/'+g_thisBrand.url , {
            'name': titleField.val().trim(),
            'position': position,
            'biography': biography,
            'imageData': imageData,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the service. " + error);
        }else{
            console.log("team member submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageTM.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageTM.set(imgs);
            }
            var msg = 'Team member submitted successfully.';
            labAlert(msg);
            window.location.href = window.location.href;
        }
    });
}


function editTeamMemberData(tmId)
{
    var titleField = $('#editTeamMember .team_member_name');
    var position = $('#editTeamMember .team_member_position').val().trim();
    var biography = $('#editTeamMember textarea.team_member_description').val().trim();
    includeImages = true;
    if((titleField.length && titleField.val().trim().length < 1))
    {
        labAlert("Please fill in the name submitting the team member.");
        return;
    }
    
    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-team-member');
    }
    window.loadingIndicator($('body'));
    
    ajaxSubmit('/$editTeamMember/'+tmId , {
            'name': titleField.val().trim(),
            'position': position,
            'biography': biography,
            'imageData': imageData,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the service. " + error);
        }else{
            console.log("team member submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageTM.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageTM.set(imgs);
            }
            var msg = 'Team member submitted successfully.';
            labAlert(msg);
            window.location.href = window.location.href;
        }
    });
}


function manageAboutMe()
{
    var titleField = $('.aboutme_form_section .aboutme_name');
    var contentField = $('.aboutme_form_section textarea.aboutme_description');
    includeImages = true;
    if((titleField.length && titleField.val().trim().length < 1) || (contentField.length && contentField.val().trim().length < 1))
    {
        labAlert("Please fill in the title and description before submitting the data.");
        return;
    }
    
    var aboutMeId =  0;
    if($('#hdnAboutMeId').length > 0)
    {
        aMeId =  $('#hdnAboutMeId').val().trim();
        if(aMeId.length > 0 && aMeId > 0)
        {
            aboutMeId = aMeId;
        }
    }
    
    
    var imageData = null;
    if(includeImages) {
        console.log('asadasd');
        imageData = prepareImageData('upload-aboutme');
    }
    
    window.loadingIndicator($('body'));
    
    var url = '/$aboutMeAdd/';
    if(aboutMeId > 0)
    {
        url = '/$aboutMeEdit/';
    }
    
    ajaxSubmit(url+g_thisBrand.url , {
            'title': titleField.val().trim(),
            'content': contentField.val().trim(),
            'imageData': imageData,
            'aboutMeId': aboutMeId,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the data. " + error);
        }else{
            console.log("about me submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageAboutme.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageAboutme.set(imgs);
            }
            var msg = 'About me data submitted successfully.';
            labAlert(msg);
            window.location.href = window.location.href;
        }
    });
}

var videoEmbed = {
    invoke: function(){

        $('body').html(function(i, html) {
            return videoEmbed.convertVideo(html);
        });

    },
    convertVideo: function(html){
        var pattern1 = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
        var pattern2 = /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g;

        if(pattern1.test(html)){
            console.log("html", html);

           var replacement = '<iframe width="420" height="345" src="//player.vimeo.com/video/$1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

           var html = html.replace(pattern1, replacement);
        }else if(pattern2.test(html)){
              console.log("html", html);

           var replacement = '<iframe width="420" height="345" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>';

            var html = html.replace(pattern2, replacement);
        } 
        else
        {
            html = false;
        }


        return html;
    }
}

function adjuctBannerImage() {
    var $img = $('.intro figure .img-wrap img');
    var $container = $img.parent();
    $container.removeClass('oversized-img');
    var maxHeight = parseFloat($container.css('max-height'));

    if($img.outerHeight() > maxHeight) {
        $container.addClass('oversized-img');
    }else{
        $container.removeClass('oversized-img');
    }
}

function setupGalleries() {
    if(window.postGalleryQueue) {
        for(var k in window.postGalleryQueue) {
            if(window.postGalleryQueue.hasOwnProperty(k)) {
                var images = window.ClassyJSON.unjsonify(window.postGalleryQueue[k]);
                var imageSet = new window.ImageSet(images, '#' + k);
                delete window.postGalleryQueue[k];
            }
        }
    }
}

window.setInterval(setupGalleries, 250);

$(document).ready(function () {

    $(window).resize(adjuctBannerImage);

    setupGalleries();

    $('.dropdown-menu.click-noclose,.notf-drop,#notification').on('click', function (e) {
            e.stopPropagation();
    });

    $("#notification").click(function (e) {
        e.preventDefault();
        $(this).next(".notf-drop").toggle();
        $(this).removeClass("active");
    });
    $('html').click(function () {
        $(".notf-drop").hide();
    });
    $(".menu-edit .head").click(function () {
        $(this).closest("ul").toggleClass("open");
    });
    $(".categories a").click(function (e) {
        e.preventDefault();
        $(this).closest("li").addClass("active");
        $(this).closest("li").siblings().removeClass("active");
    });
    $(window).scroll(function () {
        if($('.sidebar').length > 0)
        {
            if ($(this).scrollTop() > $('.sidebar').offset().top) {
                $(".sidebar").addClass("fixed");

            } else {
                $(".sidebar").removeClass("fixed");
            }
        }
    });
    $(".new-story > ul > li > .icon-close").click(function () {
        $(this).closest("li").hide();
    });

    /*
    $("#add-quote, #quote-drop").click(function () {
        $(this).closest(".story-cont").removeClass("quote");
        $(this).closest(".story-cont").removeClass("story");
        $(this).closest(".story-cont").removeClass("ad-story");

        $(this).closest(".story-cont").addClass("quote");
    });
    $("#add-story, #story-drop").click(function () {
        $(this).closest(".story-cont").removeClass("quote");
        $(this).closest(".story-cont").removeClass("story");
        $(this).closest(".story-cont").removeClass("ad-story");

        $(this).closest(".story-cont").addClass("story");
    });
    $("#add-adv-story, #ad-story-drop").click(function () {
        $(this).closest(".story-cont").removeClass("quote");
        $(this).closest(".story-cont").removeClass("story");
        $(this).closest(".story-cont").removeClass("ad-story");

        $(this).closest(".story-cont").addClass("ad-story");
    });
    $(".new-story.head .icon-close").click(function (e) {
        e.preventDefault();
        $(this).closest(".story-cont").removeClass("quote");
        $(this).closest(".story-cont").removeClass("story");
        $(this).closest(".story-cont").removeClass("ad-story");

    });
    */
    $(".toolbar .expand").click(function () {
        $(this).closest(".toolbar").addClass("open");
    });

  /*  $(".new-story > ul > #add-quote").click(function () {
        $(this).closest("ul").siblings(".head").find("h4").text("Brand Quote");
    });*/

    $('#quote').on('keypress blur', function () {

        var textLength = $(this).val().length;

        if (textLength < 20) {
            $(this).css('font-size', '36px');
        } else if (textLength < 40) {
            $(this).css('font-size', '30px');
        } else {
            $(this).css('font-size', '24px');
        }

        //console.log(textLength);
    });
    $('#quote-input').on('keyup', 'textarea', function (e) {
        $(this).css('height', 'auto');
        $(this).height(this.scrollHeight);
    });
    $('#quote-input').find('textarea').keyup();


    $('#newStory').on('show.bs.modal', function () {
        var $container = $('.main-content');
        $('html,body').animate({
            scrollTop: $container.offset().top,
        }, 200);
    });

    $('[data-show-elements]').on('click', function() {
        $($(this).data('show-elements')).show('fast');
    });
    $('[data-hide-elements]').on('click', function() {
        $($(this).data('hide-elements')).not($($(this).data('show-elements'))).hide('fast');
    });

    $('[data-tab-target]').on('click', function(e) {
        e.preventDefault();

        var $this = $(this);
        var target = $this.data('tab-target');

        var $group = $this.closest('.tab-group');
        if(!$group.length) $group = null;

        $('[data-tab-id]', $group).removeClass('active');
        $('[data-tab-id='+target+']', $group).addClass('active');

        $('[data-tab-target]', $group).removeClass('active');
        $this.addClass('active');

    });

    $('[data-editor-indicator-id]').on('click', function(e) {
        e.preventDefault();

        var $this = $(this);
        var target = $this.data('editor-indicator-id');
        var $activeIcon = $('[data-editor-indicator-id].active');
        var currentEditor = false;
        if($activeIcon.length) {
            currentEditor = $activeIcon.data('editor-indicator-id');
        }
        var active = target != currentEditor && target != 'close';

        $('[data-editor-indicator-id]').removeClass('active');
        if(active) {
            function open() {
                $('[data-editor-indicator-id=' + target + ']').addClass('active');
                $('[data-editor-section-id=' + target + ']').addClass('active');
                $('.editor-content').show('fast');
                $this.addClass('active');
            }
            if(currentEditor) {
                $('.editor-content').hide('fast', function () {
                    $('[data-editor-section-id]').removeClass('active');
                    open();
                });
            }else{
                open();
            }
        }else{
            $('.editor-content').hide('fast', function () {
                $('[data-editor-section-id]').removeClass('active');
            });
        }

    });

});