<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Image Upload Frame</title>
    <script src="/js/jquery-1.11.3.min.js" ></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" ></script>
    <script src="/js/html5imageupload.js?v1.4.3" ></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/css/html5imageupload.css?v1.3" rel="stylesheet" />
    <style>
        body, html {
            font-size: 0;
            padding: 0;
            margin: 0;
            background: none transparent;
			width: 100vw;
			height: 100vh;
			overflow: hidden;
        }
		#upContainer {
			overflow: hidden;
			width: 90vw;
			height: 90vh;
			margin: 5%;
			width: calc(100% - 30px);
			height: calc(100% - 30px);
			margin: calc(15px);
			position: absolute;
		}
    </style>
</head>
<body>
    <div id="upContainer">
        <div id="imageUpload" class="dropzone tools" data-save="false" style="width: 100%; height: 200px;">
            <input type="file" name="thumb" />
        </div>
    </div>
    <script>
        $(function() {
            window.name = location.hash.replace(/#/, "");
            if(window.parent && window.parent.imageDialogOptions && window.parent.imageDialogOptions[window.name]) {
                var dialogOptions = window.parent.imageDialogOptions[window.name];
				var height = $('#upContainer').innerHeight(); //dialogOptions.height;
                $('#imageUpload').css({height: height + 'px'});
                $(".dropzone").html5imageupload({
                    originalsize: false,
                    onSave: function(imageData) {
                        dialogOptions.callback(imageData);
                    }
                });
            }else{
                console.warn("No imageDialogOptions for ", window.name);
            }
        });
    </script>
</body>
</html>

