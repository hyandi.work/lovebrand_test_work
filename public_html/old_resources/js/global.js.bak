/*
loadScript(path, callback[, error])
    Load script from /js/PATH.js, run callback on OK, or show alert with error, if error is given

window.loadingIndicator(el) -> fn()
    Attach loading indicator to a DOM or jQuery element.
    Returns a function which removes it.

class DialogComponent(id, type, param, additionalParam, additionalParam2)
    Type: row
        Param 1: Array of child elements
        Param 2: (optional) alignment (text-align value)

    Type: label
        Param 1: HTML text. (\n works as a newline)

    Type: spacer
        No params (empty label)

    Type: checkbox
        Param 1: label text
        Param 2: (optional) checked by default?

    Type: button AND button-single
        Param 1: label text
        Param 2: (optional) value passed to the callback
        Note: button-single disables itself on click

    Type: text AND password
        Param 1: label text
        Param 2: (optional) placeholder text

    Type: imageDropZone
        Param 1: X size in pixels
        Param 2: Y size in pixels
        Param 3: Callback

class Dialog(options, components, callback[, closeCallback])
    options: Placeholder for future options
    components: Array of DialogComponent objects
    callback(Dialog, DialogComponent|"close", [UserValue]):
        Fired when a button is pressed, or dialog is closed (only if closeCallback === true)
    closeCallback(Dialog, bool closedByUser):
        Function to be fired on close; or true - then button callback is called with "close" string as second argument

    show()
        shows the dialog

    reset()
        resets the dialog

    close()
        closes and removes the dialog

    getElement() -> DOM Element
        returns DOM element of the dialog root

    getComponentById(id) -> DialogComponent
        returns the component with given id

    disableButtons(disable?)
        disable buttons (or enable them: diableButtons(false))

Dialog needs .show() to work.

labAlert(text, title, onClose) -> Dialog
labConfirm(text, title, onClose[, buttonOkLabel[, buttonCancelLabel]]) -> Dialog
labPrompt(text, title, [onClose], valueLabel, [placeholder][, defaultValue[, buttonOkLabel[, buttonCancelLabel]]]) -> Dialog
labImage(title, callback, imageX, imageY) -> Dialog

These return a Dialog with .show() already called.

 */

/**
 * Unescapes a string (ie. &amp;lt; becomes &lt;)
 * @param string string to be unescaped
 * @returns string Unescaped string
 */
function htmlunescapespecialchars(string) {
    return string.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, "\"");
}

function htmlspecialchars(string) {
    return string.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;");
}

/**
 * @param then Unix timestamp
 * @returns string Formatted time string, eg. 5 minutes ago
 */
function formatAgoTime(then) {
    var now = Math.floor(new Date().getTime() / 1000);
    var d = new Date(then * 1000);
    var ago = now - then;

    if(ago < -60) { // date in the future more than a minute
        if(ago < -3600*24*2) {
            return d.toLocaleDateString();
        }else {
            return d.toLocaleDateString() + " " + d.toLocaleTimeString();
        }
    }else if(ago < 15) {
        return "just now";
    }else if(ago < 60) {
        return "under a minute ago";
    }else if(ago < 90) {
        return "a minute ago";
    }else if(ago < 3570) {
        return Math.round(ago / 60) + " minutes ago";
    }else if(ago < 3600*23.5) {
        return Math.round(ago / 3600) + " hours ago";
    }else if(ago < 3600*24*31) {
        return Math.round(ago / 3600 / 24) + " days ago";
    }else if(ago < 3600*24*300) {
        return d.getDay() + " " + d.toLocaleString('en-GB', {month: "short"});
    }else{
        return d.toLocaleDateString();
    }
}

/**
 * Makes the textarea auto-resize to fit its contents
 * @param $textareas textareas' selector
 * @param minHeight minimum height of the textarea in pixels (default 20)
 * @param maxHeight maximum height of the textarea in pixels (default unlimited)
*/
window.autosizeTextarea = function($textareas, minHeight, maxHeight) {
    minHeight = minHeight === void 0 ? 20 : minHeight;
    maxHeight = maxHeight === void 0 || maxHeight <= 0 ? 1e10 : maxHeight;

    $textareas.on('keyup', function() {
        var $this = $(this);
        var oldHeight = $this.height();
        $this.css('height', 'auto');
        var height = this.scrollHeight + 5;
        var limit = $(window).height()*.9 - $this.offset().top + $(window).scrollTop() - 80;
        if(limit < minHeight) limit = minHeight;
        if(height > limit) height = limit;

        $this.height(oldHeight);
        height = height < minHeight ? minHeight : (height > maxHeight ? maxHeight : height);
        $this.animate({height: height}, 50);
    });
    $textareas.keyup();
};

/**
 * Wrapper for sending ajax request
 * @param url The url
 * @param data Post data
 * @param callback Called back on successful fetch, first argument is the JSON data
 * @param failCallback Optional, if not given an alert will pop up on unsuccessful request. Otherwise this will get fired.
 */
window.ajaxSubmit = function(url, data, callback, failCallback) {
    $.post(url, data, "json")
        .done(callback)
        .fail(function( jqxhr, textStatus, error ) {
            var err = textStatus + ", " + error;
            console.log("ajaxSubmit failed (" + url + "): " + err);
            if(failCallback == void 0) {
                alert("Sorry, the service is currently unavailable. If the problem persists, please contact us.");
            }else{
                failCallback(textStatus, error);
            }
        });
};

/**
 * Wrapper for sending ajax request, with expected 'ok' in 'status' response field
 * @param url The url
 * @param data Post data
 * @param callback Called back on successful fetch, first argument is the JSON data
 * @param failCallbackOrMessage (optional) Message which will be displayed (%s will be substituted for the error,
 *                                         if available) or a callback to be fired.
 *
 * The fail callback will be called with (false, textStatus, error) on network/server error
 *  or (true, jsonObject containing status) if the status is not 'ok'
 */
window.ajaxSubmitCheck = function(url, data, callback, failCallbackOrMessage) {
    $.post(url, data, "json")
        .done(function(a, r, g, s) {
            if(!a || !a.status || a.status != 'ok') {
                console.warn('ajaxSubmitCheck('+url+') failed.', 'Request:', data, 'Response:', a);
                if(failCallbackOrMessage == void 0) {
                    labAlert(a.status, 'Error');
                }else if(typeof failCallbackOrMessage == 'string') {
                    labAlert(failCallbackOrMessage.replace(/%s/, a.status), 'Error');
                }else{
                    failCallback(true, a);
                }
            }else{
                callback(a, r, g, s);
            }
        })
        .fail(function( jqxhr, textStatus, error ) {
            var err = textStatus + ", " + error;
            //console.log("ajaxSubmit failed (", url, ",", jqxhr, "): " + err);
            if(failCallbackOrMessage == void 0) {
                labAlert("Sorry, the service is currently unavailable. If the problem persists, please contact us.");
            }else if(typeof failCallbackOrMessage == 'string') {
                labAlert(failCallbackOrMessage.replace(/%s/,''), 'Error');
            }else{
                failCallback(false, textStatus, error);
            }
        });
};


// Notification fetching
var notificationFetchCounter = 0;
var cachedTemplate = false;
window.getNotifications = function(templateType) {
    if(templateType === void 0) templateType = cachedTemplate;
    cachedTemplate = templateType;
    notificationFetchCounter++;

    // todo: fix here for the new layout
    var $notificationContainer = $('#notificationDropdownContent');
    var $notificationCounters = $('.notificationCounter');
    ajaxSubmit('/$getNotifications', [], function(data) {

        $notificationContainer.empty();

        var activeNotificationCount = 0;

        for(var k in data.notifications) {
            var n = data.notifications[k];
            var url = '/$manageComments/BRANDID';
            var seen = n.dateSeen > 0;

            var template = '';

            var href = url.replace('BRANDID', n.relatedBrand);

            if(templateType == 'feed-template') {
                template = '<li><a href="' + href + '" ' + (seen ? 'class="seen"' : '') + '><p class="pull-left"><em>' + n.text + '</em></p><p class="pull-right"><em>' + formatAgoTime(n.date) + '</em></p></a></li>';
            }else {
                template = '<a href="' + href + '" ' + (seen ? 'class="seen"' : '') + '><div> ' + n.text + ' </div><small> ' + formatAgoTime(n.date) + ' </small></a>';
            }
            $notificationContainer.append($.parseHTML(template));

            if(!seen)
                activeNotificationCount++;
        }



        if(templateType == 'feed-template') {
            if(activeNotificationCount < 1) {
                $notificationCounters.hide();
                if(data.notifications.length < 1) {
                    $notificationContainer.append($.parseHTML( '<li><a href="#"><p class="text-muted"><em>No new notifications</em></p></a></li>' ));
                }
            }else{
                $notificationCounters.each(function() {
                    var $this = $(this);
                    if($this.hasClass('parentheses')) {
                        $this.text('(' + activeNotificationCount + ')');
                    }else{
                        $this.text(activeNotificationCount);
                    }
                });
                $notificationCounters.show();
            }
        }else {
            if(activeNotificationCount < 1) {
                $('#notificationDropdownIconIndicator').addClass('inactive');
                if(data.notifications.length < 1) {
                    $notificationContainer.append($.parseHTML("<div style=\"text-align: center; padding: 20px 0;\">No new notifications</div>"));
                }
            }else{
                var el = $('#notificationDropdownIconIndicator');
                el.text(activeNotificationCount);
                el.removeClass('inactive');
            }

        }


        // Slow the updates down as the user spends more time on the page without refreshing
        // 10->14->17->23 sec
        window.setTimeout(getNotifications, ~~(1000 * Math.sqrt(notificationFetchCounter) * 10));
    }, function() { // on fail
        window.setTimeout(getNotifications, ~~(1000 * Math.sqrt(notificationFetchCounter) * 10));
    });
};

window.loadScript = function(path, callback, error) {
    var url = path;

    // Check if the path is not absolute
    if(!/^(?:[a-zA-Z]+:)?\/\/.+/.test(url) || url.indexOf('/') == 0) {
        url = '/js/' + url;
    }

    $.getScript( url )
        .done(function( script, textStatus ) {
            console.log( url + " loaded: " + textStatus );
            if(callback) callback(path);
        })
        .fail(function( jqxhr, settings, exception ) {
            console.warn("loadScript(" + url + ") failed!");
            if(error) {
                if(typeof(error) == 'string') {
                    window.labAlert(error, "Loading error");
                }else{
                    error(path);
                }
            }
        });
};


window.loadingIndicator = function(el) {
    var $el = el;
    if(!$el.jquery) $el = $(el);

    var sizeClass = '';

    if($el.innerWidth() < 40 || $el.innerHeight() < 40) sizeClass = 'small ';
    else if($el.innerWidth() > 150 || $el.innerHeight() > 150) sizeClass = 'big ';
    var $loadingEl = $.parseHTML('<div class="loading-indicator-background"><div class="'+sizeClass+'loading-indicator"><img src="/img/loading.gif" alt="Loading..." /></div></div>');

    if($el.css('position') == 'static') {
        $el.css('position', 'relative');
    }

    $el.append($loadingEl);

    return function() {
        $el.each(function(num, e) { e.removeChild($loadingEl[0]); });
    };
};

window._DialogComponentCounter = 1e10;
window._DialogComponentsById = {};
window.DialogComponent = function (id, type, param, additionalParam, additionalParam2) {
    this.id = id ? id : (window._DialogComponentCounter++);
    this.type = type;
    this.dialogInit = function (dialogId) {
        this.dialogId = dialogId;
        if(this.type == "row") {
            for(var k in this.childComponents) {
                this.childComponents[k].dialogInit(dialogId);
                window._DialogComponentsById[this.childComponents[k].getElementId()] = this.childComponents[k];
            }
        }
        window._DialogComponentsById[this.getElementId()] = this;
    };
    this.getElementId = function () {
        if(!this.dialogId || ! this.id) {
            console.warn("Tried to get dialog element ID with null dialog ID!");
            return null;
        }
        return "dialog-" + this.dialogId + "-" + this.id;
    };
    this.getElement = function () {
        return document.getElementById(this.getElementId());
    };
    switch (type) {
        case "row": {
            this.childComponents = param;
            this.align = additionalParam;
            this.toString = function() {
                var ret = '<div id="'+this.getElementId()+'" class="dialogRow" '+(this.align ? 'style="text-align: '+this.align+'"' : '')+'>';
                for(var k in this.childComponents) {
                    ret += this.childComponents[k].toString();
                }
                return ret + '</div>';
            };
            break;
        }
        case "label": {
            this.label = param.replace(/\n/g, "<br>");
            this.toString = function() {
                return '<div id="'+this.getElementId()+'" class="dialogLabel">' + this.label + '</div>';
            };
            break;
        }
        case "spacer": {
            this.toString = function() {
                return '<div id="'+this.getElementId()+'" class="dialogLabel"> </div>';
            };
            break;
        }
        case "checkbox": {
            this.label = param.replace(/\n/g, "<br>");
            this.checkedByDefault = additionalParam;
            this.toString = function() {
                return '<label><input type="checkbox" id="'+this.getElementId()+'" class="dialogCheckbox"' + (this.checkedByDefault ? ' checked' : '') + ' />' + this.label + '</label>';
            };
            break;
        }
        case "button": {
            this.label = param.replace(/\n/g, "<br>");
            this.callbackData = additionalParam;
            this.toString = function() {
                return '<button id="'+this.getElementId()+'" class="lab-button btn btn-info dialogButton">' + this.label + '</button>';
            };
            break;
        }
        case "button-single": {
            this.label = param.replace(/\n/g, "<br>");
            this.callbackData = additionalParam;
            this.toString = function() {
                return '<button id="'+this.getElementId()+'" class="lab-button btn btn-info dialogButton singleClick">' + this.label + '</button>';
            };
            break;
        }
        case "text": {
            this.label = param.replace(/\n/g, "<br>");
            this.placeholder = additionalParam;
            this.toString = function() {
                return (this.label ? ('<label>'+this.label+'</label> <br />') : '') +
                    '<input id="'+this.getElementId()+'" type="text" '+(this.placeholder ? ('placeholder="'+this.placeholder+'"') : '')+' />';
            };
            break;
        }
        case "password": {
            this.label = param.replace(/\n/g, "<br>");
            this.placeholder = additionalParam;
            this.toString = function() {
                return (this.label ? ('<label>'+this.label+'</label> <br />') : '') +
                    '<input id="'+this.getElementId()+'" type="password" '+(this.placeholder ? ('placeholder="'+this.placeholder+'"') : '')+' />';
            };
            break;
        }
        case "imageDropZone": {
            this.sizeX = param;
            this.sizeY = additionalParam;
            this.callback = additionalParam2;

            this.toString = function() {
                window.imageDialogOptions[this.getElementId()] = {
                    height: this.sizeY,
                    width: this.sizeX,
                    callback: this.callback
                };
                return '<div class="imageDialogIframeContainer" style="width: '+(this.sizeX)+'px; height: '+(this.sizeY)+'px; "><iframe src="/js/html5imageuploadiframe.php#'+this.getElementId()+'" allowtransparency="true" style="width: '+(this.sizeX)+'px; height: '+(this.sizeY)+'px; border: none" class="imageDialogIframe" id="'+this.getElementId()+'"></iframe></div>';
            };
            break;
        }
    }
};

window.imageDialogOptions = [];

window._DialogCounter = 1;
/**
 * @param options Placeholder for future options
 * @param components List of DialogComponent objects
 * @param callback Takes (Dialog, DialogComponent|"close", [UserValue]); Fired when a button is pressed, or dialog is closed (see closeCallback)
 * @param closeCallback Takes (Dialog, bool closedByUser). Function to be fired on close or true - then button callback is called with "close" string as second argument
 * @constructor
 */
window.Dialog = function(options, components, callback, closeCallback) {
    this.id = window._DialogCounter++;
    this.components = components;
    this.callback = callback;
    this.closeCallback = closeCallback;

    this.getElement = function() {
        return document.getElementById("dialogContainer-" + this.id);
    };
    this.getComponentById = function(id) {
        return window._DialogComponentsById["dialog-" + this.id + "-" + id];
    };

    this._closing = false;
    this.close = function(_closedByUser) {
        if(this._closing) return;
        this._closing = true;

        if(this.closeCallback) {
            if(this.closeCallback === true) {
                if(_closedByUser) {
                    this.callback(this, "close", _closedByUser);
                }
            }else{
                this.closeCallback(this, _closedByUser ? true : false);
            }
        }
        this.getElement().remove();
    };

    this.disableButtons = function(disabled) {
        if(disabled === void 0) {
            disabled = true;
        }
        $('.dialogButton', this.getElement()).each(function() {
            $(this).prop('disabled', disabled);
        });
    };

    this.reset = function(first) {
        this._closing = false;
        var dialog = this;
        var dialogRoot = this.getElement();

        if(!dialogRoot) {

            var html =
                '<div class="dialogBox-fullscreen grayedOut" id="dialogContainer-' + this.id + '">' +
                '<div class="dialogBox-wrapper">' +
                '<div class="dialogBox">' +
                '<a href="#" class="dialogBox-closeButton oi" data-glyph="x"></a>' +
                '<div id="dialogBoxContent" class="dialogBox-content form-container fullwidth"> </div>' +
                '</div></div></div>';

            $('#dialogBox-container').append($.parseHTML(html));
            dialogRoot = this.getElement();

            var $closeButton = $('.dialogBox-closeButton', dialogRoot);
            $closeButton.click(function () {
                dialog.close(true);
            });
        }

        if(!first) {
            console.log("Reset: " + this.id + " - ", dialogRoot);
        }

        html = '';

        for(k in this.components) {
            var c = this.components[k];
            html += '<div class="dialogBox-row">' + c.toString() + '</div>';
        }
        var $contents = $('.dialogBox-content', dialogRoot);
        $contents.html(html);

        $('.dialogButton', $contents).click(function() {
            var button = _DialogComponentsById[this.id];
            if(typeof(button.callbackData) == "function") {
                button.callbackData(dialog, button);
            }else{
                dialog.callback(dialog, button, button.callbackData);
            }
            if($(this).hasClass("singleClick")) {
                $(this).prop("disabled", true);
            }
        }); // todo: "ENTERBUTTON"
    };

    this.show = function() {
        var dialog = this;

        for(var k in this.components) {
            this.components[k].dialogInit(this.id);
        }

        this.reset(true);
    };

};

window.labAlert = function(text, title, onClose) {
    var d = new Dialog({}, [
        new DialogComponent(null, 'label', title !== void 0 ? title : "Notification"),
        new DialogComponent(null, 'spacer'),
        new DialogComponent(null, 'label', text),
        new DialogComponent(null, 'row', [
            new DialogComponent(null, 'button', "OK")
        ], 'right')
    ], function(dialog, button, callbackData) {
        if(onClose) {
            onClose();
        }
        dialog.close();
    }, true);
    d.show();
    return d;
};

window.labConfirm = function(text, title, onClose, buttonOkLabel, buttonCancelLabel) {
    if(typeof(title) != 'string') { buttonCancelLabel=buttonOkLabel; buttonOkLabel=onClose; onClose=title; title=text; text='';}
    var d = new Dialog({}, [
        new DialogComponent(null, 'label', title !== void 0 ? title : "Are you sure?"),
        new DialogComponent(null, 'spacer'),
        new DialogComponent(null, 'label', text),
        new DialogComponent(null, 'row', [
            new DialogComponent(null, 'button', buttonOkLabel === void 0 ? "OK" : buttonOkLabel, true),
            new DialogComponent(null, 'button', buttonCancelLabel === void 0 ? "Cancel" : buttonCancelLabel, false)
        ], 'right')
    ], function(dialog, button, callbackData) {
        if(onClose) {
            onClose(button != "close" ? callbackData : null);
        }
        dialog.close();
    }, true);
    d.show();
    return d;
};

window.labPrompt = function(text, title, onClose, valueLabel, placeholder, defaultValue, buttonOkLabel, buttonCancelLabel) {
    var buttons = [ new DialogComponent(null, 'button', buttonOkLabel === void 0 ? "OK" : buttonOkLabel, true) ];
    if(buttonCancelLabel !== false) {
        buttons.push(new DialogComponent(null, 'button', buttonCancelLabel === void 0 ? "Cancel" : buttonCancelLabel, false));
    }
    var components = [
        new DialogComponent(null, 'label', title ? title : text),
        new DialogComponent(null, 'spacer'),
    ];
    if(title) {
        components.push(new DialogComponent(null, 'label', text));
    }
    components.push(new DialogComponent('input', 'text', valueLabel, placeholder));
    components.push(new DialogComponent(null, 'row', buttons, 'right'));


    var d = new Dialog({}, components, function(dialog, button, callbackData) {
        if(onClose) {
            if(button == "close") {
                onClose(null);
            }else if(!callbackData) {
                onClose(false);
            }else{
                onClose(dialog.getComponentById('input').getElement().value);
            }
        }
        dialog.close();
    }, true);
    d.show();
    if(defaultValue !== null && defaultValue !== void 0) {
        d.getComponentById('input').getElement().value = defaultValue;
    }
    return d;
};

window.labImage = function(title, callback, imageX, imageY) {
    var d = new Dialog({}, [
        new DialogComponent(null, 'label', title !== void 0 ? title : "Image"),
        new DialogComponent(null, 'spacer'),
        new DialogComponent(null, 'imageDropZone', imageX, imageY, function(imageData) {
            if(callback) {
                callback(imageData);
            }
            d.close();
        }),
        new DialogComponent(null, 'row', [
            new DialogComponent(null, 'button', "Cancel", false)
        ], 'right')
    ], function(dialog, button, callbackData) {
        if(callback) {
            callback(false);
        }
        dialog.close();
    }, true);
    d.show();
    return d;
};

windowAlert = window.alert;
window.alert = function(a, b) {
    console.error("Use labAlert() instead of alert()!");
    windowAlert(a, b);
};

windowConfirm = window.confirm;
window.confirm = function(a, b) {
    console.error("Use labConfirm() instead of confirm()!");
    return windowConfirm(a, b);
};

windowPrompt = window.prompt;
window.prompt = function(a, b) {
    console.error("Use labPrompt() instead of prompt()!");
    return windowPrompt(a, b);
};

// Canvas for resizing
var resizeCanvas = null;
function resizeImageFromElement(img, callback) {
    // img is an <img> element

    if(!resizeCanvas) {
        resizeCanvas = document.createElement('canvas');
        if(!resizeCanvas) {
            canvasError();
            return;
        }
    }
    var ctx;

    // Resize if necessary and possible
    if(img.width > IMG_MAX_SIZE || img.height > IMG_MAX_SIZE) {

        var width = img.width;
        var height = img.height;

        // Check if a lot of resizing is needed
        // (workaround for poor scaling algo in browsers)
        var firstResize = true;
        if(width > IMG_MAX_SIZE*2 || height > IMG_MAX_SIZE*2) {
            height = ~~(height * 0.5);
            width = ~~(width * 0.5);
            var tempCanvas = document.createElement('canvas');
            tempCanvas.height = height;
            tempCanvas.width = width;
            $('body').prepend(tempCanvas);
            var tempCtx = tempCanvas.getContext('2d');
            if(!tempCtx) {
                canvasError();
                return;
            }
            tempCtx.drawImage(img, 0, 0, width, height);

            // halve the resolution in each iteration
            while (width > IMG_MAX_SIZE * 2 || height > IMG_MAX_SIZE * 2) {
                height = ~~(height * 0.5);
                width = ~~(width * 0.5);

                tempCtx.drawImage(tempCanvas, 0, 0, width*2, height*2, 0, 0, width, height);
                firstResize = false;
            }
            var tempWidth = width,
                tempHeight = height;

            if (width > height) {
                height = ~~(height * IMG_MAX_SIZE / width);
                width = IMG_MAX_SIZE;
            }else{
                width = ~~(width * IMG_MAX_SIZE / height);
                height = IMG_MAX_SIZE;
            }
            resizeCanvas.width = width;
            resizeCanvas.height = height;

            ctx = resizeCanvas.getContext("2d");
            ctx.drawImage(tempCanvas, 0, 0, tempWidth, tempHeight, 0, 0, width, height);
            tempCanvas.remove();
        }else{
            if (width > height) {
                height = ~~(height * IMG_MAX_SIZE / width);
                width = IMG_MAX_SIZE;
            }else{
                width = ~~(width * IMG_MAX_SIZE / height);
                height = IMG_MAX_SIZE;
            }
            resizeCanvas.width = width;
            resizeCanvas.height = height;

            ctx = resizeCanvas.getContext("2d");
            if(!ctx) {
                canvasError();
                return;
            }
            ctx.drawImage(img, 0, 0, width, height);
        }

    }else{
        resizeCanvas.width = img.width;
        resizeCanvas.height = img.height;

        ctx = resizeCanvas.getContext("2d");
        if(!ctx) {
            canvasError();
            return;
        }
        ctx.drawImage(img, 0, 0, img.width, img.height);
    }

    var dataUrl = resizeCanvas.toDataURL('image/jpeg', 0.9);
    img.datastr = dataUrl.split('base64,', 2)[1];
    img.onload = function() {
        callback(img);
    };
    img.src = dataUrl;

}

window.ClassyJSON = {
    jsonify: function(obj) {
        return JSON.stringify(obj, function(k, value) {
            if (value && typeof value.jsonify === 'function') {
                return value.jsonify();
            }
            return value;
        })
    },
    unjsonify: function(str) {
        if(typeof str != 'string') {
            console.warn("unjsonify called with ", str);
            return void 0;
        }
        try {
			console.log(str);
            return JSON.parse(str, function (key, value) {
                if (value && value.type) {
                    if (window[value.type] && window[value.type].prototype && window[value.type].prototype.unjsonify) {
                        return window[value.type].prototype.unjsonify(value);
                    } else {
                        console.warn("Unjsonify unknown class");
                    }
                }
                return value;
            });
        }catch(ex) {
            console.error("Couldn't unjsonify: ", str, ex);
        }


    }
};


window.persistentVariable = function(name, defaultValue) {
    var self = this;
    this.value = defaultValue;

    if(window.localStorage && window.localStorage.getItem) {
        var storageValue = window.localStorage.getItem("persistent_" + name);
        if(storageValue !== null && storageValue) {
            try {
                this.value = window.ClassyJSON.unjsonify(storageValue);
				if(this.value === void 0) this.value = defaultValue;
            }catch (ex) {
                console.error("Couldn't parse saved value for", name, ' data: ', storageValue, ex);
            }
        }
    }

    return {
        value: value,
        name: name,
        get: function() {return self.value;},
        set: function(x) {
            self.value = x;
            if (window.localStorage && window.localStorage.setItem) {
				var val = window.ClassyJSON.jsonify(self.value);
				console.log("saving", val);
                window.localStorage.setItem("persistent_" + this.name, val);
            }
        }
    }
};



function GalleryRenderer() {
    this.elCount = 0;
    this.html = '';
    var colNames = ["zero", "one", "two", "three", "four"];

    this.addImage = function (image, maxPerRow) {
        this.elCount++;

        if (this.elCount == 1) {
            this.html = "</div>" + this.html;
        }

        this.html = '<div class="img-container col-'+colNames[this.elCount]+'"><img src="' + image.url + '" /><div class="img-panel new"></div></div>' + this.html;

        if (this.elCount == maxPerRow) {
            this.html = '<div class="img-row row-of-'+colNames[this.elCount]+'">' + this.html;
            this.elCount = 0;
        }
    };

    this.getHTML = function() {
        if (this.elCount != 0) {
            this.html = '<div class="img-row row-of-'+colNames[this.elCount]+'">' + this.html;
            this.elCount = 0;
        }
        return this.html;
    };
}

window.ImageSet = function (imageList, imageListSelector) {
    var self = this;
    this.images = [];
    this.imageListSelector = imageListSelector;


	if(!imageList) imageList = [];
    imageList = imageList.filter(function(item) {return item;});

    this.jsonify = function () {
        return {type: 'ImageSet', images: this.images, imageListSelector: this.imageListSelector};
    };

    this.imagesChanged = function () {
        var $lists = $('.image-list').filter(self.imageListSelector);

        var renderer = new GalleryRenderer();

        for(var i = self.images.length-1; i >= 0; i--) {
            var img = self.images[i];
            renderer.addImage(img, 2);
        }

        console.log("imagesChanged ====");
        var html = renderer.getHTML();

        $(self.imageListSelector).html(html);

    };

    var onSingleLoaded = function() {
        for(var k in self.images) {
            if(!self.images.hasOwnProperty(k))
                continue;
            var img = self.images[k];
            if(!img.loaded) return;
        }
        // not returned - all loaded
        self.imagesChanged();
    };

    this.addImages = function(imageList) {
        for(var k in imageList) {
            if(!imageList.hasOwnProperty(k))
                continue;
            var img = imageList[k];
            if(!img)
                continue;

            if(typeof img == "string") {
                img = new window.CachedImage(img);
            }
            img.onload = onSingleLoaded;
            self.images.push(img);
        }
        onSingleLoaded();
    };

    this.removeImages = function(imageList) {
        imageList = imageList.map(function(item) {return ((typeof item) != "string") ? item.url : item;});
        self.images = self.images.filter(function(item) {return  imageList.indexOf(item.url) === -1;});
        self.imagesChanged();
    };


    this.addImages(imageList);

    // Recheck to make sure nothing got loaded before we attached the callback
    onSingleLoaded();
};
window.ImageSet.prototype.unjsonify = function(object) {
    return new window.ImageSet(object.images, object.imageListSelector);
};

window.CachedImage = function(url, rotation, dimensions) {
    var self = this;
    this.url = url;
    this.preloader = document.createElement("IMG");

    this.width = 0;
    this.height = 0;
    // once loaded contains {x: 123, y: 123}
    this.dimensions = dimensions === void 0 ? null : dimensions;

    this.loaded = false;

    this.onload = false;

    this.preloader.onload = function() {
        self.width = self.preloader.naturalWidth;
        self.height = self.preloader.naturalHeight;
        self.dimensions = {
            x: self.width,
            y: self.height,
            width: self.width,
            height: self.height
        };
        self.loaded = true;
        if(self.onload) self.onload(self.preloader);
    };
    this.preloader.src = url;
    this.preloader.style.display = "none";
    document.body.appendChild(this.preloader);
    this.rotation = rotation === void 0 ? 0 : rotation;

    this.jsonify = function () {
        return {type: 'CachedImage', url: this.url, rotation: this.rotation, dimensions: self.dimensions};
    };
};
window.CachedImage.prototype.unjsonify = function(object) {
    return new window.CachedImage(object.url, object.rotation, object.dimensions);
};



function runGlobalJSHooks() {
    if(window.onGloabalJS) {
        for(var k in window.onGloabalJS) {
            try {
                if(window.onGloabalJS[k] !== false) {
                    window.onGloabalJS[k]();
                    window.onGloabalJS[k] = false;
                }
            }catch(e) {
                console.error("An error occured while running onGlobalJS hook", k, e);
            }
        }
    }
}

runGlobalJSHooks();
setTimeout(runGlobalJSHooks, 100);
setTimeout(runGlobalJSHooks, 500);
setTimeout(runGlobalJSHooks, 1500);
setTimeout(runGlobalJSHooks, 3000);
setTimeout(runGlobalJSHooks, 8000);

console.log("global.js loaded");