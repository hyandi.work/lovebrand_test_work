var _$comment_btn;
var _$aside_panel;
var _$content;
var aside_panel_showing = false;
var $container_viewer;
var adminBrand = (window.g_user ? window.g_user.id == window.g_brand.owner : false);

    /** SLIDER JS **/
    function getSliderPostDataStory(postId, currentIndex)
    {
        var postTitle  = $('#image-modal #photoTitle_'+currentIndex).val();
        $('#image-modal .fs-aside-title h4').html(postTitle);
        $('#image-modal .fs-aside-description').html($('#image-modal #photoCaption_'+currentIndex).val());
        $('#image-modal .comment_header .fs-time-ago small').html(formatAgoTime($('#image-modal #photoDateStamp_'+currentIndex).val()));
        var commentsCnt = ($('#image-modal #photoCommenstCnt_'+currentIndex).val() > 0) ? $('#image-modal #photoCommenstCnt_'+currentIndex).val() : '';
        console.log('Comment count: ' + commentsCnt);
        $('#image-modal .fs_modal_header .fs_slider_comments_count span').html(commentsCnt);
        $('#image-modal .fs_modal_header .fs_slider_comments_count .fs-comment-btn').attr('data-post-id', postId);
        $('#image-modal .comment-btn-contaiber .btn-comment-submit').attr('data-post-id', postId);

        var love = $('#image-modal #photoLove_'+currentIndex).val();
        var loveCnt = $('#image-modal #photoLoveCnt_'+currentIndex).val();

        var dispCnt = (loveCnt > 0) ? loveCnt : '';

        var loveTitle = 'Love';
        var loved = 1;
        var colorCls = '';
        if(love == 1)
        {
            loveTitle = 'Loved!';
            loved = 0;
            colorCls = 'color-blue';
        }

        var loveHTML = '<a href="javascript:void(0);" class="text-info '+colorCls+'" data-loved="'+loved+'" onclick="javascript: makeLovedPopup(this, '+postId+');">'+
            '<i class="fa fa-heart" aria-hidden="true"></i>'+
            loveTitle+
            '<span>'+dispCnt+'</span>'+
            '</a>';
        $('#image-modal .fs_modal_header .fs_slider_love_conainer').html(loveHTML);
        $('#image-modal .sharing_content').attr('data-post-id', postId);

        loadCommentsPopup(postId, 'all');
    }

    function getSliderPostData(postId, currentIndex)
    {
        var postTitle  = $('#photoTitle_'+currentIndex).val();
        $('#image-modal-full .fs-aside-title h4').html(postTitle);

        $('#image-modal-full .fs-aside-description').html($('#photoCaption_'+currentIndex).val());
        $('#image-modal-full .comment_header .fs-time-ago small').html(formatAgoTime($('#photoDateStamp_'+currentIndex).val()));
        var commentsCnt = ($('#photoCommenstCnt_'+currentIndex).val() > 0) ? $('#photoCommenstCnt_'+currentIndex).val() : '';
        $('#image-modal-full .fs_modal_header .fs_slider_comments_count span').html(commentsCnt);
        $('#image-modal-full .fs_modal_header .fs_slider_comments_count .fs-comment-btn').attr('data-post-id', postId);
        $('#image-modal-full .comment-btn-contaiber .btn-comment-submit').attr('data-post-id', postId);

        var love = $('#photoLove_'+currentIndex).val();
        var loveCnt = $('#photoLoveCnt_'+currentIndex).val();

        var dispCnt = (loveCnt > 0) ? loveCnt : '';

        var loveTitle = 'Love';
        var loved = 1;
        if(love == 1)
        {
            loveTitle = 'Loved!';
            loved = 0;
        }

        var loveHTML = '<a href="javascript:void(0);" class="text-info" data-loved="'+loved+'" onclick="javascript: makeLovedPopup(this, '+postId+');">'+
            '<i class="fa fa-heart" aria-hidden="true"></i>'+
            loveTitle+
            '<span>'+dispCnt+'</span>'+
            '</a>';
        $('#image-modal-full .fs_modal_header .fs_slider_love_conainer').html(loveHTML);
        loadCommentsPopup(postId, 'all');
    }

    function socialShareModule(type)
    {
        var activeSlide = $('#image-modal-full #fs-photo-slider div.active');
        var share = activeSlide.find('img').attr('src');

        if(type == 'email')
        {
            window.location.href = "mailto:?subject=Share with your friends&body="+share;
        }
        else
        {
            socialShare(type, share, '', '', '', 520, 350);
        }
    }

    function socialShareModuleStory(type, obj)
    {
        //sharing_content
        var postId = $(obj).parent('li').parent('ul').parent('.social_sharing').parent('.sharing_content').attr('data-post-id');
        var activeSlide = $('#image-modal #fs-photo-slider div.active');
        var share = activeSlide.find('img').attr('src');

        if(type == 'email')
        {
            window.location.href = "mailto:?subject=Share with your friends&body="+share;
        }
        else
        {
            socialShare(type, share, '', '', '', 520, 350);
        }
    }

    function loadCommentsPopup(postId, limit) {

        var $commentSection = $('.fs-slider .aside_panel .comments_container .comments-box');
        var $commentFrom = $('.fs-slider .aside_panel .comments_container .comments-form');
        $commentFrom.find('.previous_comments').hide();
        var loadingHTML = '<div class="no-comments-loading">Loading...</div>';
        $commentSection.children('ol').html(loadingHTML);
        var html = '';

        if(postId > 0)
        {
            window.ajaxSubmitCheck("/$getComments/"+postId, {"limit": limit}, function (data) {

            loadedComments = data.comments;
            totalComments = data.totalComments;

            /*loadedComments.sort(function (a, b) {
             return b.datePosted - a.datePosted;
             });*/
            loadedComments.sort(function (a, b) {
                return a.id - b.id;
            });

            for(var k in loadedComments) {
                if(!loadedComments.hasOwnProperty(k)) {continue;}
                var c = loadedComments[k];

                var author = htmlspecialchars(c.name);
                var authorAvatar = c.userAvatar;
                var buttons = '';
            // todo: User can edit
            if(adminBrand) {
                    if(c.public) {
                        buttons = '<a class="pull-right" href="#" onclick="commentButtonPopup(\'delete\', '+c.id+', '+postId+'); return false;"><i class="icon-close"></i></a>   ';
                    }else{
                        buttons = '<a class="pull-right" href="#" onclick="commentButtonPopup(\'delete\', '+c.id+', '+postId+'); return false;"><i class="icon-close"></i> Reject</a>   '+
                            '<a class="pull-right" href="#" onclick="commentButtonPopup(\'approve\', '+c.id+', '+postId+'); return false;"><i class="icon-checkmark"></i> Approve</a>';

                    }

                 }
                html = getCommentsHTML(c, html, true);
            }

            $commentSection.children('ol').html(html);
            $commentSection.children('ol').show();

            if(totalComments > 3 && limit != "all")
            {
                $commentFrom.find('.previous_comments').show();
            }
            else
            {
                $commentFrom.find('.previous_comments').hide();
            }

            /*            calculateDates();*/
            if($commentSection.children('.comments').length == 0)
            {
                //$commentSection.hide();
                _$content.find('.aside_panel .comments_container .comments-form .comments-form-footer .comment-text span').html(0);
            }
            else
            {

                _$content.find('.aside_panel .comments_container .comments-form .comments-form-footer .comment-text span').html(totalComments);
                $commentSection.show();
            }
                _$content.find('.fs_slider_comments_count span').html(totalComments);


        }, function(serverside, message) {
            console.log("Comment loading error", serverside, message);
            $commentSection.html('<i>Comments couldn\'t be loaded. '+message+'</i>');
        });
        }
    }

    function commentButtonPopup(action, commentId, postId, index) {
        var el = document.getElementById("post-comment-" + commentId);
        if(!el) {
            console.warn("commentButton called on nonexistent comment id!", commentId);
            return false;
        }
        var actionFn = commentActionsPopup[action];
        if(!actionFn) {
            console.warn("commentButton called with unknown action!", action);
            return false;
        }
        var $el = $(el);
        return actionFn($el, commentId, postId, index);
    }

    function makeLovedPopup(obj, postId)
    {
        var loved = $(obj).data('loved');

        var url = "/$brandPostSetLove/"+postId+"/"+loved;

        ajaxSubmitCheck(url, {}, function (data) {

            if(data.status == 'ok')
            {
                var loveCnt = (data.loveCount > 0) ? '('+data.loveCount+')' : '';
                var dispCnt = (data.loveCount > 0) ? data.loveCount : '';
                var loveTitle = 'Love';
                var data_loved = 1;
                if(data.is_love)
                {
                    /*$(obj).children('span').text('Loved! '+loveCnt);
                     $(obj).data('loved', 0);
                     $(obj).children('i').removeClass('fa-heart-o').addClass('fa-heart');*/
                    loveTitle = 'Loved!';
                    data_loved = 0;
                }
                else
                {
                    /*$(obj).children('span').text('Love '+loveCnt);
                     $(obj).data('loved', 1);
                     $(obj).children('i').removeClass('fa-heart').addClass('fa-heart-o');*/
                }
                var loveHTML = '<a href="javascript:void(0);" class="text-info" data-loved="'+data_loved+'" onclick="javascript: makeLovedPopup(this, '+postId+');">'+
                    '<i class="fa fa-heart" aria-hidden="true"></i>'+
                    loveTitle+
                    '<span>'+dispCnt+'</span>'+
                    '</a>';
                $('.fs_slider_love_conainer').html(loveHTML);
            }
            else
            {
                labAlert(data.status);
            }
        }, 'Sorry, an error has occured. \n%s');
    }

    window.popupSubmitComments = function(obj)
    {
        var postID = $(obj).attr('data-post-id');

        var url = "/$postComment/"+postID;

        //var postAs = $('#commentPostAs').val();
        //if (!postAs) postAs = 'user';
        var postAs = 'user';
        var $post = $('#image-modal');
        $commentForm = $('.comments-form', $post);

        /*        var $commentSection = $('.aside_panel .comments_container .comments-form .comments-textarea');*/
        var $commentSection = $commentInput = $commentForm.find('.comments-textarea .comment-box-text');

        /*        var $commentInput = $commentSection.find('textarea.comment-box-text');*/
        var postContent = $commentSection.val().trim();

        var $comment_id = $commentForm.find('.comment_id');
        var postCommentID = $comment_id.val().trim();
        /*console.log($commentInput);*/
        if(postContent.length < 1) {
            $commentSection.focus();
            return;
        }

        $commentInput.val('');

        ajaxSubmitCheck(url, {
            text: postContent,
            comment_id: postCommentID,
            postAs: postAs

        }, function (data) {
            $commentInput.val('');
            /*                $commentInput.val('Thank you!\n\nYour comment has been submitted and will be reviewed by an administrator.');*/
            $commentInput.css({backgroundColor: '#e9f7e8', color: '#555'});
            $commentInput.keyup(); // for autosize

            $commentInput.one( "click", function() {
                var $this = $(this);
                $this.val('');
                $this.css({backgroundColor: 'white', color: '#111'});
            });
            $(obj).html("COMMENT");
            $comment_id.val("");
            loadCommentsPopup(postID, 'all');
        }, 'Sorry, an error has occured while posting your comment. \n%s');
    };

    function deleteCommentPopup(commentID, postId)
    {
        labConfirm('Are you sure that you want to delete this comment?', 'Are you sure?', function(response) {
            if(response) {
                window.ajaxSubmitCheck(fCommentURL(commentID, 'rejectComment'), {}, function() {
                    loadCommentsPopup(postId, 'all');
                }, "The comment couldn't be hidden. %s");
            }
        }, "Delete");
    };

    function editCommentPopup(commentId, postId)
    {
        var $post = $('#image-modal');
        $commentForm = $('.comments-form', $post);
        $comment = $('.comment-section ol', $post);
        $prvComment = $('.previous_comments', $post);
        /*    alert($commentForm.length);*/
        window.ajaxSubmitCheck("/$getComments/"+postId, {'commentId' : commentId}, function (data) {
        loadedComment = data.comments;
        //console.log(loadedComment);

        $commentForm.find('.comments-textarea .comment-box-text').val(loadedComment.content);
        $commentForm.find('.comments-textarea .comment_id').val(loadedComment.id);
        $commentForm.find('.comment-box-text').focus();
        $commentForm.find('.btn-comment-submit').html('Edit');
        $comment.slideUp(g_animationSpeed);
        $prvComment.hide();

        },function(serverside, message) {
            console.log("Comment loading error", serverside, message);
            $commentSection.html('<i>Comments couldn\'t be loaded. '+message+'</i>');
        });
    }
    (function(){

        window.loadOnPage = function(postId)
        {
            var cont = $('#image-modal');
            var data = $('details#brand-post-data-'+postId).html();
            if(data){
                var elem = null;
                data = JSON.parse(data);
                cont.find('.comments_container .btn-comment-submit').attr('data-post-id', data.id);
                cont.find('.modal-brand-avatar').html(data.brand.avatar);
                cont.find('.header-title-container .title').html(data.brand.avatar);
                elem = cont.find('.modal-category');
                elem .attr('data-target-segment', data.brand.segment);
                elem.attr('href', elem.attr('data-href').replace(/segmentOrId/g, data.brand.segment) );
                elem.attr('href', elem.attr('href').replace(/param1/g, data.brand.segParam) );
                elem.attr('data-segment-param', data.brand.segParam);
                elem.find('.category').html(data.brand.postType);
                elem = cont.find('.modal-brand');
                elem.attr( 'href', elem.attr('data-href') );
                elem.find('.pagename').html(data.brand.name);
                cont.find('.header-title-container .title').html(data.postTitle);
                cont.find('.carousel-inner').html(data.sliderImages);
                cont.find('#carousel-bounding-box .list-inline').html(data.littleSlider);
                cont.find('.description_container .title').html(data.postTitle);
                cont.find('.description_container .dec').html(data.postContent);
                if(data.price) {
                    cont.find('.buynow_container .product-price').html("Price: " + data.price);
                }else{
                    cont.find('.buynow_container .product-price').html('');
                }
                if(data.buyNow !== '' && data.buyNow !== '0') {
                    cont.find('.buynow_container .buynow_btn').css('display', 'table-cell');
                    cont.find('.buynow_container .buynow_btn').attr('href', data.buyNow);
                }else{
                    cont.find('.buynow_container .buynow_btn').css('display', 'none');
                }
                if(data.commentsCnt && data.commentsCnt > 3){
                    cont.find('.previous_comments').show();
                }else{
                    cont.find('.previous_comments').hide();
                }
                cont.find('.previous_comments a').on('click', function(){
                    loadCommentsPopup(postId, 'all');
                });

                cont.html(data.content);

                cont.on('show.bs.modal', function (e) {
                    var currentSlidePostId = $('.fs-slider #fs-photo-slider div.active').data('post-id');
                    var currentIndex = $('.fs-slider #fs-photo-slider div.active').data('index');
                    getSliderPostDataStory(currentSlidePostId, currentIndex);
                });

                cont.find('.fs-slider #fs-photo-slider').carousel();

                cont.find('.fs-slider #fs-photo-slider').on('slid.bs.carousel', function () {
                    var currentSlidePostId = $('.fs-slider #fs-photo-slider div.active').data('post-id');
                    var currentIndex = $('.fs-slider #fs-photo-slider div.active').data('index');
                    getSliderPostDataStory(currentSlidePostId, currentIndex);
                });

                $container_viewer = $('.fs-slider');
                _$comment_btn = $container_viewer.find(".fs-comment-btn");
                _$aside_panel = $container_viewer.find(".aside_panel");
                _$content = $container_viewer.find(".fs-container");

                cont.find('.littleP:first').css('border', '1px solid white');
                cont.find('.littleP').click(function(){
                    cont.find('.littleP').css('border', '0');
                    $(this).css('border', '1px solid white');
                    var r= $(this).attr("data-slider-item");
                    var post_id= $(this).attr("data-post-id");
                    $('#image-modal .item.active').removeClass('active');
                    $('#image-modal .item[data-slide-number="'+r+'"]').addClass('active');
                });

                cont.find('.littleP').mouseover(function () {
                    $(this).css('width','85px');
                    $(this).css('height','65px');
                });
                cont.find('.littleP').mouseout(function () {
                    $(this).css('width','80px');
                    $(this).css('height','60px');
                });

                if(postId > 0)
                {
                    loadCommentsPopup(postId);
                }

                cont.modal('toggle');
            }
            shortText();
        };
    })();
function fs_comment_box(obj)
{
    _toggleCommentPanel(obj);
}

function _toggleCommentPanel(obj) {
    if (aside_panel_showing) {
        _hideCommentPanel()
    } else {
        _showCommentPanel();
        var postid = $(obj).attr('data-post-id');
        loadCommentsPopup(postid, 'all');
    }
}
function _showCommentPanel() {
    _$comment_btn.addClass("active");
    _$content.find('.panel_comments').addClass("show");
    aside_panel_showing = true;
    window.setTimeout(function() {
        //_$file_comment_input.focus()
    }, 250)
}
function _hideCommentPanel() {
    _$comment_btn.removeClass("active");
    _$content.find('.panel_comments').removeClass("show");
    aside_panel_showing = false;
    window.setTimeout(function() {
        _$comment_btn.blur()
    }, 250)
}
function getCommentsHTML(c, html, isPopup)
{
    html += '<li id="post-comment-'+c.id+'" class="comment-entity comment '+(c.public?'':'not-approved')+'">';
    html += '<div class="content">';
    html += '<div class="comment-body">';
    var iniCls = '';
    var imgHtml = '';
    if(c.userAvatar == '')
    {
        iniCls = 'initial-image';
        imgHtml = '<span>'+c.userInitails+'</span>';
    }
    else
    {
        imgHtml = '<img src="'+c.userAvatar+'" alt="'+htmlspecialchars(c.name)+'">';
    }
    html += '<a class="image xsmall-image '+iniCls+'">';
    html += imgHtml;
    html += '</a>';
    html += '<div class="header ">';
    html += '<div data-block="meta" class="meta '+(!(adminBrand) ? 'nohover': '')+'">  ';
    //html += '<span class="timestamp">'+c.datePosted+'</span>';
    html += '<span data-timestamp="'+c.date+'" data-timestamp-format="short"></span>';
    html += '</div>';

    if(adminBrand){
        html += '<div class="meta-actions dropdown">';
        html += '<a aria-expanded="true" aria-haspopup="true" role="button" href="#" data-toggle="dropdown" class="dropdown-toggle">';
        html += '<i class="fa fa-ellipsis-h" aria-hidden="true"></i>';
        html += '</a>';
        html += '<ul class="dropdown-menu aboutPopup">';
        html += '<div class="aboutPop">';
        html += '<ul>';
        html += '<li>';
        var onclickEdit = "commentButton('edit', "+c.id+"); return false;";
        var onclickDelete = "commentButton('delete', "+c.id+"); return false;";
        if(isPopup)
        {
            onclickEdit = 'editCommentPopup('+c.id+', '+c.postId+'); return false;';
            onclickDelete = 'deleteCommentPopup('+c.id+', '+c.postId+'); return false;';
        }
        html += '<a onclick="'+onclickEdit+'" href="javascript:void(0);">';
        html += '<img src="/images/pen.png"><span>Edit</span>';
        html += '</a>';
        html += '</li>';
        html += '<li>';
        html += '<a href="javascript:void(0);" onclick="'+onclickDelete+'">';
        html += '<img src="/images/trash.png"><span>Delete</span>';
        html += '</a>';
        html += '</li>';
        html += '</ul>';
        html += '</div>';
        html += '</ul>';
        html += '</div>';
    }

    html += '<h4 class="sub-headline">';
    html += '<a class="name" href="javascript:void(0);">'+htmlspecialchars(c.name)+'</a>';
    html += '</h4>';
    html += '</div>';

    html += '<div class="text-entity">';
    html += '<p>'+htmlspecialchars(c.text)+'</p>';
    html += '</div>';

    html += '</div>';
    html += '</div>';
    html += '</li>';

    return html;
}
    /*** END **/



    /*** Shortened Text  **/
function shortText() {
    var showChar = 100;
    var ellipsestext = "...";
    var moretext = "more";
    var lesstext = "less";
    var content = $('#showText').html();

    if(content.length > showChar) {
        var c = content.substr(0, showChar);
        var h = content.substr(showChar-1, content.length - showChar);
        var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

        $('#showText').html(html);
    }

    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle(400);
        $(this).prev().toggle(400);
        return false;
    });
}