window.registerShowHideHooks = function () {
    $("#hide").click(function (event) {
        $("#hideit").hide();
    });
    $("#hide2").click(function (event) {
        $("#hideit2").hide();
    });
    $("#hide3").click(function (event) {
        $("#hideit3").hide();
    });
    $("#hidealbum").click(function (event) {
        $("#hidethealbum").hide();
    });
    $("#hidealbumagain").click(function (event) {
        $("#addAlbum").hide();
    });
    $("#category1").click(function (event) {
        $("#categoryCover").hide();
        $("#categoryTop").hide();
        $("#addCategory2").show();
    });
    $("#prikazi").click(function (event) {
        $("#addCategory2").hide();
        $('#collectionCover').show();
    });
    $("#prikazi2").click(function (event) {
        $("#addCategory2").hide();
        $('#collectionCover').show();
    });
    $("#hideRemoveAble").click(function (event) {
        $("#removeAble").hide();
    });
    $("#hideRemoveAble1").click(function (event) {
        $("#removeAble1").hide();
    });
    $("#hideRemoveAble2").click(function (event) {
        $("#removeAble2").hide();
    });
    $("#showAllRemoves").click(function (event) {
        $("#removeAble").show();
        $("#removeAble1").show();
        $("#removeAble2").show();
    });
    $("#addNewCategory").click(function (event) {
        $("#newitem").show();
        $("#addCategory2").hide();
        $("#categoryTop").show();

    });
    $("#showCover").click(function (event) {
        $("#infoblock").show();
        $("#collectionCover").hide();

    });
    $("#hideRemoveAble4").click(function (event) {
        $("#removeAble4").hide();
    });
    $("#hideRemoveAble5").click(function (event) {
        $("#removeAble5").hide();
    });
    $("#hideRemoveAble6").click(function (event) {
        $("#removeAble6").hide();
    });
    $("#showAllRemoves1").click(function (event) {
        $("#removeAble4").show();
        $("#removeAble5").show();
        $("#removeAble6").show();
    });
    $("#prikazi3").click(function (event) {
        $("#infoblock").hide();
    });
    $("#prikaziCovere").click(function (event) {
        $("#collectionCover").show();
        $("#infoblock").hide();
    });
    $("#prikaziCovere2").click(function (event) {
        $("#collectionCover").show();
        $("#infoblock").hide();
    });
    $("#getNewProducts").click(function (event) {
        $("#old-products").hide();
        $("#infoblock").hide();
        $("#new-products").show();
    });
    $("#newproductapp").click(function (event) {
        $("#photo-upload").show();
    });
    $("#closePhotoUpload").click(function (event) {
        $("#photo-upload").hide();
    });
    /* hide title and description */
    $("#hideTitle").click(function (event) {
        $("#title").hide();
        // $("#hideTitle").hide();
        // $("#showTitle").show();
    });
    $("#hideDescription").click(function (event) {
        // $("#description").hide();
        // $("#hideDescription").hide();
        // $("#showDescription").show();
    });
    $("#showTitle").click(function (event) {
        $("#showTitle").hide();
        $("#title").show();
        $("#hideTitle").show();
    });
    $("#showDescription").click(function (event) {
        $("#showDescription").hide();
        $("#description").show();
        $("#hideDescription").show();
    });
    /* upload & add member */
    $("#add-member").click(function (event) {
        $(".upload-member").hide();

        $('.upload-image-list-team-member img').each(function () {
            var src = $(this).attr('src');
            if (src.trim() != "") {
                removeImage(src, 'team-member');
            }
        });

        $("#upload-member").show();
        $('html,body').animate({
                scrollTop: $('#upload-member').offset().top
            },
            'slow');
    });
    $("#hide-upload").click(function (event) {
        $("#upload-member").hide();
    });
    $("#exit-team").click(function (event) {
        $("#closeMember").hide();
        $("#team").show();
    });
    $("#showMember").click(function (event) {
        $("#closeMember").show();
        $("#team").hide();
    });
};

$(document).ready(window.registerShowHideHooks);
(window.onFeedJS || (window.onFeedJS = [])).push(function () {
    window.feed.onSegmentChanged.push(window.registerShowHideHooks);
});
