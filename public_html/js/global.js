/*
loadScript(path, callback[, error])
    Load script from /js/PATH.js, run callback on OK, or show alert with error, if error is given

window.loadingIndicator(el) -> fn()
    Attach loading indicator to a DOM or jQuery element.
    Returns a function which removes it.

class DialogComponent(id, type, param, additionalParam, additionalParam2)
    Type: row
        Param 1: Array of child elements
        Param 2: (optional) alignment (text-align value)

    Type: label
        Param 1: HTML text. (\n works as a newline)

    Type: spacer
        No params (empty label)

    Type: checkbox
        Param 1: label text
        Param 2: (optional) checked by default?

    Type: button AND button-single
        Param 1: label text
        Param 2: (optional) value passed to the callback
        Note: button-single disables itself on click

    Type: text AND password
        Param 1: label text
        Param 2: (optional) placeholder text

    Type: imageDropZone
        Param 1: X size in pixels
        Param 2: Y size in pixels
        Param 3: Callback

class Dialog(options, components, callback[, closeCallback])
    options: Placeholder for future options
    components: Array of DialogComponent objects
    callback(Dialog, DialogComponent|"close", [UserValue]):
        Fired when a button is pressed, or dialog is closed (only if closeCallback === true)
    closeCallback(Dialog, bool closedByUser):
        Function to be fired on close; or true - then button callback is called with "close" string as second argument

    show()
        shows the dialog

    reset()
        resets the dialog

    close()
        closes and removes the dialog

    getElement() -> DOM Element
        returns DOM element of the dialog root

    getComponentById(id) -> DialogComponent
        returns the component with given id

    disableButtons(disable?)
        disable buttons (or enable them: diableButtons(false))

Dialog needs .show() to work.

labAlert(text, title, onClose) -> Dialog
labConfirm(text, title, onClose[, buttonOkLabel[, buttonCancelLabel]]) -> Dialog
labPrompt(text, title, [onClose], valueLabel, [placeholder][, defaultValue[, buttonOkLabel[, buttonCancelLabel]]]) -> Dialog
labImage(title, callback, imageX, imageY) -> Dialog

These return a Dialog with .show() already called.

 */

window.lab = window.lab ? window.lab : [];

/**
 * Unescapes a string (ie. &amp;lt; becomes &lt;)
 * @param string string to be unescaped
 * @returns string Unescaped string
 */
function htmlunescapespecialchars(string) {
    return string.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&quot;/g, "\"");
}

function htmlspecialchars(string) {
    return string.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;");
}
/**
 * @param timestamp int Unix timestamp of event in past
 * @param outputFormat string optional "default" or "short"
 * @returns string Formatted time string, eg. '5 minutes ago'.
 *      For outputFormat 'short' returns for eg. '5m'.
 */
function formatAgoTime(timestamp, outputFormat) {
    var now = new Date();
    var then = new Date(timestamp * 1000);

    var nowTimestamp = Math.floor(now.getTime() / 1000);
    var ago = nowTimestamp - timestamp;

    var short = !outputFormat || outputFormat != "long";

    if(short) {
        if(ago < -60) { // date in the future more than a minute
            if(ago < -3600*24*2) {
                return then.toLocaleDateString();
            }else {
                return then.toLocaleDateString() + " " + then.toLocaleTimeString();
            }
        }else if(ago < 45) {
            return "now";
        }else if(ago < 90) {
            return "1m";
        }else if(ago < 3570) {
            return Math.round(ago / 60) + "m";
        }else if(ago < 3600*23.5) {
            return Math.round(ago / 3600) + "h";
        }else if(ago < 3600*24*31) {
            return Math.round(ago / 3600 / 24) + "d";
        }else if(now.getFullYear() == then.getFullYear()) {
            return then.getDay() + " " + then.toLocaleString('en-GB', {month: "short"});
        }else{
            return then.getDay() + " " + then.toLocaleString('en-GB', {month: "short"}) + " " + then.toLocaleString('en-GB', {year: "2-digit"});
        }
    }else{
        if (ago < -60) { // date in the future more than a minute
            if (ago < -3600 * 24 * 2) {
                return then.toLocaleDateString();
            } else {
                return then.toLocaleDateString() + " " + then.toLocaleTimeString();
            }
        } else if (ago < 15) {
            return "just now";
        } else if (ago < 60) {
            return "under a minute ago";
        } else if (ago < 90) {
            return "a minute ago";
        } else if (ago < 3570) {
            return Math.round(ago / 60) + " minutes ago";
        } else if (ago < 3600 * 23.5) {
            return Math.round(ago / 3600) + " hours ago";
        } else if (ago < 3600 * 24 * 31) {
            return Math.round(ago / 3600 / 24) + " days ago";
        }else if(now.getFullYear() == then.getFullYear()) {
            return then.getDay() + " " + then.toLocaleString('en-GB', {month: "short"});
        } else {
            return then.getDay() + " " + then.toLocaleString('en-GB', {month: "short"}) + " " + then.toLocaleString('en-GB', {year: "2-digit"});
        }
    }
}

/**
 * Makes the textarea auto-resize to fit its contents
 * @param $textareas textareas' selector
 * @param minHeight minimum height of the textarea in pixels (default 20)
 * @param maxHeight maximum height of the textarea in pixels (default unlimited)
*/
window.autosizeTextarea = function($textareas, minHeight, maxHeight) {
    minHeight = minHeight === void 0 ? 20 : minHeight;
    maxHeight = maxHeight === void 0 || maxHeight <= 0 ? 1e10 : maxHeight;

    $textareas.on('keyup', function() {
        var $this = $(this);
        var oldHeight = $this.height();
        $this.css('height', 'auto');
        var height = this.scrollHeight + 5;
        var limit = $(window).height()*.9 - $this.offset().top + $(window).scrollTop() - 80;
        if(limit < minHeight) limit = minHeight;
        if(height > limit) height = limit;

        $this.height(oldHeight);
        height = height < minHeight ? minHeight : (height > maxHeight ? maxHeight : height);
        $this.animate({height: height}, 50);
    });
    $textareas.keyup();
};

/**
 * Wrapper for sending ajax request
 * @param url The url
 * @param data Post data
 * @param callback Called back on successful fetch, first argument is the JSON data
 * @param failCallback Optional, if not given an alert will pop up on unsuccessful request. Otherwise this will get fired.
 */
window.ajaxSubmit = function(url, data, callback, failCallbackOrMessage) {
    $.post(url, data, "json")
        .done(callback)
        .fail(function( jqxhr, textStatus, error ) {
            var err = textStatus + ", " + error;
            console.log("ajaxSubmit failed (" + url + "): " + err);
            if(failCallbackOrMessage == void 0) {
                alert("Sorry, the service is currently unavailable. If the problem persists, please contact us.");
            }else if(typeof failCallbackOrMessage == 'string') {
                alert(failCallbackOrMessage.replace(/%s/, a.status), 'Error');
            }else{
                failCallbackOrMessage(textStatus, error);
            }
        });
};

/**
 * Wrapper for sending ajax request, with expected 'ok' in 'status' response field
 * @param url The url
 * @param data Post data
 * @param callback Called back on successful fetch, first argument is the JSON data
 * @param failCallbackOrMessage (optional) Message which will be displayed (%s will be substituted for the error,
 *                                         if available) or a callback to be fired.
 *
 * The fail callback will be called with (false, textStatus, error) on network/server error
 *  or (true, jsonObject containing status) if the status is not 'ok'
 */
window.ajaxSubmitCheck = function(url, data, callback, failCallbackOrMessage) {
    $.post(url, data, "json")
        .done(function(a, r, g, s) {
            if(!a || !a.status || a.status != 'ok') {
                console.warn('ajaxSubmitCheck('+url+') failed.', 'Request:', data, 'Response:', a);
                if(failCallbackOrMessage == void 0) {
                    labAlert(a.status, 'Error');
                }else if(typeof failCallbackOrMessage == 'string') {
                    labAlert(failCallbackOrMessage.replace(/%s/, a.status), 'Error');
                }else{
                    failCallbackOrMessage(true, a);
                }
            }else{
                callback(a, r, g, s);
            }
        })
        .fail(function( jqxhr, textStatus, error ) {
            var err = textStatus + ", " + error;
            //console.log("ajaxSubmit failed (", url, ",", jqxhr, "): " + err);
            if(failCallbackOrMessage == void 0) {
                labAlert("Sorry, the service is currently unavailable. If the problem persists, please contact us.");
            }else if(typeof failCallbackOrMessage == 'string') {
                labAlert(failCallbackOrMessage.replace(/%s/,''), 'Error');
            }else{
                failCallbackOrMessage(false, textStatus, error);
            }
        });
};


// Notification fetching
var notificationFetchCounter = 0;
window.getNotifications = function() {
    notificationFetchCounter++;

    var $notificationContainer = $('.notificationList');
    var $notificationCounters = $('.unreadNotificationCounter');
    var $unreadNotificationsIndicator = $('.unreadNotificationsIndicator');
    var $readAllMessages = $('.notifications .allMessages');
    var $notificationIcon = $('.notification-icon');

    ajaxSubmit('/$getNotifications', [], function(data) {
        if(data.brandRelation){
            $('.top-bar .red-dot').removeClass('hidden');
        }else{
            $('.top-bar .red-dot').addClass('hidden');
        }
        if(!data.notifications) {
            window.setTimeout(getNotifications, ~~(1000 * Math.sqrt(notificationFetchCounter) * 20));
            return;
        }

        $notificationContainer.empty();

        var activeNotificationCount = 0;
        for(var k in data.notifications) {
            var n = data.notifications[k];
            var url = n.link ? n.link : '#'; // todo: change from link to gray text if no url
            var seen = n.dateSeen;

            var template = '<li><a href="@URL"><span class="notification-content">@TEXT</span><span class="notSpan">@TIME</span></a></li>';

            template = template.replace('@URL', url);
            template = template.replace('@TEXT', n.text);
            template = template.replace('@TIME', formatAgoTime(n.date));

            $notificationContainer.append($.parseHTML(template));

            if(!seen) {
                activeNotificationCount++;
            }
        }


        if(activeNotificationCount < 1) {
            $unreadNotificationsIndicator.hide();
            if(data.notifications.length < 1) {
                $notificationContainer.append($.parseHTML('<p>No new notifications</p>'));
            }
            $readAllMessages.text('View Inbox');;
            $notificationIcon.attr('src', '/images/nomail.png');
        }else{
            $unreadNotificationsIndicator.show();
            $notificationCounters.text(activeNotificationCount);
            $readAllMessages.text('Read all messages')
            $notificationIcon.attr('src', '/images/gotmail.png');
        }


        // Slow the updates down as the user spends more time on the page without refreshing
        // 10->14->17->23 sec
        window.setTimeout(getNotifications, ~~(1000 * Math.sqrt(notificationFetchCounter) * 10));
    }, function() { // on fail
        $notificationContainer.empty();
        $notificationContainer.append($.parseHTML('<p>Could not fetch notifications.</p>'));
        $readAllMessages.text('');
        $notificationIcon.attr('src', '/images/nomail.png');
        window.setTimeout(getNotifications, ~~(1000 * Math.sqrt(notificationFetchCounter) * 10));
    });
};
window.setTimeout(window.getNotifications, 1000);

window.loadScript = function(path, callback, error) {
    var url = path;

    // Check if the path is not absolute
    if(!/^(?:[a-zA-Z]+:)?\/\/.+/.test(url) || url.indexOf('/') == 0) {
        url = '/js/' + url;
    }

    $.getScript( url )
        .done(function( script, textStatus ) {
            console.log( url + " loaded: " + textStatus );
            if(callback) callback(path);
        })
        .fail(function( jqxhr, settings, exception ) {
            console.warn("loadScript(" + url + ") failed!");
            if(error) {
                if(typeof(error) == 'string') {
                    window.labAlert(error, "Loading error");
                }else{
                    error(path);
                }
            }
        });
};
window.loadStyle = function(path, callback, error) {
    var url = path;

    // Check if the path is not absolute
    if(!/^(?:[a-zA-Z]+:)?\/\/.+/.test(url) || url.indexOf('/') == 0) {
        url = '/css/' + url;
    }
    var style = document.createElement('style');
    if (document.createStyleSheet){
        document.createStyleSheet(url);
        if(callback) {
            // assume okay - only on older IE
            setTimeout(function() {callback(path);}, 1);
        }
    } else {
        $.ajax(url, {
            url: url,
            dataType: 'text',
            success: function(data) {
                $("<style/>").html(data).appendTo("head");
                console.log( url );
                if(callback) callback(path);
            },
            error: function () {
                console.warn("loadStyle(" + url + ") failed!");
                if(error) {
                    if(typeof(error) == 'string') {
                        window.labAlert(error, "Loading error");
                    }else{
                        error(path);
                    }
                }
            }
        });
    }
};

var pageScrollingStopped = false;

function checkResumePageScrolling(ignoredElement) {
    if(!pageScrollingStopped) return;

    var stoppers = document.getElementsByClassName("stop-page-scrolling");
    if(!stoppers.length || (stoppers.length === 1 && stoppers[0] === ignoredElement)) {
        $('body').removeClass("page-scrolling-stopped");
        pageScrollingStopped = false;
    }
}
window.setInterval(checkResumePageScrolling, 200);

window.stopScollingUntilElementDies = function($element) {
    if($element && $element.length) {
        $element.on("remove", function (a) {
            console.log("REMOVE", this, a);
            checkResumePageScrolling(this);
        });
        $element.addClass('stop-page-scrolling');
        if(!pageScrollingStopped) {
            $('body').addClass("page-scrolling-stopped");
            pageScrollingStopped = true;
        }
    }else{
        console.warn('stopScollingUntilElementDies failed', $element);
    }
};

window.centerElementOn = function ($subject, $target) {

};

window.loadingIndicator = function(el) {
    var $el = el;
    if(!$el.jquery) $el = $(el);

    var sizeClass = '';

    if($el.innerWidth() < 40 || $el.innerHeight() < 40) sizeClass = 'small ';
    else if($el.innerWidth() > 150 || $el.innerHeight() > 150) sizeClass = 'big ';
    var $loadingEl = $.parseHTML('<div class="loading-indicator-background"><div class="'+sizeClass+'loading-indicator"><img src="/images/loading.gif" alt="Loading..." /></div></div>');

    if($el.css('position') == 'static') {
        $el.css('position', 'relative');
    }

    $el.append($loadingEl);

    return function() {
        $el.each(function(num, e) { e.removeChild($loadingEl[0]); });
    };
};

window._DialogComponentCounter = 1e10;
window._DialogComponentsById = {};
window.DialogComponent = function (id, type, param, additionalParam, additionalParam2) {
    this.id = id ? id : (window._DialogComponentCounter++);
    this.type = type;
    this.dialogInit = function (dialogId) {
        this.dialogId = dialogId;
        if(this.type == "row") {
            for(var k in this.childComponents) {
                this.childComponents[k].dialogInit(dialogId);
                window._DialogComponentsById[this.childComponents[k].getElementId()] = this.childComponents[k];
            }
        }
        window._DialogComponentsById[this.getElementId()] = this;
    };
    this.getElementId = function () {
        if(!this.dialogId || ! this.id) {
            console.warn("Tried to get dialog element ID with null dialog ID!");
            return null;
        }
        return "dialog-" + this.dialogId + "-" + this.id;
    };
    this.getElement = function () {
        return document.getElementById(this.getElementId());
    };
    switch (type) {
        case "row": {
            if(param){
                this.childComponents = param;
            }else{
                this.childComponents = [];
            }
            this.align = additionalParam;
            this.addChild = function (elem){
                if(elem){
                    this.childComponents.push(elem);
                }
            };
            this.toString = function() {
                var ret = '<div id="'+this.getElementId()+'" class="dialogRow" '+(this.align ? 'style="text-align: '+this.align+'"' : '')+'>';
                for(var k in this.childComponents) {
                    ret += this.childComponents[k].toString();
                }
                return ret + '</div>';
            };
            break;
        }
        case "label": {
            this.label = param.replace(/\n/g, "<br>");
            this.toString = function() {
                return '<div id="'+this.getElementId()+'" class="dialogLabel" style="'+additionalParam+'">' + this.label + '</div>';
            };
            break;
        }
        case "spacer": {
            this.toString = function() {
                return '<div id="'+this.getElementId()+'" class="dialogLabel"> </div>';
            };
            break;
        }
        case "checkbox": {
            this.label = param.replace(/\n/g, "<br>");
            this.checkedByDefault = additionalParam;
            this.toString = function() {
                return '<label><input type="checkbox" id="'+this.getElementId()+'" class="dialogCheckbox"' + (this.checkedByDefault ? ' checked' : '') + ' />' + this.label + '</label>';
            };
            break;
        }
        case "button": {
            this.label = param.replace(/\n/g, "<br>");
            this.callbackData = additionalParam;
            this.toString = function() {
                return '<button id="'+this.getElementId()+'" class="lab-button btn btn-info dialogButton">' + this.label + '</button>';
            };
            break;
        }
        case "button-single": {
            this.label = param.replace(/\n/g, "<br>");
            this.callbackData = additionalParam;
            this.toString = function() {
                return '<button id="'+this.getElementId()+'" class="lab-button btn btn-info dialogButton singleClick">' + this.label + '</button>';
            };
            break;
        }
        case "text": {
            this.label = param.replace(/\n/g, "<br>");
            this.placeholder = additionalParam;
            this.toString = function() {
                return (this.label ? ('<label>'+this.label+'</label> <br />') : '') +
                    '<input id="'+this.getElementId()+'" type="text" '+(this.placeholder ? ('placeholder="'+this.placeholder+'"') : '')+' />';
            };
            break;
        }
        case "password": {
            this.label = param.replace(/\n/g, "<br>");
            this.placeholder = additionalParam;
            this.toString = function() {
                return (this.label ? ('<label>'+this.label+'</label> <br />') : '') +
                    '<input id="'+this.getElementId()+'" type="password" '+(this.placeholder ? ('placeholder="'+this.placeholder+'"') : '')+' />';
            };
            break;
        }
        case "imageDropZone": {
            this.sizeX = param;
            this.sizeY = additionalParam;
            this.callback = additionalParam2;

            this.toString = function() {
                window.imageDialogOptions[this.getElementId()] = {
                    height: this.sizeY,
                    width: this.sizeX,
                    callback: this.callback
                };
                return '<div class="imageDialogIframeContainer" style="width: '+(this.sizeX)+'px; height: '+(this.sizeY)+'px; "><iframe src="/js/html5imageuploadiframe.php#'+this.getElementId()+'" allowtransparency="true" style="width: '+(this.sizeX)+'px; height: '+(this.sizeY)+'px; border: none" class="imageDialogIframe" id="'+this.getElementId()+'"></iframe></div>';
            };
            break;
        }
    }
};

window.imageDialogOptions = [];

window._DialogCounter = 1;
/**
 * @param options Placeholder for future options
 * @param components List of DialogComponent objects
 * @param callback Takes (Dialog, DialogComponent|"close", [UserValue]); Fired when a button is pressed, or dialog is closed (see closeCallback)
 * @param closeCallback Takes (Dialog, bool closedByUser). Function to be fired on close or true - then button callback is called with "close" string as second argument
 * @constructor
 */
window.Dialog = function(options, components, callback, closeCallback) {
    this.id = window._DialogCounter++;
    this.components = components;
    this.callback = callback;
    this.closeCallback = closeCallback;

    this.getElement = function() {
        return document.getElementById("dialogContainer-" + this.id);
    };
    this.getComponentById = function(id) {
        return window._DialogComponentsById["dialog-" + this.id + "-" + id];
    };

    this._closing = false;
    this.close = function(_closedByUser) {
        if(this._closing) return;
        this._closing = true;

        if(this.closeCallback) {
            if(this.closeCallback === true) {
                if(_closedByUser) {
                    this.callback(this, "close", _closedByUser);
                }
            }else{
                this.closeCallback(this, _closedByUser ? true : false);
            }
        }
        $(this.getElement()).remove();
    };

    this.disableButtons = function(disabled) {
        if(disabled === void 0) {
            disabled = true;
        }
        $('.dialogButton', this.getElement()).each(function() {
            $(this).prop('disabled', disabled);
        });
    };

    this.reset = function(first) {
        this._closing = false;
        var dialog = this;
        var dialogRoot = this.getElement();

        if(!dialogRoot) {

            var html =
                '<div class="dialogBox-fullscreen grayedOut" id="dialogContainer-' + this.id + '">' +
                '<div class="dialogBox-wrapper">' +
                '<div class="dialogBox">' +
                '<a href="#" class="dialogBox-closeButton oi" data-glyph="x"></a>' +
                '<div id="dialogBoxContent" class="dialogBox-content form-container fullwidth"> </div>' +
                '</div></div></div>';

            $('#dialogBox-container').append($.parseHTML(html));
            dialogRoot = this.getElement();

            var $closeButton = $('.dialogBox-closeButton', dialogRoot);
            $closeButton.click(function () {
                dialog.close(true);
            });
        }

        if(!first) {
            console.log("Reset: " + this.id + " - ", dialogRoot);
        }

        html = '';

        for(k in this.components) {
            var c = this.components[k];
            html += '<div class="dialogBox-row">' + c.toString() + '</div>';
        }
        var $contents = $('.dialogBox-content', dialogRoot);
        $contents.html(html);

        $('.dialogButton', $contents).click(function() {
            var button = _DialogComponentsById[this.id];
            if(typeof(button.callbackData) == "function") {
                button.callbackData(dialog, button);
            }else{
                dialog.callback(dialog, button, button.callbackData);
            }
            if($(this).hasClass("singleClick")) {
                $(this).prop("disabled", true);
            }
        }); // todo: "ENTERBUTTON"
    };

    this.show = function() {
        var dialog = this;

        for(var k in this.components) {
            this.components[k].dialogInit(this.id);
        }

        this.reset(true);

        window.stopScollingUntilElementDies($(this.getElement()));
    };

};

window.labAlert = function(text, title, onClose) {
    var d = new Dialog({}, [
        new DialogComponent(null, 'label', title !== void 0 ? title : "Notification"),
        new DialogComponent(null, 'spacer'),
        new DialogComponent(null, 'label', text),
        new DialogComponent(null, 'row', [
            new DialogComponent(null, 'button', "OK")
        ], 'right')
    ], function(dialog, button, callbackData) {
        if(onClose) {
            onClose();
        }
        dialog.close();
    }, true);
    d.show();
    return d;
};

window.labConfirm = function(text, title, onClose, buttonOkLabel, buttonCancelLabel) {
    if(typeof(title) != 'string') { buttonCancelLabel=buttonOkLabel; buttonOkLabel=onClose; onClose=title; title=text; text='';}
    var d = new Dialog({}, [
        new DialogComponent(null, 'label', title !== void 0 ? title : "Are you sure?"),
        new DialogComponent(null, 'spacer'),
        new DialogComponent(null, 'label', text),
        new DialogComponent(null, 'row', [
            new DialogComponent(null, 'button', buttonOkLabel === void 0 ? "OK" : buttonOkLabel, true),
            new DialogComponent(null, 'button', buttonCancelLabel === void 0 ? "Cancel" : buttonCancelLabel, false)
        ], 'right')
    ], function(dialog, button, callbackData) {
        if(onClose) {
            onClose(button != "close" ? callbackData : null);
        }
        dialog.close();
    }, true);
    d.show();
    return d;
};

window.labPrompt = function(text, title, onClose, valueLabel, placeholder, defaultValue, buttonOkLabel, buttonCancelLabel) {
    var buttons = [ new DialogComponent(null, 'button', buttonOkLabel === void 0 ? "OK" : buttonOkLabel, true) ];
    if(buttonCancelLabel !== false) {
        buttons.push(new DialogComponent(null, 'button', buttonCancelLabel === void 0 ? "Cancel" : buttonCancelLabel, false));
    }
    var components = [
        new DialogComponent(null, 'label', title ? title : text),
        new DialogComponent(null, 'spacer'),
    ];
    if(title) {
        components.push(new DialogComponent(null, 'label', text));
    }
    components.push(new DialogComponent('input', 'text', valueLabel, placeholder));
    components.push(new DialogComponent(null, 'row', buttons, 'right'));


    var d = new Dialog({}, components, function(dialog, button, callbackData) {
        if(onClose) {
            if(button == "close") {
                onClose(null);
            }else if(!callbackData) {
                onClose(false);
            }else{
                onClose(dialog.getComponentById('input').getElement().value);
            }
        }
        dialog.close();
    }, true);
    d.show();
    if(defaultValue !== null && defaultValue !== void 0) {
        d.getComponentById('input').getElement().value = defaultValue;
    }
    return d;
};

/*
window.labImage = function(title, callback, imageX, imageY) {
    var d = new Dialog({}, [
        new DialogComponent(null, 'label', title !== void 0 ? title : "Image"),
        new DialogComponent(null, 'spacer'),
        new DialogComponent(null, 'imageDropZone', imageX, imageY, function(imageData) {
            if(callback) {
                callback(imageData);
            }
            d.close();
        }),
        new DialogComponent(null, 'row', [
            new DialogComponent(null, 'button', "Cancel", false)
        ], 'right')
    ], function(dialog, button, callbackData) {
        if(callback) {
            callback(false);
        }
        dialog.close();
    }, true);
    d.show();
    return d;
};*/
/* Cropping image upload form */
var slimLoaded = false;
function prepareImageUploadAndRun(fn) {
    if(slimLoaded) {
        fn();
    }else{
        slimLoaded = true;
        loadStyle('slim.css');
        loadScript('slim.jquery.js', fn, "Could not load image upload form. Please refresh and try again later or contact us.");
    }
}

var cloneCanvasRotated = function(original, angle) {
    if (!original) {
        return null;
    }

    var duplicate = document.createElement('canvas');
    duplicate.setAttribute('data-file', original.getAttribute('data-file'));

    var ctx = duplicate.getContext('2d');

    angle = Math.round(angle/90) % 4;
    if(angle % 2 == 1) {
        duplicate.width = original.height;
        duplicate.height = original.width;
    }else{
        duplicate.width = original.width;
        duplicate.height = original.height;
    }

    ctx.clearRect(0, 0, duplicate.width, duplicate.height);
    ctx.save();
    ctx.translate(duplicate.width/2, duplicate.height/2);
    ctx.rotate(angle*Math.PI/2);
    ctx.drawImage(original, -original.width/2, -original.height/2);
    ctx.restore();
    return duplicate;
};

function injectImageEditPreview(uploadUrlOrSaveCallback, element, maxX, maxY, ratioIsFreeOrCallback, callback) {
    var ratioIsFree;
    if(callback === void 0) {
        ratioIsFree = false;
        callback = ratioIsFreeOrCallback;
    }else{
        ratioIsFree = ratioIsFreeOrCallback;
    }
    var url, saveCallback;
    if(typeof uploadUrlOrSaveCallback !== 'function') {
        url = uploadUrlOrSaveCallback;
    }else{
        saveCallback = uploadUrlOrSaveCallback;
    }


    var ratio = ratioIsFree ? 'free' : ((maxX > 0 && maxY > 0) ? Math.floor(maxX) + ':' + Math.floor(maxY) : 'free');

    // if maxX is not a number/null '>' will return false
    if(!(maxX > 0)) {
        maxX = maxY;
    } else if(!(maxY > 0)) {
        maxY = maxX;
    }

    if(!(maxX > 0)) {
        maxX = maxY = 2048;
    }

    prepareImageUploadAndRun(function () {
        var didUpload;
        if(url) {
            didUpload = function(error, data, response) {
                //console.log(error, data, response);
                if(callback) {
                    if (error) {
                        callback(false, error);
                    } else {
                        callback(true, data, response);
                    }
                }
            };
        }

        var slim = $(element).slim({
            ratio: ratio,
            instantEdit: true,
            buttonRemoveLabel: 'Rotate',
            buttonRemoveTitle: 'Rotate 90 degrees clockwise',
            buttonRemoveClassName: 'slim-btn-rotate',
            post: 'output',
            //push: true,
            service: url,
            didUpload: didUpload,
            didSave: function(data) {
                saveCallback.apply(this, [data.output]);
            },
            willRemove: function (data, ready) {
                var rotation = (data.meta && data.meta.rotation !== void 0) ? data.meta.rotation : 0;

                (data.meta || (data.meta = {})).rotation = (rotation + 90) % 360;

                if(data.actions && data.actions.crop) {
                    var crop = data.actions.crop;
                    this.crop({x: crop.x, y: crop.y, width: crop.width, height: crop.height}, function (result) {
                        //console.log(result)
                    });
                }else{
                    this.crop({x: 0, y: 0, width: data.output.width, height: data.output.height}, function (result) {
                        //console.log(result)
                    });
                }

            },
            willTransform: function (data, ready) {
                var rotation = (data.meta && data.meta.rotation !== void 0) ? data.meta.rotation : 0;

                //console.log("TRANSFORM: ", rotation, this, data);
                if(rotation != 0) {
                    data.output.image = cloneCanvasRotated(data.output.image, rotation);
                    data.output.width = data.output.image.width;
                    data.output.height = data.output.image.height;
                }

                ready(data);
            }
        });
        if(callback && typeof callback.init === "function") callback.init(slim);
    })
}

/**
 * @param uploadUrlOrSaveCallback Endpoint URL or function called with image data whenever user changes the image.
 *          'this' is the slim instance, first and only argument is an object containing width, height and image (data uri string)
 * @param title Dialog title
 * @param imageX Image max recommended size
 * @param imageY Image max recommended size
 * @param freeRatioOrCallback (optional) if true, allow any aspect ratio.
 *          default: limit to imageX/imageY ratio
 * @param callback Run on upload complete (true as first argument) and errors (false as first argument).
 *          'this' is the dialog instance.
 * @returns {Window.Dialog}
 */
window.labImage = function(uploadUrlOrSaveCallback, title, imageX, imageY, freeRatioOrCallback, callback) {
    var args = [freeRatioOrCallback];
    if(callback !== void 0) args.push(callback);
    callback = args.pop();
    freeRatioOrCallback = args.length ? args.pop() : false;

    var dropzone;
    var d = new Dialog({}, [
        new DialogComponent(null, 'label', title !== void 0 ? title : "Image"),
        new DialogComponent(null, 'spacer'),
        dropzone = new DialogComponent(null, 'row'),
        new DialogComponent(null, 'row', [
            new DialogComponent(null, 'button', "Cancel", false)
        ], 'right')
    ], function(dialog, button, callbackData) {
        if(callback) {
            callback.apply(d, [false]);
        }
        dialog.close();
    }, true);

    d.show();

    var $c = $("<div></div>");

    var containerId = dropzone.getElementId() + '_dropzone';

    $c.attr({ id: containerId, class: 'slim' });

    $(dropzone.getElement()).append($c);

    var slickInstance = null;

    if(!callback) { callback = {}; }

    //console.log(containerId, $c);
    injectImageEditPreview(uploadUrlOrSaveCallback, '#'+containerId, imageX, imageY, freeRatioOrCallback, (typeof callback == 'function') ? callback.bind(d) : callback);

    this.loadImage = function (url) {

    };

    return d;
};


var windowAlert = window.alert;
window.alert = function(a, b) {
    console.error("Use labAlert() instead of alert()!");
    windowAlert(a, b);
};

var windowConfirm = window.confirm;
window.confirm = function(a, b) {
    console.error("Use labConfirm() instead of confirm()!");
    return windowConfirm(a, b);
};

var windowPrompt = window.prompt;
window.prompt = function(a, b) {
    console.error("Use labPrompt() instead of prompt()!");
    return windowPrompt(a, b);
};

// Canvas for resizing
var resizeCanvas = null;
function resizeImageFromElement(img, callback) {
    // img is an <img> element

    if(!resizeCanvas) {
        resizeCanvas = document.createElement('canvas');
        if(!resizeCanvas) {
            canvasError();
            return;
        }
    }
    var ctx;

    // Resize if necessary and possible
    if(img.width > IMG_MAX_SIZE || img.height > IMG_MAX_SIZE) {

        var width = img.width;
        var height = img.height;

        // Check if a lot of resizing is needed
        // (workaround for poor scaling algo in browsers)
        var firstResize = true;
        if(width > IMG_MAX_SIZE*2 || height > IMG_MAX_SIZE*2) {
            height = ~~(height * 0.5);
            width = ~~(width * 0.5);
            var tempCanvas = document.createElement('canvas');
            tempCanvas.height = height;
            tempCanvas.width = width;
            $('body').prepend(tempCanvas);
            var tempCtx = tempCanvas.getContext('2d');
            if(!tempCtx) {
                canvasError();
                return;
            }
            tempCtx.drawImage(img, 0, 0, width, height);

            // halve the resolution in each iteration
            while (width > IMG_MAX_SIZE * 2 || height > IMG_MAX_SIZE * 2) {
                height = ~~(height * 0.5);
                width = ~~(width * 0.5);

                tempCtx.drawImage(tempCanvas, 0, 0, width*2, height*2, 0, 0, width, height);
                firstResize = false;
            }
            var tempWidth = width,
                tempHeight = height;

            if (width > height) {
                height = ~~(height * IMG_MAX_SIZE / width);
                width = IMG_MAX_SIZE;
            }else{
                width = ~~(width * IMG_MAX_SIZE / height);
                height = IMG_MAX_SIZE;
            }
            resizeCanvas.width = width;
            resizeCanvas.height = height;

            ctx = resizeCanvas.getContext("2d");
            ctx.drawImage(tempCanvas, 0, 0, tempWidth, tempHeight, 0, 0, width, height);
            tempCanvas.remove();
        }else{
            if (width > height) {
                height = ~~(height * IMG_MAX_SIZE / width);
                width = IMG_MAX_SIZE;
            }else{
                width = ~~(width * IMG_MAX_SIZE / height);
                height = IMG_MAX_SIZE;
            }
            resizeCanvas.width = width;
            resizeCanvas.height = height;

            ctx = resizeCanvas.getContext("2d");
            if(!ctx) {
                canvasError();
                return;
            }
            ctx.drawImage(img, 0, 0, width, height);
        }

    }else{
        resizeCanvas.width = img.width;
        resizeCanvas.height = img.height;

        ctx = resizeCanvas.getContext("2d");
        if(!ctx) {
            canvasError();
            return;
        }
        ctx.drawImage(img, 0, 0, img.width, img.height);
    }

    var dataUrl = resizeCanvas.toDataURL('image/jpeg', 0.9);
    img.datastr = dataUrl.split('base64,', 2)[1];
    img.onload = function() {
        callback(img);
    };
    img.src = dataUrl;

}

window.resolveClassName = function(className, forWriting) {
    var parts = className.split('.');
    if(parts[0] == 'window') parts.shift();

    var parent = window;
    for(var i = 0; i < parts.length - 1; i++) {
        parent = parent[parts[i]];
        if(!parent) {
            return null;
        }
    }

    var keyName = parts[parts.length - 1];
    if(forWriting) {
        var wrapper = {};

        wrapper.set = function (arg) {
            parent[keyName] = arg;
            return parent[keyName];
        };
        wrapper.get = function () {
            return parent[keyName];
        };
        return wrapper;
    }else{
        return parent[keyName];
    }

};


/**
 * @param className Fully qualified name eg. 'lab.ExampleClass'
 * @param parent Parent object which has .prototype
 * @param constructor (optional) Run after parent constructor
 * @returns {class}
 */
window.createClassFromParent = function(className, parent, constructor) {
    var Instance = function () {
        this.__parentConstructorNotRun = true;

        var runParentConstructor = function () {
            if(this.hasOwnProperty('__parentConstructorNotRun')) {
                delete this.__parentConstructorNotRun;
                var ctor = this.__parent;
                if(ctor.prototype.hasOwnProperty('constructor')) {
                    ctor = ctor.prototype.constructor;
                }
                if (typeof ctor === 'function') {
                    var args = arguments ? Array.prototype.slice.call(arguments) : [];
                    ctor.apply(this, args);
                }
            }else{
                console.warn('createClassFromParent problem');
            }
        }.bind(this);

        if(typeof constructor === 'function') {
            var args = arguments ? Array.prototype.slice.call(arguments) : [];
            args.unshift(runParentConstructor);
            constructor.apply(this, args);
        }
        if(this.hasOwnProperty('__parentConstructorNotRun')) {
            runParentConstructor();
        }
    };
    Instance.prototype = new Object(parent.prototype);
    Instance.prototype.__parent = parent;
    Instance.prototype.__class = className;

    var writer = window.resolveClassName(className, true);
    writer.set(Instance);

    return Instance;
};

window.ClassyJSON = {
    prepareClass: function(className, properties) {
        if(!properties) {
            properties = [];
        }
        var propertyList = [];
        for(var i in properties) {
            if(!properties.hasOwnProperty(i)) {
                continue;
            }
            var p = properties[i];
            if(p == '__class') {

            }else {
                propertyList.push(p);
            }
        }

        var clazz = window.resolveClassName(className);
        if(!clazz) {
            console.warn("ClassyJSON.prepareClass: class doesn't exist in window:", className);
        }
        if(clazz.unjsonify) {
            console.warn("ClassyJSON.prepareClass: Overwriting "+className+".unjsonify");
        }
        var thisUnjsonify = clazz.unjsonify = clazz.prototype.unjsonify = function (data) {
            var status;
            var actualClassName;
            if(typeof data.__class === 'string') {
                actualClassName = data.__class;
            }else{
                actualClassName = className;
            }
            var clazz2 = resolveClassName(actualClassName);
            var g = new clazz2();
            if(typeof g.unjsonify === 'function' && g.unjsonify !== thisUnjsonify) {
                try {
                    status = g.unjsonify(data);
                    if (status !== void 0 && typeof status !== 'function') {
                        return status;
                    }
                }catch(e) {
                    console.error('[Object '+actualClassName+'].unjsonify(',data,'): ', e.message);
                }
            }
            for(var i in propertyList) {
                var prop = propertyList[i];
                if(data[prop] !== void 0) {
                    g[prop] = data[prop];
                }
            }
            if(typeof status === 'function') {
                status = status.apply(g, [data]);
                if (!(status === void 0 || status === false || status === null || typeof status === 'function')) {
                    return status;
                }
            }
            return g;
        };

        if(clazz.prototype.jsonify) {
            console.warn("ClassyJSON.prepareClass: Overwriting "+className+".prototype.jsonify");
        }
        var thisJsonify = clazz.prototype.jsonify = function (pass) {
            var actualClassName;
            if(typeof this.__class === 'string') {
                actualClassName = this.__class;
            }else{
                actualClassName = className;
            }
            if(typeof this.jsonify === 'function' && this.jsonify !== thisJsonify) {
                var status;
                try {
                    status = this.jsonify(pass);
                    if (status !== void 0 && typeof status !== 'function') {
                        return status;
                    }
                }catch(e) {
                    console.error('[Object '+actualClassName+'].jsonify(): ', e.message);
                }
            }
            var ret = {  __class: actualClassName };
            for(var i in propertyList) {
                var prop = propertyList[i];
                if(this[prop] !== void 0) {
                    ret[prop] = this[prop];
                }
            }
            if(typeof status === 'function') {
                try {
                    status = status.apply(this, [ret, pass]);
                    if (status !== void 0) {
                        return status;
                    }
                }catch(e) {
                    console.error('[Object '+actualClassName+'].jsonify()(',ret,'): ', e.message);
                }

            }
            return ret;
        };
    },
    /**
     * Returns the passed object or json data object returned by the
     * @param value
     * @returns {*}
     */
    getObject: function(value, pass) {
        if (value && typeof value.jsonify === 'function') {
            return value.jsonify(pass);
        }
        return value;
    },
    jsonify: function(obj, pass) {
        return JSON.stringify(obj, function(k, value) {
            if (value && typeof value.jsonify === 'function') {
                return value.jsonify(pass);
            }
            return value;
        })
    },
    unjsonify: function(str) {
        if(typeof str != 'string') {
            console.warn("unjsonify called with ", str);
            return void 0;
        }
        try {
			//console.log(str);
            return JSON.parse(str, function (key, value) {
                if(!value) {
                    return value;
                }

                var type = value.__class;
                if(typeof type !== 'string') {
                    type = value.type; // todo: remove legacy
                }

                if (typeof type === 'string') {
                    value.__class = type;
                    var clazz = window.resolveClassName(type);
                    if (clazz && (clazz.unjsonify || clazz.prototype.unjsonify)) {
                        return (clazz.unjsonify || clazz.prototype.unjsonify)(value);
                    } else {
                        console.warn("ClassyJSON.unjsonify: unknown class found", type, "in object", value);
                    }
                }
                return value;
            });
        }catch(ex) {
            console.error("Couldn't unjsonify: ", str, ex);
        }
    }
};


window.persistentVariable = function(name, defaultValue) {
    var self = this;
    this.value = defaultValue;

    if(window.localStorage && window.localStorage.getItem) {
        var storageValue = window.localStorage.getItem("persistent_" + name);
		//console.log("got item Post", storageValue);
        if(storageValue !== null && storageValue) {
            try {
                this.value = window.ClassyJSON.unjsonify(storageValue);
				if(this.value === void 0) this.value = defaultValue;
            }catch (ex) {
                console.error("Couldn't parse saved value for", name, ' data: ', storageValue, ex);
            }
        }
    }

    return {
        value: value,
        name: name,
        get: function() {return self.value;},
        set: function(x) {
            //console.log(self);
            self.value = x;
            if (window.localStorage && window.localStorage.setItem) {
				var val = window.ClassyJSON.jsonify(self.value);
				//console.log("saving", val);
                if(self.value.images.length > 0)
                {
                    //console.log(self.value.imageListSelector);
                    if(self.value.imageListSelector == '.upload-image-list-photo')
                    {
                        $('#has_photos').show();
                        $('#addPhotoPost .modal-footer').show();
                        $('#addPhotoUpload .modal-footer').show();
                        $('.first_photo_section').hide();
                    }
                    
                    if(self.value.imageListSelector == '.upload-image-list-photo-album')
                    {
                        $('#has_photos_album').show();
                        $('.first_photo_album_section').hide();
                    }
                }
                window.localStorage.setItem("persistent_" + this.name, val);
            }
        }
    }
};

window.persistentVariableAdvStory = function(name, defaultValue) {
    var self = this;
    this.valueAdvStory = defaultValue;

    if(window.localStorage && window.localStorage.getItem) {
        var storageValue = window.localStorage.getItem("persistent_" + name);
        //console.log("got item Post", storageValue);
        if(storageValue !== null && storageValue) {
            try {
                this.valueAdvStory = window.ClassyJSON.unjsonify(storageValue);
                if(this.valueAdvStory === void 0) this.valueAdvStory = defaultValue;
            }catch (ex) {
                console.error("Couldn't parse saved value for", name, ' data: ', storageValue, ex);
            }
        }
    }

    return {
        valueAdvStory: valueAdvStory,
        name: name,
        get: function() {return self.valueAdvStory;},
        set: function(x) {
            //console.log(self);
            self.valueAdvStory = x;
            if (window.localStorage && window.localStorage.setItem) {
                var val = window.ClassyJSON.jsonify(self.valueAdvStory);
                //console.log("saving", val);
                if(self.valueAdvStory.images.length > 0)
                {
                    //console.log(self.value.imageListSelector);
                    if(self.valueAdvStory.imageListSelector == '.upload-image-list-photo')
                    {
                        $('#has_photos').show();
                        $('#addPhotoPost .modal-footer').show();
                        $('#addPhotoUpload .modal-footer').show();
                        $('.first_photo_section').hide();
                    }
                    
                    if(self.valueAdvStory.imageListSelector == '.upload-image-list-photo-album')
                    {
                        $('#has_photos_album').show();
                        $('.first_photo_album_section').hide();
                    }
                }
                window.localStorage.setItem("persistent_" + this.name, val);
            }
        }
    }
};

window.persistentVariableAboutme = function(name, defaultValue) {
    var self = this;
    this.valueAboutme = defaultValue;

    if(window.localStorage && window.localStorage.getItem) {
        var storageValue = window.localStorage.getItem("persistent_" + name);
        //console.log("got item Post", storageValue);
        if(storageValue !== null && storageValue) {
            try {
                this.valueAboutme = window.ClassyJSON.unjsonify(storageValue);
                if(this.valueAboutme === void 0) this.valueAboutme = defaultValue;
            }catch (ex) {
                console.error("Couldn't parse saved value for", name, ' data: ', storageValue, ex);
            }
        }
    }

    return {
        valueAboutme: valueAboutme,
        name: name,
        get: function() {return self.valueAboutme;},
        set: function(x) {
            //console.log(self);
            self.valueAboutme = x;
            if (window.localStorage && window.localStorage.setItem) {
                var val = window.ClassyJSON.jsonify(self.valueAboutme);
                console.log("saving", val);
                if(self.valueAboutme.images.length > 0)
                {
                    $('.upload-image-list-aboutme').show();
                    $('.aboutme-photo-upload').hide();
                }
                window.localStorage.setItem("persistent_" + this.name, val);
            }
        }
    }
};

window.persistentVariablePortfolio = function(name, defaultValue) {
    var self = this;
    this.valuePortfolio = defaultValue;

    if(window.localStorage && window.localStorage.getItem) {
        var storageValue = window.localStorage.getItem("persistent_" + name);
        //console.log("got item Post", storageValue);
        if(storageValue !== null && storageValue) {
            try {
                this.valuePortfolio = window.ClassyJSON.unjsonify(storageValue);
                if(this.valuePortfolio === void 0) this.valuePortfolio = defaultValue;
            }catch (ex) {
                console.error("Couldn't parse saved value for", name, ' data: ', storageValue, ex);
            }
        }
    }

    return {
        valuePortfolio: valuePortfolio,
        name: name,
        get: function() {return self.valuePortfolio;},
        set: function(x) {
            //console.log(self);
            self.valuePortfolio = x;
            if (window.localStorage && window.localStorage.setItem) {
                var val = window.ClassyJSON.jsonify(self.valuePortfolio);
                console.log("saving", val);
                if(self.valuePortfolio.images.length > 0)
                {
                    $('.upload-image-list-portfolio').show();
/*                    $('.portfolio-photo-upload').hide();*/
                }
                window.localStorage.setItem("persistent_" + this.name, val);
            }
        }
    }
};

window.persistentVariablePhoto = function(name, defaultValue) {
    var self = this;
    this.valuePhoto = defaultValue;

    if(window.localStorage && window.localStorage.getItem) {
        var storageValue = window.localStorage.getItem("persistentPhoto_" + name);
        //console.log("got item Photo", storageValue);
        if(storageValue !== null && storageValue) {
            try {
                this.valuePhoto = window.ClassyJSON.unjsonify(storageValue);
                if(this.valuePhoto === void 0) this.valuePhoto = defaultValue;
            }catch (ex) {
                console.error("Couldn't parse saved value for", name, ' data: ', storageValue, ex);
            }
        }
    }

    return {
        valuePhoto: valuePhoto,
        name: name,
        get: function() {return self.valuePhoto;},
        set: function(x) {
            self.valuePhoto = x;
            if (window.localStorage && window.localStorage.setItem) {
                var val = window.ClassyJSON.jsonify(self.valuePhoto);
                //console.log("saving", val);
                if(self.valuePhoto.images.length > 0)
                {
                    //console.log(name);
                    //console.log(self.valuePhoto.imageListSelector);
                    if(self.valuePhoto.imageListSelector == '.upload-image-list-photo')
                    {
                        $('#has_photos').show();
                        $('#photo-container').show();
                        $('#addPhotoPost .modal-footer').show();
                        $('#addPhotoUpload .modal-footer').show();
                        $('.first_photo_section').hide();
                    }
                    
                    if(self.valuePhoto.imageListSelector == '.upload-image-list-photo-album')
                    {
                        $('#has_photos_album').show();
                        $('.first_photo_album_section').hide();
                    }
                }
                window.localStorage.setItem("persistentPhoto_" + this.name, val);
            }
        }
    }
};

window.persistentVariablePhotoAlbum = function(name, defaultValue) {
    var self = this;
    this.valuePhotoAlbum = defaultValue;
    if(window.localStorage && window.localStorage.getItem) {
        var storageValue = window.localStorage.getItem("persistentPhotoAlbum_" + name);
        if(storageValue !== null && storageValue) {
            try {
                this.valuePhotoAlbum = window.ClassyJSON.unjsonify(storageValue);
                if(this.valuePhotoAlbum === void 0) this.valuePhotoAlbum = defaultValue;
            }catch (ex) {
                console.error("Couldn't parse saved value for", name, ' data: ', storageValue, ex);
            }
        }
    }

    return {
        valuePhotoAlbum: valuePhotoAlbum,
        name: name,
        get: function() {return self.valuePhotoAlbum;},
        set: function(x) {
            self.valuePhotoAlbum = x;
            if (window.localStorage && window.localStorage.setItem) {
                var val = window.ClassyJSON.jsonify(self.valuePhotoAlbum);
                if(self.valuePhotoAlbum.images.length > 0)
                {
                    //log(self.valuePhotoAlbum.imageListSelector);
                    if(self.valuePhotoAlbum.imageListSelector == '.upload-image-list-photo')
                    {
                        $('#has_photos').show();
                        $('.first_photo_section').hide();
                    }
                    
                    if(self.valuePhotoAlbum.imageListSelector == '.upload-image-list-photo-album')
                    {
                        $('#has_photos_album').show();
                        $('#photo-container').addClass('top-border');
                        $('#photo-container').show();
                        $('.first_photo_album_section').hide();
                    }
                }
                window.localStorage.setItem("persistentPhotoAlbum_" + this.name, val);
            }
        }
    }
};

window.persistentVariableProduct = function(name, defaultValue) {
    var self = this;
    this.valueProduct = defaultValue;

    if(window.localStorage && window.localStorage.getItem) {
        var storageValue = window.localStorage.getItem("persistentProduct_" + name);
        if(storageValue !== null && storageValue) {
            try {
                this.valueProduct = window.ClassyJSON.unjsonify(storageValue);
                if(this.valueProduct === void 0) this.valueProduct = defaultValue;
            }catch (ex) {
                console.error("Couldn't parse saved value for", name, ' data: ', storageValue, ex);
            }
        }
    }

    return {
        valueProduct: valueProduct,
        name: name,
        get: function() {return self.valueProduct;},
        set: function(x) {
            self.valueProduct = x;
            if (window.localStorage && window.localStorage.setItem) {
                var val = window.ClassyJSON.jsonify(self.valueProduct);
                //console.log("saving", val);
                if(self.valueProduct.images.length > 0)
                {
                    //console.log(self.valueProduct.imageListSelector);
                    
                    //$('.first-product-image').hide();
                    $('.product-form').show();                    
                    $('.upload-image-list-product').show();                    
                    $('.add_product_photo').show();                    
                }
                window.localStorage.setItem("persistentProduct_" + this.name, val);
            }
        }
    }
};


window.persistentVariableService = function(name, defaultValue) {
    var self = this;
    this.valueService = defaultValue;

    if(window.localStorage && window.localStorage.getItem) {
        var storageValue = window.localStorage.getItem("persistentService_" + name);
        if(storageValue !== null && storageValue) {
            try {
                this.valueService = window.ClassyJSON.unjsonify(storageValue);
                if(this.valueService === void 0) this.valueService = defaultValue;
            }catch (ex) {
                console.error("Couldn't parse saved value for", name, ' data: ', storageValue, ex);
            }
        }
    }

    return {
        valueService: valueService,
        name: name,
        get: function() {return self.valueService;},
        set: function(x) {
            self.valueService = x;
            if (window.localStorage && window.localStorage.setItem) {
                var val = window.ClassyJSON.jsonify(self.valueService);
                if(self.valueService.images.length > 0)
                {
                    //console.log(self.valueService.imageListSelector);
                    
                    $('.service_add_photo_icon').hide();
                    $('.service_edit_photo_icon').hide();
                    $('.upload-image-list-service').show();
                }
                window.localStorage.setItem("persistentService_" + this.name, val);
            }
        }
    }
};

window.persistentVariableTM = function(name, defaultValue) {
    var self = this;
    this.valueTM = defaultValue;

    if(window.localStorage && window.localStorage.getItem) {
        var storageValue = window.localStorage.getItem("persistentTM_" + name);
        if(storageValue !== null && storageValue) {
            try {
                this.valueTM = window.ClassyJSON.unjsonify(storageValue);
                if(this.valueTM === void 0) this.valueTM = defaultValue;
            }catch (ex) {
                console.error("Couldn't parse saved value for", name, ' data: ', storageValue, ex);
            }
        }
    }

    return {
        valueTM: valueTM,
        name: name,
        get: function() {return self.valueTM;},
        set: function(x) {
            self.valueTM = x;
            if (window.localStorage && window.localStorage.setItem) {
                var val = window.ClassyJSON.jsonify(self.valueTM);
                if(self.valueTM.images.length > 0)
                {
                    //console.log(self.valueTM.imageListSelector);
                    
                    $('.noImageBox').hide();
                    $('.upload-image-list-team-member').show();
                }
                window.localStorage.setItem("persistentTM_" + this.name, val);
            }
        }
    }
};


function GalleryRenderer() {
    this.elCount = 0;
    this.html = '';
    var colNames = ["zero", "one", "two", "three", "four"];

    this.addImage = function (image, maxPerRow) {
        this.elCount++;

        if (this.elCount == 1) {
            this.html = "</div>" + this.html;
        }

        this.html = '<div class="image-frame cover-image img-container col-'+colNames[this.elCount]+'"><img src="' + image.url + '" /><div class="img-panel new"></div></div>' + this.html;

        if (this.elCount == maxPerRow) {
            this.html = '<div class="img-row row-of-'+colNames[this.elCount]+'">' + this.html;
            this.elCount = 0;
        }
    };

    this.getHTML = function() {
        if (this.elCount != 0) {
            this.html = '<div class="img-row row-of-'+colNames[this.elCount]+'">' + this.html;
            this.elCount = 0;
        }
        return this.html;
    };
}

function GalleryRendererAdvStory() {
    this.elCount = 0;
    this.html = '';
    var colNames = ["zero", "one", "two", "three", "four"];

    this.addImage = function (image, maxPerRow) {
        this.elCount++;
        this.html = '<p><img src="' + image.url + '" /></p>';
    };

    this.getHTML = function() {
        if (this.elCount != 0) {
            this.html = this.html;
            this.elCount = 0;
        }
        return this.html;
    };
}

function GetFilename(url)
{
   if (url)
   {
      var m = url.toString().match(/.*\/(.+?)\./);
      if (m && m.length > 1)
      {
         return m[1];
      }
   }
   return "";
}

function GalleryRendererPhotoAlbum() {
    this.elCount = 0;
    this.html = '';
    var colNames = ["zero", "one", "two", "three", "four"];

    /*this.addImage = function (image, maxPerRow) {
        this.elCount++;
        console.log(image);
        var myCaption = (typeof image.caption != "undefined") ? image.caption : '';
        
        this.html = '<div class="photo_container slide">'+
                            '<a href="javascript:void(0);" onclick="removePhotoAlbumImageClicked(this, \'photo_album\');">x</a>'+
                            '<div class="photo-image-container">' + 
                                '<img src="' + image.url + '" />'+
                            '</div>'+
                            '<div class="photo_caption_container">'+
                                '<textarea data-filename="'+GetFilename(image.url)+'" placeholder="Add a photo caption" class="textarea-scrollbar scrollbar-outer form-control image-caption image-caption_'+GetFilename(image.url)+'">'+myCaption+'</textarea>'+
                            '</div>'+
                         '</div>' + this.html;
    };*/

    this.addImage = function (image, maxPerRow) {
        this.elCount++;

        if (this.elCount == 1) {
            this.html = "</div>" + this.html;
        }

        this.html = '<div class="img-container col-'+colNames[this.elCount]+'"><img src="' + image.url + '" /><div class="img-panel new"></div></div>' + this.html;

        if (this.elCount == maxPerRow) {
            this.html = '<div class="img-row row-of-'+colNames[this.elCount]+'">' + this.html;
            this.elCount = 0;
        }
    };
    
    this.getHTML = function() {
        if (this.elCount != 0) {
            this.html = '' + this.html;
            this.elCount = 0;
        }
        return this.html;
    };
}

window.ImageSet = function (imageList, imageListSelector) {
    var self = this;
    this.images = [];
    this.imageListSelector = imageListSelector;
    this.imgCallback = function(){};

	if(!imageList) imageList = [];
    imageList = imageList.filter(function(item) {return item;});

    this.jsonify = function () {
        return {type: 'ImageSet', images: this.images, imageListSelector: this.imageListSelector};
    };

    this.imagesChanged = function () {
        var $lists = $('.image-list').filter(self.imageListSelector);

        if(imageListSelector == '.upload-image-list-advanced-story')
        {
            var renderer = new GalleryRendererAdvStory();
            for(var i = self.images.length-1; i >= 0; i--) {
                var img = self.images[i];
                renderer.addImage(img, 2);
                
                self.removeImages([img]);
            }
            
            var html = renderer.getHTML();
            if(html != "")
            {
                tinyMCE.execCommand('mceInsertContent', false, html);
/*                tinyMCE.get('advanced-story').setContent(html);                */
            }
        }
        if(imageListSelector == '.upload-image-list-product'){
            var renderer = new GalleryRenderer();

            for(var i = self.images.length-1; i >= 0; i--) {
                var img = self.images[i];
                renderer.addImage(img, 10);
            }

            var html = renderer.getHTML();
            $(self.imageListSelector).html(html);
        }
        else
        {
            var renderer = new GalleryRenderer();

            for(var i = self.images.length-1; i >= 0; i--) {
                var img = self.images[i];
                renderer.addImage(img, 10);
            }

            var html = renderer.getHTML();
            $(self.imageListSelector).html(html);
        }
        self.imgCallback(self.images, $(self.imageListSelector));
    };

    var onSingleLoaded = function() {
        for(var k in self.images) {
            if(!self.images.hasOwnProperty(k))
                continue;
            var img = self.images[k];
            if(!img.loaded) return;
        }
        // not returned - all loaded
        self.imagesChanged();
    };

    this.addImages = function(imageList) {
        for(var k in imageList) {
            if(!imageList.hasOwnProperty(k))
                continue;
            var img = imageList[k];
            if(!img)
                continue;

            if(typeof img == "string") {
                img = new window.CachedImage(img);
            }
            img.onload = function (){
                onSingleLoaded();
            };
            self.images.push(img);
        }
        onSingleLoaded();
    };

    this.removeImages = function(imageList) {
        imageList = imageList.map(function(item) {return ((typeof item) != "string") ? item.url : item;});
        self.images = self.images.filter(function(item) {return  imageList.indexOf(item.url) === -1;});
        self.imagesChanged();
    };


    this.addImages(imageList);

    // Recheck to make sure nothing got loaded before we attached the callback
    onSingleLoaded();
};
window.ImageSet.prototype.unjsonify = function(object) {
    return new window.ImageSet(object.images, object.imageListSelector);
};

window.ImageSetPhoto = function (imageList, imageListSelector) {
    var self = this;
    this.images = [];
    this.imageListSelector = imageListSelector;

    if(!imageList) imageList = [];
    imageList = imageList.filter(function(item) {return item;});

    this.jsonify = function () {
        return {type: 'ImageSet', images: this.images, imageListSelector: this.imageListSelector};
    };

    this.imagesChanged = function () {
        var $lists = $('.image-list').filter(self.imageListSelector);

        var renderer = new GalleryRenderer();

        for(var i = self.images.length-1; i >= 0; i--) {
            var img = self.images[i];
            renderer.addImage(img, 2);
        }

        var html = renderer.getHTML();

        $(self.imageListSelector).html(html);

    };

    var onSingleLoaded = function() {
        for(var k in self.images) {
            if(!self.images.hasOwnProperty(k))
                continue;
            var img = self.images[k];
            if(!img.loaded) return;
        }
        // not returned - all loaded
        self.imagesChanged();
    };

    this.addImages = function(imageList) {
        for(var k in imageList) {
            if(!imageList.hasOwnProperty(k))
                continue;
            var img = imageList[k];
            if(!img)
                continue;

            if(typeof img == "string") {
                img = new window.CachedImage(img);
            }
            img.onload = onSingleLoaded;
            self.images.push(img);
        }
        onSingleLoaded();
    };

    this.removeImages = function(imageList) {
        imageList = imageList.map(function(item) {return ((typeof item) != "string") ? item.url : item;});
        self.images = self.images.filter(function(item) {return  imageList.indexOf(item.url) === -1;});
        self.imagesChanged();
    };


    this.addImages(imageList);

    // Recheck to make sure nothing got loaded before we attached the callback
    onSingleLoaded();
};
window.ImageSetPhoto.prototype.unjsonify = function(object) {
    return new window.ImageSet(object.images, object.imageListSelector);
};


window.ImageSetPhotoAlbum = function (imageList, imageListSelector) {
    var self = this;
    this.images = [];
    this.imageListSelector = imageListSelector;
    this.uploadType = 'normal';

    if(!imageList) imageList = [];
    imageList = imageList.filter(function(item) {return item;});

    this.jsonify = function () {
        return {type: 'ImageSet', images: this.images, imageListSelector: this.imageListSelector};
    };

    this.imagesChanged = function () {
        var $lists = $('.image-list').filter(self.imageListSelector);
         var myslider = '';
/*        var renderer = new GalleryRenderer();*/
        var renderer = new GalleryRendererPhotoAlbum();

        for(var i = self.images.length-1; i >= 0; i--) {
            var img = self.images[i];
/*            var myCaption = ($('textarea.image-caption_'+GetFilename(img.url)).val() != "") ? $('textarea.image-caption_'+GetFilename(img.url)).val() : '';*/
            
/*            img.caption = myCaption;*/
            renderer.addImage(img, 2);
        }

        var html = renderer.getHTML();
        
        $(self.imageListSelector).html(html);
        
        /*if(this.uploadType == 'album-edit')
        {
            if(html != "")
            {
                $('#AddPhotoAlbum .album_photo_new_container').html(html);
                $('#AddPhotoAlbum .upload_album_photo .uploadPhotoBox').hide();
                $('#AddPhotoAlbum .album_photo_new_container').show();
                $('#AddPhotoAlbum .modal-footer').show();
                
            }
            else
            {
                $('#AddPhotoAlbum .upload_album_photo .uploadPhotoBox').show();
                $('#AddPhotoAlbum .album_photo_new_container').hide();
                $('#AddPhotoAlbum .modal-footer').hide();
            }
        }
        else
        {
            $('#photo_section .slider1').html(html);
        }*/
        
        
        
        /*if($('#photo_section .slider1 .slide').length > 0)
        {*/
            /*if(typeof(myslider.destroySlider) === 'function') {
                myslider.destroySlider();
            }*/
            /*slider = $('.slider1').bxSlider();*/
/*            myslider.destroySlider();*/
            
           /*myslider = $('.slider1').bxSlider({
                minSlides: 1,
                maxSlides: 3,
                slideMargin: 10,
                slideWidth: 150,
                moveSlides: 1,
                infiniteLoop: false,
                hideControlOnEnd: true,
                pager:false,
              });*/
           
           
/*            $('.textarea-scrollbar').scrollbar();*/
/*        }*/
    };

    var onSingleLoaded = function() {
        for(var k in self.images) {
            if(!self.images.hasOwnProperty(k))
                continue;
            var img = self.images[k];
            if(!img.loaded) return;
        }
        // not returned - all loaded
        self.imagesChanged();
    };

    this.addImages = function(imageList) {
        for(var k in imageList) {
            if(!imageList.hasOwnProperty(k))
                continue;
            var img = imageList[k];
            if(!img)
                continue;

            if(typeof img == "string") {
                img = new window.CachedImage(img);
            }
            
            img.onload = onSingleLoaded;
            self.images.push(img);
        }
        onSingleLoaded();
    };

    this.removeImages = function(imageList) {
        imageList = imageList.map(function(item) {return ((typeof item) != "string") ? item.url : item;});
        self.images = self.images.filter(function(item) {return  imageList.indexOf(item.url) === -1;});
        self.imagesChanged();
    };

    this.addUploadType = function(upload_type){
        upload_type = (upload_type != "") ? upload_type : 'normal';
        this.uploadType = upload_type;
    };

    this.addImages(imageList);

    // Recheck to make sure nothing got loaded before we attached the callback
    onSingleLoaded();
};

window.CachedImage = function(url, rotation, dimensions) {
    var self = this;
    this.url = url;
    this.preloader = document.createElement("IMG");

    this.width = 0;
    this.height = 0;
    // once loaded contains {x: 123, y: 123}
    this.dimensions = dimensions === void 0 ? null : dimensions;

    this.loaded = false;

    this.onload = false;

    this.preloader.onload = function() {
        self.width = self.preloader.naturalWidth;
        self.height = self.preloader.naturalHeight;
        self.dimensions = {
            x: self.width,
            y: self.height,
            width: self.width,
            height: self.height
        };
        self.loaded = true;
        if(self.onload) self.onload(self.preloader);
    };
    this.preloader.src = url;
    this.preloader.style.display = "none";
    document.body.appendChild(this.preloader);
    this.rotation = rotation === void 0 ? 0 : rotation;
    this.caption = this.caption === void 0 ? '' : this.caption;
    this.jsonify = function () {
        return {type: 'CachedImage', url: this.url, rotation: this.rotation, dimensions: self.dimensions, caption: this.caption};
    };
};
window.CachedImage.prototype.unjsonify = function(object) {
    return new window.CachedImage(object.url, object.rotation, object.dimensions);
};


/*search Auto comeplte */
var autocompleteAnchorEl = false;
function resizeAutocomplete() {
    if(!autocompleteAnchorEl) return;

    var $anchor = $(autocompleteAnchorEl);
    var $el = $('#searchAutocompleteDropdown');

    var pos = $anchor.offset(),
        width = $anchor.outerWidth(),
        height = $anchor.outerHeight();
    if($anchor.data('paddedSearch')) {
        $el.css({
            position: 'absolute',
            top: pos.top + height,
            left: (pos.left - 2) + 'px',
            width: (width + 27) + 'px'
        });
    }else {
        $el.css({
            position: 'absolute',
            top: pos.top + height,
            left: (pos.left) + 'px',
            width: (width) + 'px'
        });
    }

}

var autocompleteEnabled = false;
var autocompleteTextQueued = false;
function autocompleteTimer() {
    if(!autocompleteEnabled) return;

    ajaxSubmit('/$autocompleteDiscover', {query: autocompleteTextQueued}, function(data) {

        if(!autocompleteEnabled) return;

        var html = '';
        for(var i in data.items) {
            var item = data.items[i];
            var url = '/BRANDURL';
            html += '<a href="' + url.replace('BRANDURL', item.url).replace(/"/g, "\\\"") + '">'+ item.text + '</a> \n';
        }
        if(html == '') {
            html = '<i>No results found</i>';
        }
        $('#searchAutocompleteLinkContainer').html(html);

        $('#searchAutocompleteDropdown').addClass('active');
        resizeAutocomplete();

        if(autocompleteTextQueued) {
            autocompleteEnabled = true;
            window.setTimeout(autocompleteTimer, 500);
        }else{
            autocompleteEnabled = false;
        }
    }, function() { // on fail
        autocompleteEnabled = false;
    });

    autocompleteTextQueued = false;
}

function checkHideAutocomplete() {
    if($('#searchAutocompleteDropdown').find('*:focus').length) {
        window.setTimeout(checkHideAutocomplete, 100);
    }else{
        $('#searchAutocompleteDropdown').removeClass('active');
        autocompleteEnabled = false;
        autocompleteAnchorEl = false;
    }
}

function hideAutocomplete(el) {
    window.setTimeout(checkHideAutocomplete, 100);
}

function searchAutocomplete(el) {
    if(el === false) hideAutocomplete();
    var text = '';
    if($(el).length > 0)
    {
        text = $(el).val().trim();
    }
    if(!text || text == '') hideAutocomplete();

    autocompleteAnchorEl = el;

    if(!autocompleteEnabled) {
        autocompleteEnabled = true;
        autocompleteTextQueued = text;
        window.setTimeout(autocompleteTimer, 500);
    }else{
        autocompleteTextQueued = text;
    }
}


// Dropdowns
var $currentlyOpenMenu = null;
var $dropdownAnchors = [];

function closeCurrentDropdown() {
    if($currentlyOpenMenu == null)
        return;
    var $icons = $dropdownAnchors[$currentlyOpenMenu[0].id];
    $icons.removeClass('active');
    $currentlyOpenMenu.removeClass('active');
    $currentlyOpenMenu = null;
}

$('body').click(function(e) {
    if($currentlyOpenMenu == null)
        return;

    var $target = $(e.target);
    var $menu = $target.closest('.dropdownMenu');

    if($menu.length) {
        var $close = $target.closest('.dropdown-close');

        if (!$close.length) {
            return;
        }
        if (!$.contains($menu[0], $close[0])) {
            return;
        }
    }
    if($target.closest('.dropdownActivator').length) {
        return;
    }
    closeCurrentDropdown();
});

 /**
 * Create dropdown menu
 * @param {*} $anchors Icons for activating dropdown - they will get .active class when menu is open
 * @param {*} $menu Single element which which will be hidden using .dropdownMenu and made visible using .active
 * @param {*} [positionParameters] If non-false argument given, menu is positioned on open.
 *      Object can be given to override defaults: <code>{ my: 'left top', at: 'right bottom', of: anchor }</code>
 * @param {boolean} [moveMenu=false] If true, menu will not be moved to overflow-hidden-free zone
 * @see {@link http://api.jqueryui.com/position/}
 * @example
 * // Run in console on home or stories and click on avatar to see how it works
 *   dropdownMenu($('.post:first-child .page-avatar.image-avatar'), $('.post:first-child .post-container')[0])
 */
function dropdownMenu($anchors, $menu, positionParameters, moveMenu) {
    $anchors = $($anchors);
    $menu = $($menu);

    if(!$anchors[0] || !$menu[0]) {
        console.warn("dropdownMenu called with empty icon or menu: ", $anchors, $menu);
        return;
    }
    if($menu.length !== 1) {
        console.warn("dropdownMenu called with more than one menu elements: ", $menu);
        return;
    }
    var id = $menu[0].id;
    if(!id) {
        id = 'dropdown_' + Math.floor(Math.random() * 10000);
        $menu[0].id = id;
    }
    $dropdownAnchors[id] = $anchors;

    if(moveMenu) {
        $menu.appendTo($('#dialogBox-container'));
    }

    $anchors.addClass('dropdownActivator');
    $menu.addClass('dropdownMenu');

    $anchors.click(function(ev) {
        ev.stopPropagation();
        closeCurrentDropdown();
        var $thisAnchor = $(this);

        $currentlyOpenMenu = $menu;
        $menu.addClass('active');
        $thisAnchor.addClass('active');

        if (positionParameters) {
            var pos = {
                my: 'left top',
                at: 'right bottom',
                of: $thisAnchor
            };
            if(typeof positionParameters === 'object') {
                if(positionParameters.my) pos.my = positionParameters.my;
                if(positionParameters.at) pos.at = positionParameters.at;
                if(positionParameters.of) pos.of = positionParameters.of;
            }
            $menu.data('menuPosition', pos);
            $menu.position(pos);
        }
    });
}
function _repositionDropdownMenus() {
    if($currentlyOpenMenu == null)
        return;
    var pos = $currentlyOpenMenu.data('menuPosition');
    if(pos) {
        $currentlyOpenMenu.position(pos);
    }
}
$(window).scroll(_repositionDropdownMenus);
$(window).resize(_repositionDropdownMenus);

function makeDropdownMenu($icon, $menu, positionAtIcon, noPositionOverride) {
    console.error("makeDropdownMenu executed - change to dropdownMenu"); // remove makeDropdownMenu if not needed
    if(!$icon[0] || !$menu[0]) {
        console.warn("makeDropdownMenu with empty icon or menu: ", $icon, $menu);
        return;
    }
    $dropdownAnchors[$menu[0].id] = $icon;
    $icon.click(function(ev) {
        ev.stopPropagation();
        closeCurrentDropdown();
        $currentlyOpenMenu = $menu;
        $menu.addClass('active');
        $icon.addClass('active');
        var $div = $menu.children('div');

        if(!noPositionOverride) {
            if (positionAtIcon) {
                $div.css('right', '0px');
                var diff = $div.offset().left - $icon.offset().left + ($div.outerWidth() - $icon.outerWidth());
                $div.css('right', Math.floor(diff) + 'px')
            } else {
                $div.css('right', '10px');
            }
        }
    });
}
//makeDropdownMenu($('#searchDropdownIcon'), $('#searchDropdown'), 0, true);

//makeDropdownMenu($('#userLoginDropdownIcon'), $('#userLoginDropdown'));
/* END */

function searchFocus(obj)
{
    if($(obj).val().trim() == "")
    {
        $(obj).val(" ");
    }
}

function searchBlur(obj)
{
    if($(obj).val().trim() == "")
    {
        $(obj).val("");
    }
}

function expandSearchBox(obj)
{
    if($(obj).val().trim() != "")
    {
        $(obj).parent('.input-append').css('width', 'auto');
        $(obj).css('width', '220px');
    }
    else
    {
        $(obj).parent('.input-append').css('width', '190px');
        $(obj).css('width', '175px');
    }
}

function positionBrandInfoSidebar() {
    var $sidebar = $('#brand-info-sidebar');
    var landingHeight = $('.landing-header').outerHeight() || 0;
    var feedHeight = $('.feed-segment-menu').outerHeight() || 0;

    var bufferHeight = landingHeight + feedHeight + 80;

    if ($(window).scrollTop() > bufferHeight) {
        $sidebar.css({
            'position': 'fixed',
            'top': '10px'
        });
    }else{
        $sidebar.css({
            'position': 'relative',
            'top': 'auto'
        });
    }
}
$(window).scroll(positionBrandInfoSidebar);
$(positionBrandInfoSidebar);
positionBrandInfoSidebar();

// New image upload queue

lab.Hook = function() {
    this.hooks = {};
};
lab.Hook.prototype = {
    add: function (name, callback, thisObject) {
        if(typeof callback === 'function') {
            this.hooks[name] = [callback];
            if(thisObject !== void 0) {
                this.hooks[name].push(thisObject);
            }
        }else{
            delete this.hooks[name];
        }
    },
    remove: function (name) {
        delete this.hooks[name];
    },
    fire: function () {
        for(var name in this.hooks) {
            if(!this.hooks.hasOwnProperty(name)) {
                continue;
            }
            var hook = this.hooks[name];
            hook[0].apply(hook.length > 1 ? hook[1] : window, arguments);
        }
    }
};

lab._serializeImage = function (img) {
    return { url: img.url, width: img.width, height: img.height };
};

lab.Image = function(url, width, height) {
    this.url = url;
    this.width = width;
    this.height = height;

    this.lqUrl = this.url;
    var parts = /(i\/[a-z0-9]{32})(?:|_([a-z_\-]*))\.(jpg|jpeg|png|webp|gif)/i.exec(this.lqUrl);
    if(parts) {
        this.lqUrl = [
            parts[1],
            '_small',
            '.',
            parts[3],
        ].join('');
    }
};
lab.Image.prototype.jsonify = function (pass) {
    if(!pass) return;
    return lab._serializeImage(this);
};
lab.Image.prototype.loadIn = function (targetEl) {
    var $target = $(targetEl);
    //todo
};

lab.CachedImage = createClassFromParent('lab.CachedImage', lab.Image, function(parent, url, width, height) {
    parent(url, width, height);

    this.onload = new lab.Hook();

    this.loaded = false;

    var self = this;
    this.preloader = document.createElement("IMG");
    this.preloader.onload = function() {
        self.width = self.preloader.naturalWidth;
        self.height = self.preloader.naturalHeight;
        self.loaded = true;
        self.onload.fire(self.preloader);
    };
    this.preloader.src = url;
    this.preloader.style.display = "none";
    document.body.appendChild(this.preloader);
});

lab.ItemList = function() {
    this.items = [];
};
lab.ItemList.prototype = {
    onModify: new lab.Hook(),
    _autoCast: function(item) {
        return item;
    },
    add: function (item) {
        this.items.push(this._autoCast(item));
        this.onModify.fire();
    },
    addAll: function (container) {
        var fire = false;
        for(var i in container) {
            if(container.hasOwnProperty(i)) {
                fire = true;
                this.items.push(this._autoCast(container[i]));
            }
        }
        if(fire) {
            this.onModify.fire();
        }
    },
    remove: function (matcher) {
        var newItems = [];
        var removedAny = false;
        if(typeof matcher === 'function') {
            for(var i = 0; i < this.items.length; i++) {
                var item = this.items[i];
                if(!matcher(item)) {
                    newItems.push(item);
                }else{
                    removedAny = true;
                }
            }
            if(removedAny) {
                this.items = newItems;
                this.onModify.fire();
            }
        }else{
            console.warn("ItemList.remove( matcher ): matcher needs to be a function")
        }
    },
    load: function(list, keepItems) {
        if(!list.hasOwnProperty('items')) {
            console.warn("ItemList.load called with wrong argument:", list);
        }
    },
    foreach: function (acceptor) {
        if(typeof acceptor === 'function') {
            var list = this.items.slice();
            for(var i = 0; i < list.length; i++) {
                acceptor.call(list[i], list[i]);
            }
        }else{
            console.warn("ItemList.foreach( acceptor ): acceptor needs to be a function")
        }
    },
    foreachReverse: function (acceptor) {
        if(typeof acceptor === 'function') {
            var list = this.items.slice();
            for(var i = list.length - 1; i >= 0; i--) {
                acceptor.call(list[i], list[i]);
            }
        }else{
            console.warn("ItemList.foreach( acceptor ): acceptor needs to be a function")
        }
    },
    toArray: function () {
        return this.items.slice();
    }
};
ClassyJSON.prepareClass('lab.ItemList', ['items']);

lab.ImageList = createClassFromParent('lab.ImageList', lab.ItemList);

//lab.ImageList.prototype = new Object(lab.ItemList.prototype);

lab.ImageList.prototype._autoCast = function (item) {
    if(typeof item === 'string') {
        return new lab.Image(item);
    }else{
        return item;
    }
};
lab.ImageList.prototype.getJson = function () {
    return ClassyJSON.jsonify(this.toArray(), true);
};

lab.ImageListRenderer = function($target, list, options) {
    options = $.extend({
        controls: {},
        maxPerRow: 2
    }, options||{});

    this.$target = $($target);
    this.hookName = 'ImageListRenderer-'+Math.floor(Math.random()*10000);
    this.list = list;
    this.controls = options.controls;
    this.maxPerRow = options.maxPerRow;
    if(list.onModify) {
        list.onModify.add(this.hookName, function() {
            this.render();
        }, this);
    }
    this.render();
};
lab.ImageListRenderer.prototype.render = function(list) {
    var $target = this.$target;

    if(list === void 0) {
        list = this.list;
    }

    $target.html(this.__renderHtml(list));
};
lab.ImageListRenderer.prototype.__renderHtml = function(list) {
    var html = '';

    var colNames = ["zero", "one", "two", "three", "four", "five"];
    var maxPerRow = this.maxPerRow;

    var currentElement = 0;
    var row = '';

    var controls = '';
    if(this.controls) {
        prepareImageUploadAndRun(function () {}); // load styles for image upload

        var wrapCallback = function(callback) {
            return function (el) {
                var $container = $(el).closest('.img-container');
                var $img = $('img', $container);
                return callback.call(el, $img.attr('src'), $container);
            }
        };

        var id;
        if(this.controls.edit) {
            id = Math.floor(Math.random() * 1e10);
            (window._g_callbacks || (window._g_callbacks = {}))[id] = wrapCallback(this.controls.edit);
            controls += '<button data-action="edit" onclick="return window._g_callbacks['+id+'](this)" class="slim-btn slim-btn-edit">Edit</button>';
        }
        if(this.controls.remove) {
            id = Math.floor(Math.random() * 1e10);
            (window._g_callbacks || (window._g_callbacks = {}))[id] = wrapCallback(this.controls.remove);
            controls += '<button data-action="remove" onclick="return window._g_callbacks['+id+'](this)" class="slim-btn slim-btn-remove no-override">Remove</button>';
        }

    }
    if(controls) {
        controls = '<div class="slim-btn-group">' + controls + '</div>';
    }
    list.foreachReverse(function() {
        currentElement++;
        if (currentElement == 1) {
            html = "</div>" + html;
        }

        row +=
            '<div class="img-container col-'+colNames[currentElement]+'">'+
                '<img src="' + this.url + '" />'+
                '<div class="img-panel new"></div>' + // do we need this?
                controls +
            '</div>';

        if (currentElement == maxPerRow) {
            html = '<div class="img-row row-of-'+colNames[currentElement]+'">'
                + row + html;
            currentElement = 0;
            row = '';
        }

    });

    if (currentElement != 0) {
        html = '<div class="img-row row-of-' + colNames[currentElement] + '">'
            + row + html;
    }


    return '<div class="image-list slim">' + html  + '</div>';
};


// Please keep interval timers and onGloabalJS handling at the end

function runGlobalJSHooks() {
    if(window.onGloabalJS) {
        for(var k in window.onGloabalJS) {
            if(!window.onGloabalJS.hasOwnProperty(k)) continue;
            try {
                if(window.onGloabalJS[k] !== false) {
                    window.onGloabalJS[k]();
                    window.onGloabalJS[k] = false;
                }
            }catch(e) {
                console.error("An error occured while running onGlobalJS hook", k, e);
                window.onGloabalJS[k] = false;
            }
        }
    }
}

function replaceTimestamps() {
    if(!$) return;
    $('[data-timestamp]').each(function () {
        var $this = $(this);
        var timestamp = $this.data('timestamp');
        var format = $this.data('timestampFormat');
        if(!format) format = void 0;
        $this.text(formatAgoTime(timestamp, format));
    })
}
(window.onGloabalJS ? window.onGloabalJS : (window.onGloabalJS = [])).push(replaceTimestamps);
(window.onFeedJS ? window.onFeedJS : (window.onFeedJS = [])).push(replaceTimestamps);
window.setInterval(replaceTimestamps, 1000);

runGlobalJSHooks();
setTimeout(runGlobalJSHooks, 100);
setInterval(runGlobalJSHooks, 500);

console.info("global.js loaded");