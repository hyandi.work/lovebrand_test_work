/*
 Required before including this script:
 window.onFeedJS = [];
 window.g_brand = { brand object };
 window.g_user = { user object };
 window.onFeedJS.push(function() {
    feed.setActiveSegmentIndicator(startSegment, true);
 });

 */
var $ = jQuery.noConflict();
window.saveCss = {'infoProfileMenu' : {}};

if(!window.onGloabalJS) { window.onGloabalJS = []; }
if(!window.onFeedJS) { window.onFeedJS = []; }

window.feed = {
    // hooks
    onSegmentChanged: [],

    availableSegments: [
        'landing',
        'home',
        'stories',
        'gallery',
        'album',
        'video',
        'products',
        'services',
        'about',
        'about-management',
        'drafts',
        'mybrands',
        'drafts',
        'aboutme',
        'portfolio',
    ],

    segmentParents: {
        'about-management': 'about'
    },

    activeSegment: 'landing',
    activeSegmentSetByUser: false,

    getBrand: function() {
        return window.g_brand;
    },
    getUser: function() {
        return window.g_user;
    },


    setActiveSegmentIndicator: function(segment, onPageLoad) {
        // make sure that segment is valid or default to landing
        if($.inArray(segment, this.availableSegments) == -1) {
            segment = 'landing';
        }

        var prevSegment = this.activeSegment;
        this.activeSegment = segment;

        var parentSegment, $newParent;

        var $prev = $("[data-target-segment='" + prevSegment +"']");
        var $prevTarget = $prev.closest('[data-activation-target]');
        if(feed.segmentParents[prevSegment]) {
            parentSegment = feed.segmentParents[prevSegment];
            $newParent = $("[data-target-segment='" + parentSegment +"']");
            $prevTarget.add($newParent.closest('[data-activation-target]'));
        }

        var $new = $("[data-target-segment='" + segment +"']");
        var $newTarget = $new.closest('[data-activation-target]');
        if(feed.segmentParents[segment]) {
            parentSegment = feed.segmentParents[segment];
            $newParent = $("[data-target-segment='" + parentSegment +"']");
            $newTarget.add($newParent.closest('[data-activation-target]'));
        }

        $prevTarget.removeClass('active');
        $newTarget.addClass('active');

        var $landingHeader = $('.landing-header');
        var $segmentMenu = $('.feed-segment-menu');

        var speed = 300;

        if(onPageLoad) {
            if (segment == 'landing') {
                //$segmentMenu.removeClass('full-width');
                $landingHeader.removeClass('hidden');
                $landingHeader.css({height: 'auto'});
                window.saveCss['infoProfileMenu'] =  $('ul.infoProfileMenu').css(['border-top', 'margin-top']);
                $('#love-brand-btn .love-brand-button, #love-brand-btn .love-brand-button+.dropdown').hide();
                $('ul.infoProfileMenu').css({'border-top' : 0, 'margin-top' : 0});

            } else {
                //$segmentMenu.addClass('full-width');
                $landingHeader.addClass('hidden');
                $landingHeader.css({height: '0'});

                $('#love-brand-btn .love-brand-button, #love-brand-btn .love-brand-button+.dropdown').show();
                $('ul.infoProfileMenu').css(window.saveCss['infoProfileMenu']);
            }
        }else {
            if (segment == 'landing') {
                //$segmentMenu.removeClass('full-width');
                $landingHeader.removeClass('hidden');

                var currHeight = $landingHeader.innerHeight();
                $landingHeader.css({height: 'auto'});
                var fullHeight = $landingHeader.innerHeight();
                $landingHeader.css({height: currHeight + 'px'});

                $landingHeader.animate({height: fullHeight + 'px'}, speed, function (){
                    window.saveCss['infoProfileMenu'] =  $('ul.infoProfileMenu').css(['border-top', 'margin-top']);
                    $('ul.infoProfileMenu').css({'border-top' : 0, 'margin-top' : 0});
                    $('#love-brand-btn .love-brand-button, #love-brand-btn .love-brand-button+.dropdown').hide();
                });
            } else {

                $landingHeader.animate({height: '0'}, speed, function () {
                    $(this).addClass('hidden');
                    $('#love-brand-btn .love-brand-button, #love-brand-btn .love-brand-button+.dropdown').show();
                    $('ul.infoProfileMenu').css(window.saveCss['infoProfileMenu']);
                    //$segmentMenu.addClass('full-width');
                });
            }
        }

    },

    _navigationInProgress: false,
    navigateToSegment: function(segment, param1, param2, param3, param4) {
        if(feed._navigationInProgress) {
            return false;
        }
        feed._navigationInProgress = true;
        $(document.body).addClass('feed-segment-changing');

        var url = '/$json-feed/:url/:segmentOrId';
        url = url.replace(':url', window.g_brand.url);
        url = url.replace(':segmentOrId', segment);

        var params = [param1, param2, param3, param4];
        for(var i = 0; i < params.length; i++) {
            if(params[i] === null || params[i] === void 0) {
                break;
            }
            url = url + '/' + params[i];
        }

        var data = {};

        window.ajaxSubmitCheck(url, data, function (data) {
            // success
            feed._navigationInProgress = false;
            $(document.body).removeClass('feed-segment-changing');

            // console.log(data);
            window.history.pushState("feed", "Love a Brand", data.segmentURL);
            feed.setActiveSegmentIndicator(segment);

            $('#dynamic-content-wrapper').html(data.content)

            for(var i in feed.onSegmentChanged) {
                if(feed.onSegmentChanged.hasOwnProperty(i)) {
                    feed.onSegmentChanged[i]();
                }
            }
        }, function (isPhpError, arg, arg2) {
            // fail
            feed._navigationInProgress = false;
            $(document.body).removeClass('feed-segment-changing');

            console.log('navigateToSegment error:', isPhpError, arg, arg2);
        });

        return true;

    },
    navigateToSegmentByUrl: function(url, segment, param1, param2, param3, param4) {
        if(feed._navigationInProgress) {
            return false;
        }
        feed._navigationInProgress = true;
        $(document.body).addClass('feed-segment-changing');

        var params = [param1, param2, param3, param4];
        for(var i = 0; i < params.length; i++) {
            if(params[i] === null || params[i] === void 0) {
                break;
            }
            url = url + '/' + params[i];
        }

        var data = {};

        window.ajaxSubmitCheck(url, data, function (data) {
            // success
            feed._navigationInProgress = false;
            $(document.body).removeClass('feed-segment-changing');

            // console.log(data);
            window.history.pushState("feed", "Love a Brand", data.segmentURL);
            feed.setActiveSegmentIndicator(data.segment);

            $('#dynamic-content-wrapper').html(data.content)

            for(var i in feed.onSegmentChanged) {
                if(feed.onSegmentChanged.hasOwnProperty(i)) {
                    feed.onSegmentChanged[i]();
                }
            }
        }, function (isPhpError, arg, arg2) {
            // fail
            feed._navigationInProgress = false;
            $(document.body).removeClass('feed-segment-changing');

            console.log('navigateToSegment error:', isPhpError, arg, arg2);
        });

        return true;

    },
    getSegmentContent: function(fnCalback, segment, param1, param2, param3, param4) {
        if(feed._navigationInProgress) {
            return false;
        }

        var url = '/$json-feed/:url/:segmentOrId';
        url = url.replace(':url', window.g_brand.url);
        url = url.replace(':segmentOrId', segment);

        var params = [param1, param2, param3, param4];
        for(var i = 0; i < params.length; i++) {
            if(params[i] === null || params[i] === void 0) {
                break;
            }
            url = url + '/' + params[i];
        }

        window.ajaxSubmitCheck(url, {}, function (data) {
            fnCalback(data);
        }, function (isPhpError, arg, arg2) {
            // fail
            console.log('getSegmentContent error:', isPhpError, arg, arg2);
        });

        return true;
    },

    setBrandLovedIndicator: function (brandId, loved) {
        if(loved) {
            $(document.body).addClass('brand-loved');
        }else{
            $(document.body).removeClass('brand-loved');
        }
    },

    sendBrandLoved: function (brandId, loved) {
        if(!brandId) {
            brandId = feed.getBrand().id;
        }

        var url = '/$brandRelation/:url/:relationType/:value';
        url = url.replace(':url', window.g_brand.url);
        url = url.replace(':relationType', '1');
        url = url.replace(':value', loved ? 'true' : 'false');

        window.ajaxSubmitCheck(url, {}, function (data) {
            // success
            feed.setBrandLovedIndicator(brandId, data.value);
            
            $sidebarBox = $('#brand-info-sidebar');
            $sidebarBox.find('.loving-us p').html(data.total);
            
            $headerState = $('.header-brand-stats');
            $headerState.find('.brand_loving_count .badge').html(data.total);
        });
    }
};

var attachSegmentHooks = function () {
    $('[data-target-segment]').click(function(event) {
        // ignore clicks on avatar button in side panel
        if($(event.target).closest('.page-avatar-change-button').length) return;

        feed.navigateToSegment(
            $(this).data('targetSegment'),
            $(this).data('segment-param'),
            $(this).data('segment-param-2'),
            $(this).data('segment-param-3'),
            $(this).data('segment-param-4'));
        event.preventDefault();
    });
   
    

    $('.love-brand-button').click(function() {        
        if(!window.g_user){
         
            var btn_row = null;
            var username = null, password = null, err_lable = null;
            var d = new Dialog({}, [
                err_lable = new DialogComponent(null, 'label', "", 'color:red;'),
                new DialogComponent(null, 'spacer'),
                username = new DialogComponent(null, 'text', 'Username'),
                password = new DialogComponent(null, 'password', 'Password'),
                new DialogComponent(null, 'row'),
                btn_row = new DialogComponent(null, 'row', [], 'right')
            ], function(dialog, button, callbackData) {
                button.click(dialog);                
            }, true);
            
            var btn = new DialogComponent(null, 'button', "Sign in", false);
            btn.click = function (dialog){
                err_lable.getElement().innerHTML = '';
               ajaxSubmitCheck('/$signinAjax', {
                   'email' : username.getElement().value, 
                   'password' : password.getElement().value
                }, function (data) {
                    window.user = data.user;
                    dialog.close();
                    location.reload();
                }, function (a, data){
                    err_lable.getElement().innerHTML = data.errorMessage;
                });
            };
            btn_row.addChild(btn);
            
            btn = new DialogComponent(null, 'button', 'Sign up');
            btn.click = function (){
              location = '/$register/email/' ;
            };
            btn_row.addChild(btn);
            
            d.show();
        }else{
            var $btn = $(this);
            var currentLoved = $(document.body).hasClass('brand-loved');
            feed.sendBrandLoved(null, !currentLoved);
        }
    })
};

window.onFeedJS.push(attachSegmentHooks);
feed.onSegmentChanged.push(attachSegmentHooks);

function runFeedJSHooks() {
    if(window.onFeedJS) {
        for(var k in window.onFeedJS) {
            if(!window.onFeedJS.hasOwnProperty(k)) continue;
            try {
                if(window.onFeedJS[k] !== false) {
                    window.onFeedJS[k]();
                    window.onFeedJS[k] = false;
                }
            }catch(e) {
                console.error("An error occured while running onFeedJS hook", k, e);
            }
        }
    }
}




function saveCategories() {
    // todo: add delay timer
}

function prepareImageData(type) {
    var imageData = [];
    var imageSet;
    switch(type)
    {
        case 'upload-photo' :
            imageSet = window.uploadImageSetStoragePhoto.get();
            break;
        case 'upload-photo-album' :
            imageSet = window.uploadImageSetStoragePhotoAlbum.get();
            break;
        case 'upload-product' :
            imageSet = window.uploadImageSetStorageProduct.get();
            break;
        case 'upload-service' :
            imageSet = window.uploadImageSetStorageService.get();
            break;
        case 'upload-team-member' :
            imageSet = window.uploadImageSetStorageTM.get();
            break;
        case 'upload-aboutme' :
            imageSet = window.uploadImageSetStorageAboutme.get();
            break;
        case 'upload-portfolio' :
            imageSet = window.uploadImageSetStoragePortfolio.get();
            break;
        default :
            imageSet = window.uploadImageSetStorage.get();
            break;
    }

    for(var i = 0; i < imageSet.images.length; i++) {
        var img = imageSet.images[i];
        if(img.loaded && img.url) {
            imageData.push(img);
        }
    }

    return window.ClassyJSON.jsonify(imageData);
}

function preparePhotoAlbumImageData(edit, uploadType) {
    var imageData = [];
    var editCls;
    if(uploadType != "")
    {
        editCls = (edit != "") ? '.'+edit : '';
    }
    else
    {
        editCls = (edit) ? '.edit #photo_section .slider1' : '#photo_section .slider1';
    }

    var imageSet = window.uploadImageSetStoragePhotoAlbum.get();
    var imageCaptions = {};

    for(var i = 0; i < imageSet.images.length; i++) {
        var img = imageSet.images[i];
        if(img.loaded && img.url) {
            var imageName= GetFilename(img.url);
            img.caption = $(editCls + ' textarea.image-caption_' + imageName).val();

            imageData.push(img);
        }
    }
    /*    console.log(imageData); return false;*/
    return window.ClassyJSON.jsonify(imageData);
}

function postStory(storyClass, editorType, includeImages) {
    var draftsPost = (storyClass == 'draft-input') ? 'draft' : "";
    storyClass = (storyClass == 'draft-input') ? 'adv-story-input' : storyClass;
    var storySelector = '.editor-content .' + storyClass;
    var $titleField = $(storySelector + ' .story-input-title');

    var $contentField;
    if(editorType == 'video')
    {
        $contentField = $(storySelector + ' textarea.video-input-text');
    }
    else
    {
        $contentField = $(storySelector + ' textarea.story-input-text');
    }

    var contenID = $contentField.attr('id');
    // console.log(storySelector);

    var contents = '';
    if(editorType != 'advanced') {
        contents = $contentField.val().trim();
        /*contents = tinyMCE.get(contenID).getContent();*/
    }else{
        contents = tinyMCE.get(contenID).getContent();
        /*contents = tinyMCE.activeEditor.getContent();*/
        /*contents = tinyMCE.get('advanced-story').getContent();*/
    }
    
    //contents = tinyMCE.get(contenID).getContent();

    var title = $titleField.length ? $titleField.val().trim() : '';

    if(storyClass == 'upload-photo')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the photos.");
            return;
        }
    }
    else if(editorType == 'video')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the video.");
            return;
        }
    }
    else
    {
        if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
            var msg = (draftsPost == 'draft') ? 'saving draft' : (storyClass == 'upload-photo-album') ? 'submitting the posts' : 'submitting the post';
            labAlert("Please fill in the title and contents before "+msg+".");
            return;
        }
    }

    var videoURL = '';
    if(editorType == 'video')
    {
        videoURL = $(storySelector + ' .video-url-text').val();
    }

    var imageData = null;

    if(includeImages) {
        if(storyClass == 'photo-album')
        {
            imageData = preparePhotoAlbumImageData();
        }
        else
        {
            imageData = prepareImageData(storyClass);
        }
    }
/*    console.log(imageData); return false;*/

    ajaxSubmit('/$brandPost/' + g_brand.url, {
        'title': title,
        'content': contents,
        'cat': g_brand.category,
        'imageData': imageData,
        'postType': editorType,
        'allowComments': true,
        'draft' : draftsPost,
        'videoURL' : videoURL,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the post. " + error);
        }else{
            console.log("post submitted", data);
            if(includeImages) {
                if(storyClass == 'photo-album')
                {
                    var imgs = window.uploadImageSetStoragePhotoAlbum.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStoragePhotoAlbum.set(imgs);
                }
                else if(storyClass == 'upload-photo')
                {
                    var imgs = window.uploadImageSetStoragePhoto.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStoragePhoto.set(imgs);
                }
                else if(storyClass == 'adv-story-input')
                {
                    var imgs = window.uploadImageSetStorageAdvStory.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStorageAdvStory.set(imgs);
                }
                else
                {
                    var imgs = window.uploadImageSetStorage.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStorage.set(imgs);
                }
            }
            var msg1 = (draftsPost == 'draft') ?
                'Draft saved sccessfully.' :
                (storyClass == 'upload-photo') ? 'Photos submitted successfully.' :
                    (storyClass == 'upload-photo-album') ? 'Album submitted successfully.' :
                        (storyClass == 'video-input') ? 'Video post submitted successfully.' : 'Post submitted successfully.';
            labAlert(msg1);
            window.location.href = window.location.href;
        }
    });
}

function editAllPostStory(storyClass, editorType, includeImages) {
    var draftsPost = (storyClass == 'draft-input') ? 'draft' : "";
    storyClass = (storyClass == 'draft-input') ? 'adv-story-input' : storyClass;
    var storySelector = '.editor-content .' + storyClass;
    var $titleField = $(storySelector + ' .story-input-title');

    var postId = $(storySelector + ' .story-id').val();

    if(postId <= 0)
    {
        labAlert("You can not edit this story.");
        return;
    }

    if(editorType == 'video')
    {
        var $contentField = $(storySelector + ' textarea.video-input-text');
    }
    else
    {
        var $contentField = $(storySelector + ' textarea.story-input-text');
    }

    var contenID = $contentField.attr('id');
    // console.log(storySelector);

    var contents = '';
    if(editorType != 'advanced') {
        contents = $contentField.val().trim();
        /*contents = tinyMCE.get(contenID).getContent();*/
    }else{
        contents = tinyMCE.get(contenID).getContent();
        /*contents = tinyMCE.activeEditor.getContent();*/
        /*contents = tinyMCE.get('advanced-story').getContent();*/
    }
    //contents = tinyMCE.get(contenID).getContent();

    var title = $titleField.length ? $titleField.val().trim() : '';

    if(storyClass == 'upload-photo')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the photos.");
            return;
        }
    }
    else if(editorType == 'video')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the video.");
            return;
        }
    }
    else
    {
        if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
            var msg = (draftsPost == 'draft') ? 'saving draft' : (storyClass == 'upload-photo-album') ? 'submitting the posts' : 'submitting the post';
            labAlert("Please fill in the title and contents before "+msg+".");
            return;
        }
    }

    var videoURL = '';
    if(editorType == 'video')
    {
        videoURL = $(storySelector + ' .video-url-text').val();
    }

    var imageData = null;
    var type = (storyClass == 'upload-photo-edit') ? 'upload-photo' : storyClass;
    if(includeImages) {
        if(storyClass == 'photo-album')
        {
            imageData = preparePhotoAlbumImageData();
        }
        else
        {
            imageData = prepareImageData(type);
        }
    }
    /*console.log(imageData); return false;*/

    ajaxSubmit('/$brandEditPostType/' + g_brand.url, {
        'title': title,
        'content': contents,
        'cat': g_brand.category,
        'imageData': imageData,
        'postType': editorType,
        'allowComments': true,
        'draft' : draftsPost,
        'videoURL' : videoURL,
        'editId' : postId,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the post. " + error);
        }else{
            console.log("post submitted", data);
            if(includeImages) {
                if(storyClass == 'photo-album')
                {
                    var imgs = window.uploadImageSetStoragePhotoAlbum.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStoragePhotoAlbum.set(imgs);
                }
                else if(type == 'upload-photo')
                {
                    var imgs = window.uploadImageSetStoragePhoto.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStoragePhoto.set(imgs);
                }
                else
                {
                    var imgs = window.uploadImageSetStorage.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStorage.set(imgs);
                }
            }
            var msg1 = (draftsPost == 'draft') ?
                'Draft saved sccessfully.' :
                (type == 'upload-photo') ? 'Photos submitted successfully.' :
                    (storyClass == 'upload-photo-album') ? 'Album submitted successfully.' :
                        (storyClass == 'video-input') ? 'Video post submitted successfully.' : 'Post submitted successfully.';
            labAlert(msg1);
            window.location.href = window.location.href;
        }
    });
}

function editPostType(storyClass, editorType, includeImages, albumId) {
    var draftsPost = (storyClass == 'draft-input') ? 'draft' : "";
    storyClass = (storyClass == 'draft-input') ? 'adv-story-input' : storyClass;
    var storySelector = '.edit .editor-content .' + storyClass;
    var $titleField = $(storySelector + ' .story-input-title');

    if(editorType == 'video')
    {
        var $contentField = $(storySelector + ' textarea.video-input-text');
    }
    else
    {
        var $contentField = $(storySelector + ' textarea.story-input-text');
    }

    var contenID = $contentField.attr('id');
    // console.log(storySelector);

    var contents = '';
    if(editorType != 'advanced') {
        contents = $contentField.val().trim();
        /*contents = tinyMCE.get(contenID).getContent();*/
    }else{
        contents = tinyMCE.get(contenID).getContent();
        /*contents = tinyMCE.activeEditor.getContent();*/
        /*contents = tinyMCE.get('advanced-story').getContent();*/
    }
    //contents = tinyMCE.get(contenID).getContent();

    var title = $titleField.length ? $titleField.val().trim() : '';

    if(storyClass == 'upload-photo')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the photos.");
            return;
        }
    }
    else if(editorType == 'video')
    {
        if(contents.length < 1) {
            labAlert("Please fill in the description before submitting the video.");
            return;
        }
    }
    else
    {
        if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
            var msg = (draftsPost == 'draft') ? 'saving draft' : (storyClass == 'upload-photo-album') ? 'submitting the posts' : 'submitting the post';
            labAlert("Please fill in the title and contents before "+msg+".");
            return;
        }
    }

    var videoURL = '';
    if(editorType == 'video')
    {
        videoURL = $(storySelector + ' .video-url-text').val();
    }

    var imageData = null;

    if(includeImages) {
        if(storyClass == 'photo-album')
        {
            imageData = preparePhotoAlbumImageData(true);
        }
        else
        {
            imageData = prepareImageData(storyClass);
        }
    }
    /*return false;*/
    ajaxSubmit('/$brandEditPostType/' + g_brand.url, {
        'title': title,
        'content': contents,
        'cat': g_brand.category,
        'imageData': imageData,
        'postType': editorType,
        'allowComments': true,
        'draft' : draftsPost,
        'videoURL' : videoURL,
        'editId' : albumId,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the post. " + error);
        }else{
            console.log("post submitted", data);
            if(includeImages) {
                if(storyClass == 'photo-album')
                {
                    var imgs = window.uploadImageSetStoragePhotoAlbum.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStoragePhotoAlbum.set(imgs);
                }
                else
                {
                    var imgs = window.uploadImageSetStorage.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStorage.set(imgs);
                }
            }
            var msg1 = (draftsPost == 'draft') ?
                'Draft saved sccessfully.' :
                (storyClass == 'upload-photo') ? 'Photos submitted successfully.' :
                    (storyClass == 'upload-photo-album') ? 'Album submitted successfully.' :
                        (storyClass == 'video-input') ? 'Video post submitted successfully.' : 'Post submitted successfully.';
            labAlert(msg1);
            window.location.href = window.location.href;
        }
    });
}

function upoloadAlbumPhoto(albumId) {
    var imageData = null;


    imageData = preparePhotoAlbumImageData('album_photo_new_container', 'edit-album-photo');
    console.log(imageData);

    ajaxSubmit('/$editAlbumPhoto/' + g_brand.url, {
        'imageData': imageData,
        'albumId' : albumId,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the post. " + error);
        }else{
            console.log("album photo submitted", data);

            var imgs = window.uploadImageSetStoragePhotoAlbum.get();
            imgs.images = [];
            imgs.imagesChanged();
            window.uploadImageSetStoragePhotoAlbum.set(imgs);

            var msg1 = 'Album photo submitted successfully.';
            labAlert(msg1);
            window.location.href = window.location.href;
        }
    });
}


function editPostStory(postID, drafts, storyClass, includeImages) {
    var $titleField = $('#edit_post_title_'+postID);
    var $contentField = $('#edit_post_content_'+postID);
    var contenID = $contentField.attr('id');
    var postType = $('#hdnPostType').val();

    if(postType == 3)
    {    
        contents = tinyMCE.get(contenID).getContent();
    }
    else
    {
        contents = $contentField.val();
    }

    var title = $titleField.length ? $titleField.val().trim() : '';
    
    var video = '';
    if(postType == 5)
    {
        video = $('#edit_video_url_'+postID).val().trim();
    }

    if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
        labAlert("Please fill in the title and contents before submitting the post.");
        return;
    }
    
    var imageData = null;

    if(includeImages) {       
        
        if(storyClass == 'photo-album')
        {
            imageData = preparePhotoAlbumImageData(storyClass);
        }
        else
        {
            imageData = prepareImageData(storyClass);
        }
    }
    
    window.loadingIndicator($('body'));
    ajaxSubmit('/$brandPostEdit/' + postID, {
        'title': title,
        'content': contents,
        'drafts' : drafts,
        'imageData':imageData,
        'postType' : postType,
        'video' : video,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the post. " + error);
        }else{
            console.log("post submitted", data);
            if(includeImages) {
                if(postType == 4)
                {
                    if(storyClass == 'photo-album')
                    {
                        var imgs = window.uploadImageSetStoragePhotoAlbum.get();
                        imgs.images = [];
                        imgs.imagesChanged();
                        window.uploadImageSetStoragePhotoAlbum.set(imgs);
                    }
                    else
                    {
                        var imgs = window.uploadImageSetStoragePhoto.get();
                        imgs.images = [];
                        imgs.imagesChanged();
                        window.uploadImageSetStoragePhoto.set(imgs);
                    }
                }
                else
                {
                    var imgs = window.uploadImageSetStorage.get();
                    imgs.images = [];
                    imgs.imagesChanged();
                    window.uploadImageSetStorage.set(imgs);
                }
            }
            
            if(postType == 1)
            {
                feed.navigateToSegment('home');
            }
            else if(postType == 5)
            {
                feed.navigateToSegment('video');
            }
            else if(postType == 4)
            {
                if(storyClass == 'photo-album')
                {
                    feed.navigateToSegment('album');
                }
                else
                {
                    feed.navigateToSegment('gallery');
                }
            }
            else
            {
                feed.navigateToSegment('stories');
            }
            $('.loading-indicator-background').remove();
        }
    });
}

function manageProduct(obj) {
    closePreview();
    var type = $(obj).data('type');
    var collID = $(obj).data('collection');
    type = (type != "") ? type : 'add';
    includeImages = true;
    var $titleField = $('#add_collection_product_'+collID+' .product-input-title');
    var $contentField = $('#add_collection_product_'+collID+' textarea.product-input-text');
    var $categoryField = $('#add_collection_product_'+collID+' .product-category-text');
    var $collectionField = $('#add_collection_product_'+collID+' .product-collection-text');
    var buyNow = $('#add_collection_product_'+collID+' input[name="buy-now"]').prop('checked');
    var radio = $('#add_collection_product_'+collID+' input[name="product-setting"]:checked').val();
    var price = $('#add_collection_product_'+collID+' input[name="price"]').val();

    buyNow = ( buyNow ? localStorage.getItem('buyNow') : '' );

    var prodcutId = 0;
    if(type == 'edit')
    {
        prodcutId = $('#add_collection_product_'+collID+' .product-edit-id').val();

        if(prodcutId < 1)
        {
            labAlert("Product id not found.");
            return;
        }
    }

    var contents = $contentField.val().trim();

    var title = $titleField.length ? $titleField.val().trim() : '';

    if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
        labAlert("Please fill in the title and description before submitting the product.");
        return;
    }

    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-product');
    }

    var actionURL = '/$brandProductAdd/'+g_brand.url;
    if(type == 'edit')
    {
        actionURL = '/$brandProductEdit/'+g_brand.url;
    }
    window.loadingIndicator($('.modal-content'));
    ajaxSubmit(actionURL , {
        'title': title,
        'content': contents,
        'category': $categoryField.val(),
        'collection': $collectionField.val(),
        'imageData': imageData,
        'prodcutId': prodcutId,
        'buyNow' : buyNow,
        'radio' : radio,
        'price' : price
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            var msg = (type == 'edit') ? 'editing the product' : 'submitting the product';
            labAlert("An error has occured while "+msg+". " + error);
        }else{
            console.log("product submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageProduct.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageProduct.set(imgs);
            }
            var msg = (type == 'edit') ? 'Product updated successfully.' : 'Product submitted successfully.';
            if(type == 'edit')
            {
                feed.navigateToSegment('products', $categoryField.val());
            }
            else
            {
                labAlert(msg);
                window.location.href = window.location.href;
            }
        }
    });
}

function addProductFromHome()
{
    includeImages = true;
    var $titleField = $('.product-input .product-input-title');
    var $contentField = $('.product-input textarea.product-input-text');
    var $categoryField = $('.product-input .product-category-text');

    var type = 'add';
    var prodcutId = 0;

    var contents = $contentField.val().trim();

    var title = $titleField.length ? $titleField.val().trim() : '';

    if(($titleField.length && $titleField.val().trim().length < 1) || contents.length < 1) {
        labAlert("Please fill in the title and description before submitting the product.");
        return;
    }

    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-product');
    }

    var actionURL = '/$brandProductAdd/'+g_brand.url;

    ajaxSubmit(actionURL , {
        'title': title,
        'content': contents,
        'category': $categoryField.val(),
        'imageData': imageData,
        'prodcutId': prodcutId,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            var msg = (type == 'edit') ? 'editing the product' : 'submitting the product';
            labAlert("An error has occured while "+msg+". " + error);
        }else{
            console.log("product submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageProduct.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageProduct.set(imgs);
            }
            var msg = 'Product submitted successfully.';
            $titleField.val('');
            $contentField.val('');
            labAlert(msg);
            window.location.href = window.location.href;
            $('.editor-tabs span:first-child').trigger('click');
        }
    });
}

function addServiceData()
{
    var titleField = $('.service_add .service_add_title');
    var contentField = $('.service_add textarea.service_add_description');
    includeImages = true;
    if((titleField.length && titleField.val().trim().length < 1) || (contentField.length && contentField.val().trim().length < 1))
    {
        labAlert("Please fill in the title and description before submitting the service.");
        return;
    }

    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-service');
    }
    window.loadingIndicator($('body'));

    ajaxSubmit('/$serviceAdd/'+g_brand.url , {
        'title': titleField.val().trim(),
        'content': contentField.val().trim(),
        'imageData': imageData,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the service. " + error);
        }else{
            console.log("service submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageService.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageService.set(imgs);
            }
            var msg = 'Service submitted successfully.';
            labAlert(msg);
            window.location.href = window.location.href;
        }
    });
}

function editServiceData()
{
    var titleField = $('#serviceEditPopup .service_add .service_add_title');
    var contentField = $('#serviceEditPopup .service_add textarea.service_add_description');
    var serviceId = $('#serviceEditPopup .service_add #hdnserviceId').val();
    includeImages = true;
    if((titleField.length && titleField.val().trim().length < 1) || (contentField.length && contentField.val().trim().length < 1))
    {
        labAlert("Please fill in the title and description before submitting the service.");
        return;
    }

    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-service');
    }
    $('.loading').show();

    ajaxSubmit('/$brandServiceEdit/'+g_brand.url , {
        'title': titleField.val().trim(),
        'content': contentField.val().trim(),
        'serviceId': serviceId,
        'imageData': imageData,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the service. " + error);
        }else{
            console.log("service submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageService.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageService.set(imgs);
            }
            var msg = 'Service submitted successfully.';
            labAlert(msg);
            window.location.href = window.location.href;
        }
    });
}


function addTeamMemberData()
{
    var titleField = $('#upload-member .team_member_name');
    var position = $('#upload-member .team_member_position').val().trim();
    var biography = $('#upload-member textarea.team_member_description').val().trim();
    includeImages = true;
    if((titleField.length && titleField.val().trim().length < 1))
    {
        labAlert("Please fill in the name submitting the team member.");
        return;
    }

    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-team-member');
    }
    window.loadingIndicator($('body'));

    ajaxSubmit('/$addTeamMember/'+g_brand.url , {
        'name': titleField.val().trim(),
        'position': position,
        'biography': biography,
        'imageData': imageData,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the service. " + error);
        }else{
            console.log("team member submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageTM.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageTM.set(imgs);
            }
            var msg = 'Team member submitted successfully.';
            labAlert(msg);
            window.location.href = window.location.href;
        }
    });
}


function editTeamMemberData(tmId)
{
    var titleField = $('#editTeamMember .team_member_name');
    var position = $('#editTeamMember .team_member_position').val().trim();
    var biography = $('#editTeamMember textarea.team_member_description').val().trim();
    includeImages = true;
    if((titleField.length && titleField.val().trim().length < 1))
    {
        labAlert("Please fill in the name submitting the team member.");
        return;
    }

    var imageData = null;
    if(includeImages) {
        imageData = prepareImageData('upload-team-member');
    }
    window.loadingIndicator($('body'));

    ajaxSubmit('/$editTeamMember/'+tmId , {
        'name': titleField.val().trim(),
        'position': position,
        'biography': biography,
        'imageData': imageData,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the service. " + error);
        }else{
            console.log("team member submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageTM.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageTM.set(imgs);
            }
            var msg = 'Team member submitted successfully.';
            labAlert(msg);
            window.location.href = window.location.href;
        }
    });
}


function manageAboutMe()
{
    var titleField = $('.aboutme_form_section .aboutme_name');
    var contentField = $('.aboutme_form_section textarea.aboutme_description');
    includeImages = true;
    if((titleField.length && titleField.val().trim().length < 1) || (contentField.length && contentField.val().trim().length < 1))
    {
        labAlert("Please fill in the title and description before submitting the data.");
        return;
    }

    var aboutMeId =  0;
    if($('#hdnAboutMeId').length > 0)
    {
        aMeId =  $('#hdnAboutMeId').val().trim();
        if(aMeId.length > 0 && aMeId > 0)
        {
            aboutMeId = aMeId;
        }
    }


    var imageData = null;
    if(includeImages) {
        console.log('asadasd');
        imageData = prepareImageData('upload-aboutme');
    }

    window.loadingIndicator($('body'));

    var url = '/$aboutMeAdd/';
    if(aboutMeId > 0)
    {
        url = '/$aboutMeEdit/';
    }

    ajaxSubmit(url+g_brand.url , {
        'title': titleField.val().trim(),
        'content': contentField.val().trim(),
        'imageData': imageData,
        'aboutMeId': aboutMeId,
    }, function(data) {
        if(!data || !data.status || data.status != 'ok') {
            console.log("error:", data);
            var error = "Please try again later or contact the support team.";
            if(data && data.status) error = data.status;
            labAlert("An error has occured while submitting the data. " + error);
        }else{
            console.log("about me submitted", data);
            if(includeImages) {
                var imgs = window.uploadImageSetStorageAboutme.get();
                imgs.images = [];
                imgs.imagesChanged();
                window.uploadImageSetStorageAboutme.set(imgs);
            }
            feed.navigateToSegment('aboutme');
            $('.loading-indicator-background').remove();
        }
    });
}

var videoEmbed = {
    invoke: function(){

        $('body').html(function(i, html) {
            return videoEmbed.convertVideo(html);
        });

    },
    convertVideo: function(html){
        var pattern1 = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
        var pattern2 = /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g;

        if(pattern1.test(html)){
            console.log("html", html);

            var replacement = '<iframe width="420" height="345" src="//player.vimeo.com/video/$1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';

            var html = html.replace(pattern1, replacement);
        }else if(pattern2.test(html)){
            console.log("html", html);

            var replacement = '<iframe width="420" height="345" src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>';

            var html = html.replace(pattern2, replacement);
        }
        else
        {
            html = false;
        }


        return html;
    }
};

function adjuctBannerImage() {
    var $img = $('.intro figure .img-wrap img');
    var $container = $img.parent();
    $container.removeClass('oversized-img');
    var maxHeight = parseFloat($container.css('max-height'));

    if($img.outerHeight() > maxHeight) {
        $container.addClass('oversized-img');
    }else{
        $container.removeClass('oversized-img');
    }
}

function setupGalleries() {
    if(window.postGalleryQueue) {
        for(var k in window.postGalleryQueue) {
            if(window.postGalleryQueue.hasOwnProperty(k)) {
                var images = window.ClassyJSON.unjsonify(window.postGalleryQueue[k]);
                var imageSet = new window.ImageSet(images, '#' + k);
                delete window.postGalleryQueue[k];
            }
        }
    }
}

window.setInterval(setupGalleries, 250);

$(document).ready(function () {

    $(window).resize(adjuctBannerImage);

    $('.dropdown-menu.click-noclose,.notf-drop,#notification').on('click', function (e) {
        e.stopPropagation();
    });

    $("#notification").click(function (e) {
        e.preventDefault();
        $(this).next(".notf-drop").toggle();
        $(this).removeClass("active");
    });
    $('html').click(function () {
        $(".notf-drop").hide();
    });


    $(window).scroll(function () {
        if($('.sidebar').length > 0)
        {
            if ($(this).scrollTop() > $('.sidebar').offset().top) {
                $(".sidebar").addClass("fixed");

            } else {
                $(".sidebar").removeClass("fixed");
            }
        }
    });
    
    //event listener

});

function getQuoteTextSize(text) {
    return Math.floor(Math.max(22, 34 - Math.pow(Math.max(0, text.length-20)/2, 2/3)))
}

function reinitContent() {
    setupGalleries();

    // Image upload
    // Banner image
    if(window.g_isBrandAdmin) {
        $('.page-banner-change-button').click(function (e) {
            /*var $form = $('#imageUploadForm');
            var $imageInput = $('#imageFileInput');
            $form.attr('action', '/$brandSettings/' + window.g_brand.url + '/uploadBanner');
            $imageInput.click();*/

            labImage('/$brandSettings/' + window.g_brand.url + '/uploadBanner', 'Choose brand banner image', 2560, 900, function (val) {
                if(val) {
                    this.close();
                    window.location.reload();
                }
            });
            e.preventDefault();
        });
        $('.page-avatar-change-button').click(function (e) {
            /*var $form = $('#imageUploadForm');
             var $imageInput = $('#imageFileInput');
             $form.attr('action', '/$brandSettings/' + window.g_brand.url + '/uploadAvatar');
             $imageInput.click();*/

            labImage('/$brandSettings/' + window.g_brand.url + '/uploadAvatar', 'Choose brand avatar', 512, 512, function (val) {
                if(val) {
                    this.close();
                    window.location.reload();
                }
            });
            e.preventDefault();
        });
    }

    $(".menu-edit .head").click(function () {
        $(this).closest("ul").toggleClass("open");
    });

    $(".categories a").click(function (e) {
        e.preventDefault();
        $(this).closest("li").addClass("active");
        $(this).closest("li").siblings().removeClass("active");
    });

    $(".new-story > ul > li > .icon-close").click(function () {
        $(this).closest("li").hide();
    });

    $(".toolbar .expand").click(function () {
        $(this).closest(".toolbar").addClass("open");
    });

    /*  $(".new-story > ul > #add-quote").click(function () {
     $(this).closest("ul").siblings(".head").find("h4").text("Brand Quote");
     });*/

    $('#quote').on('keypress blur', function () {

        var textLength = $(this).val().length;

        if (textLength < 20) {
            $(this).css('font-size', '36px');
        } else if (textLength < 40) {
            $(this).css('font-size', '30px');
        } else {
            $(this).css('font-size', '24px');
        }

        //console.log(textLength);
    });

    $('#quote-input').on('keyup', 'textarea', function (e) {
        $(this).css('height', 'auto');
        $(this).height(this.scrollHeight);
    });
    $('#quote-input').find('textarea').keyup();


    $('#newStory').on('show.bs.modal', function () {
        var $container = $('.main-content');
        $('html,body').animate({
            scrollTop: $container.offset().top,
        }, 200);
    });

    $('[data-show-elements]').on('click', function() {
        $($(this).data('show-elements')).show('fast');
    });
    $('[data-hide-elements]').on('click', function() {
        $($(this).data('hide-elements')).not($($(this).data('show-elements'))).hide('fast');
    });

    $('[data-tab-target]').on('click', function(e) {
        e.preventDefault();

        var $this = $(this);
        var target = $this.data('tab-target');

        var $group = $this.closest('.tab-group');
        if(!$group.length) $group = null;

        $('[data-tab-id]', $group).removeClass('active');
        $('[data-tab-id='+target+']', $group).addClass('active');

        $('[data-tab-target]', $group).removeClass('active');
        $this.addClass('active');

    });

    $('[data-editor-indicator-id]').on('click', function(e) {
        e.preventDefault();

        var $this = $(this);
        var target = $this.data('editor-indicator-id');
        var $activeIcon = $('[data-editor-indicator-id].active');
        var currentEditor = false;
        if($activeIcon.length) {
            currentEditor = $activeIcon.data('editor-indicator-id');
        }
        var active = target != currentEditor && target != 'close';

        $('[data-editor-indicator-id]').removeClass('active');
        if(active) {
            function open() {
                $('[data-editor-indicator-id=' + target + ']').addClass('active');
                $('[data-editor-section-id=' + target + ']').addClass('active');
                $('.editor-content').show('fast');
                $this.addClass('active');
            }
            if(currentEditor) {
                $('.editor-content').hide('fast', function () {
                    $('[data-editor-section-id]').removeClass('active');
                    open();
                });
            }else{
                open();
            }
        }else{
            $('.editor-content').hide('fast', function () {
                $('[data-editor-section-id]').removeClass('active');
            });
        }

    });

    $('.image-frame.cover-image img').each(function() {
        var $this = $(this);
        var $parent = $this.parent();
        if($('[data-image]', $parent).length) {
            return;
        }
        
        if($this.hasClass('no-frame'))
        {
            return;
        }
        var $div = $("<div>");
        var src = $this.attr('src');
        $div.attr('data-image', src);
        $div.css('background-image', 'url(\''+src+'\')');
        $this.addClass('shadow-image');
        $parent.prepend($div);
        $div.on('click', function(args) {
            $this.click(args);
        });
    });
    $('.post-content .post-read-more').click(function () {
        var $this = $(this);
        var $postContent = $this.closest('.post-content');
        var $post = $this.closest('.post-story');
        $('.post-image-list.collapsed-only', $post).html('');
        $postContent.css({maxHeight: 'none'});
        $postContent.removeClass('hide-child-images');
        $this.remove();
    });
    $('.post-content .post-read-more.hide-by-default').each(function () {
        var $this = $(this);
        var $postContent = $this.closest('.post-content');
        var height = $postContent.outerHeight();
        var maxHeight = parseInt($postContent.css('maxHeight'));

        if (height + 10 < maxHeight) {
            $postContent.css({maxHeight: 'none'});
        }else{
            $this.removeClass('hide-by-default');

            // for html story, create gallery of images from post
            var $post = $postContent.closest('.post-story');
            if($post.data('postType') === 'advanced') {
                var html = $postContent.html();

                var regex = /<\s*img.+?src=(["'])(.*?)\1.*>/gi;
                var match;
                var images = [];
                do {
                    match = regex.exec(html);
                    if(match) {
                        images.push(match[2]);
                    }
                } while (match != null);

                var $galleryTarget = $('.post-image-list', $post);
                $galleryTarget.addClass('collapsed-only');
                $postContent.addClass('hide-child-images');
                var id = $(this).closest('.post-story').data('postid');
                if(id) {
                    $galleryTarget.css({cursor: 'pointer'});
                    $galleryTarget.click(function () {
                        $this.click();
                    });
                }

                var imageList = new lab.ImageList();
                var gallery = new lab.ImageListRenderer($galleryTarget, imageList);
                imageList.addAll(images);
            }
        }
    });

    // Autosize quote text
    $('[data-post-type=quote] .actual-post-content').each(function() {
        var $this = $(this);
        var text = $this.text();
        var size = getQuoteTextSize(text);
        $this.css({fontSize: size + 'px'});
    });


    $('.littleP').mouseover(function () {
        $(this).css('width','85px');
        $(this).css('height','65px');
    });
    $('.littleP').mouseout(function () {
        $(this).css('width','80px');
        $(this).css('height','60px');
    });

    $('.littleP:first').css('border', '1px solid white');
    $('.littleP').click(function(){
        $('.littleP').css('border', '0');
        $(this).css('border', '1px solid white');
        var r= $(this).attr("data-slider-item");
        var post_id= $(this).attr("data-post-id");
        $('#image-modal .item.active').removeClass('active');
        $('#image-modal .item[data-slide-number="'+r+'"]').addClass('active');
    });


}

window.onFeedJS.push(reinitContent);
feed.onSegmentChanged.push(reinitContent);

//

// Keep this at the end
runFeedJSHooks();
setTimeout(runFeedJSHooks, 100);
setInterval(runFeedJSHooks, 500);
console.info("feed.js loaded");

function getProductStatusNameAndCSS(statusNumber){
    var data=[];
    data.css=[];
    switch (statusNumber){
        case '0':

            data.css.backgroundColor= 'transparent';
            data.text='';
            break;
        case '1':
            data.css.backgroundColor= '#D9544F';
            data.css.width='70px';
            data.text='Sale';
            break;
        case '2':
            data.css.backgroundColor= 'green';
            data.css.width=' 70px';
            data.text='New';
            break;
        case '3':
            data.css.backgroundColor= '#4169E1';
            data.css.width='120px';
            data.text='Best Seller';
            break;
    }
    return data;
}


